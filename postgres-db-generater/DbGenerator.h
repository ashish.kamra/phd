#include <pqxx/pqxx>

class DbGenerator
{
public:
	DbGenerator(int,int,int);
	~DbGenerator();

	void create_tables();
	void insert_data();
	void generate_scripts();

private:
	int num_tables;
	int num_cols_per_table;
	int num_rows_per_table;
	std::string conn_str;
};
