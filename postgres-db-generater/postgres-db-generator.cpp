//============================================================================
// Name        : postgres-db-generator.cpp
// Author      : Ashish Kamra
// Version     :
// Copyright   : GPL
// Description : Hello World in C, Ansi-style
//============================================================================

#include <iostream>
#include <stdlib.h>

#include "DbGenerator.h"

using namespace std;

static void Usage();

int main(int argc, char** argv) {

	int num_tables;
	int num_cols_per_table;
	int num_rows_per_table;

	if (argc != 7)
	{
		Usage();
		return 1;
	}

	num_tables = atoi(argv[2]);
	num_cols_per_table = atoi(argv[4]);
	num_rows_per_table = atoi(argv[6]);

	DbGenerator dbg(num_tables, num_cols_per_table, num_rows_per_table);

	dbg.create_tables();
	dbg.insert_data();
	dbg.generate_scripts();

	return 0;
}

static void
Usage()
{
	cout << "Usage: postgres-db-generator -t <num_tables> -c <num_cols_per_table> -r <num_rows_per_table>";
}

