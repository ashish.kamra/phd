/*
 * DbGenerator.cpp
 *
 *  Created on: Feb 28, 2010
 *      Author: postgres
 */
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>

#include "DbGenerator.h"
#define TRAN_SIZE 5

using namespace pqxx;
using namespace std;

static string table_prefix = "t_";
static string col_prefix = "c_";
static string db = "postgres";
static string user = "postgres";
static string pwd = "";

DbGenerator::DbGenerator(int nt, int nc, int nr)
{
	num_tables = nt;
	num_cols_per_table = nc;
	num_rows_per_table = nr;

	conn_str = "dbname="+db;
}

DbGenerator::~DbGenerator(){}


void DbGenerator::create_tables()
{
	// connect to the database
	int i = 0;
	int j = 0;
	try
	{
		connection conn(conn_str);
		work wk(conn);

		for (i = 0; i < num_tables; i++)
		{
			char tab_ct[6];
			sprintf(tab_ct,"%d",i);

			string table_name = table_prefix+tab_ct;
			wk.exec("drop table if exists "+table_name);
			//wk.exec("create table "+table_name+" ("+col_prefix+"1 int PRIMARY KEY)");
			wk.exec("create table "+table_name+" ("+col_prefix+"0 int)");

			for (j = 1; j < num_cols_per_table; j++)
			{
				char col_ct[4];
				sprintf(col_ct,"%d",j);

				string col_name = col_prefix+col_ct;

				wk.exec("alter table "+table_name+" add column "+col_name+" int");
			}
		
			wk.exec("create index idx_"+table_name+" on "+table_name+"(c_0)");	
			wk.exec("grant select,insert,update on "+table_name+" to r1");
			wk.exec("grant select,insert,update on "+table_name+" to r2");
			wk.exec("grant select,insert,update on "+table_name+" to r3");
			wk.exec("grant select,insert,update on "+table_name+" to r4");
			wk.exec("grant select,insert,update on "+table_name+" to r5");
			wk.exec("grant select,insert,update on "+table_name+" to r6");

		}

		wk.commit();
		conn.disconnect();
	}

   catch (const std::exception &e)
   {
	 cerr << e.what() << std::endl;
	 return ;
   }

}

void DbGenerator::insert_data()
{
	int i = 0;
	int j = 0;
	int k = 0;

	try
	{
		connection conn(conn_str);
		work wk(conn);

		for (k = 0; k < num_rows_per_table; k++)
		{
			for (i = 0; i < TRAN_SIZE; i++)
			{
				char tab_ct[6];
				sprintf(tab_ct,"%d",i);

				string table_name = table_prefix+tab_ct;
				string values = "";
				for (j = 0; j < num_cols_per_table; j++)
				{
					char val[10];
					sprintf(val,"%d",(j+1)*(k+1));
					if (j != num_cols_per_table -1)
						values = values+val+",";
					else
						values = values+val;
				}

				wk.exec("insert into "+table_name+" values("+values+")");
			}
		}
		wk.commit();
		conn.disconnect();
	}

	catch (const std::exception &e)
	{
		cerr << e.what() << std::endl;
		return ;
    }
}

void DbGenerator::generate_scripts()
{
	ofstream script_file;
	int i = 0;
	int j = 0;

	script_file.open ("pgbench_scripts", ios::trunc);

	if (!script_file.is_open())
	{
		cerr << "Can not open the script file." << endl;
		return;
	}

	script_file << "BEGIN;" << endl;

	for (j = 0; j < num_cols_per_table; j++)
	{
		char j_str[4];
		sprintf(j_str,"%d",j);
		script_file << "\\setrandom i_val_" << j_str << " 1 2147483647" << endl;
	}

	for (i = 0; i < TRAN_SIZE; i++)
	{
		char tab_ct[6], tab_ct_1[6];
		sprintf(tab_ct,"%d",i);
		if (i != 4)
			sprintf(tab_ct_1,"%d",i+1);
		else
			sprintf(tab_ct_1,"%d",0);


		string table_name = table_prefix+tab_ct;
		string table_name_1 = table_prefix+tab_ct_1;

		string select_cmd = "SELECT ";
		string insert_cmd = "INSERT ";
		string update_cmd = "UPDATE ";

		string cols = "";
		string values = "";
		for (j = 0; j < num_cols_per_table; j++)
		{
			char col[10];
			sprintf(col,"%d",j);
			if (j != num_cols_per_table -1)
			{
				values = values+":i_val_"+col+",";
				cols = cols+col_prefix+col+",";
			}
			else
			{
				values = values+":i_val_"+col;
				cols = cols+col_prefix+col;
			}
		}

		select_cmd = select_cmd + " * FROM " + table_name + " , " + table_name_1  + " WHERE "+table_name+".c_0 = :i_val_0 AND "+table_name_1+".c_1 = :i_val_1 AND "+table_name+".c_1 = :i_val_1;";
		//select_cmd = select_cmd + cols + " FROM " + table_name + " WHERE c_0 = :i_val_0 ;";
		//insert_cmd = insert_cmd + "into " + table_name + " values("+values+")" + ";";
		//update_cmd = update_cmd + table_name + " SET c_1 = :i_val_0 WHERE c_0 = :i_val_0 AND c_1 = :i_val_1;";

		int j=0;
		for (j =0 ; j < 3; j++)
			script_file << select_cmd << endl;

		//script_file << insert_cmd << endl;
		//script_file << update_cmd << endl;
	}

	script_file << "END;" << endl;
	script_file.close();
}
