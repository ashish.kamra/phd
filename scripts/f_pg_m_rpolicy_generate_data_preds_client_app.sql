-- Function: f_pg_m_rpolicy_generate_data_preds_client_app(text, integer)

-- DROP FUNCTION f_pg_m_rpolicy_generate_data_preds_client_app(text, integer);

CREATE OR REPLACE FUNCTION f_pg_m_rpolicy_generate_data_preds_client_app(attrname text, num_preds integer)
  RETURNS integer AS
$BODY$
DECLARE
	pred_count		integer;
	pred_val		text;
	inp_operator		text;
	inppredname		text;
	pred_oid		oid;
	pred_entry		text[];
	max_num_client_apps_per_pred	integer;
	num_client_apps_per_pred	integer;
	client_app_name		integer;
BEGIN
	
	pred_count := 1;

	inp_operator := '<@';

	max_num_client_apps_per_pred := 10;
	
	while (pred_count <= num_preds) loop

		select into num_client_apps_per_pred cast(max_num_client_apps_per_pred*random() as integer);

		if (num_client_apps_per_pred = 0) then
			num_client_apps_per_pred := 1;
		end if;
		
		for i in 1..num_client_apps_per_pred loop
			select into client_app_name cast(1000*random() as integer);
			pred_entry[i] := 'c_'||cast(client_app_name as text);
		end loop;
				
		pred_val := '{'||array_to_string(pred_entry, ',')||'}' ;
		
		inppredname := attrname||'_pred_'||cast(pred_count as text); 
		
		Select into pred_oid f_pg_m_rpolicy_createpred(inppredname, attrname, inp_operator, pred_val);
		
		pred_count := pred_count + 1;
	end loop;			

	return 0;
END;

$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION f_pg_m_rpolicy_generate_data_preds_client_app(text, integer) OWNER TO postgres;
