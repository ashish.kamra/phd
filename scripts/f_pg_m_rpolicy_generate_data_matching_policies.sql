-- Function: f_pg_m_rpolicy_generate_data_matching_policies(integer)

-- DROP FUNCTION f_pg_m_rpolicy_generate_data_matching_policies(integer);

CREATE OR REPLACE FUNCTION f_pg_m_rpolicy_generate_data_matching_policies(num_match_policies integer, 
																			rolename text,
																			relation_id text)
  RETURNS void AS
$BODY$

DECLARE
	num_policies			integer;
	policydef_oid			Oid;
	policypred_predid		Oid;
	pred_attrid			Oid;
	attr_name			text;
	count_p				integer;
	counter				integer;
	jump				integer;
	next_policy_match		integer;
	new_val				text[];	
	pred_val			text;
	add_db_id			text[];
	add_db_id_tmp			text;
	add_user_id			text[];
	add_user_id_tmp			text;
	add_relation_id			text[];
	add_client_app			text[];
	add_command			bit(32);
	add_ip				inet;
	new_val_cmd			bit(32);
	
BEGIN
	 
	select into add_db_id_tmp oid from pg_database where datname = current_database();
	add_db_id[1] := add_db_id_tmp;

	select into add_user_id_tmp oid from pg_authid where rolname = rolename;	
	add_user_id[1] := add_user_id_tmp;
	
	add_relation_id[1] := relation_id;
	add_client_app[1] := 'c_0';	
	
	select into add_command cast(2 as bit(32)); -- select command
	
	count_p := 0;

	select into num_policies count(*) from pg_m_rpolicy_def;

	jump := num_policies/num_match_policies;

	counter := 1;
	next_policy_match := jump;
	
	for policydef_oid in Select oid from pg_m_rpolicy_def loop

		if (count_p = num_match_policies) then
			exit;
		end if;

		if (counter != next_policy_match) then
			counter := counter + 1; -- increment the policy counter
			continue;
		end if;

		raise notice 'matching policy oid % and policy number %',policydef_oid, next_policy_match;
		
		count_p := count_p + 1; -- increment the matching policy counter
		counter := counter + 1; -- increment the policy counter		
		next_policy_match := next_policy_match + jump;

		
		for policypred_predid in select predid from pg_m_rpolicy_policypreds where policyid = policydef_oid loop
		
			select into pred_attrid attrid from pg_m_rpolicy_preds where pg_m_rpolicy_preds.oid = policypred_predid;
			select into pred_val val from pg_m_rpolicy_preds where pg_m_rpolicy_preds.oid = policypred_predid;
			select into attr_name attrname from pg_m_rpolicy_attrs where pg_m_rpolicy_attrs.oid = pred_attrid;

			if (attr_name = 'database_id') then
				select into new_val cast(pred_val as text[]);				
				if add_db_id <@ new_val then
					continue;
				end if; 
				
				new_val := new_val || add_db_id;
				update pg_m_rpolicy_preds set val = '{'||array_to_string(new_val,',')||'}' where oid = policypred_predid;
				
			elsif (attr_name = 'user_id') then
				select into new_val cast(pred_val as text[]);				
				if add_user_id <@ new_val then
					continue;
				end if; 
				
				new_val := new_val || add_user_id;
				update pg_m_rpolicy_preds set val = '{'||array_to_string(new_val,',')||'}' where oid = policypred_predid;
			
			elsif (attr_name = 'relation_id') then
				select into new_val cast(pred_val as text[]);				
				if add_relation_id <@ new_val then
					continue;
				end if; 
				
				new_val := new_val || add_relation_id;
				update pg_m_rpolicy_preds set val = '{'||array_to_string(new_val,',')||'}' where oid = policypred_predid;

			
			elsif (attr_name = 'client_app') then
				select into new_val cast(pred_val as text[]);				
				if add_client_app <@ new_val then
					continue;
				end if; 
				
				new_val := new_val || add_client_app;
				update pg_m_rpolicy_preds set val = '{'||array_to_string(new_val,',')||'}' where oid = policypred_predid;
			
			elsif (attr_name = 'command') then
				select into new_val_cmd cast(pred_val as bit(32));

				new_val_cmd := new_val_cmd | add_command;
				update pg_m_rpolicy_preds set val = cast(new_val_cmd as text) where oid = policypred_predid;
			end if;
			
		end loop;
	end loop;	
	
END;

$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION f_pg_m_rpolicy_generate_data_matching_policies(integer, text, text) OWNER TO postgres;
