-- Function: f_pg_m_rpolicy_generate_data_preds(text, integer)

-- DROP FUNCTION f_pg_m_rpolicy_generate_data_preds(text, integer);

CREATE OR REPLACE FUNCTION f_pg_m_rpolicy_generate_data_preds(attrname text, num_preds integer)
  RETURNS void AS
$BODY$
DECLARE
	dev_null	integer;
BEGIN

	if (attrname = 'relation_id' or attrname = 'user_id' or attrname = 'database_id') then
		Select into dev_null f_pg_m_rpolicy_generate_data_preds_oid(attrname, num_preds);
	elsif attrname = 'source_ip' then
		Select into dev_null f_pg_m_rpolicy_generate_data_preds_source_ip(attrname, num_preds);
	elsif attrname = 'command' then
		Select into dev_null f_pg_m_rpolicy_generate_data_preds_command(attrname, num_preds);
	elseif attrname = 'client_app' then
		Select into dev_null f_pg_m_rpolicy_generate_data_preds_client_app(attrname, num_preds);
	else
		RAISE EXCEPTION 'Invalid input attribute name --> %',attrname;
	end if;
		
END;

$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION f_pg_m_rpolicy_generate_data_preds(text, integer) OWNER TO postgres;
