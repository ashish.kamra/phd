-- Function: f_pg_m_rpolicy_generate_data_preds_command(text, integer)

-- DROP FUNCTION f_pg_m_rpolicy_generate_data_preds_command(text, integer);

CREATE OR REPLACE FUNCTION f_pg_m_rpolicy_generate_data_preds_command(attrname text, num_preds integer)
  RETURNS integer AS
$BODY$
DECLARE
	pred_count		integer;
	pred_val		text;
	inp_operator		text;
	inppredname		text;
	pred_oid		oid;
	num_commands_per_pred	integer;
	max_num_commands_per_pred	integer;
	pred_entry		bit(32);
	mask			bit(32);
	pred_cmd_num		integer;
BEGIN
	
	pred_count := 1;

	inp_operator := '~';

	max_num_commands_per_pred := 8;
	
	while (pred_count <= num_preds) loop

		select into num_commands_per_pred cast(max_num_commands_per_pred*random() as integer);
		Select into pred_entry cast(0 as bit(32));
		Select into mask cast(1 as bit(32));
		for i in 1..num_commands_per_pred loop
			select into pred_cmd_num cast(10*random() as integer);
			pred_entry := pred_entry | (mask << pred_cmd_num);			
		end loop;
				
		pred_val := cast(pred_entry as text);
		
		inppredname := attrname||'_pred_'||cast(pred_count as text); 
		
		Select into pred_oid f_pg_m_rpolicy_createpred(inppredname, attrname, inp_operator, pred_val);
		
		pred_count := pred_count + 1;
	end loop;			

	return 0;
END;

$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION f_pg_m_rpolicy_generate_data_preds_command(text, integer) OWNER TO postgres;
