insert into "ANOMALY" (
			"ANOMALY_TYPE",
			"USER",
			"ROLE",
			"SOURCE",
			"CLIENT",
			"DB",
			"SCHEMA",
			"OBJ_TYPEs",
			"SQL_CMDs",
			"OBJs",
			"OBJ_ATTRs",
			"DATE_TIME",
			"ANOMALY_DESC") 
			values
			('ANOMALY',
			NULL, -- db1_readUser
			NULL, -- dataReader
			NULL,
			'c1',
			NULL,
			NULL,
			NULL,
			'{insert,update,delete}',
			NULL,
			NULL,
			NULL,
			NULL)