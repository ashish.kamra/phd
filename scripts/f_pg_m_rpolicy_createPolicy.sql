-- Function: f_pg_m_rpolicy_createpolicy(text, text[], text[], text[], text[], text[])

-- DROP FUNCTION f_pg_m_rpolicy_createpolicy(text, text[], text[], text[], text[], text[]);

CREATE OR REPLACE FUNCTION f_pg_m_rpolicy_createpolicy(inppolicyname text, inppredicatesname text[], inpinitactionsname text[], inpconfirmactionsname text[], inpresolveactionsname text[], inpfailactionsname text[], event_id integer)
  RETURNS oid AS
$BODY$
declare
	return_policy_oid		Oid;
	inpPredicatesName_Oid		Oid[];
	inpPredicatesOid_text		text;	
	inpInitActionsName_Oid		Oid[];
	inpInitActionsOid_text		text;
	inpConfirmActionsName_Oid	Oid[];
	inpConfirmActionsOid_text	text;
	inpResolveActionsName_Oid	Oid[];
	inpResolveActionsOid_text	text;
	inpFailActionsName_Oid		Oid[];
	inpFailActionsOid_text		text;
	inpSeverity			int4;
	i				int4;
	temp				Oid;
	command_to_execute		text;
begin
	if (inpPredicatesName is null or inpInitActionsName is null or inpPolicyName is null) then
		RAISE EXCEPTION 'Policyname/PredicateNames/InitialActionNames can not be null.';
		return null;
	end if;

	-- right now severity is simply the maximum severity level of the initial actions
	Select max(severity) into inpSeverity from pg_m_rpolicy_actions where actionname = ANY(inpInitActionsName);

	for i IN 1..array_upper(inpPredicatesName,1) loop
		Select Oid into temp from pg_m_rpolicy_preds where predname = inpPredicatesName[i];

		if (temp is null) then
			RAISE EXCEPTION 'Predicate --> % is not present in the pg_m_rpolicy_preds table.',inpPredicatesName[i];
			return null;		
		end if;

		inpPredicatesName_Oid[i] := temp;
		
	end loop;

	inpPredicatesOid_text := '{'
				   ||array_to_string(inpPredicatesName_Oid,',')
				   ||'}';

	for i IN 1..array_upper(inpInitActionsName,1) loop
		Select Oid into temp from pg_m_rpolicy_actions where actionname = inpInitActionsName[i];

		if (temp is null) then
			RAISE EXCEPTION 'Action --> % is not a valid response action.',inpInitActionsName[i];
			return null;		
		end if;

		inpInitActionsName_Oid[i] := temp;
		
	end loop;

	inpInitActionsOid_text := '{'
				    ||array_to_string(inpInitActionsName_Oid,',')
				    ||'}';
				    

	if (inpConfirmActionsName is not null) then

		if (inpResolveActionsName is null or inpFailActionsName is null) then
			RAISE EXCEPTION 'Resolution Action and Failure Actions can not be null if Confirmation Action is not null.';
			return null;
		end if;
		
		for i IN 1..array_upper(inpConfirmActionsName,1) loop
			Select Oid into temp from pg_m_rpolicy_actions where actionname = inpConfirmActionsName[i];

			if (temp is null) then
				RAISE EXCEPTION 'Action --> % is not a valid response action.',inpConfirmActionsName[i];
				return null;		
			end if;

			inpConfirmActionsName_Oid[i] := temp;		
		end loop;

		inpConfirmActionsOid_text := '{'
				    ||array_to_string(inpConfirmActionsName_Oid,',')
				    ||'}';
				    
		for i IN 1..array_upper(inpResolveActionsName,1) loop
			Select Oid into temp from pg_m_rpolicy_actions where actionname = inpResolveActionsName[i];

			if (temp is null) then
				RAISE EXCEPTION 'Action --> % is not a valid response action.',inpResolveActionsName[i];
				return null;		
			end if;

			inpResolveActionsName_Oid[i] := temp;		
		end loop;

		inpResolveActionsOid_text := '{'
				    ||array_to_string(inpResolveActionsName_Oid,',')
				    ||'}';


		for i IN 1..array_upper(inpFailActionsName,1) loop
			Select Oid into temp from pg_m_rpolicy_actions where actionname = inpFailActionsName[i];

			if (temp is null) then
				RAISE EXCEPTION 'Action --> % is not a valid response action.',inpFailActionsName[i];
				return null;		
			end if;

			inpFailActionsName_Oid[i] := temp;		
		end loop;

		inpFailActionsOid_text := '{'
				    ||array_to_string(inpFailActionsName_Oid,',')
				    ||'}';

		command_to_execute := 'CREATE RPOLICY DEF '
				      || '''' || inpPolicyName || ''''
				      || ' WITH EVENTID '
                      || event_id 
					  || ' SEVERITY '
				      || inpSeverity
				      || ' CONDITIONS '
				      || '''' || inpPredicatesOid_text || ''''
				      || ' INIT_ACTIONS '
				      || '''' || inpInitActionsOid_text || ''''
				      || ' CONFIRM_ACTIONS '
				      || '''' || inpConfirmActionsOid_text || ''''
				      || ' RESOLVE_ACTIONS '
				      || '''' || inpResolveActionsOid_text || ''''
				      || ' FAIL_ACTIONS '
				      || '''' || inpFailActionsOid_text || ''''
				      || ' JOINT ADM BY 2 / 6 USERS';
				      
	else
		command_to_execute := 'CREATE RPOLICY DEF '
		      || '''' || inpPolicyName || ''''
              || ' WITH EVENTID '
              || event_id
              || ' SEVERITY '
              || inpSeverity
		      || ' CONDITIONS '
		      || '''' || inpPredicatesOid_text || ''''
		      || ' INIT_ACTIONS '
		      || '''' || inpInitActionsOid_text || ''''
		      || ' JOINT ADM BY 2 / 6 USERS';
	end if;

	Execute command_to_execute;

	return 0;
end;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;

ALTER FUNCTION f_pg_m_rpolicy_createpolicy(text, text[], text[], text[], text[], text[], integer) OWNER TO postgres;
