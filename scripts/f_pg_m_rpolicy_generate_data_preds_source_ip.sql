-- Function: f_pg_m_rpolicy_generate_data_preds_source_ip(text, integer)

-- DROP FUNCTION f_pg_m_rpolicy_generate_data_preds_source_ip(text, integer);

CREATE OR REPLACE FUNCTION f_pg_m_rpolicy_generate_data_preds_source_ip(attrname text, num_preds integer)
  RETURNS integer AS
$BODY$
DECLARE
	pred_count		integer;
	pred_val		text;
	inp_operator		text;
	inppredname		text;
	pred_oid		oid;
	pred_entry		text;	
BEGIN
	
	pred_count := 1;

	inp_operator := '<<=';
	
	while (pred_count <= num_preds) loop

		select into pred_entry cast(255*random() as integer);
		
		pred_val := '127.0.'||cast(pred_entry as text)||'/16';
		
		inppredname := attrname||'_pred_'||cast(pred_count as text); 
		
		Select into pred_oid f_pg_m_rpolicy_createpred(inppredname, attrname, inp_operator, pred_val);
		
		pred_count := pred_count + 1;
	end loop;			

	return 0;
END;

$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION f_pg_m_rpolicy_generate_data_preds_source_ip(text, integer) OWNER TO postgres;
