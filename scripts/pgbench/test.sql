BEGIN;
insert into t1 values(1,2,3,4);
insert into t2 values(1,2,3,4);
insert into t5(a5,b5,d5) values(1,3,4);
select t2.d2,t3.d3 from t2,t3 where t2.a2 = 1;
update t5 set a5 = 10 where a5 = 1;
update t2 set c2 = 10 where a2 = 1;
select t3.a3, t3.d3 from t3;
END;