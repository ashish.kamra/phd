create or replace function tableOID (tabName text) returns OID as 
$$
DECLARE
	tabOID	OID;
BEGIN

	select oid into tabOID from pg_class where relname = tabName and 
	 pg_class.relkind = 'r'::"char";

	return tabOID;
END;
$$ 
LANGUAGE 'plpgsql';

