-- Function: f_pg_m_rpolicy_createpred(text, text, text, text)

-- DROP FUNCTION f_pg_m_rpolicy_createpred(text, text, text, text);

CREATE OR REPLACE FUNCTION f_pg_m_rpolicy_createpred(inppredname text, inpattrname text, inpoperator text, inppredval text)
  RETURNS oid AS
$BODY$
declare
	attrOid 		Oid;
	inserted_predicate_oid	Oid;
	attrTypeOid		Oid;
	anyArrayTypeOid		Oid;
	inpOprOid		Oid;	
	not_supported_type	text;
	command_to_execute	text;
begin

	-- get the oid for the requested attribute
	Select Oid, attrtype into attrOid, attrTypeOid from pg_m_rpolicy_attrs where attrname = inpattrName;
			
	if (attrOid is null) then
		RAISE EXCEPTION 'No attribute present in pg_m_rpolicy_attrs with name --> %', inpattrName;
		return null;
	end if;

	-- get the oid of the operator type
	-- the correct oid is obtained after checking the oid for the attribute 
	
	if (attrTypeOid = 1028 or attrTypeOid = 1007 or attrTypeOid = 1009) then -- inpAttrType is either _Oid or _int4 or _text
		anyArrayTypeOid := 2277;
		-- form the query with the oid of type anyarray as that is the only type availabe in pg_operatror for arrays
		Select Oid into inpOprOid from pg_operator where oprright = anyArrayTypeOid and oprname = inpOperator;
	elsif (attrTypeOid = 869 or attrTypeOid = 23) then -- inpAttrType is an inet or an integer
		Select Oid into inpOprOid from pg_operator where oprright = attrTypeOid and oprname = inpOperator;
	else 
		Select typname into not_supported_type from pg_type where Oid = attrTypeOid;
		RAISE EXCEPTION 'The input attribute --> % has type --> % which is not supported.',inpAttrName,not_supported_type;
		return null;	    
	end if;

	if (inpOprOid is null) then
		RAISE EXCEPTION 'The input operator --> % is invalid.',inpOperator;
		return null;	    
	end if;

	
	command_to_execute :=   'CREATE RPOLICY PRED '
				||''''
				||inpPredName
				||''''
				||' WITH ATTROID '
				||attrOid
				||' OPROID '
				||inpOprOid
				||' VAL '
				||''''
				||inpPredVal
				||''''
				||';'
				;		

	Execute command_to_execute;
	
	Select Oid into inserted_predicate_oid from pg_m_rpolicy_preds 
	where 
	predname = inpPredName and 
	attrid = attrTypeOid and 
	oprid = inpOprOid and
	val = inpPredVal;
	
	return inserted_predicate_oid;
end;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION f_pg_m_rpolicy_createpred(text, text, text, text) OWNER TO postgres;
