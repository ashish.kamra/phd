

--DROP FUNCTION f_pg_m_rpolicy_generate_data_policies(integer, integer, integer, integer);

CREATE OR REPLACE FUNCTION f_pg_m_rpolicy_generate_data_policies(num_policies integer, eventid integer)
  RETURNS void AS
$BODY$
DECLARE
	prob_attr_exclusion		float;
	tmp				float;	
	attroid				Oid;
	cursor_preds CURSOR (key Oid) IS SELECT * FROM pg_m_rpolicy_preds WHERE attrid = key;
	selected_pred_row		pg_m_rpolicy_preds;
	policy_oid			Oid;
	inp_pred_names			text[];
	inp_policy_name			text;
	pred_num			integer;
	pred_count			integer;
	max_preds			integer;
	i				integer;
BEGIN
	prob_attr_exclusion := 0.0;
	
	PERFORM 'Select setseed(EXTRACT(EPOCH FROM CURRENT_TIME))';

	For i in 1..num_policies loop	
	
		pred_count := 1;
		inp_pred_names := NULL;
		
		For attroid in Select Oid From pg_m_rpolicy_attrs loop
		
			Select into tmp random();
			
			if (tmp >= prob_attr_exclusion) then
				 
				open cursor_preds(attroid);

				-- only 1 predicate is possible per attribute so decide which one
				Select into max_preds count(*) from pg_m_rpolicy_preds where pg_m_rpolicy_preds.attrid = attroid;

				if (max_preds = 0) then
					close cursor_preds;
					CONTINUE;
				end if;
				
				Select into pred_num cast (max_preds*random() as integer);
				
				if (pred_num = 0) then
					pred_num := 1;
				end if;
				
				FETCH ABSOLUTE pred_num FROM cursor_preds INTO selected_pred_row;
				inp_pred_names[pred_count] := selected_pred_row.predname;
				pred_count := pred_count + 1;

				close cursor_preds;
				
			end if;
		end loop;

		inp_policy_name := 'random_policy_'||i;

		--raise notice '%',inp_pred_names;
		
		Select into policy_oid f_pg_m_rpolicy_createpolicy(inp_policy_name, 
								  inp_pred_names, 
								   '{alert}', 
								   null, 
								   null, 
								   null,
								   eventid);
		
	end loop;
	
		
END;

$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION f_pg_m_rpolicy_generate_data_policies(integer, integer) OWNER TO postgres;
