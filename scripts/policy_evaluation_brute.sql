
CREATE OR REPLACE FUNCTION policy_evaluation_brute()
  RETURNS trigger AS
$BODY$
    DECLARE 
	index integer;
	i integer;

	-- database stuff
	db "ANOMALY"."DB"%TYPE;
	_role "ANOMALY"."ROLE"%TYPE;
	_user "ANOMALY"."USER"%TYPE;
	objs 	"ANOMALY"."OBJs"%TYPE;

	-- free text stuff
	sql_cmds	"ANOMALY"."SQL_CMDs"%TYPE;
	_schema		"ANOMALY"."SCHEMA"%TYPE;
	client		"ANOMALY"."CLIENT"%TYPE;

	-- source
	source		"ANOMALY"."SOURCE"%TYPE;

	cursor1 REFCURSOR;
	transactionStart timestamp;
	transactionEnd timestamp;
	timeElapsed interval;
	severity integer;
	maxSeverity integer;	
	policy_row "PG_POLICY"%ROWTYPE;
	policy_id int[];
	
    BEGIN

	select into transactionStart clock_timestamp();
	
	db := NEW."DB";
    	_role := NEW."ROLE";
	objs := NEW."OBJs";

	-- free text stuff
	sql_cmds := NEW."SQL_CMDs";
	_schema := NEW."SCHEMA";
	client := NEW."CLIENT";

	-- source
	source := NEW."SOURCE";

	if objs is null then

		open cursor1 FOR Select *
			 From "PG_POLICY"
			 Where 
			     db = ANY("DBs") 
			     and
			     _role = ANY("ROLEs") 			    
			     and	
			     source << ANY("SOURCEs")			     
			     and		    
			     client = ANY("CLIENTs");
	else
		open cursor1 FOR Select *
			 From "PG_POLICY"
			 Where 
			     db = ANY("DBs") 
			     and
			     _role = ANY("ROLEs") 			    
			     and	
			     source << ANY("SOURCEs")			     
			     and		    
			     client = ANY("CLIENTs") 
			     and
			     sql_cmds <@ "SQL_CMDs" 
			     and
			     objs <@ "OBJs";
	end if;
			     
	-- now filter these for sql commands and objects	
	Fetch cursor1 INTO policy_row;	
	index := 1;
	maxSeverity := 0;			
	While policy_row."POLICY_ID" is not null loop
		severity := policy_row."ACTION_SEVERITY_LEVEL";
		
		if severity > maxSeverity then
			maxSeverity := severity;
		end if;
		
		policy_id[index] := policy_row."POLICY_ID";
		index := index+1;		
		Fetch cursor1 INTO policy_row;
	end loop;	
	close cursor1;
	
	select into transactionEnd clock_timestamp();
	select into timeElapsed extract(milliseconds from age(transactionEnd,transactionStart));
				
	raise notice 'applicable policies brute= %', policy_id;		
	raise notice 'elapsed time brute = %', timeElapsed;	
	
	return NEW;
	
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION policy_evaluation_brute() OWNER TO postgres;
