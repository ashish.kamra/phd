-- Function: f_pg_m_rpolicy_generate_data_preds(text, integer)

-- DROP FUNCTION f_pg_m_rpolicy_generate_data_preds(text, integer);

CREATE OR REPLACE FUNCTION f_pg_m_rpolicy_generate_data_all_preds(num_preds integer)
  RETURNS void AS
$BODY$
DECLARE
	dev_null	integer;
BEGIN
	
	Select into dev_null f_pg_m_rpolicy_generate_data_preds_oid('relation_id', num_preds);
	Select into dev_null f_pg_m_rpolicy_generate_data_preds_oid('user_id', num_preds);
	Select into dev_null f_pg_m_rpolicy_generate_data_preds_oid('database_id', num_preds);	
	Select into dev_null f_pg_m_rpolicy_generate_data_preds_source_ip('source_ip', num_preds);
	Select into dev_null f_pg_m_rpolicy_generate_data_preds_command('command', num_preds);
	Select into dev_null f_pg_m_rpolicy_generate_data_preds_client_app('client_app', num_preds);		
END;

$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
