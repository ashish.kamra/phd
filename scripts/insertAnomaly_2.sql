insert into "ANOMALY" (
			"ANOMALY_TYPE",
			"USER",
			"ROLE",
			"SOURCE",
			"CLIENT",
			"DB",
			"SCHEMA",
			"OBJ_TYPEs",
			"SQL_CMDs",
			"OBJs",
			"OBJ_ATTRs",
			"DATE_TIME",
			"ANOMALY_DESC") 
			values
			('ANOMALY',
			NULL, -- db1_readUser
			NULL, -- dataReader
			NULL, -- source
			'c5', -- client
			NULL, -- database
			NULL, -- schema		
			NULL, -- object types
			'{select}', -- sql commands
			NULL, -- objects
			NULL, -- object attributes
			NULL, -- date and time
			NULL) -- description

			--'16675', -- database
			--'c5', -- client
			--'192.168.55.128'::inet, -- source
			--'{16675.11022,16675.10996,16675.3604,16675.10947,16675.10992}', -- objects
			-- '{select}', -- sql commands