-- Function: f_pg_m_rpolicy_generate_data_preds_oid(text, integer)

-- DROP FUNCTION f_pg_m_rpolicy_generate_data_preds_oid(text, integer);

CREATE OR REPLACE FUNCTION f_pg_m_rpolicy_generate_data_preds_oid(attrname text, num_preds integer)
  RETURNS integer AS
$BODY$
DECLARE
	pred_count		integer;
	entry_limit		integer;
	pred_entries		integer[];
	pred_entry		integer[];
	pred_entry_limit	integer;
	pred_val		text;
	inp_operator		text;
	inppredname		text;
	num_entries		integer;
	i			integer;
	tmp			integer;
	pred_oid		oid;
BEGIN
	-- limit the number of entries per predicate
	entry_limit := 10;

	pred_entry_limit := 50000;
	
	pred_count := 1;

	inp_operator := '<@';
	
	while (pred_count <= num_preds) loop

		select into num_entries cast(entry_limit*random() as integer);

		select into tmp cast(pred_entry_limit*random() as integer);
		if (attrname = 'database_id') then
			pred_entries[1] := tmp;
		elsif (attrname = 'relation_id') then
			pred_entries[1] := tmp + pred_entry_limit;
		elsif (attrname = 'user_id') then
			pred_entries[1] := tmp + 2*pred_entry_limit;
		end if;				

		pred_entry[1] := pred_entries[1];
		
		for i IN 2..num_entries loop
			
			while (pred_entry <@ pred_entries) loop
				select into tmp cast(pred_entry_limit*random() as integer);
				if (attrname = 'database_id') then
					pred_entry[1] := tmp;
				elsif (attrname = 'relation_id') then
					pred_entry[1] := tmp + pred_entry_limit;
				elsif (attrname = 'user_id') then
					pred_entry[1] := tmp + 2*pred_entry_limit;
				end if;				
			end loop;

			pred_entries[i] := pred_entry[1];			
						
		end loop;		

		pred_val := '{'||array_to_string(pred_entries, ',')||'}';
		
		inppredname := attrname||'_pred_'||cast(pred_count as text); 
		
		Select into pred_oid f_pg_m_rpolicy_createpred(inppredname, attrname, inp_operator, pred_val);
		
		pred_count := pred_count + 1;
	end loop;			

	return 0;
END;

$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION f_pg_m_rpolicy_generate_data_preds_oid(text, integer) OWNER TO postgres;
