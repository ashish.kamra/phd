-- Function: f_pg_m_rpolicy_jtam_register(integer, integer, integer, integer)

-- DROP FUNCTION f_pg_m_rpolicy_jtam_register(integer, integer, integer, integer);

CREATE OR REPLACE FUNCTION f_pg_m_rpolicy_jtam_register(bits integer, e integer)
  RETURNS integer AS
'$libdir/response.so', 'f_pg_m_rpolicy_jtam_register'
  LANGUAGE 'c' VOLATILE
  COST 1;
ALTER FUNCTION f_pg_m_rpolicy_jtam_register(integer, integer) OWNER TO postgres;
