insert into pg_m_rpolicy_attrs values ('source_ip', 869); -- inet, 869 is the oid for inet datatype
insert into pg_m_rpolicy_attrs values ('relation_id', 1028); -- oid[] or _oid, 1028 is the oid for the oid[] datatype
insert into pg_m_rpolicy_attrs values ('command', 23); -- int4, or the AclMode if you like
insert into pg_m_rpolicy_attrs values ('user_id', 1028);    -- oid[] or _oid

-- predicates
insert into pg_m_rpolicy_preds values ('In internal network',16384,931,'192.168.1/24'); -- source_ip << inet '192.168.1/24' (contained within), 931 is the oid of the << op class applicable to inet
insert into pg_m_rpolicy_preds values ('In internal network - II',16384,931,'10.10.1/24'); -- source_ip << inet '10.10.1/24' (contained within),
create user untrusted_user password 'skims';
insert into pg_m_rpolicy_preds values ('User name = untrusted_user',16387,607,'{16390}'); -- user_id = 16390 (user name = 'untrusted_user'), 607 is the oid of the = op class applicable to oid
insert into pg_m_rpolicy_preds values ('command = insert,select',16386,96,'00000000000000000000000000000011'); -- command = ANY {insert,select}, 96 is the oid of the = op class applicable to integers, we ofcourse interpret is as IN

-- actions
insert into pg_m_rpolicy_actions values ('re-authenticate',3);
insert into pg_m_rpolicy_actions values ('alert',1);
insert into pg_m_rpolicy_actions values ('drop_query',4);
insert into pg_m_rpolicy_actions values ('suspend_privilege',3);
insert into pg_m_rpolicy_actions values ('grant_privilege',1);

-- policy definition
insert into pg_m_rpolicy_def values ('default',0,'{16394}',null,null,null);
insert into pg_m_rpolicy_def values ('internal network policy',3,'{16397}','{16393}','{16398}','{16396}');

-- policy predicates, associated predicates to policies
insert into pg_m_rpolicy_policypreds values (16388,16399);
insert into pg_m_rpolicy_policypreds values (16391,16399);
insert into pg_m_rpolicy_policypreds values (16392,16399);
