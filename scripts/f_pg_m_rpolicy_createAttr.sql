-- Function: f_pg_m_rpolicy_createattr(text, name)

-- DROP FUNCTION f_pg_m_rpolicy_createattr(text, name);

CREATE OR REPLACE FUNCTION f_pg_m_rpolicy_createattr(inpattrname text, inpattrtypename name)
  RETURNS oid AS
$BODY$
declare
	attrTypeOid Oid;
	inserted_attribute_oid Oid;
	attrNameCheck name;	
begin

	-- get the oid for the requested type
	Select Oid into attrTypeOid from pg_type where typname = inpAttrTypeName;
			
	if (attrTypeOid is null) then
		RAISE EXCEPTION 'No type with type name --> %', inpAttrTypeName;
		return null;
	end if;
	
	-- check whether the name already exists
	Select attrname into attrNameCheck from pg_m_rpolicy_attrs where attrname = inpAttrName;

	if (attrNameCheck is not null) then
		RAISE EXCEPTION 'Attribute Name --> % already present in pg_m_rpolicy_attrs catalog', inpAttrName;
		return null;
	end if;

	Execute 'CREATE RPOLICY ATTR '
		||''''
		||inpAttrName
		||''''
		||' WITH TYPEOID '
		||attrTypeOid
		||';'
		;		

	Select Oid into inserted_attribute_oid from pg_m_rpolicy_attrs where attrname = inpAttrName and attrtype = attrTypeOid;
	
	return inserted_attribute_oid;			

end;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION f_pg_m_rpolicy_createattr(text, name) OWNER TO postgres;
