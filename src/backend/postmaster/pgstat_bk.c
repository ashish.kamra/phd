/* ----------
 * pgstat.c
 *
 *	All the statistics collector stuff hacked up in one big, ugly file.
 *
 *	TODO:	- Separate collector, postmaster and backend stuff
 *			  into different files.
 *
 *			- Add some automatic call for pgstat vacuuming.
 *
 *			- Add a pgstat config column to pg_database, so this
 *			  entire thing can be enabled/disabled on a per db basis.
 *
 *	Copyright (c) 2001-2008, PostgreSQL Global Development Group
 *
 *	$PostgreSQL: pgsql/src/backend/postmaster/pgstat.c,v 1.169 2008/01/01 19:45:51 momjian Exp $ 
 *
 *      # Ashish : Stuff marked with %%% is the code taken from v 1.178 and hacked into this file to improve the performance of the stats collector writing process. Basically,
 *                 We move pgstat.tmp into a temporary directory under $PGDATA named pg_stat_tmp. This allows the use of a ramdrive (either through mount or symlink) for
 *		   the temporary file that's written every half second, which should reduce I/O. On server shutdown/startup, the file is written to the old location in
 *		   the global directory, to preserve data across restarts.
 *
 * ----------
 */
#include "postgres.h"

#include <unistd.h>
#include <fcntl.h>
#include <sys/param.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <time.h>
#ifdef HAVE_POLL_H
#include <poll.h>
#endif
#ifdef HAVE_SYS_POLL_H
#include <sys/poll.h>
#endif

#include "pgstat.h"

#include "access/heapam.h"
#include "access/transam.h"
#include "access/twophase_rmgr.h"
#include "access/xact.h"
#include "catalog/pg_database.h"
#include "libpq/ip.h"
#include "libpq/libpq.h"
#include "libpq/pqsignal.h"
#include "mb/pg_wchar.h"
#include "miscadmin.h"
#include "postmaster/autovacuum.h"
#include "postmaster/fork_process.h"
#include "postmaster/postmaster.h"
#include "storage/backendid.h"
#include "storage/fd.h"
#include "storage/ipc.h"
#include "storage/pg_shmem.h"
#include "storage/pmsignal.h"
#include "utils/guc.h"
#include "utils/memutils.h"
#include "utils/ps_status.h"

/* Ashish */
#include "utils/catcache.h"
#include "utils/syscache.h"

/* ----------
 * Paths for the statistics files (relative to installation's $PGDATA).
 * ----------
 */
//#define PGSTAT_STAT_FILENAME	"global/pgstat.stat"
//#define PGSTAT_STAT_TMPFILE	"global/pgstat.tmp"

/* %%% Ashish : v 1.178 */
#define PGSTAT_STAT_PERMANENT_FILENAME	"global/pgstat.stat"
#define PGSTAT_STAT_PERMANENT_TMPFILE	"global/pgstat.tmp"
#define PGSTAT_STAT_FILENAME			"pg_stat_tmp/pgstat.stat"
#define PGSTAT_STAT_TMPFILE				"pg_stat_tmp/pgstat.tmp"


/* ----------
 * Timer definitions.
 * ----------
 */
<<<<<<< .mine
#define PGSTAT_STAT_INTERVAL	1000		/* How often to write the status file;
=======
#define PGSTAT_STAT_INTERVAL	1000	/* How often to write the status file;
>>>>>>> .r86
										 * in milliseconds. */

#define PGSTAT_RESTART_INTERVAL 60		/* How often to attempt to restart a
										 * failed statistics collector; in
										 * seconds. */

#define PGSTAT_SELECT_TIMEOUT	2		/* How often to check for postmaster
										 * death; in seconds. */


/* ----------
 * GUC parameters
 * ----------
 */
bool		pgstat_track_activities = false;
bool		pgstat_track_counts = false;

/*
 * BgWriter global statistics counters (unused in other processes).
 * Stored directly in a stats message structure so it can be sent
 * without needing to copy things around.  We assume this inits to zeroes.
 */
PgStat_MsgBgWriter BgWriterStats;

/* ----------
 * Local data
 * ----------
 */
NON_EXEC_STATIC int pgStatSock = -1;

static struct sockaddr_storage pgStatAddr;

static time_t last_pgstat_start_time;

static bool pgStatRunningInCollector = false;

/*
 * Structures in which backends store per-table info that's waiting to be
 * sent to the collector.
 *
 * NOTE: once allocated, TabStatusArray structures are never moved or deleted
 * for the life of the backend.  Also, we zero out the t_id fields of the
 * contained PgStat_TableStatus structs whenever they are not actively in use.
 * This allows relcache pgstat_info pointers to be treated as long-lived data,
 * avoiding repeated searches in pgstat_initstats() when a relation is
 * repeatedly opened during a transaction.
 */
#define TABSTAT_QUANTUM		100 /* we alloc this many at a time */

typedef struct TabStatusArray
{
	struct TabStatusArray *tsa_next;	/* link to next array, if any */
	int			tsa_used;		/* # entries currently used */
	PgStat_TableStatus tsa_entries[TABSTAT_QUANTUM];	/* per-table data */
} TabStatusArray;

static TabStatusArray *pgStatTabList = NULL;

/*
 * Tuple insertion/deletion counts for an open transaction can't be propagated
 * into PgStat_TableStatus counters until we know if it is going to commit
 * or abort.  Hence, we keep these counts in per-subxact structs that live
 * in TopTransactionContext.  This data structure is designed on the assumption
 * that subxacts won't usually modify very many tables.
 */
typedef struct PgStat_SubXactStatus
{
	int			nest_level;		/* subtransaction nest level */
	struct PgStat_SubXactStatus *prev;	/* higher-level subxact if any */
	PgStat_TableXactStatus *first;		/* head of list for this subxact */
} PgStat_SubXactStatus;

static PgStat_SubXactStatus *pgStatXactStack = NULL;

static int	pgStatXactCommit = 0;
static int	pgStatXactRollback = 0;

/* Record that's written to 2PC state file when pgstat state is persisted */
typedef struct TwoPhasePgStatRecord
{
	PgStat_Counter tuples_inserted;		/* tuples inserted in xact */
	PgStat_Counter tuples_deleted;		/* tuples deleted in xact */
	Oid			t_id;			/* table's OID */
	bool		t_shared;		/* is it a shared catalog? */
} TwoPhasePgStatRecord;

/*
 * Info about current "snapshot" of stats file
 */
static MemoryContext pgStatLocalContext = NULL;
static HTAB *pgStatDBHash = NULL;
static PgBackendStatus *localBackendStatusTable = NULL;
static int	localNumBackends = 0;

/*
 * Cluster wide statistics, kept in the stats collector.
 * Contains statistics that are not collected per database
 * or per table.
 */
static PgStat_GlobalStats globalStats;

static volatile bool need_exit = false;
static volatile bool need_statwrite = false;


/* ----------
 * Local function forward declarations
 * ----------
 */
#ifdef EXEC_BACKEND
static pid_t pgstat_forkexec(void);
#endif

NON_EXEC_STATIC void PgstatCollectorMain(int argc, char *argv[]);
static void pgstat_exit(SIGNAL_ARGS);
static void force_statwrite(SIGNAL_ARGS);
static void pgstat_beshutdown_hook(int code, Datum arg);

static PgStat_StatDBEntry *pgstat_get_db_entry(Oid databaseid, bool create);

//static void pgstat_write_statsfile(void);
//static HTAB *pgstat_read_statsfile(Oid onlydb);

/* %%% Ashish : v 1.178 */
static void pgstat_write_statsfile(bool permanent);
static HTAB *pgstat_read_statsfile(Oid onlydb, bool permanent);

static void backend_read_statsfile(void);
static void pgstat_read_current_status(void);

static void pgstat_send_tabstat(PgStat_MsgTabstat *tsmsg);
static HTAB *pgstat_collect_oids(Oid catalogid);

static PgStat_TableStatus *get_tabstat_entry(Oid rel_id, bool isshared);

static void pgstat_setup_memcxt(void);

static void pgstat_setheader(PgStat_MsgHdr *hdr, StatMsgType mtype);
static void pgstat_send(void *msg, int len);

static void pgstat_recv_tabstat(PgStat_MsgTabstat *msg, int len);
static void pgstat_recv_tabpurge(PgStat_MsgTabpurge *msg, int len);

/* Ashish */
void pgstat_recv_loginstat(PgStat_MsgLoginStat *msg, int len);
void pgstat_recv_taboidstat(PgStat_MsgTabOidStat *msg, int len);
void pgstat_recv_rolestat(PgStat_MsgRoleStat *msg, int len);
void pgstat_recv_rolestat_new(char *msg, int len);
static void clearCounts();

static inline uint32 append(char** p,
							const void* data,
							uint32 size,
							uint32 size_so_far,
							char **to_send_msg)
{
	if (size_so_far + size >= MSG_SIZE)
	{
		char new_msg[2*MSG_SIZE];
		memset(new_msg, 0, 2*MSG_SIZE);
		memcpy(new_msg, *to_send_msg, size_so_far);
		*to_send_msg = new_msg;
		*p = *to_send_msg + size_so_far;
	}

	memcpy(*p, data, size);
	*p += size;

	return size;
}

static inline void remove_item(void** msg,
							   void* data,
							   uint32 size)
{
	memcpy(data, *msg, size);
	*msg += size;
}

/* Ashish */

static void pgstat_recv_dropdb(PgStat_MsgDropdb *msg, int len);
static void pgstat_recv_resetcounter(PgStat_MsgResetcounter *msg, int len);
static void pgstat_recv_autovac(PgStat_MsgAutovacStart *msg, int len);
static void pgstat_recv_vacuum(PgStat_MsgVacuum *msg, int len);
static void pgstat_recv_analyze(PgStat_MsgAnalyze *msg, int len);
static void pgstat_recv_bgwriter(PgStat_MsgBgWriter *msg, int len);


/* ------------------------------------------------------------
 * Public functions called from postmaster follow
 * ------------------------------------------------------------
 */

/* ----------
 * pgstat_init() -
 *
 *	Called from postmaster at startup. Create the resources required
 *	by the statistics collector process.  If unable to do so, do not
 *	fail --- better to let the postmaster start with stats collection
 *	disabled.
 * ----------
 */
void
pgstat_init(void)
{
	ACCEPT_TYPE_ARG3 alen;
	struct addrinfo *addrs = NULL,
			   *addr,
				hints;
	int			ret;
	fd_set		rset;
	struct timeval tv;
	char		test_byte;
	int			sel_res;
	int			tries = 0;

#define TESTBYTEVAL ((char) 199)

	/*
	 * Create the UDP socket for sending and receiving statistic messages
	 */
	hints.ai_flags = AI_PASSIVE;
	hints.ai_family = PF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_protocol = 0;
	hints.ai_addrlen = 0;
	hints.ai_addr = NULL;
	hints.ai_canonname = NULL;
	hints.ai_next = NULL;
	ret = pg_getaddrinfo_all("localhost", NULL, &hints, &addrs);
	if (ret || !addrs)
	{
		ereport(LOG,
				(errmsg("could not resolve \"localhost\": %s",
						gai_strerror(ret))));
		goto startup_failed;
	}

	/*
	 * On some platforms, pg_getaddrinfo_all() may return multiple addresses
	 * only one of which will actually work (eg, both IPv6 and IPv4 addresses
	 * when kernel will reject IPv6).  Worse, the failure may occur at the
	 * bind() or perhaps even connect() stage.	So we must loop through the
	 * results till we find a working combination. We will generate LOG
	 * messages, but no error, for bogus combinations.
	 */
	for (addr = addrs; addr; addr = addr->ai_next)
	{
#ifdef HAVE_UNIX_SOCKETS
		/* Ignore AF_UNIX sockets, if any are returned. */
		if (addr->ai_family == AF_UNIX)
			continue;
#endif

		if (++tries > 1)
			ereport(LOG,
			(errmsg("trying another address for the statistics collector")));

		/*
		 * Create the socket.
		 */
		if ((pgStatSock = socket(addr->ai_family, SOCK_DGRAM, 0)) < 0)
		{
			ereport(LOG,
					(errcode_for_socket_access(),
			errmsg("could not create socket for statistics collector: %m")));
			continue;
		}

		/*
		 * Bind it to a kernel assigned port on localhost and get the assigned
		 * port via getsockname().
		 */
		if (bind(pgStatSock, addr->ai_addr, addr->ai_addrlen) < 0)
		{
			ereport(LOG,
					(errcode_for_socket_access(),
			  errmsg("could not bind socket for statistics collector: %m")));
			closesocket(pgStatSock);
			pgStatSock = -1;
			continue;
		}

		alen = sizeof(pgStatAddr);
		if (getsockname(pgStatSock, (struct sockaddr *) & pgStatAddr, &alen) < 0)
		{
			ereport(LOG,
					(errcode_for_socket_access(),
					 errmsg("could not get address of socket for statistics collector: %m")));
			closesocket(pgStatSock);
			pgStatSock = -1;
			continue;
		}

		/*
		 * Connect the socket to its own address.  This saves a few cycles by
		 * not having to respecify the target address on every send. This also
		 * provides a kernel-level check that only packets from this same
		 * address will be received.
		 */
		if (connect(pgStatSock, (struct sockaddr *) & pgStatAddr, alen) < 0)
		{
			ereport(LOG,
					(errcode_for_socket_access(),
			errmsg("could not connect socket for statistics collector: %m")));
			closesocket(pgStatSock);
			pgStatSock = -1;
			continue;
		}

		/*
		 * Try to send and receive a one-byte test message on the socket. This
		 * is to catch situations where the socket can be created but will not
		 * actually pass data (for instance, because kernel packet filtering
		 * rules prevent it).
		 */
		test_byte = TESTBYTEVAL;

retry1:
		if (send(pgStatSock, &test_byte, 1, 0) != 1)
		{
			if (errno == EINTR)
				goto retry1;	/* if interrupted, just retry */
			ereport(LOG,
					(errcode_for_socket_access(),
					 errmsg("could not send test message on socket for statistics collector: %m")));
			closesocket(pgStatSock);
			pgStatSock = -1;
			continue;
		}

		/*
		 * There could possibly be a little delay before the message can be
		 * received.  We arbitrarily allow up to half a second before deciding
		 * it's broken.
		 */
		for (;;)				/* need a loop to handle EINTR */
		{
			FD_ZERO(&rset);
			FD_SET(pgStatSock, &rset);
			tv.tv_sec = 0;
			tv.tv_usec = 500000;
			sel_res = select(pgStatSock + 1, &rset, NULL, NULL, &tv);
			if (sel_res >= 0 || errno != EINTR)
				break;
		}
		if (sel_res < 0)
		{
			ereport(LOG,
					(errcode_for_socket_access(),
					 errmsg("select() failed in statistics collector: %m")));
			closesocket(pgStatSock);
			pgStatSock = -1;
			continue;
		}
		if (sel_res == 0 || !FD_ISSET(pgStatSock, &rset))
		{
			/*
			 * This is the case we actually think is likely, so take pains to
			 * give a specific message for it.
			 *
			 * errno will not be set meaningfully here, so don't use it.
			 */
			ereport(LOG,
					(errcode(ERRCODE_CONNECTION_FAILURE),
					 errmsg("test message did not get through on socket for statistics collector")));
			closesocket(pgStatSock);
			pgStatSock = -1;
			continue;
		}

		test_byte++;			/* just make sure variable is changed */

retry2:
		if (recv(pgStatSock, &test_byte, 1, 0) != 1)
		{
			if (errno == EINTR)
				goto retry2;	/* if interrupted, just retry */
			ereport(LOG,
					(errcode_for_socket_access(),
					 errmsg("could not receive test message on socket for statistics collector: %m")));
			closesocket(pgStatSock);
			pgStatSock = -1;
			continue;
		}

		if (test_byte != TESTBYTEVAL)	/* strictly paranoia ... */
		{
			ereport(LOG,
					(errcode(ERRCODE_INTERNAL_ERROR),
					 errmsg("incorrect test message transmission on socket for statistics collector")));
			closesocket(pgStatSock);
			pgStatSock = -1;
			continue;
		}

		/* If we get here, we have a working socket */
		break;
	}

	/* Did we find a working address? */
	if (!addr || pgStatSock < 0)
		goto startup_failed;

	/*
	 * Set the socket to non-blocking IO.  This ensures that if the collector
	 * falls behind, statistics messages will be discarded; backends won't
	 * block waiting to send messages to the collector.
	 */
	if (!pg_set_noblock(pgStatSock))
	{
		ereport(LOG,
				(errcode_for_socket_access(),
				 errmsg("could not set statistics collector socket to nonblocking mode: %m")));
		goto startup_failed;
	}

	pg_freeaddrinfo_all(hints.ai_family, addrs);

	return;

startup_failed:
	ereport(LOG,
	  (errmsg("disabling statistics collector for lack of working socket")));

	if (addrs)
		pg_freeaddrinfo_all(hints.ai_family, addrs);

	if (pgStatSock >= 0)
		closesocket(pgStatSock);
	pgStatSock = -1;

	/*
	 * Adjust GUC variables to suppress useless activity, and for debugging
	 * purposes (seeing track_counts off is a clue that we failed here). We
	 * use PGC_S_OVERRIDE because there is no point in trying to turn it back
	 * on from postgresql.conf without a restart.
	 */
	SetConfigOption("track_counts", "off", PGC_INTERNAL, PGC_S_OVERRIDE);
}

/*
 * pgstat_reset_all() -
 *
 * Remove the stats file.  This is currently used only if WAL
 * recovery is needed after a crash.
 */
void
pgstat_reset_all(void)
{
	unlink(PGSTAT_STAT_FILENAME);

	/* %%% Ashish : v 1.178 */
	unlink(PGSTAT_STAT_PERMANENT_FILENAME);
}

#ifdef EXEC_BACKEND

/*
 * pgstat_forkexec() -
 *
 * Format up the arglist for, then fork and exec, statistics collector process
 */
static pid_t
pgstat_forkexec(void)
{
	char	   *av[10];
	int			ac = 0;

	av[ac++] = "postgres";
	av[ac++] = "--forkcol";
	av[ac++] = NULL;			/* filled in by postmaster_forkexec */

	av[ac] = NULL;
	Assert(ac < lengthof(av));

	return postmaster_forkexec(ac, av);
}
#endif   /* EXEC_BACKEND */


/*
 * pgstat_start() -
 *
 *	Called from postmaster at startup or after an existing collector
 *	died.  Attempt to fire up a fresh statistics collector.
 *
 *	Returns PID of child process, or 0 if fail.
 *
 *	Note: if fail, we will be called again from the postmaster main loop.
 */
int
pgstat_start(void)
{
	time_t		curtime;
	pid_t		pgStatPid;

	/*
	 * Check that the socket is there, else pgstat_init failed and we can do
	 * nothing useful.
	 */
	if (pgStatSock < 0)
		return 0;

	/*
	 * Do nothing if too soon since last collector start.  This is a safety
	 * valve to protect against continuous respawn attempts if the collector
	 * is dying immediately at launch.	Note that since we will be re-called
	 * from the postmaster main loop, we will get another chance later.
	 */
	curtime = time(NULL);
	if ((unsigned int) (curtime - last_pgstat_start_time) <
		(unsigned int) PGSTAT_RESTART_INTERVAL)
		return 0;
	last_pgstat_start_time = curtime;

	/*
	 * Okay, fork off the collector.
	 */
#ifdef EXEC_BACKEND
	switch ((pgStatPid = pgstat_forkexec()))
#else
	switch ((pgStatPid = fork_process()))
#endif
	{
		case -1:
			ereport(LOG,
					(errmsg("could not fork statistics collector: %m")));
			return 0;

#ifndef EXEC_BACKEND
		case 0:
			/* in postmaster child ... */
			/* Close the postmaster's sockets */
			ClosePostmasterPorts(false);

			/* Lose the postmaster's on-exit routines */
			on_exit_reset();

			/* Drop our connection to postmaster's shared memory, as well */
			PGSharedMemoryDetach();

			PgstatCollectorMain(0, NULL);
			break;
#endif

		default:
			return (int) pgStatPid;
	}

	/* shouldn't get here */
	return 0;
}

void
allow_immediate_pgstat_restart(void)
{
	last_pgstat_start_time = 0;
}

/* ------------------------------------------------------------
 * Public functions used by backends follow
 *------------------------------------------------------------
 */

/*
 * 
 * Ashish: used in initpostgres to tell the collector about the login user and the activated roles.
 *  
 */

void
pgstat_report_login_stat(List *roles_list, List *tab_list)
{
	PgStat_MsgLoginStat		login_msg;
	PgStat_MsgTabOidStat	taboid_msg;
	ListCell				*role_cell;
	ListCell				*tab_cell;
	uint32					n, len;
	bool					anythingtosend = false;
	int						counter = 1;
	
	/* check if collector is running or not */
	if (pgStatSock < 0)
		return;

	/* send role oid information */
	MemSetAligned(&login_msg, 0, sizeof(PgStat_MsgLoginStat));
	login_msg.m_databaseid = MyDatabaseId;		
	login_msg.m_entries = 0;
	
	foreach(role_cell, roles_list) 
		login_msg.m_loginentry[login_msg.m_entries++] = lfirst_oid(role_cell);
	
	pgstat_setheader(&login_msg.m_hdr, PGSTAT_MTYPE_LOGINSTAT);
	n = login_msg.m_entries;
	len = offsetof(PgStat_MsgLoginStat, m_loginentry[0]) + n * sizeof(Oid);
	pgstat_send(&login_msg, len);
	
	/* no need send more messages for IDR_LEVEL == 0 */
	if (IDR_LEVEL == 0)
		return;
	
	/* send tab oid information */
	MemSetAligned(&taboid_msg, 0, sizeof(PgStat_MsgTabOidStat));
	taboid_msg.m_databaseid = MyDatabaseId;		
	taboid_msg.m_userid = login_msg.m_loginentry[0];			/* the first entry in the login message is the user id */
	taboid_msg.m_entries = 0;
	anythingtosend = false;
	
	counter = 1;
	foreach(tab_cell, tab_list)
	{	
		if (counter%2 != 0)		/* odd entry - table oid */
			taboid_msg.m_taboidentry[taboid_msg.m_entries].tab_oid  = lfirst_oid(tab_cell);
		else  					/* even entry - num columns for that table oid */
		{
			taboid_msg.m_taboidentry[taboid_msg.m_entries].num_atts = lfirst_oid(tab_cell);
			taboid_msg.m_entries++;
			anythingtosend = true;
		}
		
		counter++;
			
		if (taboid_msg.m_entries == PGSTAT_NUM_TABOIDENTRIES)
		{
			/* send the message */
			pgstat_setheader(&taboid_msg.m_hdr, PGSTAT_MTYPE_TABOIDSTAT);
			n = taboid_msg.m_entries;
			len = offsetof(PgStat_MsgTabOidStat, m_taboidentry[0]) + n * sizeof(PgStat_TabInfo);
			pgstat_send(&taboid_msg, len);			
			
			/* reinitialize the struct */
			MemSetAligned(&taboid_msg, 0, sizeof(PgStat_MsgTabOidStat));
			taboid_msg.m_databaseid = MyDatabaseId;		
			taboid_msg.m_userid = login_msg.m_loginentry[0];			/* the first entry in the login message is the user id */
			taboid_msg.m_entries = 0;			
			anythingtosend = false;
		}
	}
		
	if (anythingtosend)
	{
		pgstat_setheader(&taboid_msg.m_hdr, PGSTAT_MTYPE_TABOIDSTAT);
		n = taboid_msg.m_entries;
		len = offsetof(PgStat_MsgTabOidStat, m_taboidentry[0]) + n * sizeof(PgStat_TabInfo);
		pgstat_send(&taboid_msg, len);
	}
	
}

/* ----------
 * Ashish
 * pgstat_report_role_stat_fine() -
 *
 *	Called from tcop/postgres.c to send the so far collected user/role's command execution 
 *	statistics to the collector.
 * 
 * ----------
 */

void
pgstat_report_role_stat_fine(bool force, bool only_purge)
{

	PgStat_RoleStatus			*rolestatus;
	PgStat_RoleCmdStatus		*rolecmdstatus;
	PgStat_RoleCmdTabStatus		*rolecmdtabstatus;
	PgStat_RoleCmdTabColStatus	*rolecmdtabcolstatus;

	static TimestampTz	last_report = 0;
	HASH_SEQ_STATUS 	hstat, hstat1, hstat2, hstat3;

	TimestampTz 		now;
	PgStat_MsgRoleStat 	rol_msg;

	bool anythingtosend = false;

	/* check if collector is running or not */
	if (pgStatSock < 0)
		return;

	/* Don't expend a clock check if nothing to do */
	if (MyProcPort->pgstat_user_status == NULL)
		return;

	/* only clear the counts, do not report the stats */
	if (only_purge)
	{
		clearCounts();
		return;
	}

	/*
	 * Don't send a message unless it's been at least PGSTAT_STAT_INTERVAL
	 * msec since we last sent one, or the caller wants to force stats out.
	 *
	 */
	now = GetCurrentTransactionStopTimestamp();
	if (!force &&
		!TimestampDifferenceExceeds(last_report, now, PGSTAT_STAT_INTERVAL))
		return;

	char msg[MSG_SIZE];
	char *to_send_msg = msg;
	memset(msg, 0, MSG_SIZE);
	char *p = to_send_msg;

	int zero = 0;

	int size_so_far = 0;

	// header
	PgStat_MsgHdr hdr;
	hdr.m_type = PGSTAT_MTYPE_ROLESTAT;
	size_so_far += append(&p, &hdr, sizeof(PgStat_MsgHdr), size_so_far, &to_send_msg);

	// database id
	size_so_far += append(&p, &MyDatabaseId, sizeof(Oid),size_so_far, &to_send_msg);

	// user id
	Oid uid = GetUserId();
	size_so_far += append(&p, &uid, sizeof(Oid),size_so_far, &to_send_msg);

	int *const marker_m_roleentries = p;

	size_so_far += append(&p, &zero, sizeof(int), size_so_far, &to_send_msg);

	hash_seq_init(&hstat, MyProcPort->pgstat_user_status->rolStatus);
	while ((rolestatus = (PgStat_RoleStatus *) hash_seq_search(&hstat)) != NULL)
	{
		int role_id = rolestatus->role_id;

		size_so_far += append(&p, &role_id, sizeof(int), size_so_far, &to_send_msg);
		(*marker_m_roleentries)++;

		int *const marker_m_cmdentries = p;
		size_so_far += append(&p, &zero, sizeof(int), size_so_far, &to_send_msg);

		hash_seq_init(&hstat1, rolestatus->cmdStatus);
		while ((rolecmdstatus = (PgStat_RoleCmdStatus *) hash_seq_search(&hstat1)) != NULL)
		{
			if (rolecmdstatus->count == 0)
				continue;

			int command = rolecmdstatus->command;
			int command_count = rolecmdstatus->count;
			anythingtosend = true;

			size_so_far += append(&p, &command, sizeof(int), size_so_far, &to_send_msg);
			size_so_far += append(&p, &command_count, sizeof(int), size_so_far, &to_send_msg);
			(*marker_m_cmdentries)++;

			/* clear the count */
			rolecmdstatus->count = 0;

			int *const marker_m_tabentries = p;
			size_so_far += append(&p, &zero, sizeof(int), size_so_far, &to_send_msg);

			hash_seq_init(&hstat2, rolecmdstatus->tableStatus);
			while ((rolecmdtabstatus = (PgStat_RoleCmdTabStatus *) hash_seq_search(&hstat2)) != NULL)
			{
				if (rolecmdtabstatus->count == 0)
					continue;

				int table = rolecmdtabstatus->table;
				int table_count = rolecmdtabstatus->count;

				size_so_far += append(&p, &table, sizeof(int), size_so_far, &to_send_msg);
				size_so_far += append(&p, &table_count, sizeof(int), size_so_far, &to_send_msg);
				(*marker_m_tabentries)++;

				/* clear the count */
				rolecmdtabstatus->count = 0;

				if (rolecmdstatus->command == CMD_DELETE)
					continue;

				int *const marker_m_colentries = p;
				size_so_far += append(&p, &zero, sizeof(int), size_so_far, &to_send_msg);

				hash_seq_init(&hstat3, rolecmdtabstatus->colStatus);
				while ((rolecmdtabcolstatus = (PgStat_RoleCmdTabColStatus *) hash_seq_search(&hstat3)) != NULL)
				{
					if (rolecmdtabcolstatus->column == 0)
						continue;

					int column = rolecmdtabcolstatus->column;
					int column_count = rolecmdtabcolstatus->count;
					int column_count_0 = rolecmdtabcolstatus->count_0;

					size_so_far += append(&p, &column, sizeof(int), size_so_far, &to_send_msg);
					size_so_far += append(&p, &column_count, sizeof(int), size_so_far, &to_send_msg);
					size_so_far += append(&p, &column_count_0, sizeof(int), size_so_far, &to_send_msg);
					(*marker_m_colentries)++;

					/* clear the counts */
					rolecmdtabcolstatus->count = 0;
					rolecmdtabcolstatus->count_0 = 0;
				}
			}
		}
	}

	if (anythingtosend)
	{
		last_report = now;
		pgstat_send(to_send_msg, size_so_far);
	}
}

/* Ashish */
pgstat_report_role_stat_coarse(bool force, bool only_purge)
{
	
	PgStat_RoleStatus			*rolestatus;
	PgStat_RoleCmdStatus		*rolecmdstatus;
	PgStat_RoleCmdTabStatus		*rolenumtabsstatus;
	PgStat_RoleCmdTabColStatus	*rolenumcolsstatus;
	
	static TimestampTz	last_report = 0;
	HASH_SEQ_STATUS 	hstat, hstat1, hstat2, hstat3;
	int					num_tables, num_columns;
	
	TimestampTz 		now;
	
	bool anythingtosend = false;
	
	int n,len;
	
	int *marker_m_tabentries_new;
	int *marker_m_colentries_new;

	/* check if collector is running or not */
	if (pgStatSock < 0)
		return;

	/* Don't expend a clock check if nothing to do */
	if (MyProcPort->pgstat_user_status == NULL)
		return;

	/* only clear the counts, do not report the stats */
	if (only_purge)
	{
		clearCounts();
		return;
	}
	/*
	 * Don't send a message unless it's been at least PGSTAT_STAT_INTERVAL
	 * msec since we last sent one, or the caller wants to force stats out.
	 * 
	 */
	now = GetCurrentTransactionStopTimestamp();
	if (!force &&
		!TimestampDifferenceExceeds(last_report, now, PGSTAT_STAT_INTERVAL))
		return;	
		
	char msg[MSG_SIZE];
	char *to_send_msg = msg;
	memset(msg, 0, MSG_SIZE);
	char *p = to_send_msg;

	int zero = 0;

	int size_so_far = 0;

	// header
	PgStat_MsgHdr hdr;
	hdr.m_type = PGSTAT_MTYPE_ROLESTAT;
	size_so_far += append(&p, &hdr, sizeof(PgStat_MsgHdr), size_so_far, &to_send_msg);

	// database id
	size_so_far += append(&p, &MyDatabaseId, sizeof(Oid),size_so_far, &to_send_msg);

	// user id
	Oid uid = GetUserId();
	size_so_far += append(&p, &uid, sizeof(Oid),size_so_far, &to_send_msg);

	int *const marker_m_roleentries = p;

	size_so_far += append(&p, &zero, sizeof(int), size_so_far, &to_send_msg);
	
	hash_seq_init(&hstat, MyProcPort->pgstat_user_status->rolStatus);
	while ((rolestatus = (PgStat_RoleStatus *) hash_seq_search(&hstat)) != NULL)
	{
		int role_id = rolestatus->role_id;

		size_so_far += append(&p, &role_id, sizeof(int), size_so_far, &to_send_msg);
		(*marker_m_roleentries)++;

		int *const marker_m_cmdentries = p;
		size_so_far += append(&p, &zero, sizeof(int), size_so_far, &to_send_msg);

		hash_seq_init(&hstat1, rolestatus->cmdStatus);
		while ((rolecmdstatus = (PgStat_RoleCmdStatus *) hash_seq_search(&hstat1)) != NULL)
		{
			if (rolecmdstatus->count == 0)
				continue;
			
			int command = rolecmdstatus->command;
			int command_count = rolecmdstatus->count;
			anythingtosend = true;

			size_so_far += append(&p, &command, sizeof(int), size_so_far, &to_send_msg);
			size_so_far += append(&p, &command_count, sizeof(int), size_so_far, &to_send_msg);
			(*marker_m_cmdentries)++;

			/* clear the count */
			rolecmdstatus->count = 0;

			int *const marker_m_tabentries = p;
			size_so_far += append(&p, &zero, sizeof(int), size_so_far, &to_send_msg);
			marker_m_tabentries_new = marker_m_tabentries;
		}

		hash_seq_init(&hstat2, rolestatus->numTabStatus);
		while ((rolenumtabsstatus = (PgStat_RoleCmdTabStatus *) hash_seq_search(&hstat2)) != NULL)
		{
			if (rolenumtabsstatus->count == 0)
				continue;
			
			int table = rolenumtabsstatus->table;
			int table_count = rolenumtabsstatus->count;

			size_so_far += append(&p, &table, sizeof(int), size_so_far, &to_send_msg);
			size_so_far += append(&p, &table_count, sizeof(int), size_so_far, &to_send_msg);
			(*marker_m_tabentries_new)++;

			/* clear the count */
			rolenumtabsstatus->count = 0;

			int *const marker_m_colentries = p;
			size_so_far += append(&p, &zero, sizeof(int), size_so_far, &to_send_msg);
			marker_m_colentries_new = marker_m_colentries;
		}

		hash_seq_init(&hstat3, rolestatus->numColStatus);
		while ((rolenumcolsstatus = (PgStat_RoleCmdTabColStatus *) hash_seq_search(&hstat3)) != NULL)
		{
			if (rolenumcolsstatus->count == 0)
				continue;
				
			int column = rolenumcolsstatus->column;
			int column_count = rolenumcolsstatus->count;
			int column_count_0 = 0;

			size_so_far += append(&p, &column, sizeof(int), size_so_far, &to_send_msg);
			size_so_far += append(&p, &column_count, sizeof(int), size_so_far, &to_send_msg);
			size_so_far += append(&p, &column_count_0, sizeof(int), size_so_far, &to_send_msg);
			(*marker_m_colentries_new)++;

			/* clear the counts */
			rolenumcolsstatus->count = 0;
		}
	}
	
	if (anythingtosend)
	{
		last_report = now;
		pgstat_send(to_send_msg, size_so_far);
	}
}

/* Ashish */
void
pgstat_report_role_stat_medium(bool force, bool only_purge)
{
	
	PgStat_RoleStatus			*rolestatus;
	PgStat_RoleCmdStatus		*rolecmdstatus;
	PgStat_RoleCmdTabStatus		*rolenumcolstabstatus;
	PgStat_RoleCmdTabColStatus	*rolenumcolsstatus;
	
	static TimestampTz	last_report = 0;
	HASH_SEQ_STATUS 	hstat, hstat1, hstat2, hstat3;
	
	int					num_cols_per_table;
	TimestampTz 		now;
	PgStat_MsgRoleStat 	rol_msg;
	
	bool anythingtosend;
	
	int n,len;

	int *marker_m_tabentries_new;

	/* check if collector is running or not */
	if (pgStatSock < 0)
		return;

	/* Don't expend a clock check if nothing to do */
	if (MyProcPort->pgstat_user_status == NULL)
		return;
 
	/* only clear the counts, do not report the stats */
	if (only_purge)
	{
		clearCounts();
		return;
	}

	/*
	 * Don't send a message unless it's been at least PGSTAT_STAT_INTERVAL
	 * msec since we last sent one, or the caller wants to force stats out.
	 * 
	 */
	now = GetCurrentTransactionStopTimestamp();
	if (!force &&
		!TimestampDifferenceExceeds(last_report, now, PGSTAT_STAT_INTERVAL))
		return;	
		
	char msg[MSG_SIZE];
	char *to_send_msg = msg;
	memset(msg, 0, MSG_SIZE);
	char *p = to_send_msg;

	int zero = 0;

	int size_so_far = 0;

	// header
	PgStat_MsgHdr hdr;
	hdr.m_type = PGSTAT_MTYPE_ROLESTAT;
	size_so_far += append(&p, &hdr, sizeof(PgStat_MsgHdr), size_so_far, &to_send_msg);

	// database id
	size_so_far += append(&p, &MyDatabaseId, sizeof(Oid),size_so_far, &to_send_msg);

	// user id
	Oid uid = GetUserId();
	size_so_far += append(&p, &uid, sizeof(Oid),size_so_far, &to_send_msg);

	int *const marker_m_roleentries = p;

	size_so_far += append(&p, &zero, sizeof(int), size_so_far, &to_send_msg);
	
	hash_seq_init(&hstat, MyProcPort->pgstat_user_status->rolStatus);
	while ((rolestatus = (PgStat_RoleStatus *) hash_seq_search(&hstat)) != NULL)
	{

		int role_id = rolestatus->role_id;

		size_so_far += append(&p, &role_id, sizeof(int), size_so_far, &to_send_msg);
		(*marker_m_roleentries)++;

		int *const marker_m_cmdentries = p;
		size_so_far += append(&p, &zero, sizeof(int), size_so_far, &to_send_msg);

		hash_seq_init(&hstat1, rolestatus->cmdStatus);
		while ((rolecmdstatus = (PgStat_RoleCmdStatus *) hash_seq_search(&hstat1)) != NULL)
		{
			if (rolecmdstatus->count == 0)
				continue;
			
			int command = rolecmdstatus->command;
			int command_count = rolecmdstatus->count;
			anythingtosend = true;

			size_so_far += append(&p, &command, sizeof(int), size_so_far, &to_send_msg);
			size_so_far += append(&p, &command_count, sizeof(int), size_so_far, &to_send_msg);
			(*marker_m_cmdentries)++;

			/* clear the count */
			rolecmdstatus->count = 0;

			int *const marker_m_tabentries = p;
			size_so_far += append(&p, &zero, sizeof(int), size_so_far, &to_send_msg);

			marker_m_tabentries_new = marker_m_tabentries;

			anythingtosend = true;

		}

		hash_seq_init(&hstat2, rolestatus->numColTabStatus);
		while ((rolenumcolstabstatus = (PgStat_RoleCmdTabStatus *) hash_seq_search(&hstat2)) != NULL)
		{
			int table = rolenumcolstabstatus->table;
			int table_count = 0;

			size_so_far += append(&p, &table, sizeof(int), size_so_far, &to_send_msg);
			size_so_far += append(&p, &table_count, sizeof(int), size_so_far, &to_send_msg);
			(*marker_m_tabentries_new)++;

			int *const marker_m_colentries = p;
			size_so_far += append(&p, &zero, sizeof(int), size_so_far, &to_send_msg);

			hash_seq_init(&hstat3, rolenumcolstabstatus->colStatus);
			while ((rolenumcolsstatus = (PgStat_RoleCmdTabColStatus *) hash_seq_search(&hstat3)) != NULL)
			{						
				if (rolenumcolsstatus->column == 0 ||
						rolenumcolsstatus->count == 0)
					continue;

				int column = rolenumcolsstatus->column;
				int column_count = rolenumcolsstatus->count;
				int column_count_0 = 0;

				size_so_far += append(&p, &column, sizeof(int), size_so_far, &to_send_msg);
				size_so_far += append(&p, &column_count, sizeof(int), size_so_far, &to_send_msg);
				size_so_far += append(&p, &column_count_0, sizeof(int), size_so_far, &to_send_msg);
				(*marker_m_colentries)++;


				/* clear the count */
				rolenumcolsstatus->count = 0;								
			}			
		}		
	}
		
	if (anythingtosend)
	{
		last_report = now;
		pgstat_send(to_send_msg, size_so_far);
	}
}

/* ----------
 * pgstat_report_tabstat() -
 *
 *	Called from tcop/postgres.c to send the so far collected per-table
 *	access statistics to the collector.  Note that this is called only
 *	when not within a transaction, so it is fair to use transaction stop
 *	time as an approximation of current time.
 * ----------
 */
void
pgstat_report_tabstat(bool force)
{
	/* we assume this inits to all zeroes: */
	static const PgStat_TableCounts all_zeroes;
	static TimestampTz last_report = 0;

	TimestampTz now;
	PgStat_MsgTabstat regular_msg;
	PgStat_MsgTabstat shared_msg;
	TabStatusArray *tsa;
	int			i;

	/* Don't expend a clock check if nothing to do */
	if (pgStatTabList == NULL ||
		pgStatTabList->tsa_used == 0)
		return;

	/*
	 * Don't send a message unless it's been at least PGSTAT_STAT_INTERVAL
	 * msec since we last sent one, or the caller wants to force stats out.
	 */
	now = GetCurrentTransactionStopTimestamp();
	if (!force &&
		!TimestampDifferenceExceeds(last_report, now, PGSTAT_STAT_INTERVAL))
		return;
	last_report = now;

	/*
	 * Scan through the TabStatusArray struct(s) to find tables that actually
	 * have counts, and build messages to send.  We have to separate shared
	 * relations from regular ones because the databaseid field in the message
	 * header has to depend on that.
	 */
	regular_msg.m_databaseid = MyDatabaseId;
	shared_msg.m_databaseid = InvalidOid;
	regular_msg.m_nentries = 0;
	shared_msg.m_nentries = 0;

	for (tsa = pgStatTabList; tsa != NULL; tsa = tsa->tsa_next)
	{
		for (i = 0; i < tsa->tsa_used; i++)
		{
			PgStat_TableStatus *entry = &tsa->tsa_entries[i];
			PgStat_MsgTabstat *this_msg;
			PgStat_TableEntry *this_ent;

			/* Shouldn't have any pending transaction-dependent counts */
			Assert(entry->trans == NULL);

			/*
			 * Ignore entries that didn't accumulate any actual counts, such
			 * as indexes that were opened by the planner but not used.
			 */
			if (memcmp(&entry->t_counts, &all_zeroes,
					   sizeof(PgStat_TableCounts)) == 0)
				continue;

			/*
			 * OK, insert data into the appropriate message, and send if full.
			 */
			this_msg = entry->t_shared ? &shared_msg : &regular_msg;
			this_ent = &this_msg->m_entry[this_msg->m_nentries];
			this_ent->t_id = entry->t_id;
			memcpy(&this_ent->t_counts, &entry->t_counts,
				   sizeof(PgStat_TableCounts));
			if (++this_msg->m_nentries >= PGSTAT_NUM_TABENTRIES)
			{
				pgstat_send_tabstat(this_msg);
				this_msg->m_nentries = 0;
			}
		}
		/* zero out TableStatus structs after use */
		MemSet(tsa->tsa_entries, 0,
			   tsa->tsa_used * sizeof(PgStat_TableStatus));
		tsa->tsa_used = 0;
	}

	/*
	 * Send partial messages.  If force is true, make sure that any pending
	 * xact commit/abort gets counted, even if no table stats to send.
	 */
	if (regular_msg.m_nentries > 0 ||
		(force && (pgStatXactCommit > 0 || pgStatXactRollback > 0)))
		pgstat_send_tabstat(&regular_msg);
	if (shared_msg.m_nentries > 0)
		pgstat_send_tabstat(&shared_msg);
}

/*
 * Subroutine for pgstat_report_tabstat: finish and send a tabstat message
 */
static void
pgstat_send_tabstat(PgStat_MsgTabstat *tsmsg)
{
	int			n;
	int			len;

	/* It's unlikely we'd get here with no socket, but maybe not impossible */
	if (pgStatSock < 0)
		return;

	/*
	 * Report accumulated xact commit/rollback whenever we send a normal
	 * tabstat message
	 */
	if (OidIsValid(tsmsg->m_databaseid))
	{
		tsmsg->m_xact_commit = pgStatXactCommit;
		tsmsg->m_xact_rollback = pgStatXactRollback;
		pgStatXactCommit = 0;
		pgStatXactRollback = 0;
	}
	else
	{
		tsmsg->m_xact_commit = 0;
		tsmsg->m_xact_rollback = 0;
	}

	n = tsmsg->m_nentries;
	len = offsetof(PgStat_MsgTabstat, m_entry[0]) +
		n * sizeof(PgStat_TableEntry);

	pgstat_setheader(&tsmsg->m_hdr, PGSTAT_MTYPE_TABSTAT);
	pgstat_send(tsmsg, len);
}


/* ----------
 * pgstat_vacuum_tabstat() -
 *
 *	Will tell the collector about objects he can get rid of.
 * ----------
 */
void
pgstat_vacuum_tabstat(void)
{
	HTAB	   *htab;
	PgStat_MsgTabpurge msg;
	HASH_SEQ_STATUS hstat;
	PgStat_StatDBEntry *dbentry;
	PgStat_StatTabEntry *tabentry;
	int			len;

	if (pgStatSock < 0)
		return;

	/*
	 * If not done for this transaction, read the statistics collector stats
	 * file into some hash tables.
	 */
	backend_read_statsfile();

	/*
	 * Read pg_database and make a list of OIDs of all existing databases
	 */
	htab = pgstat_collect_oids(DatabaseRelationId);

	/*
	 * Search the database hash table for dead databases and tell the
	 * collector to drop them.
	 */
	hash_seq_init(&hstat, pgStatDBHash);
	while ((dbentry = (PgStat_StatDBEntry *) hash_seq_search(&hstat)) != NULL)
	{
		Oid			dbid = dbentry->databaseid;

		CHECK_FOR_INTERRUPTS();

		/* the DB entry for shared tables (with InvalidOid) is never dropped */
		if (OidIsValid(dbid) &&
			hash_search(htab, (void *) &dbid, HASH_FIND, NULL) == NULL)
			pgstat_drop_database(dbid);
	}

	/* Clean up */
	hash_destroy(htab);

	/*
	 * Lookup our own database entry; if not found, nothing more to do.
	 */
	dbentry = (PgStat_StatDBEntry *) hash_search(pgStatDBHash,
												 (void *) &MyDatabaseId,
												 HASH_FIND, NULL);
	if (dbentry == NULL || dbentry->tables == NULL)
		return;

	/*
	 * Similarly to above, make a list of all known relations in this DB.
	 */
	htab = pgstat_collect_oids(RelationRelationId);

	/*
	 * Initialize our messages table counter to zero
	 */
	msg.m_nentries = 0;

	/*
	 * Check for all tables listed in stats hashtable if they still exist.
	 */
	hash_seq_init(&hstat, dbentry->tables);
	while ((tabentry = (PgStat_StatTabEntry *) hash_seq_search(&hstat)) != NULL)
	{
		Oid			tabid = tabentry->tableid;

		CHECK_FOR_INTERRUPTS();

		if (hash_search(htab, (void *) &tabid, HASH_FIND, NULL) != NULL)
			continue;

		/*
		 * Not there, so add this table's Oid to the message
		 */
		msg.m_tableid[msg.m_nentries++] = tabid;

		/*
		 * If the message is full, send it out and reinitialize to empty
		 */
		if (msg.m_nentries >= PGSTAT_NUM_TABPURGE)
		{
			len = offsetof(PgStat_MsgTabpurge, m_tableid[0])
				+msg.m_nentries * sizeof(Oid);

			pgstat_setheader(&msg.m_hdr, PGSTAT_MTYPE_TABPURGE);
			msg.m_databaseid = MyDatabaseId;
			pgstat_send(&msg, len);

			msg.m_nentries = 0;
		}
	}

	/*
	 * Send the rest
	 */
	if (msg.m_nentries > 0)
	{
		len = offsetof(PgStat_MsgTabpurge, m_tableid[0])
			+msg.m_nentries * sizeof(Oid);

		pgstat_setheader(&msg.m_hdr, PGSTAT_MTYPE_TABPURGE);
		msg.m_databaseid = MyDatabaseId;
		pgstat_send(&msg, len);
	}

	/* Clean up */
	hash_destroy(htab);
}


/* ----------
 * pgstat_collect_oids() -
 *
 *	Collect the OIDs of either all databases or all tables, according to
 *	the parameter, into a temporary hash table.  Caller should hash_destroy
 *	the result when done with it.
 * ----------
 */
static HTAB *
pgstat_collect_oids(Oid catalogid)
{
	HTAB	   *htab;
	HASHCTL		hash_ctl;
	Relation	rel;
	HeapScanDesc scan;
	HeapTuple	tup;

	memset(&hash_ctl, 0, sizeof(hash_ctl));
	hash_ctl.keysize = sizeof(Oid);
	hash_ctl.entrysize = sizeof(Oid);
	hash_ctl.hash = oid_hash;
	htab = hash_create("Temporary table of OIDs",
					   PGSTAT_TAB_HASH_SIZE,
					   &hash_ctl,
					   HASH_ELEM | HASH_FUNCTION);

	rel = heap_open(catalogid, AccessShareLock);
	scan = heap_beginscan(rel, SnapshotNow, 0, NULL);
	while ((tup = heap_getnext(scan, ForwardScanDirection)) != NULL)
	{
		Oid			thisoid = HeapTupleGetOid(tup);

		CHECK_FOR_INTERRUPTS();

		(void) hash_search(htab, (void *) &thisoid, HASH_ENTER, NULL);
	}
	heap_endscan(scan);
	heap_close(rel, AccessShareLock);

	return htab;
}


/* ----------
 * pgstat_drop_database() -
 *
 *	Tell the collector that we just dropped a database.
 *	(If the message gets lost, we will still clean the dead DB eventually
 *	via future invocations of pgstat_vacuum_tabstat().)
 * ----------
 */
void
pgstat_drop_database(Oid databaseid)
{
	PgStat_MsgDropdb msg;

	if (pgStatSock < 0)
		return;

	pgstat_setheader(&msg.m_hdr, PGSTAT_MTYPE_DROPDB);
	msg.m_databaseid = databaseid;
	pgstat_send(&msg, sizeof(msg));
}


/* ----------
 * pgstat_drop_relation() -
 *
 *	Tell the collector that we just dropped a relation.
 *	(If the message gets lost, we will still clean the dead entry eventually
 *	via future invocations of pgstat_vacuum_tabstat().)
 *
 *	Currently not used for lack of any good place to call it; we rely
 *	entirely on pgstat_vacuum_tabstat() to clean out stats for dead rels.
 * ----------
 */
#ifdef NOT_USED
void
pgstat_drop_relation(Oid relid)
{
	PgStat_MsgTabpurge msg;
	int			len;

	if (pgStatSock < 0)
		return;

	msg.m_tableid[0] = relid;
	msg.m_nentries = 1;

	len = offsetof(PgStat_MsgTabpurge, m_tableid[0]) +sizeof(Oid);

	pgstat_setheader(&msg.m_hdr, PGSTAT_MTYPE_TABPURGE);
	msg.m_databaseid = MyDatabaseId;
	pgstat_send(&msg, len);
}
#endif   /* NOT_USED */


/* ----------
 * pgstat_drop_user() -
 *
 *	Tell the collector that we just dropped a user.
 *  not called anywhere as of 17 Feb 2009 
 *
 */
void
pgstat_drop_user(Oid userid)
{
/*
	PgStat_MsgUserpurge msg;

	msg.m_databaseid = MyDatabaseId;
	msg.m_userid = userid; 

	pgstat_setheader(&msg.m_hdr, PGSTAT_MTYPE_USERPURGE);
	
	pgstat_send(&msg, sizeof(msg));
*/
}

/* ----------
 * pgstat_reset_counters() 
 *
 *	Tell the statistics collector to reset counters for our database.
 * ----------
 */
void
pgstat_reset_counters(void)
{
	PgStat_MsgResetcounter msg;

	if (pgStatSock < 0)
		return;

	if (!superuser())
		ereport(ERROR,
				(errcode(ERRCODE_INSUFFICIENT_PRIVILEGE),
				 errmsg("must be superuser to reset statistics counters")));

	pgstat_setheader(&msg.m_hdr, PGSTAT_MTYPE_RESETCOUNTER);
	msg.m_databaseid = MyDatabaseId;
	pgstat_send(&msg, sizeof(msg));
}


/* ----------
 * pgstat_report_autovac() -
 *
 *	Called from autovacuum.c to report startup of an autovacuum process.
 *	We are called before InitPostgres is done, so can't rely on MyDatabaseId;
 *	the db OID must be passed in, instead.
 * ----------
 */
void
pgstat_report_autovac(Oid dboid)
{
	PgStat_MsgAutovacStart msg;

	if (pgStatSock < 0)
		return;

	pgstat_setheader(&msg.m_hdr, PGSTAT_MTYPE_AUTOVAC_START);
	msg.m_databaseid = dboid;
	msg.m_start_time = GetCurrentTimestamp();

	pgstat_send(&msg, sizeof(msg));
}


/* ---------
 * pgstat_report_vacuum() -
 *
 *	Tell the collector about the table we just vacuumed.
 * ---------
 */
void
pgstat_report_vacuum(Oid tableoid, bool shared,
					 bool analyze, PgStat_Counter tuples)
{
	PgStat_MsgVacuum msg;

	if (pgStatSock < 0 || !pgstat_track_counts)
		return;

	pgstat_setheader(&msg.m_hdr, PGSTAT_MTYPE_VACUUM);
	msg.m_databaseid = shared ? InvalidOid : MyDatabaseId;
	msg.m_tableoid = tableoid;
	msg.m_analyze = analyze;
	msg.m_autovacuum = IsAutoVacuumWorkerProcess();		/* is this autovacuum? */
	msg.m_vacuumtime = GetCurrentTimestamp();
	msg.m_tuples = tuples;
	pgstat_send(&msg, sizeof(msg));
}

/* --------
 * pgstat_report_analyze() -
 *
 *	Tell the collector about the table we just analyzed.
 * --------
 */
void
pgstat_report_analyze(Oid tableoid, bool shared, PgStat_Counter livetuples,
					  PgStat_Counter deadtuples)
{
	PgStat_MsgAnalyze msg;

	if (pgStatSock < 0 || !pgstat_track_counts)
		return;

	pgstat_setheader(&msg.m_hdr, PGSTAT_MTYPE_ANALYZE);
	msg.m_databaseid = shared ? InvalidOid : MyDatabaseId;
	msg.m_tableoid = tableoid;
	msg.m_autovacuum = IsAutoVacuumWorkerProcess();		/* is this autovacuum? */
	msg.m_analyzetime = GetCurrentTimestamp();
	msg.m_live_tuples = livetuples;
	msg.m_dead_tuples = deadtuples;
	pgstat_send(&msg, sizeof(msg));
}


/* ----------
 * pgstat_ping() -
 *
 *	Send some junk data to the collector to increase traffic.
 * ----------
 */
void
pgstat_ping(void)
{
	PgStat_MsgDummy msg;

	if (pgStatSock < 0)
		return;

	pgstat_setheader(&msg.m_hdr, PGSTAT_MTYPE_DUMMY);
	pgstat_send(&msg, sizeof(msg));
}

/* ----------
 * pgstat_initstats() -
 *
 *	Initialize a relcache entry to count access statistics.
 *	Called whenever a relation is opened.
 *
 *	We assume that a relcache entry's pgstat_info field is zeroed by
 *	relcache.c when the relcache entry is made; thereafter it is long-lived
 *	data.  We can avoid repeated searches of the TabStatus arrays when the
 *	same relation is touched repeatedly within a transaction.
 * ----------
 */
void
pgstat_initstats(Relation rel)
{
	Oid			rel_id = rel->rd_id;
	char		relkind = rel->rd_rel->relkind;

	/* We only count stats for things that have storage */
	if (!(relkind == RELKIND_RELATION ||
		  relkind == RELKIND_INDEX ||
		  relkind == RELKIND_TOASTVALUE))
	{
		rel->pgstat_info = NULL;
		return;
	}

	if (pgStatSock < 0 || !pgstat_track_counts)
	{
		/* We're not counting at all */
		rel->pgstat_info = NULL;
		return;
	}

	/*
	 * If we already set up this relation in the current transaction, nothing
	 * to do.
	 */
	if (rel->pgstat_info != NULL &&
		rel->pgstat_info->t_id == rel_id)
		return;

	/* Else find or make the PgStat_TableStatus entry, and update link */
	rel->pgstat_info = get_tabstat_entry(rel_id, rel->rd_rel->relisshared);
}

/* Ashish: called when the login stats are reported ..  
 * 
 * currently dont take care of the case when the user is changed within a session 
 * 
 * */

void 
pgstat_initUserRoles(List *activated_roles)
{
	HASHCTL	hash_ctl;	
	PgStat_RoleStatus  		*rolestatus;
	PgStat_RoleCmdStatus   *rolecmdstatus;
	ListCell		   *role_cell;
	bool			found;
	int				count;
	
	if (pgStatSock < 0 || !pgstat_track_counts || !IsUnderPostmaster || !MyProcPort)
	{
		return;
	}	
	
	Assert(MyProcPort->pgstat_user_status == NULL);
	
	MemoryContext old_ctxt = MemoryContextSwitchTo(TopMemoryContext);
	MyProcPort->pgstat_user_status = (PgStat_RoleStatus *)palloc(sizeof(PgStat_RoleStatus));
	MemoryContextSwitchTo(old_ctxt);
							
	count = 0;
	foreach(role_cell, activated_roles)
	{
		Oid role_id = lfirst_oid(role_cell);
		
		if (count == 0)
		{			
			MyProcPort->pgstat_user_status->role_id = role_id;		
			
			/* not storing any command stats for the user */
			MyProcPort->pgstat_user_status->cmdStatus = NULL;
			MyProcPort->pgstat_user_status->numTabStatus = NULL;
			MyProcPort->pgstat_user_status->numColStatus = NULL;
			MyProcPort->pgstat_user_status->numColTabStatus = NULL;
			
			/* create the hashtable for roles of this user */
			memset(&hash_ctl, 0, sizeof(hash_ctl));
			hash_ctl.keysize = sizeof(Oid);
			hash_ctl.entrysize = sizeof(PgStat_RoleStatus);
			hash_ctl.hash = oid_hash;
			MyProcPort->pgstat_user_status->rolStatus = hash_create("Per-database per-user per-role status",
														PGSTAT_ROLE_HASH_SIZE,
														&hash_ctl,
														HASH_ELEM | HASH_FUNCTION);	

			
		} else {
		
			rolestatus = (PgStat_RoleStatus *)hash_search(MyProcPort->pgstat_user_status->rolStatus, 
														  (void *) &role_id, 
														  HASH_ENTER, 
														  &found);
			
			if (!found)
			{
				rolestatus->role_id = role_id;
				rolestatus->rolStatus = NULL;
				rolestatus->numTabStatus = NULL;
				rolestatus->numColStatus = NULL;
				rolestatus->numColTabStatus = NULL;
				
				/* create the hashtable for commands for this role */
				memset(&hash_ctl, 0, sizeof(hash_ctl));
				hash_ctl.keysize = sizeof(Oid);
				hash_ctl.entrysize = sizeof(PgStat_RoleCmdStatus);
				hash_ctl.hash = oid_hash;
				rolestatus->cmdStatus = hash_create("Per-database per-user per-role per-command status",
										 PGSTAT_ROLE_CMD_HASH_SIZE,
										 &hash_ctl,
										 HASH_ELEM | HASH_FUNCTION);				
				
				if (IDR_LEVEL == 0)
				{
					/* numTabStatus hashtable for storing num tabs count */
					memset(&hash_ctl, 0, sizeof(hash_ctl));
					hash_ctl.keysize = sizeof(Oid);
					hash_ctl.entrysize = sizeof(PgStat_RoleCmdTabStatus); /* num tabs count */
					hash_ctl.hash = oid_hash;
					rolestatus->numTabStatus = hash_create("Per-database per-user per-role num tabs status",
															PGSTAT_ROLE_CMD_TAB_HASH_SIZE,
															&hash_ctl,
															HASH_ELEM | HASH_FUNCTION);				
					
					/* numTabStatus hashtable for storing num cols count */
					memset(&hash_ctl, 0, sizeof(hash_ctl));
					hash_ctl.keysize = sizeof(Oid);
					hash_ctl.entrysize = sizeof(PgStat_RoleCmdTabColStatus); /* num cols count */
					hash_ctl.hash = oid_hash;
					rolestatus->numColStatus = hash_create("Per-database per-user per-role num cols status",
														PGSTAT_ROLE_CMD_TAB_COL_HASH_SIZE,
														&hash_ctl,
														HASH_ELEM | HASH_FUNCTION);																			
				}
				
				if (IDR_LEVEL == 1)
				{
					/* numColTabStatus hashtable for storing num columns per table count */
					memset(&hash_ctl, 0, sizeof(hash_ctl));
					hash_ctl.keysize = sizeof(Oid);
					hash_ctl.entrysize = sizeof(PgStat_RoleCmdTabStatus); /* num tabs count */
					hash_ctl.hash = oid_hash;
					rolestatus->numColTabStatus = hash_create("Per-database per-user per-role num cols per tab status",
															PGSTAT_ROLE_CMD_TAB_HASH_SIZE,
															&hash_ctl,
															HASH_ELEM | HASH_FUNCTION);				
					
				}
			}
		}
		
		count++;
	}	
}		

/* Ashish: called at the end of every command processing in postgres.c */
void 
pgstat_incRoleCmdCounts()
{
	HASHCTL	hash_ctl;	
	PgStat_RoleStatus 		*rolestatus;
	PgStat_RoleCmdStatus 	*rolecmdstatus;
	HASH_SEQ_STATUS			hstat1, hstat2;
	
	if (pgStatSock < 0 || !pgstat_track_counts || !IsUnderPostmaster || !MyProcPort)
	{
		return;
	}	
	
	Assert(MyProcPort->pgstat_user_status != NULL);
			
	hash_seq_init(&hstat1, MyProcPort->pgstat_user_status->rolStatus);
	while ((rolestatus = (PgStat_RoleStatus *) hash_seq_search(&hstat1)) != NULL)
	{
		hash_seq_init(&hstat2, rolestatus->cmdStatus);
		while ((rolecmdstatus = (PgStat_RoleCmdStatus *) hash_seq_search(&hstat2)) != NULL)
		{
			if (rolecmdstatus->to_inc)
			{
				rolecmdstatus->count++;
				rolecmdstatus->to_inc = false;
			}
		}		
	}
}		

/* Ashish: called at the end of every command processing in postgres.c for coarse triplets */
void 
pgstat_setNumTabNumColCounts()
{
	HASHCTL	hash_ctl;	
	PgStat_RoleStatus 				*rolestatus;
	PgStat_RoleCmdStatus 			*rolecmdstatus;
	PgStat_RoleCmdTabStatus 		*rolecmdtabstatus, *rolenumtabsstatus;
	PgStat_RoleCmdTabColStatus 		*rolecmdtabcolstatus, *rolenumcolsstatus;
	uint32							num_tabs, num_cols;
	HASH_SEQ_STATUS					hstat1, hstat2, hstat3, hstat4;
	bool							found;
	
	if (pgStatSock < 0 || !pgstat_track_counts || !IsUnderPostmaster || !MyProcPort)
	{
		return;
	}	
	
	Assert(MyProcPort->pgstat_user_status != NULL);
			
	hash_seq_init(&hstat1, MyProcPort->pgstat_user_status->rolStatus);
	while ((rolestatus = (PgStat_RoleStatus *) hash_seq_search(&hstat1)) != NULL)
	{
		num_tabs = 0;
		num_cols = 0;
		hash_seq_init(&hstat2, rolestatus->cmdStatus);
		while ((rolecmdstatus = (PgStat_RoleCmdStatus *) hash_seq_search(&hstat2)) != NULL)
		{
			hash_seq_init(&hstat3, rolecmdstatus->tableStatus);
			while ((rolecmdtabstatus = (PgStat_RoleCmdTabStatus *) hash_seq_search(&hstat3)) != NULL)
			{
				num_tabs += rolecmdtabstatus->count;
				
				/* clear the count */
				rolecmdtabstatus->count = 0;
				
				hash_seq_init(&hstat4, rolecmdtabstatus->colStatus);
				while ((rolecmdtabcolstatus = (PgStat_RoleCmdTabColStatus *) hash_seq_search(&hstat4)) != NULL)
				{
					num_cols += rolecmdtabcolstatus->count;
					
					/* clear the count for next query */
					rolecmdtabcolstatus->count = 0;
				}				
			}			
		}
		
		/* for every role put the counts in the num tabs and num cols hash table */		
		Assert(rolestatus->numTabStatus != NULL);
		Assert(rolestatus->numColStatus != NULL);
		
		/* for num tabs */
		rolenumtabsstatus = (PgStat_RoleCmdTabStatus *)hash_search(rolestatus->numTabStatus, 
												 					(void *) &num_tabs, 
												 					HASH_ENTER, 
												 					&found);
		if (!found)		
		{
			rolenumtabsstatus->table = num_tabs; /* key */
			rolenumtabsstatus->colStatus = NULL;
			rolenumtabsstatus->count = 0;			
		}

		rolenumtabsstatus->count++;		
		
		/* for num cols */
		rolenumcolsstatus = (PgStat_RoleCmdTabColStatus *)hash_search(rolestatus->numColStatus, 
												 (void *) &num_cols, 
												  HASH_ENTER, 
												  &found);
		if (!found)		
		{
			rolenumcolsstatus->column = num_cols; /* key */
			rolenumcolsstatus->count = 0;
		}

		rolenumcolsstatus->count++;				
	}
}		


/* Ashish: called at the end of every command processing in postgres.c for medium triplets */
void 
pgstat_setNumColsPerTabCounts()
{
	HASHCTL	hash_ctl;	
	PgStat_RoleStatus 				*rolestatus;
	PgStat_RoleCmdStatus 			*rolecmdstatus;
	PgStat_RoleCmdTabStatus 		*rolecmdtabstatus, *rolenumcolstabstatus, *p1;
	PgStat_RoleCmdTabColStatus 		*rolecmdtabcolstatus, *rolenumcolsstatus;
	HASH_SEQ_STATUS					hstat1, hstat2, hstat3, hstat4, hstat5;
	bool							found;
	
	if (pgStatSock < 0 || !pgstat_track_counts || !IsUnderPostmaster || !MyProcPort)
	{
		return;
	}	
	
	Assert(MyProcPort->pgstat_user_status != NULL);
			
	hash_seq_init(&hstat1, MyProcPort->pgstat_user_status->rolStatus);
	while ((rolestatus = (PgStat_RoleStatus *) hash_seq_search(&hstat1)) != NULL)
	{		
		hash_seq_init(&hstat2, rolestatus->cmdStatus);
		while ((rolecmdstatus = (PgStat_RoleCmdStatus *) hash_seq_search(&hstat2)) != NULL)
		{
			hash_seq_init(&hstat3, rolecmdstatus->tableStatus);
			while ((rolecmdtabstatus = (PgStat_RoleCmdTabStatus *) hash_seq_search(&hstat3)) != NULL)
			{					
				rolenumcolstabstatus = (PgStat_RoleCmdTabStatus *)hash_search(rolestatus->numColTabStatus, 
														 					(void *) &(rolecmdtabstatus->table), 
														 					HASH_ENTER, 
														 					&found);

				if (!found)
				{
					rolenumcolstabstatus->table = rolecmdtabstatus->table;
					rolenumcolstabstatus->count = 0; 

					memset(&hash_ctl, 0, sizeof(hash_ctl));
					hash_ctl.keysize = sizeof(Oid);
					hash_ctl.entrysize = sizeof(PgStat_RoleCmdTabColStatus); /* num tabs count */
					hash_ctl.hash = oid_hash;
					rolenumcolstabstatus->colStatus = hash_create("Per-database per-user per-role num cols per tab status",
															PGSTAT_ROLE_CMD_TAB_COL_HASH_SIZE,
															&hash_ctl,
															HASH_ELEM | HASH_FUNCTION);				

				}
				
				hash_seq_init(&hstat4, rolecmdtabstatus->colStatus);
				while ((rolecmdtabcolstatus = (PgStat_RoleCmdTabColStatus *) hash_seq_search(&hstat4)) != NULL)
				{
					if (rolecmdtabcolstatus->count == 0)
						continue;
					
					rolenumcolstabstatus->count += rolecmdtabcolstatus->count;
					
					/* clear the count for next query */
					rolecmdtabcolstatus->count = 0;
				}				
			}			
		}
		
		/* now we have number of columns accessed per table for each role in rolenumcolstabstatus */
		hash_seq_init(&hstat5, rolestatus->numColTabStatus);
		while ((rolenumcolstabstatus = (PgStat_RoleCmdTabStatus *)hash_seq_search(&hstat5)) != NULL)
		{
			rolenumcolsstatus = (PgStat_RoleCmdTabColStatus *)hash_search(rolenumcolstabstatus->colStatus, 
													 					(void *) &(rolenumcolstabstatus->count), 
													 					HASH_ENTER, 
													 					&found);

			if (!found)
			{
				rolenumcolsstatus->column = rolenumcolstabstatus->count;
				rolenumcolsstatus->count = 0;				
			}
			
			rolenumcolsstatus->count++;
			rolenumcolstabstatus->count = 0;
		}		
	}
}		

/* Ashish: called from the access contrl check code in acl.c */
void 
pgstat_setRoleCmdTabCounts(PgStat_RoleStatus *rolestatus, CmdType operation, uint32 table)
{	
	HASHCTL	hash_ctl;	
	bool found;
	PgStat_RoleCmdStatus 		*rolecmdstatus = NULL;
	PgStat_RoleCmdTabStatus 	*rolecmdtabstatus = NULL;
	
	if (pgStatSock < 0 || !pgstat_track_counts || !IsUnderPostmaster || !MyProcPort)
	{
		return;
	}	
	
	Assert(MyProcPort->pgstat_user_status != NULL);
	
	rolecmdstatus = (PgStat_RoleCmdStatus *)hash_search(rolestatus->cmdStatus, 
												  (void *)&operation, 
												  HASH_ENTER, 
												  &found);

	if (!rolecmdstatus)
		return;

	if (!found)
	{
		rolecmdstatus->command = operation;
		rolecmdstatus->to_inc = false;
		
		memset(&hash_ctl, 0, sizeof(hash_ctl));
		hash_ctl.keysize = sizeof(Oid);
		hash_ctl.entrysize = sizeof(PgStat_RoleCmdTabStatus);
		hash_ctl.hash = oid_hash;
		rolecmdstatus->tableStatus = hash_create("Per-database per-role per-command per-table status",
												PGSTAT_ROLE_CMD_TAB_HASH_SIZE,
												&hash_ctl,
												HASH_ELEM | HASH_FUNCTION);																
	}
	
	/* very important */
	if (!rolecmdstatus->to_inc)
		rolecmdstatus->to_inc = true;
		
	/* check for the input table_id, if not create new entry in the hash table */
	rolecmdtabstatus = (PgStat_RoleCmdTabStatus *)hash_search(rolecmdstatus->tableStatus, 
												  (void *) &table, 
												  HASH_ENTER, 
												  &found);
	
	if (!rolecmdtabstatus)
		return;
	
	/* initialize new command table hash table entry */
	if (!found)
	{
		rolecmdtabstatus->table = table;
		rolecmdtabstatus->count = 0; 
		
		memset(&hash_ctl, 0, sizeof(hash_ctl));
		hash_ctl.keysize = sizeof(Oid);
		hash_ctl.entrysize = sizeof(PgStat_RoleCmdTabColStatus);
		hash_ctl.hash = oid_hash;
		rolecmdtabstatus->colStatus = hash_create("Per-database per-role per-command per-table per-column status",
												PGSTAT_ROLE_CMD_TAB_COL_HASH_SIZE,
												&hash_ctl,
												HASH_ELEM | HASH_FUNCTION);																
		
	}
	
	/* increment the count for the table */
	rolecmdtabstatus->count++; 		
}

/* Ashish: called from the access control check code in acl.c */
void 
pgstat_setRoleCmdTabColCounts(PgStat_RoleStatus *rolestatus, CmdType operation, uint32 table, List *col_list)
{
	HASHCTL	hash_ctl;	
	bool found;
	PgStat_RoleCmdStatus 		*rolecmdstatus = NULL;
	PgStat_RoleCmdTabStatus 	*rolecmdtabstatus = NULL;
	PgStat_RoleCmdTabColStatus 	*rolecmdtabcolstatus = NULL;
	ListCell					*col_cell;
	int 						col, column_0;
	
	if (pgStatSock < 0 || !pgstat_track_counts || !IsUnderPostmaster || !MyProcPort)
	{
		return;
	}	
	
	Assert(MyProcPort->pgstat_user_status != NULL);
	
	rolecmdstatus = (PgStat_RoleCmdStatus *)hash_search(rolestatus->cmdStatus, 
												  (void *)&operation, 
												  HASH_ENTER, 
												  &found);

	if (!rolecmdstatus)
		return;

	if (!found)
	{
		rolecmdstatus->command = operation;
		rolecmdstatus->to_inc = false;
		
		memset(&hash_ctl, 0, sizeof(hash_ctl));
		hash_ctl.keysize = sizeof(Oid);
		hash_ctl.entrysize = sizeof(PgStat_RoleCmdTabStatus);
		hash_ctl.hash = oid_hash;
		rolecmdstatus->tableStatus = hash_create("Per-database per-role per-command per-table status ",
												PGSTAT_ROLE_CMD_TAB_HASH_SIZE,
												&hash_ctl,
												HASH_ELEM | HASH_FUNCTION);																
	}
	
	/* very important */
	if (!rolecmdstatus->to_inc)
		rolecmdstatus->to_inc = true;
		
	/* check for the input table_id, if not create new entry in the hash table */
	rolecmdtabstatus = (PgStat_RoleCmdTabStatus *)hash_search(rolecmdstatus->tableStatus, 
												  (void *) &table, 
												  HASH_ENTER, 
												  &found);
	
	if (!rolecmdtabstatus)
		return;
	
	/* initialize new command table hash table entry */
	if (!found)
	{
		rolecmdtabstatus->table = table;
		rolecmdtabstatus->count = 0;

		memset(&hash_ctl, 0, sizeof(hash_ctl));
		hash_ctl.keysize = sizeof(Oid);
		hash_ctl.entrysize = sizeof(PgStat_RoleCmdTabColStatus);
		hash_ctl.hash = oid_hash;
		rolecmdtabstatus->colStatus = hash_create("Per-database per-role per-command per-table per-column status",
												PGSTAT_ROLE_CMD_TAB_COL_HASH_SIZE,
												&hash_ctl,
												HASH_ELEM | HASH_FUNCTION);																

		
	}
	
	/* take care of the columns */
	foreach(col_cell, col_list)
	{
		col = lfirst_int(col_cell);
		
		rolecmdtabcolstatus = (PgStat_RoleCmdTabColStatus *)hash_search(rolecmdtabstatus->colStatus, 
													  (void *) &col, 
													  HASH_ENTER, 
													  &found);
		
		if (!found)
		{
			rolecmdtabcolstatus->column = col;
			rolecmdtabcolstatus->count = 0;
			rolecmdtabcolstatus->count_0 = 0;
		}
		
		rolecmdtabcolstatus->count++;			
	}
	
	/* for the rest of the columns, increment the 0 count */
	CatCList	*relcolList = (CatCList *)SearchSysCacheList(ATTNUM, 1, ObjectIdGetDatum(table), 0, 0, 0);
	int k;
	
	if (relcolList)
	{
		for (k = 0; k <= (relcolList->n_members + 1); k++)		
		{
			column_0 = SYSCOLSTARTNUM + k; /* SYSCOLSTARTNUM ... -1 are the system columns */
			col;
			
			/* no column with column num == 0 */
			if (column_0 == 0)
				continue;
			
			/* check if this column was reported */
			bool	found = false;
			foreach(col_cell, col_list)
			{
				col = lfirst_int(col_cell);
				
				if (col == column_0)				
					found = true;
			}
			
			if (found)
				continue;
				
			rolecmdtabcolstatus = (PgStat_RoleCmdTabColStatus *)hash_search(rolecmdtabstatus->colStatus, 
																			(void *) &column_0, 
																			HASH_ENTER, 
																			&found);
				
			if (!found)
			{
				rolecmdtabcolstatus->column = column_0;
				rolecmdtabcolstatus->count = 0;
				rolecmdtabcolstatus->count_0 = 0;
			}		
			
			rolecmdtabcolstatus->count_0++;
		}		
	}
		
	ReleaseSysCacheList(relcolList);
}


/*
 * get_tabstat_entry - find or create a PgStat_TableStatus entry for rel
 */
static PgStat_TableStatus *
get_tabstat_entry(Oid rel_id, bool isshared)
{
	PgStat_TableStatus *entry;
	TabStatusArray *tsa;
	TabStatusArray *prev_tsa;
	int			i;

	/*
	 * Search the already-used tabstat slots for this relation.
	 */
	prev_tsa = NULL;
	for (tsa = pgStatTabList; tsa != NULL; prev_tsa = tsa, tsa = tsa->tsa_next)
	{
		for (i = 0; i < tsa->tsa_used; i++)
		{
			entry = &tsa->tsa_entries[i];
			if (entry->t_id == rel_id)
				return entry;
		}

		if (tsa->tsa_used < TABSTAT_QUANTUM)
		{
			/*
			 * It must not be present, but we found a free slot instead. Fine,
			 * let's use this one.  We assume the entry was already zeroed,
			 * either at creation or after last use.
			 */
			entry = &tsa->tsa_entries[tsa->tsa_used++];
			entry->t_id = rel_id;
			entry->t_shared = isshared;
			return entry;
		}
	}

	/*
	 * We ran out of tabstat slots, so allocate more.  Be sure they're zeroed.
	 */
	tsa = (TabStatusArray *) MemoryContextAllocZero(TopMemoryContext,
													sizeof(TabStatusArray));
	if (prev_tsa)
		prev_tsa->tsa_next = tsa;
	else
		pgStatTabList = tsa;

	/*
	 * Use the first entry of the new TabStatusArray.
	 */
	entry = &tsa->tsa_entries[tsa->tsa_used++];
	entry->t_id = rel_id;
	entry->t_shared = isshared;
	return entry;
}

/*
 * get_tabstat_stack_level - add a new (sub)transaction stack entry if needed
 */
static PgStat_SubXactStatus *
get_tabstat_stack_level(int nest_level)
{
	PgStat_SubXactStatus *xact_state;

	xact_state = pgStatXactStack;
	if (xact_state == NULL || xact_state->nest_level != nest_level)
	{
		xact_state = (PgStat_SubXactStatus *)
			MemoryContextAlloc(TopTransactionContext,
							   sizeof(PgStat_SubXactStatus));
		xact_state->nest_level = nest_level;
		xact_state->prev = pgStatXactStack;
		xact_state->first = NULL;
		pgStatXactStack = xact_state;
	}
	return xact_state;
}

/*
 * add_tabstat_xact_level - add a new (sub)transaction state record
 */
static void
add_tabstat_xact_level(PgStat_TableStatus *pgstat_info, int nest_level)
{
	PgStat_SubXactStatus *xact_state;
	PgStat_TableXactStatus *trans;

	/*
	 * If this is the first rel to be modified at the current nest level, we
	 * first have to push a transaction stack entry.
	 */
	xact_state = get_tabstat_stack_level(nest_level);

	/* Now make a per-table stack entry */
	trans = (PgStat_TableXactStatus *)
		MemoryContextAllocZero(TopTransactionContext,
							   sizeof(PgStat_TableXactStatus));
	trans->nest_level = nest_level;
	trans->upper = pgstat_info->trans;
	trans->parent = pgstat_info;
	trans->next = xact_state->first;
	xact_state->first = trans;
	pgstat_info->trans = trans;
}

/*
 * pgstat_count_heap_insert - count a tuple insertion
 */
void
pgstat_count_heap_insert(Relation rel)
{
	PgStat_TableStatus *pgstat_info = rel->pgstat_info;

	if (pgstat_track_counts && pgstat_info != NULL)
	{
		int			nest_level = GetCurrentTransactionNestLevel();

		/* t_tuples_inserted is nontransactional, so just advance it */
		pgstat_info->t_counts.t_tuples_inserted++;

		/* We have to log the transactional effect at the proper level */
		if (pgstat_info->trans == NULL ||
			pgstat_info->trans->nest_level != nest_level)
			add_tabstat_xact_level(pgstat_info, nest_level);

		pgstat_info->trans->tuples_inserted++;
	}
}

/*
 * pgstat_count_heap_update - count a tuple update
 */
void
pgstat_count_heap_update(Relation rel, bool hot)
{
	PgStat_TableStatus *pgstat_info = rel->pgstat_info;

	if (pgstat_track_counts && pgstat_info != NULL)
	{
		int			nest_level = GetCurrentTransactionNestLevel();

		/* t_tuples_updated is nontransactional, so just advance it */
		pgstat_info->t_counts.t_tuples_updated++;
		/* ditto for the hot_update counter */
		if (hot)
			pgstat_info->t_counts.t_tuples_hot_updated++;

		/* We have to log the transactional effect at the proper level */
		if (pgstat_info->trans == NULL ||
			pgstat_info->trans->nest_level != nest_level)
			add_tabstat_xact_level(pgstat_info, nest_level);

		/* An UPDATE both inserts a new tuple and deletes the old */
		pgstat_info->trans->tuples_inserted++;
		pgstat_info->trans->tuples_deleted++;
	}
}

/*
 * pgstat_count_heap_delete - count a tuple deletion
 */
void
pgstat_count_heap_delete(Relation rel)
{
	PgStat_TableStatus *pgstat_info = rel->pgstat_info;

	if (pgstat_track_counts && pgstat_info != NULL)
	{
		int			nest_level = GetCurrentTransactionNestLevel();

		/* t_tuples_deleted is nontransactional, so just advance it */
		pgstat_info->t_counts.t_tuples_deleted++;

		/* We have to log the transactional effect at the proper level */
		if (pgstat_info->trans == NULL ||
			pgstat_info->trans->nest_level != nest_level)
			add_tabstat_xact_level(pgstat_info, nest_level);

		pgstat_info->trans->tuples_deleted++;
	}
}

/*
 * pgstat_update_heap_dead_tuples - update dead-tuples count
 *
 * The semantics of this are that we are reporting the nontransactional
 * recovery of "delta" dead tuples; so t_new_dead_tuples decreases
 * rather than increasing, and the change goes straight into the per-table
 * counter, not into transactional state.
 */
void
pgstat_update_heap_dead_tuples(Relation rel, int delta)
{
	PgStat_TableStatus *pgstat_info = rel->pgstat_info;

	if (pgstat_track_counts && pgstat_info != NULL)
		pgstat_info->t_counts.t_new_dead_tuples -= delta;
}


/* ----------
 * AtEOXact_PgStat
 *
 *	Called from access/transam/xact.c at top-level transaction commit/abort.
 * ----------
 */
void
AtEOXact_PgStat(bool isCommit)
{
	PgStat_SubXactStatus *xact_state;

	/*
	 * Count transaction commit or abort.  (We use counters, not just bools,
	 * in case the reporting message isn't sent right away.)
	 */
	if (isCommit)
		pgStatXactCommit++;
	else
		pgStatXactRollback++;

	/*
	 * Transfer transactional insert/update counts into the base tabstat
	 * entries.  We don't bother to free any of the transactional state, since
	 * it's all in TopTransactionContext and will go away anyway.
	 */
	xact_state = pgStatXactStack;
	if (xact_state != NULL)
	{
		PgStat_TableXactStatus *trans;

		Assert(xact_state->nest_level == 1);
		Assert(xact_state->prev == NULL);
		for (trans = xact_state->first; trans != NULL; trans = trans->next)
		{
			PgStat_TableStatus *tabstat;

			Assert(trans->nest_level == 1);
			Assert(trans->upper == NULL);
			tabstat = trans->parent;
			Assert(tabstat->trans == trans);
			if (isCommit)
			{
				tabstat->t_counts.t_new_live_tuples +=
					trans->tuples_inserted - trans->tuples_deleted;
				tabstat->t_counts.t_new_dead_tuples += trans->tuples_deleted;
			}
			else
			{
				/* inserted tuples are dead, deleted tuples are unaffected */
				tabstat->t_counts.t_new_dead_tuples += trans->tuples_inserted;
			}
			tabstat->trans = NULL;
		}
	}
	pgStatXactStack = NULL;

	/* Make sure any stats snapshot is thrown away */
	pgstat_clear_snapshot();
}

/* ----------
 * AtEOSubXact_PgStat
 *
 *	Called from access/transam/xact.c at subtransaction commit/abort.
 * ----------
 */
void
AtEOSubXact_PgStat(bool isCommit, int nestDepth)
{
	PgStat_SubXactStatus *xact_state;

	/*
	 * Transfer transactional insert/update counts into the next higher
	 * subtransaction state.
	 */
	xact_state = pgStatXactStack;
	if (xact_state != NULL &&
		xact_state->nest_level >= nestDepth)
	{
		PgStat_TableXactStatus *trans;
		PgStat_TableXactStatus *next_trans;

		/* delink xact_state from stack immediately to simplify reuse case */
		pgStatXactStack = xact_state->prev;

		for (trans = xact_state->first; trans != NULL; trans = next_trans)
		{
			PgStat_TableStatus *tabstat;

			next_trans = trans->next;
			Assert(trans->nest_level == nestDepth);
			tabstat = trans->parent;
			Assert(tabstat->trans == trans);
			if (isCommit)
			{
				if (trans->upper && trans->upper->nest_level == nestDepth - 1)
				{
					trans->upper->tuples_inserted += trans->tuples_inserted;
					trans->upper->tuples_deleted += trans->tuples_deleted;
					tabstat->trans = trans->upper;
					pfree(trans);
				}
				else
				{
					/*
					 * When there isn't an immediate parent state, we can just
					 * reuse the record instead of going through a
					 * palloc/pfree pushup (this works since it's all in
					 * TopTransactionContext anyway).  We have to re-link it
					 * into the parent level, though, and that might mean
					 * pushing a new entry into the pgStatXactStack.
					 */
					PgStat_SubXactStatus *upper_xact_state;

					upper_xact_state = get_tabstat_stack_level(nestDepth - 1);
					trans->next = upper_xact_state->first;
					upper_xact_state->first = trans;
					trans->nest_level = nestDepth - 1;
				}
			}
			else
			{
				/*
				 * On abort, inserted tuples are dead (and can be bounced out
				 * to the top-level tabstat), deleted tuples are unaffected
				 */
				tabstat->t_counts.t_new_dead_tuples += trans->tuples_inserted;
				tabstat->trans = trans->upper;
				pfree(trans);
			}
		}
		pfree(xact_state);
	}
}


/*
 * AtPrepare_PgStat
 *		Save the transactional stats state at 2PC transaction prepare.
 *
 * In this phase we just generate 2PC records for all the pending
 * transaction-dependent stats work.
 */
void
AtPrepare_PgStat(void)
{
	PgStat_SubXactStatus *xact_state;

	xact_state = pgStatXactStack;
	if (xact_state != NULL)
	{
		PgStat_TableXactStatus *trans;

		Assert(xact_state->nest_level == 1);
		Assert(xact_state->prev == NULL);
		for (trans = xact_state->first; trans != NULL; trans = trans->next)
		{
			PgStat_TableStatus *tabstat;
			TwoPhasePgStatRecord record;

			Assert(trans->nest_level == 1);
			Assert(trans->upper == NULL);
			tabstat = trans->parent;
			Assert(tabstat->trans == trans);

			record.tuples_inserted = trans->tuples_inserted;
			record.tuples_deleted = trans->tuples_deleted;
			record.t_id = tabstat->t_id;
			record.t_shared = tabstat->t_shared;

			RegisterTwoPhaseRecord(TWOPHASE_RM_PGSTAT_ID, 0,
								   &record, sizeof(TwoPhasePgStatRecord));
		}
	}
}

/*
 * PostPrepare_PgStat
 *		Clean up after successful PREPARE.
 *
 * All we need do here is unlink the transaction stats state from the
 * nontransactional state.	The nontransactional action counts will be
 * reported to the stats collector immediately, while the effects on live
 * and dead tuple counts are preserved in the 2PC state file.
 *
 * Note: AtEOXact_PgStat is not called during PREPARE.
 */
void
PostPrepare_PgStat(void)
{
	PgStat_SubXactStatus *xact_state;

	/*
	 * We don't bother to free any of the transactional state, since it's all
	 * in TopTransactionContext and will go away anyway.
	 */
	xact_state = pgStatXactStack;
	if (xact_state != NULL)
	{
		PgStat_TableXactStatus *trans;

		for (trans = xact_state->first; trans != NULL; trans = trans->next)
		{
			PgStat_TableStatus *tabstat;

			tabstat = trans->parent;
			tabstat->trans = NULL;
		}
	}
	pgStatXactStack = NULL;

	/* Make sure any stats snapshot is thrown away */
	pgstat_clear_snapshot();
}

/*
 * 2PC processing routine for COMMIT PREPARED case.
 *
 * Load the saved counts into our local pgstats state.
 */
void
pgstat_twophase_postcommit(TransactionId xid, uint16 info,
						   void *recdata, uint32 len)
{
	TwoPhasePgStatRecord *rec = (TwoPhasePgStatRecord *) recdata;
	PgStat_TableStatus *pgstat_info;

	/* Find or create a tabstat entry for the rel */
	pgstat_info = get_tabstat_entry(rec->t_id, rec->t_shared);

	pgstat_info->t_counts.t_new_live_tuples +=
		rec->tuples_inserted - rec->tuples_deleted;
	pgstat_info->t_counts.t_new_dead_tuples += rec->tuples_deleted;
}

/*
 * 2PC processing routine for ROLLBACK PREPARED case.
 *
 * Load the saved counts into our local pgstats state, but treat them
 * as aborted.
 */
void
pgstat_twophase_postabort(TransactionId xid, uint16 info,
						  void *recdata, uint32 len)
{
	TwoPhasePgStatRecord *rec = (TwoPhasePgStatRecord *) recdata;
	PgStat_TableStatus *pgstat_info;

	/* Find or create a tabstat entry for the rel */
	pgstat_info = get_tabstat_entry(rec->t_id, rec->t_shared);

	/* inserted tuples are dead, deleted tuples are no-ops */
	pgstat_info->t_counts.t_new_dead_tuples += rec->tuples_inserted;
}


/* ----------
 * pgstat_fetch_stat_dbentry() -
 *
 *	Support function for the SQL-callable pgstat* functions. Returns
 *	the collected statistics for one database or NULL. NULL doesn't mean
 *	that the database doesn't exist, it is just not yet known by the
 *	collector, so the caller is better off to report ZERO instead.
 * ----------
 */
PgStat_StatDBEntry *
pgstat_fetch_stat_dbentry(Oid dbid)
{
	/*
	 * If not done for this transaction, read the statistics collector stats
	 * file into some hash tables.
	 */
	backend_read_statsfile();

	/*
	 * Lookup the requested database; return NULL if not found
	 */
	return (PgStat_StatDBEntry *) hash_search(pgStatDBHash,
											  (void *) &dbid,
											  HASH_FIND, NULL);
}


/* ----------
 * pgstat_fetch_stat_tabentry() -
 *
 *	Support function for the SQL-callable pgstat* functions. Returns
 *	the collected statistics for one table or NULL. NULL doesn't mean
 *	that the table doesn't exist, it is just not yet known by the
 *	collector, so the caller is better off to report ZERO instead.
 * ----------
 */
PgStat_StatTabEntry *
pgstat_fetch_stat_tabentry(Oid relid)
{
	Oid			dbid;
	PgStat_StatDBEntry *dbentry;
	PgStat_StatTabEntry *tabentry;

	/*
	 * If not done for this transaction, read the statistics collector stats
	 * file into some hash tables.
	 */
	backend_read_statsfile();

	/*
	 * Lookup our database, then look in its table hash table.
	 */
	dbid = MyDatabaseId;
	dbentry = (PgStat_StatDBEntry *) hash_search(pgStatDBHash,
												 (void *) &dbid,
												 HASH_FIND, NULL);
	if (dbentry != NULL && dbentry->tables != NULL)
	{
		tabentry = (PgStat_StatTabEntry *) hash_search(dbentry->tables,
													   (void *) &relid,
													   HASH_FIND, NULL);
		if (tabentry)
			return tabentry;
	}

	/*
	 * If we didn't find it, maybe it's a shared table.
	 */
	dbid = InvalidOid;
	dbentry = (PgStat_StatDBEntry *) hash_search(pgStatDBHash,
												 (void *) &dbid,
												 HASH_FIND, NULL);
	if (dbentry != NULL && dbentry->tables != NULL)
	{
		tabentry = (PgStat_StatTabEntry *) hash_search(dbentry->tables,
													   (void *) &relid,
													   HASH_FIND, NULL);
		if (tabentry)
			return tabentry;
	}

	return NULL;
}

/* ----------
 * pgstat_fetch_stat_beentry() -
 *
 *	Support function for the SQL-callable pgstat* functions. Returns
 *	our local copy of the current-activity entry for one backend.
 *
 *	NB: caller is responsible for a check if the user is permitted to see
 *	this info (especially the querystring).
 * ----------
 */
PgBackendStatus *
pgstat_fetch_stat_beentry(int beid)
{
	pgstat_read_current_status();

	if (beid < 1 || beid > localNumBackends)
		return NULL;

	return &localBackendStatusTable[beid - 1];
}


/* ----------
 * pgstat_fetch_stat_numbackends() -
 *
 *	Support function for the SQL-callable pgstat* functions. Returns
 *	the maximum current backend id.
 * ----------
 */
int
pgstat_fetch_stat_numbackends(void)
{
	pgstat_read_current_status();

	return localNumBackends;
}

/*
 * ---------
 * pgstat_fetch_global() -
 *
 *	Support function for the SQL-callable pgstat* functions. Returns
 *	a pointer to the global statistics struct.
 * ---------
 */
PgStat_GlobalStats *
pgstat_fetch_global(void)
{
	backend_read_statsfile();

	return &globalStats;
}


/* ------------------------------------------------------------
 * Functions for management of the shared-memory PgBackendStatus array
 * ------------------------------------------------------------
 */

static PgBackendStatus *BackendStatusArray = NULL;
static PgBackendStatus *MyBEEntry = NULL;


/*
 * Report shared-memory space needed by CreateSharedBackendStatus.
 */
Size
BackendStatusShmemSize(void)
{
	Size		size;

	size = mul_size(sizeof(PgBackendStatus), MaxBackends);
	return size;
}

/*
 * Initialize the shared status array during postmaster startup.
 */
void
CreateSharedBackendStatus(void)
{
	Size		size = BackendStatusShmemSize();
	bool		found;

	/* Create or attach to the shared array */
	BackendStatusArray = (PgBackendStatus *)
		ShmemInitStruct("Backend Status Array", size, &found);

	if (!found)
	{
		/*
		 * We're the first - initialize.
		 */
		MemSet(BackendStatusArray, 0, size);
	}
}


/* ----------
 * pgstat_initialize() -
 *
 *	Initialize pgstats state, and set up our on-proc-exit hook.
 *	Called from InitPostgres.  MyBackendId must be set,
 *	but we must not have started any transaction yet (since the
 *	exit hook must run after the last transaction exit).
 * ----------
 */
void
pgstat_initialize(void)
{
	/* Initialize MyBEEntry */
	Assert(MyBackendId >= 1 && MyBackendId <= MaxBackends);
	MyBEEntry = &BackendStatusArray[MyBackendId - 1];

	/* Set up a process-exit hook to clean up */
	on_shmem_exit(pgstat_beshutdown_hook, 0);
}

/* ----------
 * pgstat_bestart() -
 *
 *	Initialize this backend's entry in the PgBackendStatus array.
 *	Called from InitPostgres.  MyDatabaseId and session userid must be set
 *	(hence, this cannot be combined with pgstat_initialize).
 * ----------
 */
void
pgstat_bestart(void)
{
	TimestampTz proc_start_timestamp;
	Oid			userid;
	SockAddr	clientaddr;
	volatile PgBackendStatus *beentry;

	/*
	 * To minimize the time spent modifying the PgBackendStatus entry, fetch
	 * all the needed data first.
	 *
	 * If we have a MyProcPort, use its session start time (for consistency,
	 * and to save a kernel call).
	 */
	if (MyProcPort)
		proc_start_timestamp = MyProcPort->SessionStartTime;
	else
		proc_start_timestamp = GetCurrentTimestamp();
	userid = GetSessionUserId();

	/*
	 * We may not have a MyProcPort (eg, if this is the autovacuum process).
	 * If so, use all-zeroes client address, which is dealt with specially in
	 * pg_stat_get_backend_client_addr and pg_stat_get_backend_client_port.
	 */
	if (MyProcPort)
		memcpy(&clientaddr, &MyProcPort->raddr, sizeof(clientaddr));
	else
		MemSet(&clientaddr, 0, sizeof(clientaddr));

	/*
	 * Initialize my status entry, following the protocol of bumping
	 * st_changecount before and after; and make sure it's even afterwards. We
	 * use a volatile pointer here to ensure the compiler doesn't try to get
	 * cute.
	 */
	beentry = MyBEEntry;
	do
	{
		beentry->st_changecount++;
	} while ((beentry->st_changecount & 1) == 0);

	beentry->st_procpid = MyProcPid;
	beentry->st_proc_start_timestamp = proc_start_timestamp;
	beentry->st_activity_start_timestamp = 0;
	beentry->st_xact_start_timestamp = 0;
	beentry->st_databaseid = MyDatabaseId;
	beentry->st_userid = userid;
	beentry->st_clientaddr = clientaddr;
	beentry->st_waiting = false;
	beentry->st_activity[0] = '\0';
	/* Also make sure the last byte in the string area is always 0 */
	beentry->st_activity[PGBE_ACTIVITY_SIZE - 1] = '\0';

	beentry->st_changecount++;
	Assert((beentry->st_changecount & 1) == 0);
}

/*
 * Shut down a single backend's statistics reporting at process exit.
 *
 * Flush any remaining statistics counts out to the collector.
 * Without this, operations triggered during backend exit (such as
 * temp table deletions) won't be counted.
 *
 * Lastly, clear out our entry in the PgBackendStatus array.
 */
static void
pgstat_beshutdown_hook(int code, Datum arg)
{
	volatile PgBackendStatus *beentry = MyBEEntry;

	pgstat_report_tabstat(true);

	/*
	 * Clear my status entry, following the protocol of bumping st_changecount
	 * before and after.  We use a volatile pointer here to ensure the
	 * compiler doesn't try to get cute.
	 */
	beentry->st_changecount++;

	beentry->st_procpid = 0;	/* mark invalid */

	beentry->st_changecount++;
	Assert((beentry->st_changecount & 1) == 0);
}


/* ----------
 * pgstat_report_activity() -
 *
 *	Called from tcop/postgres.c to report what the backend is actually doing
 *	(usually "<IDLE>" or the start of the query to be executed).
 * ----------
 */
void
pgstat_report_activity(const char *cmd_str)
{
	volatile PgBackendStatus *beentry = MyBEEntry;
	TimestampTz start_timestamp;
	int			len;

	if (!pgstat_track_activities || !beentry)
		return;

	/*
	 * To minimize the time spent modifying the entry, fetch all the needed
	 * data first.
	 */
	start_timestamp = GetCurrentStatementStartTimestamp();

	len = strlen(cmd_str);
	len = pg_mbcliplen(cmd_str, len, PGBE_ACTIVITY_SIZE - 1);

	/*
	 * Update my status entry, following the protocol of bumping
	 * st_changecount before and after.  We use a volatile pointer here to
	 * ensure the compiler doesn't try to get cute.
	 */
	beentry->st_changecount++;

	beentry->st_activity_start_timestamp = start_timestamp;
	memcpy((char *) beentry->st_activity, cmd_str, len);
	beentry->st_activity[len] = '\0';

	beentry->st_changecount++;
	Assert((beentry->st_changecount & 1) == 0);
}

/*
 * Report current transaction start timestamp as the specified value.
 * Zero means there is no active transaction.
 */
void
pgstat_report_xact_timestamp(TimestampTz tstamp)
{
	volatile PgBackendStatus *beentry = MyBEEntry;

	if (!pgstat_track_activities || !beentry)
		return;

	/*
	 * Update my status entry, following the protocol of bumping
	 * st_changecount before and after.  We use a volatile pointer here to
	 * ensure the compiler doesn't try to get cute.
	 */
	beentry->st_changecount++;
	beentry->st_xact_start_timestamp = tstamp;
	beentry->st_changecount++;
	Assert((beentry->st_changecount & 1) == 0);
}

/* ----------
 * pgstat_report_waiting() -
 *
 *	Called from lock manager to report beginning or end of a lock wait.
 *
 * NB: this *must* be able to survive being called before MyBEEntry has been
 * initialized.
 * ----------
 */
void
pgstat_report_waiting(bool waiting)
{
	volatile PgBackendStatus *beentry = MyBEEntry;

	if (!pgstat_track_activities || !beentry)
		return;

	/*
	 * Since this is a single-byte field in a struct that only this process
	 * may modify, there seems no need to bother with the st_changecount
	 * protocol.  The update must appear atomic in any case.
	 */
	beentry->st_waiting = waiting;
}


/* ----------
 * pgstat_read_current_status() -
 *
 *	Copy the current contents of the PgBackendStatus array to local memory,
 *	if not already done in this transaction.
 * ----------
 */
static void
pgstat_read_current_status(void)
{
	volatile PgBackendStatus *beentry;
	PgBackendStatus *localtable;
	PgBackendStatus *localentry;
	int			i;

	Assert(!pgStatRunningInCollector);
	if (localBackendStatusTable)
		return;					/* already done */

	pgstat_setup_memcxt();

	localtable = (PgBackendStatus *)
		MemoryContextAlloc(pgStatLocalContext,
						   sizeof(PgBackendStatus) * MaxBackends);
	localNumBackends = 0;

	beentry = BackendStatusArray;
	localentry = localtable;
	for (i = 1; i <= MaxBackends; i++)
	{
		/*
		 * Follow the protocol of retrying if st_changecount changes while we
		 * copy the entry, or if it's odd.  (The check for odd is needed to
		 * cover the case where we are able to completely copy the entry while
		 * the source backend is between increment steps.)	We use a volatile
		 * pointer here to ensure the compiler doesn't try to get cute.
		 */
		for (;;)
		{
			int			save_changecount = beentry->st_changecount;

			/*
			 * XXX if PGBE_ACTIVITY_SIZE is really large, it might be best to
			 * use strcpy not memcpy for copying the activity string?
			 */
			memcpy(localentry, (char *) beentry, sizeof(PgBackendStatus));

			if (save_changecount == beentry->st_changecount &&
				(save_changecount & 1) == 0)
				break;

			/* Make sure we can break out of loop if stuck... */
			CHECK_FOR_INTERRUPTS();
		}

		beentry++;
		/* Only valid entries get included into the local array */
		if (localentry->st_procpid > 0)
		{
			localentry++;
			localNumBackends++;
		}
	}

	/* Set the pointer only after completion of a valid table */
	localBackendStatusTable = localtable;
}


/* ------------------------------------------------------------
 * Local support functions follow
 * ------------------------------------------------------------
 */


/* ----------
 * pgstat_setheader() -
 *
 *		Set common header fields in a statistics message
 * ----------
 */
static void
pgstat_setheader(PgStat_MsgHdr *hdr, StatMsgType mtype)
{
	hdr->m_type = mtype;
}


/* ----------
 * pgstat_send() -
 *
 *		Send out one statistics message to the collector
 * ----------
 */
static void
pgstat_send(void *msg, int len)
{
	int			rc;

	if (pgStatSock < 0)
		return;

	((PgStat_MsgHdr *) msg)->m_size = len;

	/* We'll retry after EINTR, but ignore all other failures */
	do
	{
		rc = send(pgStatSock, msg, len, 0);
	} while (rc < 0 && errno == EINTR);

#ifdef USE_ASSERT_CHECKING
	/* In debug builds, log send failures ... */
	if (rc < 0)
		elog(LOG, "could not send to statistics collector: %m");
#endif
}

/* ----------
 * pgstat_send_bgwriter() -
 *
 *		Send bgwriter statistics to the collector
 * ----------
 */
void
pgstat_send_bgwriter(void)
{
	/* We assume this initializes to zeroes */
	static const PgStat_MsgBgWriter all_zeroes;

	/*
	 * This function can be called even if nothing at all has happened. In
	 * this case, avoid sending a completely empty message to the stats
	 * collector.
	 */
	if (memcmp(&BgWriterStats, &all_zeroes, sizeof(PgStat_MsgBgWriter)) == 0)
		return;

	/*
	 * Prepare and send the message
	 */
	pgstat_setheader(&BgWriterStats.m_hdr, PGSTAT_MTYPE_BGWRITER);
	pgstat_send(&BgWriterStats, sizeof(BgWriterStats));

	/*
	 * Clear out the statistics buffer, so it can be re-used.
	 */
	MemSet(&BgWriterStats, 0, sizeof(BgWriterStats));
}


/* ----------
 * PgstatCollectorMain() -
 *
 *	Start up the statistics collector process.	This is the body of the
 *	postmaster child process.
 *
 *	The argc/argv parameters are valid only in EXEC_BACKEND case.
 * ----------
 */
NON_EXEC_STATIC void
PgstatCollectorMain(int argc, char *argv[])
{
	struct itimerval write_timeout;
	bool		need_timer = false;
	int			len;
	PgStat_Msg	msg;

#ifndef WIN32
#ifdef HAVE_POLL
	struct pollfd input_fd;
#else
	struct timeval sel_timeout;
	fd_set		rfds;
#endif
#endif

	IsUnderPostmaster = true;	/* we are a postmaster subprocess now */

	MyProcPid = getpid();		/* reset MyProcPid */

	MyStartTime = time(NULL);	/* record Start Time for logging */
	
	/*
	 * If possible, make this process a group leader, so that the postmaster
	 * can signal any child processes too.	(pgstat probably never has any
	 * child processes, but for consistency we make all postmaster child
	 * processes do this.)
	 */
#ifdef HAVE_SETSID
	if (setsid() < 0)
		elog(FATAL, "setsid() failed: %m");
#endif

	/*
	 * Ignore all signals usually bound to some action in the postmaster,
	 * except SIGQUIT and SIGALRM.
	 */
	pqsignal(SIGHUP, SIG_IGN);
	pqsignal(SIGINT, SIG_IGN);
	pqsignal(SIGTERM, SIG_IGN);
	pqsignal(SIGQUIT, pgstat_exit);
	pqsignal(SIGALRM, force_statwrite);
	pqsignal(SIGPIPE, SIG_IGN);
	pqsignal(SIGUSR1, SIG_IGN);
	pqsignal(SIGUSR2, SIG_IGN);
	pqsignal(SIGCHLD, SIG_DFL);
	pqsignal(SIGTTIN, SIG_DFL);
	pqsignal(SIGTTOU, SIG_DFL);
	pqsignal(SIGCONT, SIG_DFL);
	pqsignal(SIGWINCH, SIG_DFL);
	PG_SETMASK(&UnBlockSig);

	/*
	 * Identify myself via ps
	 */
	init_ps_display("stats collector process", "", "", "");

	// Ashish
	//sleep(30);

	/*
	 * Arrange to write the initial status file right away
	 */
	need_statwrite = true;

	/* Preset the delay between status file writes */
	MemSet(&write_timeout, 0, sizeof(struct itimerval));
	write_timeout.it_value.tv_sec = PGSTAT_STAT_INTERVAL / 1000;
	write_timeout.it_value.tv_usec = (PGSTAT_STAT_INTERVAL % 1000) * 1000;

	/*
	 * Read in an existing statistics stats file or initialize the stats to
	 * zero.
	 */
	pgStatRunningInCollector = true;
	//pgStatDBHash = pgstat_read_statsfile(InvalidOid);

	/* %%% Ashish : v 1.178 */
	pgStatDBHash = pgstat_read_statsfile(InvalidOid, true);

	/*
	 * Setup the descriptor set for select(2).	Since only one bit in the set
	 * ever changes, we need not repeat FD_ZERO each time.
	 */
#if !defined(HAVE_POLL) && !defined(WIN32)
	FD_ZERO(&rfds);
#endif

	/*
	 * Loop to process messages until we get SIGQUIT or detect ungraceful
	 * death of our parent postmaster.
	 *
	 * For performance reasons, we don't want to do a PostmasterIsAlive() test
	 * after every message; instead, do it at statwrite time and if
	 * select()/poll() is interrupted by timeout.
	 */
	for (;;)
	{
		int			got_data;

		/*
		 * Quit if we get SIGQUIT from the postmaster.
		 */
		if (need_exit)
			break;

		/*
		 * If time to write the stats file, do so.	Note that the alarm
		 * interrupt isn't re-enabled immediately, but only after we next
		 * receive a stats message; so no cycles are wasted when there is
		 * nothing going on.
		 */
		if (need_statwrite)
		{
			/* Check for postmaster death; if so we'll write file below */
			if (!PostmasterIsAlive(true))
				break;

			// pgstat_write_statsfile();

			/* %%% Ashish : v 1.178 */
			pgstat_write_statsfile(false);

			need_statwrite = false;
			need_timer = true;
		}

		/*
		 * Wait for a message to arrive; but not for more than
		 * PGSTAT_SELECT_TIMEOUT seconds. (This determines how quickly we will
		 * shut down after an ungraceful postmaster termination; so it needn't
		 * be very fast.  However, on some systems SIGQUIT won't interrupt the
		 * poll/select call, so this also limits speed of response to SIGQUIT,
		 * which is more important.)
		 *
		 * We use poll(2) if available, otherwise select(2). Win32 has its own
		 * implementation.
		 */
#ifndef WIN32
#ifdef HAVE_POLL
		input_fd.fd = pgStatSock;
		input_fd.events = POLLIN | POLLERR;
		input_fd.revents = 0;

		if (poll(&input_fd, 1, PGSTAT_SELECT_TIMEOUT * 1000) < 0)
		{
			if (errno == EINTR)
				continue;
			ereport(ERROR,
					(errcode_for_socket_access(),
					 errmsg("poll() failed in statistics collector: %m")));
		}

		got_data = (input_fd.revents != 0);
#else							/* !HAVE_POLL */

		FD_SET(pgStatSock, &rfds);

		/*
		 * timeout struct is modified by select() on some operating systems,
		 * so re-fill it each time.
		 */
		sel_timeout.tv_sec = PGSTAT_SELECT_TIMEOUT;
		sel_timeout.tv_usec = 0;

		if (select(pgStatSock + 1, &rfds, NULL, NULL, &sel_timeout) < 0)
		{
			if (errno == EINTR)
				continue;
			ereport(ERROR,
					(errcode_for_socket_access(),
					 errmsg("select() failed in statistics collector: %m")));
		}

		got_data = FD_ISSET(pgStatSock, &rfds);
#endif   /* HAVE_POLL */
#else							/* WIN32 */
		got_data = pgwin32_waitforsinglesocket(pgStatSock, FD_READ,
											   PGSTAT_SELECT_TIMEOUT * 1000);
#endif

		/*
		 * If there is a message on the socket, read it and check for
		 * validity.
		 */
		if (got_data)
		{
			len = recv(pgStatSock, (char *) &msg,
					   sizeof(PgStat_Msg), 0);
			if (len < 0)
			{
				if (errno == EINTR)
					continue;
				ereport(ERROR,
						(errcode_for_socket_access(),
						 errmsg("could not read statistics message: %m")));
			}

			/*
			 * We ignore messages that are smaller than our common header
			 */
			if (len < sizeof(PgStat_MsgHdr))
				continue;

			/*
			 * The received length must match the length in the header
			 */
			if (msg.msg_hdr.m_size != len)
				continue;

			/*
			 * O.K. - we accept this message.  Process it.
			 */
			// Ashish
			char hdr[sizeof(PgStat_MsgHdr)];
			char *m_msg = &msg;
			switch (msg.msg_hdr.m_type)
			{
				case PGSTAT_MTYPE_DUMMY:
					break;

				case PGSTAT_MTYPE_TABSTAT:
					pgstat_recv_tabstat((PgStat_MsgTabstat *) &msg, len);
					break;

				/* Ashish */
				case PGSTAT_MTYPE_LOGINSTAT:
					pgstat_recv_loginstat((PgStat_MsgLoginStat *) &msg, len);
					break;

				/* Ashish */
				case PGSTAT_MTYPE_TABOIDSTAT:
					pgstat_recv_taboidstat((PgStat_MsgTabOidStat *) &msg, len);
					break;
					
				/* Ashish */
				case PGSTAT_MTYPE_ROLESTAT:
					//pgstat_recv_rolestat((PgStat_MsgRoleStat *) &msg, len);
					remove_item(&m_msg, hdr, sizeof(PgStat_MsgHdr));
					pgstat_recv_rolestat_new(m_msg, len - sizeof(PgStat_MsgHdr));
					break;
					
				case PGSTAT_MTYPE_TABPURGE:
					pgstat_recv_tabpurge((PgStat_MsgTabpurge *) &msg, len);
					break;

				case PGSTAT_MTYPE_DROPDB:
					pgstat_recv_dropdb((PgStat_MsgDropdb *) &msg, len);
					break;

				case PGSTAT_MTYPE_RESETCOUNTER:
					pgstat_recv_resetcounter((PgStat_MsgResetcounter *) &msg,
											 len);
					break;

				case PGSTAT_MTYPE_AUTOVAC_START:
					pgstat_recv_autovac((PgStat_MsgAutovacStart *) &msg, len);
					break;

				case PGSTAT_MTYPE_VACUUM:
					pgstat_recv_vacuum((PgStat_MsgVacuum *) &msg, len);
					break;

				case PGSTAT_MTYPE_ANALYZE:
					pgstat_recv_analyze((PgStat_MsgAnalyze *) &msg, len);
					break;

				case PGSTAT_MTYPE_BGWRITER:
					pgstat_recv_bgwriter((PgStat_MsgBgWriter *) &msg, len);
					break;

				default:
					break;
			}

			/*
			 * If this is the first message after we wrote the stats file the
			 * last time, enable the alarm interrupt to make it be written
			 * again later.
			 */
			if (need_timer)
			{
				if (setitimer(ITIMER_REAL, &write_timeout, NULL))
					ereport(ERROR,
					(errmsg("could not set statistics collector timer: %m")));
				need_timer = false;
			}
		}
		else
		{
			/*
			 * We can only get here if the select/poll timeout elapsed. Check
			 * for postmaster death.
			 */
			if (!PostmasterIsAlive(true))
				break;
		}
	}							/* end of message-processing loop */

	/*
	 * Save the final stats to reuse at next startup.
	 */
	//pgstat_write_statsfile();

	/* %%% Ashish : v 1.178 */
	pgstat_write_statsfile(true);

	exit(0);
}


/* SIGQUIT signal handler for collector process */
static void
pgstat_exit(SIGNAL_ARGS)
{
	need_exit = true;
}

/* SIGALRM signal handler for collector process */
static void
force_statwrite(SIGNAL_ARGS)
{
	need_statwrite = true;
}


/*
 * Ashish: modified to include role counts as well .. 
 * 
 * Lookup the hash table entry for the specified database. If no hash
 * table entry exists, initialize it, if the create parameter is true.
 * Else, return NULL.
 */
static PgStat_StatDBEntry *
pgstat_get_db_entry(Oid databaseid, bool create)
{
	PgStat_StatDBEntry *result;
	bool		found;
	HASHACTION	action = (create ? HASH_ENTER : HASH_FIND);

	/* Lookup or create the hash table entry for this database */
	result = (PgStat_StatDBEntry *) hash_search(pgStatDBHash,
												(void *)&databaseid,
												action, &found);

	if (!create && !found)
		return NULL;

	/* If not found, initialize the new one. */
	if (!found)
	{
		HASHCTL		hash_ctl;

		result->tables = NULL;
		result->users = NULL; 		/* Ashish */
		result->n_xact_commit = 0;
		result->n_xact_rollback = 0;
		result->n_blocks_fetched = 0;
		result->n_blocks_hit = 0;
		result->n_tuples_returned = 0;
		result->n_tuples_fetched = 0;
		result->n_tuples_inserted = 0;
		result->n_tuples_updated = 0;
		result->n_tuples_deleted = 0;
		result->last_autovac_time = 0;

		/* Ashish */
		result->all_roles_cmd_count = 0;
		result->all_roles_tab_count = 0;
		result->all_roles_col_count = 0;
		result->num_roles = 0;
		
		memset(&hash_ctl, 0, sizeof(hash_ctl));
		hash_ctl.keysize = sizeof(Oid);
		hash_ctl.entrysize = sizeof(PgStat_StatTabEntry);
		hash_ctl.hash = oid_hash;
		result->tables = hash_create("Per-database table",
									 PGSTAT_TAB_HASH_SIZE,
									 &hash_ctl,
									 HASH_ELEM | HASH_FUNCTION);
		
		/* Ashish */
		memset(&hash_ctl, 0, sizeof(hash_ctl));
		hash_ctl.keysize = sizeof(Oid);
		hash_ctl.entrysize = sizeof(PgStat_StatRoleEntry);
		hash_ctl.hash = oid_hash;
		result->users = hash_create("Per-database user",
									 PGSTAT_ROLE_HASH_SIZE,
									 &hash_ctl,
									 HASH_ELEM | HASH_FUNCTION);
		
	}

	return result;
}


/* ----------
 * pgstat_write_statsfile() -
 *
 *	Tell the news.
 *
 *  %%% Ashish : v 1.178
 *	If writing to the permanent file (happens when the collector is
 *	shutting down only), remove the temporary file so that backends
 *	starting up under a new postmaster can't read the old data before
 *	the new collector is ready.
 * 
 *  Ashish: changed to include user query statistics ...
 * ----------
 */
static void
//pgstat_write_statsfile(void)
/* %%% Ashish : v 1.178 */
pgstat_write_statsfile(bool permanent)
{
	HASH_SEQ_STATUS hstat;
	HASH_SEQ_STATUS tstat;
	
	HASH_SEQ_STATUS ustat;				/* Ashish */
	HASH_SEQ_STATUS rstat;				/* Ashish */
	HASH_SEQ_STATUS rolecmd_stat;			/* Ashish */
	HASH_SEQ_STATUS rolecmdtab_stat;		/* Ashish */
	HASH_SEQ_STATUS rolecmdtabcol_stat;		/* Ashish */
	
	HASH_SEQ_STATUS roletab_stat;		/* Ashish */
	HASH_SEQ_STATUS roletabcol_stat;	/* Ashish */
	HASH_SEQ_STATUS rolecol_stat;	/* Ashish */
	
	PgStat_StatDBEntry *dbentry;
	PgStat_StatTabEntry *tabentry;
	
	PgStat_StatRoleEntry 					*userentry;					/* Ashish */
	PgStat_StatRoleEntry 					*roleentry;					/* Ashish */
	PgStat_StatRoleCmdEntry 				*rolecmd_entry;				/* Ashish */
	PgStat_StatRoleCmdTabEntry 				*rolecmdtab_entry;			/* Ashish */
	PgStat_StatRoleCmdTabColEntry			*rolecmdtabcol_entry;		/* Ashish */
	
	PgStat_StatRoleCmdTabEntry 				*roletab_entry;			/* Ashish */
	PgStat_StatRoleCmdTabColEntry			*roletabcol_entry;		/* Ashish */	
	PgStat_StatRoleCmdTabColEntry			*rolecol_entry;		/* Ashish */	

	/* %%% Ashish : v 1.178 */
 	const char *tmpfile = permanent?PGSTAT_STAT_PERMANENT_TMPFILE:PGSTAT_STAT_TMPFILE;
 	const char *statfile = permanent?PGSTAT_STAT_PERMANENT_FILENAME:PGSTAT_STAT_FILENAME;
	
	FILE	   *fpout;
	int32		format_id;

	/*
	 * Open the statistics temp file to write out the current values.
	 */
	//fpout = fopen(PGSTAT_STAT_TMPFILE, PG_BINARY_W);
	
	/* %%% Ashish : v 1.178 */
	fpout = fopen(tmpfile, PG_BINARY_W);

	if (fpout == NULL)
	{

	/*	ereport(LOG,
				(errcode_for_file_access(),
				 errmsg("could not open temporary statistics file \"%s\": %m",
					PGSTAT_STAT_TMPFILE)));
	*/

	/* %%% Ashish : v 1.178 */
	ereport(LOG,
			(errcode_for_file_access(),
			 errmsg("could not open temporary statistics file \"%s\": %m",
					tmpfile)));

		return;
	}

	/*
	 * Write the file header --- currently just a format ID.
	 */
	format_id = PGSTAT_FILE_FORMAT_ID;
	fwrite(&format_id, sizeof(format_id), 1, fpout);

	/*
	 * Write global stats struct
	 */
	fwrite(&globalStats, sizeof(globalStats), 1, fpout);

	/*
	 * Walk through the database table.
	 */
	hash_seq_init(&hstat, pgStatDBHash);
	while ((dbentry = (PgStat_StatDBEntry *) hash_seq_search(&hstat)) != NULL)
	{
		/*
		 * Write out the DB entry including the number of live backends. We
		 * don't write the tables pointer since it's of no use to any other
		 * process.
		 */
		fputc('D', fpout);
		fwrite(dbentry, offsetof(PgStat_StatDBEntry, tables), 1, fpout);

		//ereport(pgStatRunningInCollector ? LOG : WARNING,
			//	(errmsg("written db record for dbid --> %d", dbentry->databaseid)));
		
		/*
		 * Walk through the database's access stats per table.
		 */
		hash_seq_init(&tstat, dbentry->tables);
		while ((tabentry = (PgStat_StatTabEntry *) hash_seq_search(&tstat)) != NULL)
		{
			fputc('T', fpout);
			fwrite(tabentry, sizeof(PgStat_StatTabEntry), 1, fpout);
			
			//ereport(pgStatRunningInCollector ? LOG : WARNING,
				//	(errmsg("written tab record for table id --> %d", tabentry->tableid)));

		}

		/* 
		 * Ashish
		 * 
		 */
				
		hash_seq_init(&ustat, dbentry->users);
		while ((userentry = (PgStat_StatRoleEntry *) hash_seq_search(&ustat)) != NULL)
		{
			if (userentry->roleEntry != NULL)
			{
				fputc('U', fpout);
				fwrite(userentry, offsetof(PgStat_StatRoleEntry, roleEntry), 1, fpout);
				
				//ereport(pgStatRunningInCollector ? LOG : WARNING,
					//	(errmsg("written role record for role id --> %d", roleentry->role_id)));
	
				/*  
				 * Walk through this user's activated role stats 			  			 
				 */			
				hash_seq_init(&rstat, userentry->roleEntry);
				while ((roleentry = (PgStat_StatRoleEntry *) hash_seq_search(&rstat)) != NULL)
				{
					fputc('R', fpout);				
					fwrite(roleentry, offsetof(PgStat_StatRoleEntry, roleEntry), 1, fpout);
	
					hash_seq_init(&rolecmd_stat, roleentry->cmdEntry);
					while ((rolecmd_entry = (PgStat_StatRoleCmdEntry *) hash_seq_search(&rolecmd_stat)) != NULL)
					{				
						fputc('M', fpout);
						fwrite(rolecmd_entry, offsetof(PgStat_StatRoleCmdEntry, tabEntry), 1, fpout);
	
						hash_seq_init(&rolecmdtab_stat, rolecmd_entry->tabEntry);
						while ((rolecmdtab_entry = (PgStat_StatRoleCmdTabEntry *) hash_seq_search(&rolecmdtab_stat)) != NULL)
						{
							fputc('Z', fpout);
							fwrite(rolecmdtab_entry, offsetof(PgStat_StatRoleCmdTabEntry, colEntry), 1, fpout);
						    
							/*	
							//hash_seq_init(&rolecmdtabcol_stat, rolecmdtab_entry->colEntry);
							//while ((rolecmdtabcol_entry = (PgStat_StatRoleCmdTabColEntry *) hash_seq_search(&rolecmdtabcol_stat)) != NULL)
							for(int k = 0; k < rolecmdtab_entry->num_cols; k++)
							{
								fputc('C', fpnt k = 0; k < rolecmdtab_entry->num_cols; k++)(ut);
								
								fwrite(rolecmdtabcol_entry, sizeof(PgStat_StatRoleCmdTabColEntry), 1, fpout);										
							}
							*/						
							/*
							 * Mark the end of this table for this command
							 */
							fputc('z', fpout);									
						}
						/*
						 * Mark the end of this command for this role
						 */
						fputc('m', fpout);												
					}				
					/*
					 * Mark the end of this role for this DB
					 */
					fputc('r', fpout);							
				}						
				/*
				 * Mark the end of this user for this DB
				 */
				fputc('u', fpout);			
			}
			else /* for the nbc */
			{
				fputc('U', fpout);			/* a role */
				fwrite(userentry, offsetof(PgStat_StatRoleEntry, roleEntry), 1, fpout);
				
				//ereport(pgStatRunningInCollector ? LOG : WARNING,
					//	(errmsg("written role record for role id --> %d", roleentry->role_id)));
	
				/*  
				 * Walk through this role's commands 			  			 
				 */			
				
				Assert(userentry->cmdEntry != NULL);
				hash_seq_init(&rolecmd_stat, userentry->cmdEntry);
				while ((rolecmd_entry = (PgStat_StatRoleCmdEntry *) hash_seq_search(&rolecmd_stat)) != NULL)
				{
					fputc('M', fpout);				
					fwrite(rolecmd_entry, offsetof(PgStat_StatRoleCmdEntry, tabEntry), 1, fpout);
		
				}
				
				/* walk through this role's tables */
				Assert(userentry->tabEntry != NULL);
				hash_seq_init(&roletab_stat, userentry->tabEntry);
				while ((roletab_entry = (PgStat_StatRoleCmdTabEntry *) hash_seq_search(&roletab_stat)) != NULL)
				{
					fputc('Z', fpout);
					fwrite(roletab_entry, offsetof(PgStat_StatRoleCmdTabEntry, colEntry), 1, fpout);
					
					if (IDR_LEVEL == 1 ||IDR_LEVEL == 2)
					{
						Assert(roletab_entry->colEntry != NULL);
						
						//hash_seq_init(&roletabcol_stat, roletab_entry->colEntry);
						//while ((roletabcol_entry = (PgStat_StatRoleCmdTabColEntry *) hash_seq_search(&roletabcol_stat)) != NULL)
						int k = 0;
						for(k = 0; k < roletab_entry->num_cols; k++)
						{
							fputc('C', fpout);
							roletabcol_entry = (PgStat_StatRoleCmdTabColEntry *)(roletab_entry->colEntry + k);
							fwrite(roletabcol_entry, sizeof(PgStat_StatRoleCmdTabColEntry), 1, fpout);										
						}						
					}
					/*
					 * Mark the end of this table for this role
					 */
					fputc('z', fpout);									
					
				}
				
				if (IDR_LEVEL == 0)
				{
					Assert(userentry->colEntry != NULL);
					
					hash_seq_init(&rolecol_stat, userentry->colEntry);
					while ((rolecol_entry = (PgStat_StatRoleCmdTabColEntry *) hash_seq_search(&rolecol_stat)) != NULL)
					{
						fputc('C', fpout);
						fwrite(rolecol_entry, sizeof(PgStat_StatRoleCmdTabColEntry), 1, fpout);										
					}											
				}
				
				/*
				 * Mark the end of this role for this DB
				 */
				fputc('u', fpout);							
			}												
		}
		/*
		 * Mark the end of this DB
		 */
		fputc('d', fpout);
	}

	/*
	 * No more output to be done. Close the temp file and replace the old
	 * pgstat.stat with it.  The ferror() check replaces testing for error
	 * after each individual fputc or fwrite above.
	 */
	fputc('E', fpout);

	if (ferror(fpout))
	{
		/*
		ereport(LOG,
				(errcode_for_file_access(),
			   errmsg("could not write temporary statistics file \"%s\": %m",
					  PGSTAT_STAT_TMPFILE)));
		fclose(fpout);
		unlink(PGSTAT_STAT_TMPFILE);
		*/

		/* %%% Ashish : v 1.178 */	
		ereport(LOG,
				(errcode_for_file_access(),
			   errmsg("could not write temporary statistics file \"%s\": %m",
					  tmpfile)));

		fclose(fpout);
		unlink(tmpfile);

	}
	else if (fclose(fpout) < 0)
	{
		/*
		ereport(LOG,
				(errcode_for_file_access(),
			   errmsg("could not close temporary statistics file \"%s\": %m",
					  PGSTAT_STAT_TMPFILE)));
		unlink(PGSTAT_STAT_TMPFILE);
		*/

		/* %%% Ashish : v 1.178 */
  		ereport(LOG,
  				(errcode_for_file_access(),
  			   errmsg("could not close temporary statistics file \"%s\": %m",
 					  tmpfile)));
 		unlink(tmpfile);

	}
	/*
	else if (rename(PGSTAT_STAT_TMPFILE, PGSTAT_STAT_FILENAME) < 0)
	{
		ereport(LOG,
				(errcode_for_file_access(),
				 errmsg("could not rename temporary statistics file \"%s\" to \"%s\": %m",
						PGSTAT_STAT_TMPFILE, PGSTAT_STAT_FILENAME)));
		unlink(PGSTAT_STAT_TMPFILE);
	}
	*/

	/* %%% Ashish : v 1.178 */
 	else if (rename(tmpfile, statfile) < 0)
  	{
  		ereport(LOG,
  				(errcode_for_file_access(),
  				 errmsg("could not rename temporary statistics file \"%s\" to \"%s\": %m",
 						tmpfile, statfile)));
 		unlink(tmpfile);
  	}

	/* %%% Ashish : v 1.178 */	
 	if (permanent)
 		unlink(PGSTAT_STAT_FILENAME);
}


/* ----------
 * 
 * Ashish : changed to accomodate role hash tables 
 * pgstat_read_statsfile() -
 *
 *	Reads in an existing statistics collector file and initializes the
 *	databases' hash table (whose entries point to the tables' hash tables).
 * ----------
 */
static HTAB *
//pgstat_read_statsfile(Oid onlydb)

/* %%% Ashish : v 1.178 */
pgstat_read_statsfile(Oid onlydb, bool permanent)
{
	PgStat_StatDBEntry 		*dbentry;
	PgStat_StatDBEntry 		dbbuf;
	PgStat_StatTabEntry 	*tabentry;
	PgStat_StatTabEntry 	tabbuf;
	
	PgStat_StatRoleEntry 					*userentry;				/* Ashish */
	PgStat_StatRoleEntry 					*roleentry;				/* Ashish */
	PgStat_StatRoleCmdEntry 				*rolecmd_entry;				/* Ashish */
	PgStat_StatRoleCmdTabEntry 				*rolecmdtab_entry;			/* Ashish */
	PgStat_StatRoleCmdTabColEntry			*rolecmdtabcol_entry;		/* Ashish */	

	PgStat_StatRoleCmdEntry 				*nbcrolecmd_entry;				/* Ashish */
	PgStat_StatRoleCmdTabEntry 				*nbcroletab_entry;			/* Ashish */
	PgStat_StatRoleCmdTabColEntry			*nbcroletabcol_entry;		/* Ashish */
	PgStat_StatRoleCmdTabColEntry			*nbcrolecol_entry;		/* Ashish */
	
	PgStat_StatRoleEntry 					userbuf;				/* Ashish */
	PgStat_StatRoleEntry 					rolebuf;				/* Ashish */
	PgStat_StatRoleCmdEntry 				rolecmd_buf;				/* Ashish */
	PgStat_StatRoleCmdTabEntry 				rolecmdtab_buf;			/* Ashish */
	PgStat_StatRoleCmdTabColEntry			rolecmdtabcol_buf;			/* Ashish */	
	
	HASHCTL		hash_ctl;
	HTAB	   *dbhash;
	HTAB	   *tabhash = NULL;
	
	HTAB	   *userhash = NULL;			/* Ashish */	
	HTAB	   *rolehash = NULL;	   		/* Ashish */
	HTAB	   *rolecmd_hash = NULL;	  	/* Ashish */
	HTAB	   *rolecmdtab_hash = NULL;		/* Ashish */	
//	HTAB	   *rolecmdtabcol_hash = NULL;		/* Ashish */	
	PgStat_StatRoleCmdTabColEntry           *rolecmdtabcol_hash = NULL;

	HTAB	   *nbc_rolecmd_hash = NULL;	  	/* Ashish */
	HTAB	   *nbc_roletab_hash = NULL;		/* Ashish */	
	HTAB	   *nbc_rolecol_hash = NULL;		/* Ashish */	
	//HTAB	   *nbc_roletabcol_hash = NULL;		/* Ashish */
	PgStat_StatRoleCmdTabColEntry           *nbc_roletabcol_hash = NULL;
	
	/* %%% Ashish : v 1.178 */
	const char *statfile = permanent?PGSTAT_STAT_PERMANENT_FILENAME:PGSTAT_STAT_FILENAME;
	
	FILE	   *fpin;
	int32		format_id;
	bool		found;

	/*
	 * The tables will live in pgStatLocalContext.
	 */
	pgstat_setup_memcxt();

	/*
	 * Create the DB hashtable
	 */
	memset(&hash_ctl, 0, sizeof(hash_ctl));
	hash_ctl.keysize = sizeof(Oid);
	hash_ctl.entrysize = sizeof(PgStat_StatDBEntry);
	hash_ctl.hash = oid_hash;
 //	hash_ctl.hcxt = pgStatLocalContext;
    hash_ctl.hcxt = TopMemoryContext;
	dbhash = hash_create("Databases hash", PGSTAT_DB_HASH_SIZE, &hash_ctl,
						 HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);

	/*
	 * Clear out global statistics so they start from zero in case we can't
	 * load an existing statsfile.
	 */
	memset(&globalStats, 0, sizeof(globalStats));

	/*
	 * Try to open the status file. If it doesn't exist, the backends simply
	 * return zero for anything and the collector simply starts from scratch
	 * with empty counters.
	 */
	//if ((fpin = AllocateFile(PGSTAT_STAT_FILENAME, PG_BINARY_R)) == NULL)

	/* %%% Ashish : v 1.178 */
	if ((fpin = AllocateFile(statfile, PG_BINARY_R)) == NULL)
		return dbhash;

	/*
	 * Verify it's of the expected format.
	 */
	if (fread(&format_id, 1, sizeof(format_id), fpin) != sizeof(format_id)
		|| format_id != PGSTAT_FILE_FORMAT_ID)
	{
		ereport(pgStatRunningInCollector ? LOG : WARNING,
				(errmsg("corrupted pgstat.stat file1")));
		goto done;
	}

	/*
	 * Read global stats struct
	 */
	if (fread(&globalStats, 1, sizeof(globalStats), fpin) != sizeof(globalStats))
	{
		ereport(pgStatRunningInCollector ? LOG : WARNING,
				(errmsg("corrupted pgstat.stat file2")));
		goto done;
	}

	/*
	 * We found an existing collector stats file. Read it and put all the
	 * hashtable entries into place.
	 */
	for (;;)
	{
		/* Ashish */
		char	marker = fgetc(fpin);
		//ereport(pgStatRunningInCollector ? LOG : WARNING,
			//	(errmsg("marker --> %c", marker)));
		
		switch (marker)
		{
				/*
				 *  Ashish
				 * 
				 * 'D'	A PgStat_StatDBEntry struct describing a database
				 * follows. Subsequently, zero to many 'T' entries and 'U' 'R', 'u', 'M', 'Z', 'C', 'z', 'm' will follow
				 * until a 'd' is encountered.
				 */
			case 'D':
				if (fread(&dbbuf, 1, offsetof(PgStat_StatDBEntry, tables),
						  fpin) != offsetof(PgStat_StatDBEntry, tables))
				{
					ereport(pgStatRunningInCollector ? LOG : WARNING,
							(errmsg("corrupted pgstat.stat file3")));
					goto done;
				}
				
				//ereport(pgStatRunningInCollector ? LOG : WARNING,
					//	(errmsg("dbid --> %d", dbbuf.databaseid)));

				/*
				 * Add to the DB hash
				 */
				dbentry = (PgStat_StatDBEntry *) hash_search(dbhash,
												  (void *) &dbbuf.databaseid,
															 HASH_ENTER,
															 &found);
				if (found)
				{
					ereport(pgStatRunningInCollector ? LOG : WARNING,
							(errmsg("corrupted pgstat.stat file4")));
					goto done;
				}

				memcpy(dbentry, &dbbuf, sizeof(PgStat_StatDBEntry));
				dbentry->tables = NULL;
				dbentry->users = NULL;		/* Ashish */

				/*
				 * Don't collect tables if not the requested DB (or the
				 * shared-table info)
				 */
				if (onlydb != InvalidOid)
				{
					if (dbbuf.databaseid != onlydb &&
						dbbuf.databaseid != InvalidOid)
						break;
				}

				memset(&hash_ctl, 0, sizeof(hash_ctl));
				hash_ctl.keysize = sizeof(Oid);
				hash_ctl.entrysize = sizeof(PgStat_StatTabEntry);
				hash_ctl.hash = oid_hash;
				//hash_ctl.hcxt = pgStatLocalContext;
				hash_ctl.hcxt = TopMemoryContext;
				dbentry->tables = hash_create("Per-database table",
											  PGSTAT_TAB_HASH_SIZE,
											  &hash_ctl,
								   HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);				
				
				/*
				 * Arrange that following 'T's add entries to this database's
				 * tables hash table.
				 */
				tabhash = dbentry->tables;
				
				/* Ashish: User hash table creation */				
				memset(&hash_ctl, 0, sizeof(hash_ctl));
				hash_ctl.keysize = sizeof(Oid);
				hash_ctl.entrysize = sizeof(PgStat_StatRoleEntry);
				hash_ctl.hash = oid_hash;
				//hash_ctl.hcxt = pgStatLocalContext;
				hash_ctl.hcxt = TopMemoryContext;
				dbentry->users = hash_create("Per-database user and role(for nbc)",
											  PGSTAT_ROLE_HASH_SIZE,
											  &hash_ctl,
											  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);				

				/* Ashish
				 * Arrange that following 'U's add entries to this database's
				 * users hash table.
				 */
				userhash = dbentry->users;
												
				break;				

				/*
				 * 'd'	End of this database.
				 */
			case 'd':
				tabhash = NULL;
				userhash = NULL;		/* Ashish */
				break;

				/*
				 * 'T'	A PgStat_StatTabEntry follows.
				 */
			case 'T':
				if (fread(&tabbuf, 1, sizeof(PgStat_StatTabEntry),
						  fpin) != sizeof(PgStat_StatTabEntry))
				{
					ereport(pgStatRunningInCollector ? LOG : WARNING,
							(errmsg("corrupted pgstat.stat file5")));
					goto done;
				}

				/*
				 * Skip if table belongs to a not requested database.
				 */
				if (tabhash == NULL)
					break;

				tabentry = (PgStat_StatTabEntry *) hash_search(tabhash,
													(void *) &tabbuf.tableid,
														 HASH_ENTER, &found);

				if (found)
				{
					ereport(pgStatRunningInCollector ? LOG : WARNING,
							(errmsg("corrupted pgstat.stat file6")));
					goto done;
				}

				memcpy(tabentry, &tabbuf, sizeof(tabbuf));
				break;

				/* Ashish
				 * 'U'	A user follows.
				 */
			case 'U':
				
				if (fread(&userbuf, 1, offsetof(PgStat_StatRoleEntry, roleEntry),
						  fpin) != offsetof(PgStat_StatRoleEntry, roleEntry))
				{
					ereport(pgStatRunningInCollector ? LOG : WARNING,
							(errmsg("corrupted pgstat.stat file7")));
					goto done;
				}

				/*
				 * Skip if user belongs to a not requested database.
				*/ 
				if (userhash == NULL)
					break;

				userentry = (PgStat_StatRoleEntry *) hash_search(userhash,
																(void *) &userbuf.role_id,
																HASH_ENTER, &found);

			//	ereport(pgStatRunningInCollector ? LOG : WARNING,
				//		(errmsg("roleid --> %d", rolebuf.role_id)));

				if (found)
				{
					ereport(pgStatRunningInCollector ? LOG : WARNING,
							(errmsg("corrupted pgstat.stat file8")));
					goto done;
				}

				memcpy(userentry, &userbuf, sizeof(userbuf));
				userentry->roleEntry = NULL;
				userentry->cmdEntry = NULL;
				userentry->tabEntry = NULL;
				userentry->colEntry = NULL;
				
				/* Arrange for the role entries for this user to be collected */
				if (userbuf.is_under_user)
				{
					memset(&hash_ctl, 0, sizeof(hash_ctl));
					hash_ctl.keysize = sizeof(Oid);
					hash_ctl.entrysize = sizeof(PgStat_StatRoleEntry);
					hash_ctl.hash = oid_hash;
					//hash_ctl.hcxt = pgStatLocalContext;
					hash_ctl.hcxt = TopMemoryContext;
					userentry->roleEntry = hash_create("Per-database per-user role stats",
												  PGSTAT_ROLE_HASH_SIZE,
												  &hash_ctl,
												  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);	

					/* Ashish
					 * Arrange that following 'R's add entries to this user's hash table.
					 */
					rolehash = userentry->roleEntry;

				} else
				{
					memset(&hash_ctl, 0, sizeof(hash_ctl));
					hash_ctl.keysize = sizeof(Oid);
					hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdEntry);
					hash_ctl.hash = oid_hash;
					//hash_ctl.hcxt = pgStatLocalContext;
					hash_ctl.hcxt = TopMemoryContext;
					userentry->cmdEntry = hash_create("Per-database per-role per-command stats",
													  PGSTAT_ROLE_CMD_HASH_SIZE,
													  &hash_ctl,
													  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);							
					
					nbc_rolecmd_hash = userentry->cmdEntry;					

					memset(&hash_ctl, 0, sizeof(hash_ctl));
					hash_ctl.keysize = sizeof(Oid);
					hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabEntry);
					hash_ctl.hash = oid_hash;
					//hash_ctl.hcxt = pgStatLocalContext;
					hash_ctl.hcxt = TopMemoryContext;
					userentry->tabEntry = hash_create("Per-database per-role per-table stats",
													  PGSTAT_ROLE_CMD_TAB_HASH_SIZE,
													  &hash_ctl,
													  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);			
					
					nbc_roletab_hash = userentry->tabEntry;
					
					/* only used for coarse triplets */
					if (IDR_LEVEL == 0)
					{
						memset(&hash_ctl, 0, sizeof(hash_ctl));
						hash_ctl.keysize = sizeof(Oid);
						hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabColEntry);
						hash_ctl.hash = oid_hash;
						//hash_ctl.hcxt = pgStatLocalContext;
						hash_ctl.hcxt = TopMemoryContext;
						userentry->colEntry = hash_create("Per-database per-role per-num_cols stats",
														  PGSTAT_ROLE_CMD_TAB_COL_HASH_SIZE,
														  &hash_ctl,
														  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);
						
						nbc_rolecol_hash = userentry->colEntry; 
					}
				}
				
				break;
			
				/* Ashish
				 * 'u' end of the current user 
				 */ 
			case 'u':
				rolehash = NULL;
				
				nbc_rolecmd_hash = NULL;
				nbc_roletab_hash = NULL;
				nbc_rolecol_hash = NULL;
				
				break;
				
			case 'R':
				
				if (fread(&rolebuf, 1, offsetof(PgStat_StatRoleEntry, roleEntry),
						  fpin) != offsetof(PgStat_StatRoleEntry, roleEntry))
				{
					ereport(pgStatRunningInCollector ? LOG : WARNING,
							(errmsg("corrupted pgstat.stat file 9")));
					goto done;
				}

				/*
				 * Skip if role belongs to a not requested database.
				*/ 
				if (rolehash == NULL)
					break;

				roleentry = (PgStat_StatRoleEntry *) hash_search(rolehash,
																(void *) &rolebuf.role_id,
																HASH_ENTER, &found);

			//	ereport(pgStatRunningInCollector ? LOG : WARNING,
				//		(errmsg("roleid --> %d", rolebuf.role_id)));

				if (found)
				{
					ereport(pgStatRunningInCollector ? LOG : WARNING,
							(errmsg("corrupted pgstat.stat file 10")));
					goto done;
				}

				memcpy(roleentry, &rolebuf, sizeof(rolebuf));
				roleentry->roleEntry = NULL;
				roleentry->cmdEntry = NULL;
				
				/* Arrange for the command entries for this role to be collected */
				memset(&hash_ctl, 0, sizeof(hash_ctl));
				hash_ctl.keysize = sizeof(Oid);
				hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdEntry);
				hash_ctl.hash = oid_hash;
				//hash_ctl.hcxt = pgStatLocalContext;
				hash_ctl.hcxt = TopMemoryContext;
				roleentry->cmdEntry = hash_create("Per-database per-user per-role per-command stats",
											  PGSTAT_ROLE_CMD_HASH_SIZE,
											  &hash_ctl,
											  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);				

				
				/* Ashish
				 * Arrange that following 'M's add entries to this role's hash table.
				 */
				rolecmd_hash = roleentry->cmdEntry;

				break;

			/* end of role */
			case 'r':
				rolecmd_hash = NULL;
				break;
				
				/* Ashish
				 * 'M'	A command follows.
				 */
			case 'M':
				
				if (fread(&rolecmd_buf, 1, offsetof(PgStat_StatRoleCmdEntry, tabEntry),
						  fpin) != offsetof(PgStat_StatRoleCmdEntry, tabEntry))
				{
					ereport(pgStatRunningInCollector ? LOG : WARNING,
							(errmsg("corrupted pgstat.stat file 11")));
					goto done;
				}

				/*
				 * 
				*/ 
				if (rolecmd_buf.is_under_user && !rolecmd_hash)
					break;

				if (!rolecmd_buf.is_under_user && !nbc_rolecmd_hash)
					break;
					
				if (rolecmd_buf.is_under_user)
					rolecmd_entry = (PgStat_StatRoleCmdEntry *) hash_search(rolecmd_hash,
																			(void *) &rolecmd_buf.command,
																			HASH_ENTER, 
																			&found);
				else
					nbcrolecmd_entry = (PgStat_StatRoleCmdEntry *) hash_search(nbc_rolecmd_hash,
																				(void *) &rolecmd_buf.command,
																				HASH_ENTER, 
																				&found);

			//	ereport(pgStatRunningInCollector ? LOG : WARNING,
				//		(errmsg("roleid --> %d", rolebuf.role_id)));

				if (found)
				{
					ereport(pgStatRunningInCollector ? LOG : WARNING,
							(errmsg("corrupted pgstat.stat file 12")));
					goto done;
				}

				if (rolecmd_buf.is_under_user)
				{
					memcpy(rolecmd_entry, &rolecmd_buf, sizeof(rolecmd_buf));
					rolecmd_entry->tabEntry = NULL;
				
					/* Arrange for the table entries for this command to be collected */
					memset(&hash_ctl, 0, sizeof(hash_ctl));
					hash_ctl.keysize = sizeof(uint32);
					hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabEntry);
					hash_ctl.hash = oid_hash;
					//hash_ctl.hcxt = pgStatLocalContext;
					hash_ctl.hcxt = TopMemoryContext;
					rolecmd_entry->tabEntry = hash_create("Per-database per-user per-role per-command table stats",
												  PGSTAT_ROLE_CMD_TAB_HASH_SIZE,
												  &hash_ctl,
												  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);
					
				

					/* Ashish
					 * Arrange that following 'Z's add entries to this command's hash table.
					 */
					rolecmdtab_hash = rolecmd_entry->tabEntry;
				} else
				{
					memcpy(nbcrolecmd_entry, &rolecmd_buf, sizeof(rolecmd_buf));
					nbcrolecmd_entry->tabEntry = NULL;
				}
					
				
				break;
			
				/* Ashish
				 * 'm' end of the current command 
				 */ 
			case 'm':
				rolecmdtab_hash = NULL;
				break;
												
				/* Ashish
				 * 'Z'	A command's table follows.
				 */
			case 'Z':
				if (fread(&rolecmdtab_buf, 1, offsetof(PgStat_StatRoleCmdTabEntry, colEntry),
						  fpin) != offsetof(PgStat_StatRoleCmdTabEntry, colEntry))
				{
					ereport(pgStatRunningInCollector ? LOG : WARNING,
							(errmsg("corrupted pgstat.stat file 12")));
					goto done;
				}

				/*
				 * Skip if command table belongs to a not requested database.
				*/ 
				if (rolecmdtab_buf.is_under_user && rolecmdtab_hash == NULL)
					break;
				
				if (!rolecmdtab_buf.is_under_user && nbc_roletab_hash == NULL)
					break;
								

				if (rolecmdtab_buf.is_under_user)
					rolecmdtab_entry = (PgStat_StatRoleCmdTabEntry *) hash_search(rolecmdtab_hash,
																	(void *) &rolecmdtab_buf.table,
																	HASH_ENTER, &found);
				else
					nbcroletab_entry = (PgStat_StatRoleCmdTabEntry *) hash_search(nbc_roletab_hash,
																				  (void *) &rolecmdtab_buf.table,
																				  HASH_ENTER, &found);

			//	ereport(pgStatRunningInCollector ? LOG : WARNING,
				//		(errmsg("roleid --> %d", rolebuf.role_id)));

				if (found)
				{
					ereport(pgStatRunningInCollector ? LOG : WARNING,
							(errmsg("corrupted pgstat.stat file 13")));
					goto done;
				}

				if (rolecmdtab_buf.is_under_user)
				{
					memcpy(rolecmdtab_entry, &rolecmdtab_buf, sizeof(rolecmdtab_buf));
					rolecmdtab_entry->colEntry = NULL;
					
					MemoryContext old_ctxt = MemoryContextSwitchTo(TopMemoryContext);
					rolecmdtab_entry->colEntry = (PgStat_StatRoleCmdTabColEntry *)palloc(rolecmdtab_entry->num_cols*sizeof(PgStat_StatRoleCmdTabColEntry));
					MemoryContextSwitchTo(old_ctxt);

					/*
					// Arrange for the column entries for this command's table to be collected 
					memset(&hash_ctl, 0, sizeof(hash_ctl));
					hash_ctl.keysize = sizeof(uint32);
					hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabColEntry);
					hash_ctl.hash = oid_hash;
					//hash_ctl.hcxt = pgStatLocalContext;
					hash_ctl.hcxt = TopMemoryContext;
					rolecmdtab_entry->colEntry = hash_create("Per-database per-user per-role per-command per-table per-column stats",
																PGSTAT_ROLE_CMD_TAB_COL_HASH_SIZE,
																&hash_ctl,
																HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);									
					*/
	
					/* Ashish
					 * Arrange that following 'C's add entries to this command's table's hash table.
					 */
					rolecmdtabcol_hash = rolecmdtab_entry->colEntry;
				}else
				{
					memcpy(nbcroletab_entry, &rolecmdtab_buf, sizeof(rolecmdtab_buf));
					nbcroletab_entry->colEntry = NULL;

					if (IDR_LEVEL == 1 || IDR_LEVEL == 2)
					{

	                    MemoryContext old_ctxt = MemoryContextSwitchTo(TopMemoryContext);
    	                nbcroletab_entry->colEntry = (PgStat_StatRoleCmdTabColEntry *)palloc(nbcroletab_entry->num_cols*sizeof(PgStat_StatRoleCmdTabColEntry));
        	            old_ctxt = MemoryContextSwitchTo(old_ctxt);

						/*
						memset(&hash_ctl, 0, sizeof(hash_ctl));
						hash_ctl.keysize = sizeof(uint32);
						hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabColEntry);
						hash_ctl.hash = oid_hash;
						//hash_ctl.hcxt = pgStatLocalContext;
						hash_ctl.hcxt = TopMemoryContext;
						nbcroletab_entry->colEntry = hash_create("Per-database per-user per-role per-command per-table per-column stats",
																	PGSTAT_ROLE_CMD_TAB_COL_HASH_SIZE,
																	&hash_ctl,
																	HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);
						*/
	
						nbc_roletabcol_hash = nbcroletab_entry->colEntry;
						
					}
					
				}
									
				break;
			
				/* Ashish
				 * 'z' end of the current command table 
				 */ 
			case 'z':
				rolecmdtabcol_hash = NULL;
				nbc_roletabcol_hash = NULL;
				break;

				/* Ashish
				 * 'C'	A command's table's column follows.
				 */
			case 'C':
				if (fread(&rolecmdtabcol_buf, 1, sizeof(PgStat_StatRoleCmdTabColEntry),
						  fpin) != sizeof(PgStat_StatRoleCmdTabColEntry))
				{
					ereport(pgStatRunningInCollector ? LOG : WARNING,
							(errmsg("corrupted pgstat.stat file 14")));
					goto done;
				}

				/*
				 * Skip if command table column belongs to a not requested database.
				*/ 
				if (rolecmdtabcol_buf.is_under_user && rolecmdtabcol_hash == NULL)
					break;
				
				if (!rolecmdtabcol_buf.is_under_user && nbc_roletabcol_hash == NULL && IDR_LEVEL != 0)
					break;

				if (rolecmdtabcol_buf.is_under_user)
					rolecmdtabcol_entry = (PgStat_StatRoleCmdTabColEntry *)(rolecmdtabcol_hash + rolecmdtabcol_buf.column + COL_SHIFT);
					/*
					rolecmdtabcol_entry = (PgStat_StatRoleCmdTabColEntry *) hash_search(rolecmdtabcol_hash,
																			(void *) &rolecmdtabcol_buf.column,
																			HASH_ENTER, &found);
					*/
				else
				{
					if (IDR_LEVEL == 1 || IDR_LEVEL == 2)
						nbcroletabcol_entry = (PgStat_StatRoleCmdTabColEntry *)(nbc_roletabcol_hash + rolecmdtabcol_buf.column + COL_SHIFT);
						/*
						nbcroletabcol_entry = (PgStat_StatRoleCmdTabColEntry *) hash_search(nbc_roletabcol_hash,
																							(void *) &rolecmdtabcol_buf.column,
																							HASH_ENTER, &found);
						*/
					else if(IDR_LEVEL == 0)
						nbcrolecol_entry = (PgStat_StatRoleCmdTabColEntry *) hash_search(nbc_rolecol_hash,
																						(void *) &rolecmdtabcol_buf.column,
																						 HASH_ENTER, &found);							
				}
					
				if (found)
				{
					ereport(pgStatRunningInCollector ? LOG : WARNING,
							(errmsg("corrupted pgstat.stat file 15 (column = %d)",rolecmdtabcol_buf.column)));
					goto done;
				}

				if (rolecmdtabcol_buf.is_under_user)
					memcpy(rolecmdtabcol_entry, &rolecmdtabcol_buf, sizeof(rolecmdtabcol_buf));
				else
				{
					if (IDR_LEVEL == 1 || IDR_LEVEL == 2)
						memcpy(nbcroletabcol_entry, &rolecmdtabcol_buf, sizeof(rolecmdtabcol_buf));
					else if(IDR_LEVEL == 0)
						memcpy(nbcrolecol_entry, &rolecmdtabcol_buf, sizeof(rolecmdtabcol_buf));					
				}					
									
				break;
															
				/*
				 * 'E'	The EOF marker of a complete stats file.
				 */
			case 'E':
				goto done;

			default:
				ereport(pgStatRunningInCollector ? LOG : WARNING,
						(errmsg("corrupted pgstat.stat file 16: got --> %c",marker)));
				goto done;
		}
	}

done:
	FreeFile(fpin);

	/* %%% Ashish : v 1.178 */
 	if (permanent)
 		unlink(PGSTAT_STAT_PERMANENT_FILENAME);
 

	return dbhash;
}

/*
 * If not already done, read the statistics collector stats file into
 * some hash tables.  The results will be kept until pgstat_clear_snapshot()
 * is called (typically, at end of transaction).
 */
static void
backend_read_statsfile(void)
{
	/* already read it? */
	if (pgStatDBHash)
		return;
	Assert(!pgStatRunningInCollector);

	/* Autovacuum launcher wants stats about all databases */
	
	/*
	if (IsAutoVacuumLauncherProcess())
		pgStatDBHash = pgstat_read_statsfile(InvalidOid);
	else
		pgStatDBHash = pgstat_read_statsfile(MyDatabaseId);
	*/
	
	/* %%% Ashish : v 1.178 */
  	if (IsAutoVacuumLauncherProcess())
 		pgStatDBHash = pgstat_read_statsfile(InvalidOid, false);
  	else
 		pgStatDBHash = pgstat_read_statsfile(MyDatabaseId, false);

}


/* ----------
 * pgstat_setup_memcxt() -
 *
 *	Create pgStatLocalContext, if not already done.
 * ----------
 */
static void
pgstat_setup_memcxt(void)
{
	if (!pgStatLocalContext)
		pgStatLocalContext = AllocSetContextCreate(TopMemoryContext,
												   "Statistics snapshot",
												   ALLOCSET_SMALL_MINSIZE,
												   ALLOCSET_SMALL_INITSIZE,
												   ALLOCSET_SMALL_MAXSIZE);
}


/* ----------
 * pgstat_clear_snapshot() -
 *
 *	Discard any data collected in the current transaction.	Any subsequent
 *	request will cause new snapshots to be read.
 *
 *	This is also invoked during transaction commit or abort to discard
 *	the no-longer-wanted snapshot.
 * ----------
 */
void
pgstat_clear_snapshot(void)
{
	/* Release memory, if any was allocated */
	if (pgStatLocalContext)
		MemoryContextDelete(pgStatLocalContext);

	/* Reset variables */
	pgStatLocalContext = NULL;

	// Its in Top Memory Context now :-)
	//pgStatDBHash = NULL; 
	localBackendStatusTable = NULL;
	localNumBackends = 0;
}


/* ----------
 * pgstat_recv_tabstat() -
 *
 *	Count what the backend has done.
 * ----------
 */
static void
pgstat_recv_tabstat(PgStat_MsgTabstat *msg, int len)
{
	PgStat_TableEntry *tabmsg = &(msg->m_entry[0]);
	PgStat_StatDBEntry *dbentry;
	PgStat_StatTabEntry *tabentry;
	int			i;
	bool		found;

	dbentry = pgstat_get_db_entry(msg->m_databaseid, true);

	/*
	 * Update database-wide stats.
	 */
	dbentry->n_xact_commit += (PgStat_Counter) (msg->m_xact_commit);
	dbentry->n_xact_rollback += (PgStat_Counter) (msg->m_xact_rollback);

	/*
	 * Process all table entries in the message.
	 */
	for (i = 0; i < msg->m_nentries; i++)
	{
		tabentry = (PgStat_StatTabEntry *) hash_search(dbentry->tables,
												  (void *) &(tabmsg[i].t_id),
													   HASH_ENTER, &found);

		if (!found)
		{
			/*
			 * If it's a new table entry, initialize counters to the values we
			 * just got.
			 */
			tabentry->numscans = tabmsg[i].t_counts.t_numscans;
			tabentry->tuples_returned = tabmsg[i].t_counts.t_tuples_returned;
			tabentry->tuples_fetched = tabmsg[i].t_counts.t_tuples_fetched;
			tabentry->tuples_inserted = tabmsg[i].t_counts.t_tuples_inserted;
			tabentry->tuples_updated = tabmsg[i].t_counts.t_tuples_updated;
			tabentry->tuples_deleted = tabmsg[i].t_counts.t_tuples_deleted;
			tabentry->tuples_hot_updated = tabmsg[i].t_counts.t_tuples_hot_updated;
			tabentry->n_live_tuples = tabmsg[i].t_counts.t_new_live_tuples;
			tabentry->n_dead_tuples = tabmsg[i].t_counts.t_new_dead_tuples;
			tabentry->blocks_fetched = tabmsg[i].t_counts.t_blocks_fetched;
			tabentry->blocks_hit = tabmsg[i].t_counts.t_blocks_hit;

			tabentry->last_anl_tuples = 0;
			tabentry->vacuum_timestamp = 0;
			tabentry->autovac_vacuum_timestamp = 0;
			tabentry->analyze_timestamp = 0;
			tabentry->autovac_analyze_timestamp = 0;
		}
		else
		{
			/*
			 * Otherwise add the values to the existing entry.
			 */
			tabentry->numscans += tabmsg[i].t_counts.t_numscans;
			tabentry->tuples_returned += tabmsg[i].t_counts.t_tuples_returned;
			tabentry->tuples_fetched += tabmsg[i].t_counts.t_tuples_fetched;
			tabentry->tuples_inserted += tabmsg[i].t_counts.t_tuples_inserted;
			tabentry->tuples_updated += tabmsg[i].t_counts.t_tuples_updated;
			tabentry->tuples_deleted += tabmsg[i].t_counts.t_tuples_deleted;
			tabentry->tuples_hot_updated += tabmsg[i].t_counts.t_tuples_hot_updated;
			tabentry->n_live_tuples += tabmsg[i].t_counts.t_new_live_tuples;
			tabentry->n_dead_tuples += tabmsg[i].t_counts.t_new_dead_tuples;
			tabentry->blocks_fetched += tabmsg[i].t_counts.t_blocks_fetched;
			tabentry->blocks_hit += tabmsg[i].t_counts.t_blocks_hit;
		}

		/* Clamp n_live_tuples in case of negative new_live_tuples */
		tabentry->n_live_tuples = Max(tabentry->n_live_tuples, 0);
		/* Likewise for n_dead_tuples */
		tabentry->n_dead_tuples = Max(tabentry->n_dead_tuples, 0);

		/*
		 * Add per-table stats to the per-database entry, too.
		 */
		dbentry->n_tuples_returned += tabmsg[i].t_counts.t_tuples_returned;
		dbentry->n_tuples_fetched += tabmsg[i].t_counts.t_tuples_fetched;
		dbentry->n_tuples_inserted += tabmsg[i].t_counts.t_tuples_inserted;
		dbentry->n_tuples_updated += tabmsg[i].t_counts.t_tuples_updated;
		dbentry->n_tuples_deleted += tabmsg[i].t_counts.t_tuples_deleted;
		dbentry->n_blocks_fetched += tabmsg[i].t_counts.t_blocks_fetched;
		dbentry->n_blocks_hit += tabmsg[i].t_counts.t_blocks_hit;
	}
}


/* ----------
 * 
 * Ashish
 * 
 * pgstat_recv_loginstat() -
 *
 *	Count what the backend has done for the login user
 * ----------
 */
void
pgstat_recv_loginstat(PgStat_MsgLoginStat *msg, int len)
{
	
	Oid						login_oid = msg->m_loginentry[0];
	
	PgStat_StatDBEntry 	 	*dbentry;
	PgStat_StatRoleEntry 	*stat_loginentry;
	PgStat_StatRoleEntry 	*stat_roleentry;
 
	HASHCTL			    hash_ctl;
	int 				i,j,k;
	bool				found;	
	
	dbentry = pgstat_get_db_entry(msg->m_databaseid, true);
	
	if (!dbentry)
		return;

#ifdef B_NET
	/* search for the login user in the dbentry->users hashtable, if do not find it then create it */
	stat_loginentry = (PgStat_StatRoleEntry *) hash_search(dbentry->users, 
														   (void *) &login_oid, 
															HASH_ENTER, 
															&found);
	
	/* initialize the newly created user entry */	
	if (!found)
	{
		stat_loginentry->roleEntry = NULL;
		stat_loginentry->cmdEntry = NULL;

		stat_loginentry->all_tab_count = 0;
		stat_loginentry->all_col_count = 0;
		stat_loginentry->num_col_entries = 0;
		
		stat_loginentry->tabEntry = NULL;
		stat_loginentry->colEntry = NULL;			
		
		stat_loginentry->role_id = login_oid;
		stat_loginentry->is_under_user = true;
		
		stat_loginentry->login_or_activation_count = 0;
		stat_loginentry->all_cmd_count = 0;
		//stat_loginentry->nb_prob = 0.0;
		
		memset(&hash_ctl, 0, sizeof(hash_ctl));
		hash_ctl.keysize = sizeof(Oid);
		hash_ctl.entrysize = sizeof(PgStat_StatRoleEntry);
		hash_ctl.hash = oid_hash;
		hash_ctl.hcxt = pgStatLocalContext;
		stat_loginentry->roleEntry = hash_create("Per-database per-user per-role stats",
										  PGSTAT_ROLE_HASH_SIZE,
										  &hash_ctl,
										  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);				
	}
	
	/* now increment the count for this login user */
	stat_loginentry->login_or_activation_count += 1;

#endif
		
	/* do this for all the role id entries in the login stat msg */
	for (i = 1; i < msg->m_entries; i++)		
	{

#ifdef B_NET

		/* search for the role in the stat_loginentry->roleEntry hashtable, if do not find it then create it */
		stat_roleentry = (PgStat_StatRoleEntry *) hash_search(stat_loginentry->roleEntry, 
															   (void *) &msg->m_loginentry[i], 
																HASH_ENTER, 
																&found);
		
		/* initialize the newly created role entry */	
		if (!found)
		{
			stat_roleentry->roleEntry = NULL;
			stat_roleentry->cmdEntry = NULL;
			
			/* for the nbc */
			stat_roleentry->tabEntry = NULL;
			stat_roleentry->colEntry = NULL; /* only for coarse triplets */
			stat_roleentry->all_tab_count = 0;
			stat_roleentry->all_col_count = 0;
			stat_roleentry->num_col_entries = 0;
			/* for the nbc */

			stat_roleentry->is_under_user = true;	/* because this role entry calculates per user stats */
			stat_roleentry->role_id = msg->m_loginentry[i];			
			stat_roleentry->login_or_activation_count = 0;
			stat_roleentry->all_cmd_count = 0;
			//stat_roleentry->nb_prob = 0.0;
			
			memset(&hash_ctl, 0, sizeof(hash_ctl));
			hash_ctl.keysize = sizeof(Oid);
			hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdEntry);
			hash_ctl.hash = oid_hash;
			hash_ctl.hcxt = pgStatLocalContext;
			stat_roleentry->cmdEntry = hash_create("Per-database per-user per-role per-command stats",
											  PGSTAT_ROLE_CMD_HASH_SIZE,
											  &hash_ctl,
											  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);							
			
		}

		/* now increment the activation count for this role*/
		stat_roleentry->login_or_activation_count += 1;
		
#endif
		
		/* for the naive bayes classifier */
		stat_roleentry = (PgStat_StatRoleEntry *) hash_search(dbentry->users, 
															   (void *) &msg->m_loginentry[i], 
																HASH_ENTER, 
																&found);
		
		/* initialize the newly created role entry */	
		if (!found)
		{
			/* to calculate the m-estimate of probability for a role prior */
			dbentry->num_roles++;
			
			stat_roleentry->roleEntry = NULL;
			stat_roleentry->cmdEntry = NULL;			

			/* for the nbc */
			stat_roleentry->tabEntry = NULL;
			stat_roleentry->colEntry = NULL; /* only for coarse triplets */
			stat_roleentry->all_tab_count = 0;
			stat_roleentry->all_col_count = 0;
			stat_roleentry->num_col_entries = 0;
			/* for the nbc */
			
			stat_roleentry->is_under_user = false;
			stat_roleentry->role_id = msg->m_loginentry[i];			
			stat_roleentry->login_or_activation_count = 0;
			stat_roleentry->all_cmd_count = 0;						
			
			memset(&hash_ctl, 0, sizeof(hash_ctl));
			hash_ctl.keysize = sizeof(Oid);
			hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdEntry);
			hash_ctl.hash = oid_hash;
			hash_ctl.hcxt = pgStatLocalContext;
			stat_roleentry->cmdEntry = hash_create("Per-database per-role per-command stats",
											  PGSTAT_ROLE_CMD_HASH_SIZE,
											  &hash_ctl,
											  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);										

			memset(&hash_ctl, 0, sizeof(hash_ctl));
			hash_ctl.keysize = sizeof(Oid);
			hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabEntry);
			hash_ctl.hash = oid_hash;
			hash_ctl.hcxt = pgStatLocalContext;
			stat_roleentry->tabEntry = hash_create("Per-database per-role per-table stats",
											  PGSTAT_ROLE_CMD_TAB_HASH_SIZE,
											  &hash_ctl,
											  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);			
																		
			
			/* only used for coarse triplets */
			if (IDR_LEVEL == 0)
			{			
				memset(&hash_ctl, 0, sizeof(hash_ctl));
				hash_ctl.keysize = sizeof(Oid);
				hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabColEntry);
				hash_ctl.hash = oid_hash;
				hash_ctl.hcxt = pgStatLocalContext;
				stat_roleentry->colEntry = hash_create("Per-database per-role per-num_cols stats",
												  PGSTAT_ROLE_CMD_TAB_COL_HASH_SIZE,
												  &hash_ctl,
												  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);							
			}			
			
		}

		/* now increment the activation count for this role */
		stat_roleentry->login_or_activation_count += 1;
				
	}		
	
}

/* ----------
 * 
 * Ashish
 * 
 * pgstat_recv_taboidstat() -
 *
 * ----------
 */
void
pgstat_recv_taboidstat(PgStat_MsgTabOidStat *msg, int len)
{	
	PgStat_StatDBEntry 	 	*dbentry;
	PgStat_StatRoleEntry 	*stat_roleentry;
 
	HASHCTL			    hash_ctl;
	int 				i,j,k;
	bool				found;	
	
	HASH_SEQ_STATUS			hstat;
	
	dbentry = pgstat_get_db_entry(msg->m_databaseid, true);
	
	if (!dbentry)
		return;
		
	/* do the following for all the roles of in dbentry->users. The same needs to be done for all the roles
	 * under the current user which I will do later as it is not required by
	 * the naive bayses classifier */
	
	hash_seq_init(&hstat, dbentry->users);
	while ((stat_roleentry = (PgStat_StatRoleEntry *) hash_seq_search(&hstat)) != NULL)
	{
		if (stat_roleentry->is_under_user)
			continue;

		for (i = 0; i < msg->m_entries; i++)
		{
			Oid rel_oid = msg->m_taboidentry[i].tab_oid;
			
			/* populate this hash table with all the tables in the database for IDR_LEVEL == 1 and 2 and for IDR_LEVEL == 2,
			 * populate each column num as well */
			PgStat_StatRoleCmdTabEntry *stat_roletabentry = (PgStat_StatRoleCmdTabEntry *) hash_search(stat_roleentry->tabEntry, 
						   																					(void *) &rel_oid, 
						   																					HASH_ENTER, 
						   																					&found);
			if (found)
				continue;
			
			stat_roletabentry->table = rel_oid;
			stat_roletabentry->command = 0;			
			stat_roletabentry->count = 0;
			stat_roletabentry->count_0 = 0;
			stat_roletabentry->all_col_count = 0;
			stat_roletabentry->is_under_user = stat_roleentry->is_under_user;

	        /* no need to initialize the column hashtable for IDR_LEVEL == 1 */
            if (IDR_LEVEL == 1)
                continue;

			stat_roletabentry->num_cols = msg->m_taboidentry[i].num_atts + 1;
			stat_roletabentry->colEntry = (PgStat_StatRoleCmdTabColEntry *)palloc(stat_roletabentry->num_cols*sizeof(PgStat_StatRoleCmdTabColEntry));

			/*			
			memset(&hash_ctl, 0, sizeof(hash_ctl));
			hash_ctl.keysize = sizeof(Oid);
			hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabColEntry);
			hash_ctl.hash = oid_hash;
			hash_ctl.hcxt = pgStatLocalContext;
			stat_roletabentry->colEntry = hash_create("Per-database per-role per-command per-table per-column stats",
											  PGSTAT_ROLE_CMD_TAB_COL_HASH_SIZE,
											  &hash_ctl,
											  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);
			*/

			//for (k = 0; k <= (msg->m_taboidentry[i].num_atts + 1); k++)		
			for (k = 0; k < (stat_roletabentry->num_cols); k++) // k = 0 will be unused
			{
		       int column = SYSCOLSTARTNUM + k; // SYSCOLSTARTNUM ... -1 are the system columns

				(stat_roletabentry->colEntry[k]).column = column;
				(stat_roletabentry->colEntry[k]).table = rel_oid;
				(stat_roletabentry->colEntry[k]).command = 0;
				(stat_roletabentry->colEntry[k]).count = 0;
				(stat_roletabentry->colEntry[k]).count_0 = 0;
				(stat_roletabentry->colEntry[k]).is_under_user = false;

/*
				int column = SYSCOLSTARTNUM + k; // SYSCOLSTARTNUM ... -1 are the system columns 
				
				// no column with column num == 0 
				if (column == 0)
					continue;
				
				PgStat_StatRoleCmdTabColEntry *stat_roletabcolentry = (PgStat_StatRoleCmdTabColEntry *) hash_search(stat_roletabentry->colEntry, 
						   																							(void *) &column, 
						   																							HASH_ENTER, 
						   																							&found);										
				if (found)
					continue;
				
				stat_roletabcolentry->column = column;
				stat_roletabcolentry->table = rel_oid;
				stat_roletabcolentry->command = 0;			
				stat_roletabcolentry->count = 0;	
				stat_roletabcolentry->count_0 = 0;
				stat_roletabcolentry->is_under_user = false;
*/

				stat_roleentry->num_col_entries++;				
																												
			}										
		}
	}	
}


/* ----------
 * 
 * Ashish
 * 
 * pgstat_recv_rolestat() -
 *
 *	Count what the backend has done for the roles .. to be converted to a static function .. 
 * ----------
 */
#ifdef _TMP
void
pgstat_recv_rolestat(PgStat_MsgRoleStat *msg, int len)
{
	
	PgStat_RoleEntry 				*msg_roleentry;

	/* new code */
	PgStat_CmdEntry					*msg_cmdentry;
	PgStat_TabEntry					*msg_tabentry;
	PgStat_ColEntry					*msg_colentry;
	/* new code */

	PgStat_StatDBEntry 	 			*dbentry;
	PgStat_StatRoleEntry 			*stat_roleentry;
	PgStat_StatRoleEntry			*stat_userentry;
	PgStat_StatRoleCmdEntry			*stat_rolecmdentry;
	PgStat_StatRoleCmdTabEntry		*stat_rolecmdtabentry;
	PgStat_StatRoleCmdTabColEntry	*stat_rolecmdtabcolentry;
	
	PgStat_StatRoleEntry 			*dbstat_roleentry;
	PgStat_StatRoleCmdEntry			*dbstat_rolecmdentry;
	PgStat_StatRoleCmdTabEntry		*dbstat_roletabentry;
	PgStat_StatRoleCmdTabColEntry	*dbstat_roletabcolentry;	
	PgStat_StatRoleCmdTabColEntry	*dbstat_rolecolentry;		/* only used for coarse triplets */
 
	struct rolecmdentry
	{
		Oid		role_id;		
		int		cmd_count;;
	}*tmp_cmdentry;
	
	PgStat_StatRoleCmdTabEntry		*tmp_tabentry;
	
	HASHCTL			    hash_ctl;
	int 				i, j, k, l;
	bool				found;	
	int 				userid, all_cmd_counts;
	
	HTAB			   *tmp_msg_tab_htab = NULL;
	HTAB			   *tmp_msg_cmd_htab = NULL;
	
	List	   		   *roles_list	 	 = NULL;
	HASH_SEQ_STATUS	   hstat1, hstat2, hstat3;
	
	elog(NOTICE, "stats received");

	dbentry = pgstat_get_db_entry(msg->m_databaseid, true);
	
	if (!dbentry)
		return;

#ifdef B_NET
	userid = msg->m_userid;

/*
	stat_userentry = (PgStat_StatRoleEntry *)hash_search(dbentry->users, 
														(void *)&userid, 
														HASH_FIND, 
														NULL);
	if (!stat_userentry)
		return;
<<<<<<< .mine
*/		
=======
#endif

>>>>>>> .r86
	/* create a temp hash table to store all the table ids */
	memset(&hash_ctl, 0, sizeof(hash_ctl));
	hash_ctl.keysize = sizeof(Oid);
	hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabEntry);
	hash_ctl.hash = oid_hash;
	hash_ctl.hcxt = pgStatLocalContext;
	tmp_msg_tab_htab = hash_create("tmp hash table tab",
									 PGSTAT_ROLE_CMD_TAB_HASH_SIZE,
									 &hash_ctl,
									 HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);
	
	/* create a temp hash table to store the num commands per role */
	memset(&hash_ctl, 0, sizeof(hash_ctl));
	hash_ctl.keysize = sizeof(Oid);
	hash_ctl.entrysize = sizeof(struct rolecmdentry);
	hash_ctl.hash = oid_hash;
	hash_ctl.hcxt = pgStatLocalContext;
	tmp_msg_cmd_htab = hash_create("tmp hash table cmd",
									 PGSTAT_ROLE_CMD_HASH_SIZE,
									 &hash_ctl,
									 HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);
	
		
	//for (i = 0; i < msg->m_entries; i++)
	for (i = 0; i < msg->m_role_entries; i++)
	{
<<<<<<< .mine
		msg_roleentry = &msg->m_roleentry[i];

/*				
=======
		//msg_roleentry = &msg->m_roleentry[i];

#ifdef B_NET
>>>>>>> .r86
		stat_roleentry = (PgStat_StatRoleEntry *) hash_search(stat_userentry->roleEntry, 
															   //(void *) &msg_roleentry->role_id,
																 (void *) &(msg->m_roleentry[i].role_id),
																HASH_FIND, 
																&found);
		if (!found)
		{
			elog(LOG, "Encountered a PgStat_MsgRoleStat message with role (%d) that has not been activated.", msg->m_roleentry[i].role_id);
			continue;
		}
<<<<<<< .mine
*/		
=======
#endif
		
>>>>>>> .r86
		/* for the nbc:start */
		dbstat_roleentry = (PgStat_StatRoleEntry *) hash_search(dbentry->users, 
															   //(void *) &msg_roleentry->role_id,
																(void *) &(msg->m_roleentry[i].role_id),
																HASH_FIND, 
																&found);
		if (!found)
		{
			elog(LOG, "Encountered a PgStat_MsgRoleStat message with role (%d) that has not been activated.", msg->m_roleentry[i].role_id);
			continue;
		}
		/* for the nbc:end */
		
		/* store this role for future */
		//roles_list = list_append_unique_oid(roles_list, msg_roleentry->role_id);
		roles_list = list_append_unique_oid(roles_list, msg->m_roleentry[i].role_id);
		
<<<<<<< .mine
		/***************** count the commands *********************/		
/*
		stat_rolecmdentry = (PgStat_StatRoleCmdEntry *) hash_search(stat_roleentry->cmdEntry, 
=======
		/***************** count the commands *********************/
		for (j = 0; j < msg->m_roleentry[i].m_cmd_entries; j++)
		{
			//msg_cmdentry = &msg_roleentry->m_cmdentry[j];

#ifdef B_NET
			/*
			stat_rolecmdentry = (PgStat_StatRoleCmdEntry *) hash_search(stat_roleentry->cmdEntry,
>>>>>>> .r86
															   (void *) &msg_roleentry->command, 
																HASH_ENTER, 
																&found);
			*/
			stat_rolecmdentry = (PgStat_StatRoleCmdEntry *) hash_search(stat_roleentry->cmdEntry,
															   //(void *) &msg_cmdentry->command,
																(void *)&(msg->m_roleentry[i].m_cmdentry[j].command),
																HASH_ENTER,
																&found);


			if (!found)
			{
				//stat_rolecmdentry->command = msg_roleentry->command;
				stat_rolecmdentry->command = msg->m_roleentry[i].m_cmdentry[j].command;
				stat_rolecmdentry->count = 0;
				stat_rolecmdentry->all_tab_count = 0;
				stat_rolecmdentry->is_under_user = true;

				memset(&hash_ctl, 0, sizeof(hash_ctl));
				hash_ctl.keysize = sizeof(Oid);
				hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabEntry);
				hash_ctl.hash = oid_hash;
				hash_ctl.hcxt = pgStatLocalContext;
				stat_rolecmdentry->tabEntry = hash_create("Per-database per-user per-role per-command per-table stats",
												  PGSTAT_ROLE_CMD_TAB_HASH_SIZE,
												  &hash_ctl,
												  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);

			}

			//stat_rolecmdentry->count += msg_roleentry->command_count;
			//stat_userentry->all_cmd_count += msg_roleentry->command_count;
			//stat_roleentry->all_cmd_count += msg_roleentry->command_count;
			//dbentry->all_roles_cmd_count += msg_roleentry->command_count;

			stat_rolecmdentry->count += msg->m_roleentry[i].m_cmdentry[j].command_count;
			stat_userentry->all_cmd_count += msg->m_roleentry[i].m_cmdentry[j].command_count;
			stat_roleentry->all_cmd_count += msg->m_roleentry[i].m_cmdentry[j].command_count;
			dbentry->all_roles_cmd_count += msg->m_roleentry[i].m_cmdentry[j].command_count;
#endif
			
<<<<<<< .mine
			tmp_cmdentry->cmd_count += msg_roleentry->command_count;
		}
*/
		
		/* for the nbc:start */
		dbstat_rolecmdentry = (PgStat_StatRoleCmdEntry *) hash_search(dbstat_roleentry->cmdEntry, 
															   (void *) &msg_roleentry->command, 
																HASH_ENTER, 
																&found);
		
		if (!found)
		{
			dbstat_rolecmdentry->command = msg_roleentry->command;
			dbstat_rolecmdentry->count = 0;
			dbstat_rolecmdentry->all_tab_count = 0;
			dbstat_rolecmdentry->is_under_user = false;
=======
			/*
			if (msg_roleentry->command_count != 0)
			{
				tmp_cmdentry = (struct rolecmdentry *) hash_search(tmp_msg_cmd_htab,
																	 (void *) &msg_roleentry->role_id,
																	HASH_ENTER,
																	&found);
>>>>>>> .r86
			
				if (!found)
				{
					memset(tmp_cmdentry, 0, sizeof(struct rolecmdentry));
					tmp_cmdentry->role_id = msg_roleentry->role_id;
				}

				tmp_cmdentry->cmd_count += msg_roleentry->command_count;
			}
			*/

			if (msg->m_roleentry[i].m_cmdentry[j].command_count != 0)
			{
				tmp_cmdentry = (struct rolecmdentry *) hash_search(tmp_msg_cmd_htab,
																	 //(void *) &msg_roleentry->role_id,
																	(void *)&(msg->m_roleentry[i].role_id),
																	HASH_ENTER,
																	&found);
			
<<<<<<< .mine
		}
		dbstat_rolecmdentry->count += msg_roleentry->command_count;
		dbstat_roleentry->all_cmd_count += msg_roleentry->command_count; 						
		/* for the nbc:end */				
		
		/***************** count the tables *********************/				
/*
		stat_rolecmdtabentry = (PgStat_StatRoleCmdTabEntry *) hash_search(stat_rolecmdentry->tabEntry, 
				   														(void *) &msg_roleentry->table, 
				   														HASH_ENTER, 
				   														&found);
=======
				if (!found)
				{
					memset(tmp_cmdentry, 0, sizeof(struct rolecmdentry));
					tmp_cmdentry->role_id = msg->m_roleentry[i].role_id;
				}
>>>>>>> .r86
				
				tmp_cmdentry->cmd_count += msg->m_roleentry[i].m_cmdentry[j].command_count;
			}
			

			/* for the nbc:start */
			/*
			dbstat_rolecmdentry = (PgStat_StatRoleCmdEntry *) hash_search(dbstat_roleentry->cmdEntry,
																   (void *) &msg_roleentry->command,
																	HASH_ENTER,
																	&found);
			*/
*/

<<<<<<< .mine
		/* for the nbc: start */
		dbstat_roletabentry = (PgStat_StatRoleCmdTabEntry *) hash_search(dbstat_roleentry->tabEntry, 
						   														(void *) &msg_roleentry->table, 
						   														HASH_ENTER, 
						   														&found);
		
		if (!found)
		{
			/*
			dbstat_roletabentry->table = msg_roleentry->table;
			dbstat_roletabentry->colEntry = NULL;
			dbstat_roletabentry->command = 0;			
			dbstat_roletabentry->count = 0;
			dbstat_roletabentry->count_0 = 0;
			dbstat_roletabentry->all_col_count = 0;
			dbstat_roletabentry->is_under_user = false;
=======
			/* for the nbc:start */
			dbstat_rolecmdentry = (PgStat_StatRoleCmdEntry *) hash_search(dbstat_roleentry->cmdEntry,
																   (void *) &(msg->m_roleentry[i].m_cmdentry[j].command),
																	HASH_ENTER,
																	&found);
>>>>>>> .r86
			
<<<<<<< .mine
			// used for fine and medium triplets 
			memset(&hash_ctl, 0, sizeof(hash_ctl));
			hash_ctl.keysize = sizeof(Oid);
			hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabColEntry);
			hash_ctl.hash = oid_hash;
			hash_ctl.hcxt = pgStatLocalContext;
			dbstat_roletabentry->colEntry = hash_create("Per-database per-role per-table per-column stats",
											  PGSTAT_ROLE_CMD_TAB_COL_HASH_SIZE,
											  &hash_ctl,
											  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);							
=======
			if (!found)
			{
				//dbstat_rolecmdentry->command = msg_roleentry->command;
				dbstat_rolecmdentry->command = msg->m_roleentry[i].m_cmdentry[j].command;
				dbstat_rolecmdentry->count = 0;
				dbstat_rolecmdentry->all_tab_count = 0;
				dbstat_rolecmdentry->is_under_user = false;

				memset(&hash_ctl, 0, sizeof(hash_ctl));
				hash_ctl.keysize = sizeof(Oid);
				hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabEntry);
				hash_ctl.hash = oid_hash;
				hash_ctl.hcxt = pgStatLocalContext;
				dbstat_rolecmdentry->tabEntry = hash_create("Per-database per-role per-command per-table stats",
												  PGSTAT_ROLE_CMD_TAB_HASH_SIZE,
												  &hash_ctl,
												  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);

			}
>>>>>>> .r86
			
<<<<<<< .mine
			*/

            elog(ERROR, "Encountered a PgStat_MsgRoleStat message with table (%d) that has not been activated.", msg_roleentry->table);
            return;
=======
			//dbstat_rolecmdentry->count += msg_roleentry->command_count;
			//dbstat_roleentry->all_cmd_count += msg_roleentry->command_count;
			
			dbstat_rolecmdentry->count += msg->m_roleentry[i].m_cmdentry[j].command_count;
			dbstat_roleentry->all_cmd_count += msg->m_roleentry[i].m_cmdentry[j].command_count;
			
>>>>>>> .r86
			/* for the nbc:end */

			/***************** count the tables *********************/
			for (k = 0; k < msg->m_roleentry[i].m_cmdentry[j].m_tab_entries; k++)
			{
				//msg_tabentry = &msg_cmdentry->m_tabentry[k];
#ifdef B_NET
				/*
				stat_rolecmdtabentry = (PgStat_StatRoleCmdTabEntry *) hash_search(stat_rolecmdentry->tabEntry,
																	(void *) &msg_roleentry->table,
																	HASH_ENTER,
																	&found);
				*/
				stat_rolecmdtabentry = (PgStat_StatRoleCmdTabEntry *) hash_search(stat_rolecmdentry->tabEntry,
																	(void *) &(msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].table),
																	HASH_ENTER,
																	&found);

				if (!found)
				{
					//stat_rolecmdtabentry->table = msg_roleentry->table;
					//stat_rolecmdtabentry->command = msg_roleentry->command;
					stat_rolecmdtabentry->table = msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].table;
					stat_rolecmdtabentry->command = msg->m_roleentry[i].m_cmdentry[j].command;
					stat_rolecmdtabentry->count = 0;
					stat_rolecmdtabentry->count_0 = 0;
					stat_rolecmdtabentry->all_col_count = 0;
					stat_rolecmdtabentry->is_under_user = true;

					memset(&hash_ctl, 0, sizeof(hash_ctl));
					hash_ctl.keysize = sizeof(Oid);
					hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabColEntry);
					hash_ctl.hash = oid_hash;
					hash_ctl.hcxt = pgStatLocalContext;
					stat_rolecmdtabentry->colEntry = hash_create("Per-database per-role per-command per-table per-column stats",
													  PGSTAT_ROLE_CMD_TAB_COL_HASH_SIZE,
													  &hash_ctl,
													  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);
				}

				//stat_rolecmdtabentry->count += msg_roleentry->table_count;
				//stat_rolecmdentry->all_tab_count += msg_roleentry->table_count;
				stat_rolecmdtabentry->count += msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].table_count;
				stat_rolecmdentry->all_tab_count += msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].table_count;
#endif

				/* for the nbc: start */
				/*
				dbstat_roletabentry = (PgStat_StatRoleCmdTabEntry *) hash_search(dbstat_roleentry->tabEntry,
						   														(void *) &msg_roleentry->table,
						   														HASH_ENTER,
						   														&found);
				*/
				
<<<<<<< .mine
		/***************** count the columns *********************/
		/* to be fixed for the change in colEntry type .... 
		stat_rolecmdtabcolentry = (PgStat_StatRoleCmdTabColEntry *)stat_rolecmdtabentry->colEntry[&msg_roleentry->column + COL_SHIFT];

		
		stat_rolecmdtabcolentry = (PgStat_StatRoleCmdTabColEntry *) hash_search(stat_rolecmdtabentry->colEntry, 
						   														(void *) &msg_roleentry->column, 
						   														HASH_ENTER, 
=======
				dbstat_roletabentry = (PgStat_StatRoleCmdTabEntry *) hash_search(dbstat_roleentry->tabEntry,
						   														(void *) &(msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].table),
						   														HASH_ENTER,
>>>>>>> .r86
						   														&found);
<<<<<<< .mine
		
		if (!found)
		{
			stat_rolecmdtabcolentry->column = msg_roleentry->column;
			stat_rolecmdtabcolentry->table = msg_roleentry->table;
			stat_rolecmdtabcolentry->command = msg_roleentry->command;			
			stat_rolecmdtabcolentry->count = 0;							
			stat_rolecmdtabcolentry->count_0 = 0;						// for fine triplets 
			stat_rolecmdtabcolentry->is_under_user = true;						
		}
=======
				
				if (!found)
				{
					//dbstat_roletabentry->table = msg_roleentry->table;
					dbstat_roletabentry->table = msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].table;
					dbstat_roletabentry->colEntry = NULL;
					dbstat_roletabentry->command = 0;
					dbstat_roletabentry->count = 0;
					dbstat_roletabentry->count_0 = 0;
					dbstat_roletabentry->all_col_count = 0;
					dbstat_roletabentry->is_under_user = false;
>>>>>>> .r86
		

<<<<<<< .mine
		stat_rolecmdtabcolentry->count += msg_roleentry->column_count;
		stat_rolecmdtabcolentry->count_0 += msg_roleentry->column_count_0;
		stat_rolecmdtabentry->all_col_count += msg_roleentry->column_count;

		*/		

		/* for the nbc:start */
		if (IDR_LEVEL == 0)
		{
			dbstat_rolecolentry = (PgStat_StatRoleCmdTabColEntry *) hash_search(dbstat_roleentry->colEntry, 
																				(void *) &msg_roleentry->column, 
																				HASH_ENTER, 
																				&found);
=======
					/* used for fine and medium triplets */
					memset(&hash_ctl, 0, sizeof(hash_ctl));
					hash_ctl.keysize = sizeof(Oid);
					hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabColEntry);
					hash_ctl.hash = oid_hash;
					hash_ctl.hcxt = pgStatLocalContext;
					dbstat_roletabentry->colEntry = hash_create("Per-database per-role per-table per-column stats",
													  PGSTAT_ROLE_CMD_TAB_COL_HASH_SIZE,
													  &hash_ctl,
													  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);
>>>>>>> .r86


<<<<<<< .mine
			dbentry->all_roles_col_count += msg_roleentry->column_count;
			dbstat_roleentry->all_col_count += msg_roleentry->column_count;			
			dbstat_rolecolentry->count += msg_roleentry->column_count;			
		}
		else
		{
			dbstat_roletabcolentry = (PgStat_StatRoleCmdTabColEntry *) (dbstat_roletabentry->colEntry + (msg_roleentry->column + COL_SHIFT));

			/*
			dbstat_roletabcolentry = (PgStat_StatRoleCmdTabColEntry *) hash_search(dbstat_roletabentry->colEntry, 
				   														(void *) &msg_roleentry->column, 
				   														HASH_ENTER, 
				   														&found);
		
			if (!found)
			{
				dbstat_roletabcolentry->column = msg_roleentry->column;
				dbstat_roletabcolentry->table = msg_roleentry->table;
				dbstat_roletabcolentry->command = msg_roleentry->command;			
				dbstat_roletabcolentry->count = 0;	
				dbstat_roletabcolentry->count_0 = 0;
				dbstat_roletabcolentry->is_under_user = false;
				
				dbstat_roleentry->num_col_entries++;				
			}
=======
>>>>>>> .r86
			*/

		
<<<<<<< .mine
			dbentry->all_roles_col_count += msg_roleentry->column_count;
			dbstat_roleentry->all_col_count += msg_roleentry->column_count;
			dbstat_roletabentry->all_col_count += msg_roleentry->column_count;			

			dbstat_roletabcolentry->column = msg_roleentry->column;
			dbstat_roletabcolentry->table = msg_roleentry->table;
			dbstat_roletabcolentry->command = msg_roleentry->command;
			
			dbstat_roletabcolentry->count += msg_roleentry->column_count;			
			dbstat_roletabcolentry->count_0 += msg_roleentry->column_count_0;
			
			/* to support NB_LEVEL == 1 at IDR_LEVEL == 1 we need the count of 0 columns as well for each table */
			dbstat_roletabentry->all_col_count += msg_roleentry->column_count_0;			
		}
		/* for the nbc:end */																	
		
		/* store in the tmp hash table list */
		/* in both these cases, we have a table access */
		if (msg_roleentry->column_count != 0 || msg_roleentry->column_count_0 != 0)
		{
			tmp_tabentry = (PgStat_StatRoleCmdTabEntry *) hash_search(tmp_msg_tab_htab, 
						   										 (void *) &msg_roleentry->table, 
						   										HASH_ENTER, 
						   										&found);
		
			if (!found)
			{
				memset(tmp_tabentry, 0, sizeof(PgStat_StatRoleCmdTabEntry));			
				tmp_tabentry->table = msg_roleentry->table;
				tmp_tabentry->role_id = msg_roleentry->role_id;				
			}
		}		
=======
				}

				//dbentry->all_roles_tab_count += msg_roleentry->table_count;
				//dbstat_roleentry->all_tab_count += msg_roleentry->table_count;
				//dbstat_roletabentry->count += msg_roleentry->table_count;

				dbentry->all_roles_tab_count += msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].table_count;
				dbstat_roleentry->all_tab_count += msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].table_count;
				dbstat_roletabentry->count += msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].table_count;
				/* for the nbc:end */

				/***************** count the columns *********************/
				for (l = 0; l < msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_col_entries; l++)
				{

					//msg_colentry = &msg_tabentry->m_colentry[l];
#ifdef B_NET
					/*
					stat_rolecmdtabcolentry = (PgStat_StatRoleCmdTabColEntry *) hash_search(stat_rolecmdtabentry->colEntry,
																							(void *) &msg_roleentry->column,
																							HASH_ENTER,
																							&found);
					*/

					stat_rolecmdtabcolentry = (PgStat_StatRoleCmdTabColEntry *) hash_search(stat_rolecmdtabentry->colEntry,
																							(void *) &(msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column),
																							HASH_ENTER,
																							&found);

					if (!found)
					{
						//stat_rolecmdtabcolentry->column = msg_roleentry->column;
						//stat_rolecmdtabcolentry->table = msg_roleentry->table;
						//stat_rolecmdtabcolentry->command = msg_roleentry->command;
						stat_rolecmdtabcolentry->column = msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column;
						stat_rolecmdtabcolentry->table = msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].table;
						stat_rolecmdtabcolentry->command = msg->m_roleentry[i].m_cmdentry[j].command;

						stat_rolecmdtabcolentry->count = 0;
						stat_rolecmdtabcolentry->count_0 = 0;						/* for fine triplets */
						stat_rolecmdtabcolentry->is_under_user = true;
					}

					//stat_rolecmdtabcolentry->count += msg_roleentry->column_count;
					//stat_rolecmdtabcolentry->count_0 += msg_roleentry->column_count_0;
					//stat_rolecmdtabentry->all_col_count += msg_roleentry->column_count;

					stat_rolecmdtabcolentry->count += msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column_count;
					stat_rolecmdtabcolentry->count_0 += msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column_count_0;
					stat_rolecmdtabentry->all_col_count += msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column_count;
#endif

					/* for the nbc:start */
					if (IDR_LEVEL == 0)
					{
						/*
						dbstat_rolecolentry = (PgStat_StatRoleCmdTabColEntry *) hash_search(dbstat_roleentry->colEntry,
																							(void *) &msg_roleentry->column,
																							HASH_ENTER,
																							&found);
						*/

						dbstat_rolecolentry = (PgStat_StatRoleCmdTabColEntry *) hash_search(dbstat_roleentry->colEntry,
																							(void *) &(msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column),
																							HASH_ENTER,
																							&found);

						if (!found)
						{
							//dbstat_rolecolentry->column = msg_roleentry->column;
							//dbstat_rolecolentry->table = msg_roleentry->table;
							//dbstat_rolecolentry->command = msg_roleentry->command;

							dbstat_rolecolentry->column = msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column;
							dbstat_rolecolentry->table = msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].table;
							dbstat_rolecolentry->command = msg->m_roleentry[i].m_cmdentry[j].command;
							dbstat_rolecolentry->count = 0;
							dbstat_rolecolentry->count_0 = 0;
							dbstat_rolecolentry->is_under_user = false;

							dbstat_roleentry->num_col_entries++;
						}

						//dbentry->all_roles_col_count += msg_roleentry->column_count;
						//dbstat_roleentry->all_col_count += msg_roleentry->column_count;
						//dbstat_rolecolentry->count += msg_roleentry->column_count;

						dbentry->all_roles_col_count += msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column_count;
						dbstat_roleentry->all_col_count += msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column_count;
						dbstat_rolecolentry->count += msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column_count;
					}
					else
					{
						/*
						dbstat_roletabcolentry = (PgStat_StatRoleCmdTabColEntry *) hash_search(dbstat_roletabentry->colEntry,
																					(void *) &msg_roleentry->column,
																					HASH_ENTER,
																					&found);
						*/

						dbstat_roletabcolentry = (PgStat_StatRoleCmdTabColEntry *) hash_search(dbstat_roletabentry->colEntry,
																					(void *) &(msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column),
																					HASH_ENTER,
																					&found);

						if (!found)
						{
							//dbstat_roletabcolentry->column = msg_roleentry->column;
							//dbstat_roletabcolentry->table = msg_roleentry->table;
							//dbstat_roletabcolentry->command = msg_roleentry->command;
							dbstat_roletabcolentry->column = msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column;
							dbstat_roletabcolentry->table = msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].table;
							dbstat_roletabcolentry->command = msg->m_roleentry[i].m_cmdentry[j].command;

							dbstat_roletabcolentry->count = 0;
							dbstat_roletabcolentry->count_0 = 0;
							dbstat_roletabcolentry->is_under_user = false;

							dbstat_roleentry->num_col_entries++;
						}

						//dbentry->all_roles_col_count += msg_roleentry->column_count;
						//dbstat_roleentry->all_col_count += msg_roleentry->column_count;
						//dbstat_roletabentry->all_col_count += msg_roleentry->column_count;
						//dbstat_roletabcolentry->count += msg_roleentry->column_count;

						//dbstat_roletabcolentry->count_0 += msg_roleentry->column_count_0;

						dbentry->all_roles_col_count += msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column_count;
						dbstat_roleentry->all_col_count += msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column_count;
						dbstat_roletabentry->all_col_count += msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column_count;
						dbstat_roletabcolentry->count += msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column_count;

						dbstat_roletabcolentry->count_0 += msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column_count_0;

						/* to support NB_LEVEL == 1 at IDR_LEVEL == 1 we need the count of 0 columns as well for each table */
						//dbstat_roletabentry->all_col_count += msg_roleentry->column_count_0;
						dbstat_roletabentry->all_col_count += msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column_count_0;
					}

					/* for the nbc:end */

					/* store in the tmp hash table list */
					/* in both these cases, we have a table access */
					/*
					if (msg_roleentry->column_count != 0 || msg_roleentry->column_count_0 != 0)
					{
						tmp_tabentry = (PgStat_StatRoleCmdTabEntry *) hash_search(tmp_msg_tab_htab,
																			 (void *) &msg_roleentry->table,
																			HASH_ENTER,
																			&found);

						if (!found)
						{
							memset(tmp_tabentry, 0, sizeof(PgStat_StatRoleCmdTabEntry));
							tmp_tabentry->table = msg_roleentry->table;
							tmp_tabentry->role_id = msg_roleentry->role_id;
						}
					}
					*/

					if (msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column_count != 0 ||
							msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].m_colentry[l].column_count_0 != 0)
					{
						tmp_tabentry = (PgStat_StatRoleCmdTabEntry *) hash_search(tmp_msg_tab_htab,
																			 (void *) &(msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].table),
																			HASH_ENTER,
																			&found);

						if (!found)
						{
							memset(tmp_tabentry, 0, sizeof(PgStat_StatRoleCmdTabEntry));
							tmp_tabentry->table = msg->m_roleentry[i].m_cmdentry[j].m_tabentry[k].table;
							tmp_tabentry->role_id = msg->m_roleentry[i].role_id;
						}
					}
				} /* end of columns per table */
			} /* end of tables per command */
		} /* end of commands per role */
>>>>>>> .r86
	}	/* end of message processing */
	
	if (IDR_LEVEL == 0)
	{
		list_free(roles_list);
		hash_destroy(tmp_msg_tab_htab);
		return;
	}
	
	/* do this for all the roles in this message */
	ListCell	*role_cell;
	foreach(role_cell, roles_list)
	{
		Oid role_id = lfirst_oid(role_cell);
		dbstat_roleentry = (PgStat_StatRoleEntry *) hash_search(dbentry->users, 
															   (void *) &role_id, 
																HASH_FIND, 
																&found);
		
		if (!found)
			continue;
		
		tmp_cmdentry = (struct rolecmdentry *) hash_search(tmp_msg_cmd_htab, 
				   											(void *) &role_id, 
				   											HASH_FIND, 
				   											&found);
		
		if (!found)
			continue;
		
		/* do this for all the commands in this messages for this role */
		for (i = 0; i < tmp_cmdentry->cmd_count; i++)
		{
			/* do this for all tables under intrusion detection */
			hash_seq_init(&hstat1, dbstat_roleentry->tabEntry);
			while ((dbstat_roletabentry = (PgStat_StatRoleCmdTabEntry *) hash_seq_search(&hstat1)) != NULL)
			{
				Oid 			rel_oid = dbstat_roletabentry->table;

				/* was this rel_oid counted in this message for this role */
				PgStat_StatRoleCmdTabEntry *tmp_tabentry = (PgStat_StatRoleCmdTabEntry *) hash_search(tmp_msg_tab_htab, 
																										(void *) &rel_oid, 
																										HASH_FIND, 
																										&found);

				/* if found then it should belong to the loop role otherwise increment the 0 count
				 * if not found then we can sefly increment the 0 count as not found => not found for all roles */
				if (found && (tmp_tabentry->role_id == role_id))									
					continue;				
																	
				dbstat_roletabentry->count_0++; /* increment the zero count for this table */
				
				/* for IDR_LEVEL = 2, increment the zero count for all columns of this table */
				if (IDR_LEVEL == 2)
				{	
										
					//for (k = 0; k <= hash_get_num_entries(dbstat_roletabentry->colEntry); k++)			
					for (k = 0; k < dbstat_roletabentry->num_cols; k++)			
					{	
						/*						 
						int column = SYSCOLSTARTNUM + k; // SYSCOLSTARTNUM .. -1 are the system columns 
						
						if (column == 0)
							continue;
						
						dbstat_roletabcolentry = (PgStat_StatRoleCmdTabColEntry *) hash_search(dbstat_roletabentry->colEntry, 
							   															(void *) &column, 
							   															HASH_FIND, 
							   															&found);		
						
						// not creating a new entry as we were supposed to create all column entries in pgstat_recv_taboidstat
						if (dbstat_roletabcolentry == NULL && !found)
							continue;
						*/

						dbstat_roletabcolentry = (PgStat_StatRoleCmdTabColEntry *) (dbstat_roletabentry->colEntry + k);
						dbstat_roletabcolentry->count_0++;				
				
					}
				}																		
			
			}		
		}	
	}
	
	list_free(roles_list);
	hash_destroy(tmp_msg_tab_htab);
	
}

#endif

/* ----------
 *
 * Ashish
 *
 * pgstat_recv_rolestat_new() -
 *
 *	Count what the backend has done for the roles .. to be converted to a static function ..
 * ----------
 */

void
pgstat_recv_rolestat_new(char *msg, int len)
{

	PgStat_StatDBEntry 	 			*dbentry;
	PgStat_StatRoleEntry 			*stat_roleentry;
	PgStat_StatRoleEntry			*stat_userentry;
	PgStat_StatRoleCmdEntry			*stat_rolecmdentry;
	PgStat_StatRoleCmdTabEntry		*stat_rolecmdtabentry;
	PgStat_StatRoleCmdTabColEntry	*stat_rolecmdtabcolentry;

	PgStat_StatRoleEntry 			*dbstat_roleentry;
	PgStat_StatRoleCmdEntry			*dbstat_rolecmdentry;
	PgStat_StatRoleCmdTabEntry		*dbstat_roletabentry;
	PgStat_StatRoleCmdTabColEntry	*dbstat_roletabcolentry;
	PgStat_StatRoleCmdTabColEntry	*dbstat_rolecolentry;		/* only used for coarse triplets */

	struct rolecmdentry
	{
		Oid		role_id;
		int		cmd_count;;
	}*tmp_cmdentry;

	PgStat_StatRoleCmdTabEntry		*tmp_tabentry;
	PgStat_StatRoleCmdTabColEntry	*tmp_tabcolentry;

	HASHCTL			    hash_ctl;
	int 				i, j, k, l;
	bool				found;
	int 				all_cmd_counts;

	HTAB			   *tmp_msg_tab_htab = NULL;
	HTAB			   *tmp_msg_cmd_htab = NULL;

	List	   		   *roles_list	 	 = NULL;
	HASH_SEQ_STATUS	   hstat1, hstat2, hstat3;

	elog(NOTICE, "stats received");

	Oid dbid;
	remove_item(&msg, &dbid, sizeof(Oid));
	dbentry = pgstat_get_db_entry(dbid, true);

	if (!dbentry)
		return;

	Oid userid;
	remove_item(&msg, &userid, sizeof(Oid));

#ifdef B_NET
	stat_userentry = (PgStat_StatRoleEntry *)hash_search(dbentry->users,
														(void *)&userid,
														HASH_FIND,
														NULL);
	if (!stat_userentry)
		return;
#endif

	/* create a temp hash table to store all the table ids */
	memset(&hash_ctl, 0, sizeof(hash_ctl));
	hash_ctl.keysize = sizeof(Oid);
	hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabEntry);
	hash_ctl.hash = oid_hash;
	hash_ctl.hcxt = pgStatLocalContext;
	tmp_msg_tab_htab = hash_create("tmp hash table tab",
									 PGSTAT_ROLE_CMD_TAB_HASH_SIZE,
									 &hash_ctl,
									 HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);

	/* create a temp hash table to store the num commands per role */
	memset(&hash_ctl, 0, sizeof(hash_ctl));
	hash_ctl.keysize = sizeof(Oid);
	hash_ctl.entrysize = sizeof(struct rolecmdentry);
	hash_ctl.hash = oid_hash;
	hash_ctl.hcxt = pgStatLocalContext;
	tmp_msg_cmd_htab = hash_create("tmp hash table cmd",
									 PGSTAT_ROLE_CMD_HASH_SIZE,
									 &hash_ctl,
									 HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);

	int m_role_entries;
	remove_item(&msg, &m_role_entries, sizeof(int));

	for (i = 0; i < m_role_entries; i++)
	{
		Oid role_id;
		remove_item(&msg, &role_id, sizeof(Oid));

#ifdef B_NET
		stat_roleentry = (PgStat_StatRoleEntry *) hash_search(stat_userentry->roleEntry,
																 (void *) &role_id,
																HASH_FIND,
																&found);
		if (!found)
		{
			elog(LOG, "Encountered a PgStat_MsgRoleStat message with role (%d) that has not been activated.", role_id);
			continue;
		}
#endif

		/* for the nbc:start */
		dbstat_roleentry = (PgStat_StatRoleEntry *) hash_search(dbentry->users,
																(void *) &(role_id),
																HASH_FIND,
																&found);
		if (!found)
		{
			elog(LOG, "Encountered a PgStat_MsgRoleStat message with role (%d) that has not been activated.", role_id);
			continue;
		}
		/* for the nbc:end */

		/* store this role for future */
		roles_list = list_append_unique_oid(roles_list, role_id);

		/***************** count the commands *********************/
		int m_cmd_entries;
		remove_item(&msg, &m_cmd_entries, sizeof(int));
		for (j = 0; j < m_cmd_entries; j++)
		{
			int command,command_count;
			remove_item(&msg, &command, sizeof(int));
			remove_item(&msg, &command_count, sizeof(int));

#ifdef B_NET
			stat_rolecmdentry = (PgStat_StatRoleCmdEntry *) hash_search(stat_roleentry->cmdEntry,
																(void *)&(command),
																HASH_ENTER,
																&found);


			if (!found)
			{
				stat_rolecmdentry->command = command;
				stat_rolecmdentry->count = 0;
				stat_rolecmdentry->all_tab_count = 0;
				stat_rolecmdentry->is_under_user = true;

				memset(&hash_ctl, 0, sizeof(hash_ctl));
				hash_ctl.keysize = sizeof(Oid);
				hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabEntry);
				hash_ctl.hash = oid_hash;
				hash_ctl.hcxt = pgStatLocalContext;
				stat_rolecmdentry->tabEntry = hash_create("Per-database per-user per-role per-command per-table stats",
												  PGSTAT_ROLE_CMD_TAB_HASH_SIZE,
												  &hash_ctl,
												  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);

			}

			stat_rolecmdentry->count += command_count;
			stat_userentry->all_cmd_count += command_count;
			stat_roleentry->all_cmd_count += command_count;
			dbentry->all_roles_cmd_count += command_count;
#endif

			if (command_count != 0)
			{
				tmp_cmdentry = (struct rolecmdentry *) hash_search(tmp_msg_cmd_htab,
																	(void *)&(role_id),
																	HASH_ENTER,
																	&found);

				if (!found)
				{
					memset(tmp_cmdentry, 0, sizeof(struct rolecmdentry));
					tmp_cmdentry->role_id = role_id;
				}

				tmp_cmdentry->cmd_count += command_count;
			}


			/* for the nbc:start */
			dbstat_rolecmdentry = (PgStat_StatRoleCmdEntry *) hash_search(dbstat_roleentry->cmdEntry,
																   (void *) &(command),
																	HASH_ENTER,
																	&found);

			if (!found)
			{
				dbstat_rolecmdentry->command = command;
				dbstat_rolecmdentry->count = 0;
				dbstat_rolecmdentry->all_tab_count = 0;
				dbstat_rolecmdentry->is_under_user = false;

				memset(&hash_ctl, 0, sizeof(hash_ctl));
				hash_ctl.keysize = sizeof(Oid);
				hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabEntry);
				hash_ctl.hash = oid_hash;
				hash_ctl.hcxt = pgStatLocalContext;
				dbstat_rolecmdentry->tabEntry = hash_create("Per-database per-role per-command per-table stats",
												  PGSTAT_ROLE_CMD_TAB_HASH_SIZE,
												  &hash_ctl,
												  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);

			}

			dbstat_rolecmdentry->count += command_count;
			dbstat_roleentry->all_cmd_count += command_count;

			/* for the nbc:end */

			/***************** count the tables *********************/
			int m_tab_entries;
			remove_item(&msg, &m_tab_entries, sizeof(int));
			for (k = 0; k < m_tab_entries; k++)
			{
				int table,table_count;
				remove_item(&msg, &table, sizeof(int));
				remove_item(&msg, &table_count, sizeof(int));

#ifdef B_NET
				stat_rolecmdtabentry = (PgStat_StatRoleCmdTabEntry *) hash_search(stat_rolecmdentry->tabEntry,
																	(void *) &(table),
																	HASH_ENTER,
																	&found);

				if (!found)
				{
					stat_rolecmdtabentry->table = table;
					stat_rolecmdtabentry->command = command;
					stat_rolecmdtabentry->count = 0;
					stat_rolecmdtabentry->count_0 = 0;
					stat_rolecmdtabentry->all_col_count = 0;
					stat_rolecmdtabentry->is_under_user = true;

					memset(&hash_ctl, 0, sizeof(hash_ctl));
					hash_ctl.keysize = sizeof(Oid);
					hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabColEntry);
					hash_ctl.hash = oid_hash;
					hash_ctl.hcxt = pgStatLocalContext;
					stat_rolecmdtabentry->colEntry = hash_create("Per-database per-role per-command per-table per-column stats",
													  PGSTAT_ROLE_CMD_TAB_COL_HASH_SIZE,
													  &hash_ctl,
													  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);
				}

				stat_rolecmdtabentry->count += table_count;
				stat_rolecmdentry->all_tab_count += table_count;
#endif

				/* for the nbc: start */
				dbstat_roletabentry = (PgStat_StatRoleCmdTabEntry *) hash_search(dbstat_roleentry->tabEntry,
						   														(void *) &(table),
						   														HASH_ENTER,
						   														&found);

				if (!found)
				{
					dbstat_roletabentry->table = table;
					dbstat_roletabentry->colEntry = NULL;
					dbstat_roletabentry->command = 0;
					dbstat_roletabentry->count = 0;
					dbstat_roletabentry->count_0 = 0;
					dbstat_roletabentry->all_col_count = 0;
					dbstat_roletabentry->is_under_user = false;

					/* used for fine and medium triplets */
					memset(&hash_ctl, 0, sizeof(hash_ctl));
					hash_ctl.keysize = sizeof(Oid);
					hash_ctl.entrysize = sizeof(PgStat_StatRoleCmdTabColEntry);
					hash_ctl.hash = oid_hash;
					hash_ctl.hcxt = pgStatLocalContext;
					dbstat_roletabentry->colEntry = hash_create("Per-database per-role per-table per-column stats",
													  PGSTAT_ROLE_CMD_TAB_COL_HASH_SIZE,
													  &hash_ctl,
													  HASH_ELEM | HASH_FUNCTION | HASH_CONTEXT);



				}

				dbentry->all_roles_tab_count += table_count;
				dbstat_roleentry->all_tab_count += table_count;
				dbstat_roletabentry->count += table_count;
				/* for the nbc:end */

				/***************** count the columns *********************/
				int m_col_entries;
				remove_item(&msg, &m_col_entries, sizeof(int));

				for (l = 0; l < m_col_entries; l++)
				{

					int column,column_count,column_count_0;
					remove_item(&msg, &column, sizeof(int));
					remove_item(&msg, &column_count, sizeof(int));
					remove_item(&msg, &column_count_0, sizeof(int));
#ifdef B_NET

					stat_rolecmdtabcolentry = (PgStat_StatRoleCmdTabColEntry *) hash_search(stat_rolecmdtabentry->colEntry,
																							(void *) &(column),
																							HASH_ENTER,
																							&found);

					if (!found)
					{
						stat_rolecmdtabcolentry->column = column;
						stat_rolecmdtabcolentry->table = table;
						stat_rolecmdtabcolentry->command = command;

						stat_rolecmdtabcolentry->count = 0;
						stat_rolecmdtabcolentry->count_0 = 0;						/* for fine triplets */
						stat_rolecmdtabcolentry->is_under_user = true;
					}


					stat_rolecmdtabcolentry->count += column_count;
					stat_rolecmdtabcolentry->count_0 += column_count_0;
					stat_rolecmdtabentry->all_col_count += column_count;
#endif

					/* for the nbc:start */
					if (IDR_LEVEL == 0)
					{

						dbstat_rolecolentry = (PgStat_StatRoleCmdTabColEntry *) hash_search(dbstat_roleentry->colEntry,
																							(void *) &(column),
																							HASH_ENTER,
																							&found);

						if (!found)
						{
							dbstat_rolecolentry->column = column;
							dbstat_rolecolentry->table = table;
							dbstat_rolecolentry->command = command;
							dbstat_rolecolentry->count = 0;
							dbstat_rolecolentry->count_0 = 0;
							dbstat_rolecolentry->is_under_user = false;

							dbstat_roleentry->num_col_entries++;
						}

						dbentry->all_roles_col_count += column_count;
						dbstat_roleentry->all_col_count += column_count;
						dbstat_rolecolentry->count += column_count;
					}
					else
					{

						dbstat_roletabcolentry = (PgStat_StatRoleCmdTabColEntry *) hash_search(dbstat_roletabentry->colEntry,
																					(void *) &(column),
																					HASH_ENTER,
																					&found);

						if (!found)
						{
							dbstat_roletabcolentry->column = column;
							dbstat_roletabcolentry->table = table;
							dbstat_roletabcolentry->command = command;

							dbstat_roletabcolentry->count = 0;
							dbstat_roletabcolentry->count_0 = 0;
							dbstat_roletabcolentry->is_under_user = false;

							dbstat_roleentry->num_col_entries++;
						}

						dbentry->all_roles_col_count += column_count;
						dbstat_roleentry->all_col_count += column_count;
						dbstat_roletabentry->all_col_count += column_count;
						dbstat_roletabcolentry->count += column_count;

						dbstat_roletabcolentry->count_0 += column_count_0;

						/* to support NB_LEVEL == 1 at IDR_LEVEL == 1 we need the count of 0 columns as well for each table */
						dbstat_roletabentry->all_col_count += column_count_0;
					}

					/* for the nbc:end */

					/* store in the tmp hash table list */
					/* in both these cases, we have a table access */

					if (column_count != 0 ||
							column_count_0 != 0)
					{
						tmp_tabentry = (PgStat_StatRoleCmdTabEntry *) hash_search(tmp_msg_tab_htab,
																			(void *) &(table),
																			HASH_ENTER,
																			&found);

						if (!found)
						{
							memset(tmp_tabentry, 0, sizeof(PgStat_StatRoleCmdTabEntry));
							tmp_tabentry->table = table;
							tmp_tabentry->role_id = role_id;
						}
					}
				} /* end of columns per table */
			} /* end of tables per command */
		} /* end of commands per role */
	}	/* end of message processing */

	if (IDR_LEVEL == 0)
	{
		list_free(roles_list);
		hash_destroy(tmp_msg_tab_htab);
		return;
	}

	/* do this for all the roles in this message */
	ListCell	*role_cell;
	foreach(role_cell, roles_list)
	{
		Oid role_id = lfirst_oid(role_cell);
		dbstat_roleentry = (PgStat_StatRoleEntry *) hash_search(dbentry->users,
															   (void *) &role_id,
																HASH_FIND,
																&found);

		if (!found)
			continue;

		tmp_cmdentry = (struct rolecmdentry *) hash_search(tmp_msg_cmd_htab,
				   											(void *) &role_id,
				   											HASH_FIND,
				   											&found);

		if (!found)
			continue;

		/* do this for all the commands in this messages for this role */
		for (i = 0; i < tmp_cmdentry->cmd_count; i++)
		{
			/* do this for all tables under intrusion detection */
			hash_seq_init(&hstat1, dbstat_roleentry->tabEntry);
			while ((dbstat_roletabentry = (PgStat_StatRoleCmdTabEntry *) hash_seq_search(&hstat1)) != NULL)
			{
				Oid 			rel_oid = dbstat_roletabentry->table;

				/* was this rel_oid counted in this message for this role */
				PgStat_StatRoleCmdTabEntry *tmp_tabentry = (PgStat_StatRoleCmdTabEntry *) hash_search(tmp_msg_tab_htab,
																										(void *) &rel_oid,
																										HASH_FIND,
																										&found);

				/* if found then it should belong to the loop role otherwise increment the 0 count
				 * if not found then we can sefly increment the 0 count as not found => not found for all roles */
				if (found && (tmp_tabentry->role_id == role_id))
					continue;

				dbstat_roletabentry->count_0++; /* increment the zero count for this table */

				/* for IDR_LEVEL = 2, increment the zero count for all columns of this table */
				if (IDR_LEVEL == 2)
				{
					for (k = 0; k <= hash_get_num_entries(dbstat_roletabentry->colEntry); k++)
					{
						int column = SYSCOLSTARTNUM + k; /* SYSCOLSTARTNUM .. -1 are the system columns */

						if (column == 0)
							continue;

						dbstat_roletabcolentry = (PgStat_StatRoleCmdTabColEntry *) hash_search(dbstat_roletabentry->colEntry,
							   															(void *) &column,
							   															HASH_FIND,
							   															&found);

						/* not creating a new entry as we were supposed to create all column entries in pgstat_recv_taboidstat */
						if (dbstat_roletabcolentry == NULL && !found)
							continue;

						dbstat_roletabcolentry->count_0++;

					}
				}

			}
		}
	}

	list_free(roles_list);
	hash_destroy(tmp_msg_tab_htab);

}

/* ----------
 * pgstat_recv_tabpurge() -
 *
 *	Arrange for dead table removal.
 * ----------
 */
static void
pgstat_recv_tabpurge(PgStat_MsgTabpurge *msg, int len)
{
	PgStat_StatDBEntry *dbentry;
	int			i;

	dbentry = pgstat_get_db_entry(msg->m_databaseid, false);

	/*
	 * No need to purge if we don't even know the database.
	 */
	if (!dbentry || !dbentry->tables)
		return;

	/*
	 * Process all table entries in the message.
	 */
	for (i = 0; i < msg->m_nentries; i++)
	{
		/* Remove from hashtable if present; we don't care if it's not. */
		(void) hash_search(dbentry->tables,
						   (void *) &(msg->m_tableid[i]),
						   HASH_REMOVE, NULL);
	}
}


/* ----------
 * 
 * Ashish: changed to remove user entries as well
 * 
 * pgstat_recv_dropdb() -
 *
 *	Arrange for dead database removal
 * ----------
 */
static void
pgstat_recv_dropdb(PgStat_MsgDropdb *msg, int len)
{
	PgStat_StatDBEntry *dbentry;

	/*
	 * Lookup the database in the hashtable.
	 */
	dbentry = pgstat_get_db_entry(msg->m_databaseid, false);

	/*
	 * If found, remove it.
	 */
	if (dbentry)
	{
		if (dbentry->tables != NULL)
			hash_destroy(dbentry->tables);

		/* Ashish */		
		if (dbentry->users != NULL)
			hash_destroy(dbentry->users);
		
		if (hash_search(pgStatDBHash,
						(void *) &(dbentry->databaseid),
						HASH_REMOVE, NULL) == NULL)
			ereport(ERROR,
					(errmsg("database hash table corrupted "
							"during cleanup --- abort")));
	}
}


/* ----------
 * 
 * Ashish: changed to include users as well ..  
 * 
 * pgstat_recv_resetcounter() -
 *
 *	Reset the statistics for the specified database.
 * ----------
 */
static void
pgstat_recv_resetcounter(PgStat_MsgResetcounter *msg, int len)
{
	HASHCTL							hash_ctl;
	PgStat_StatDBEntry	 			*dbentry;
	/* Ashish */
	HASH_SEQ_STATUS					hstat1, hstat2, hstat3, hstat4;
	PgStat_StatRoleEntry			*userentry;
	PgStat_StatRoleEntry			*roleentry;
	PgStat_StatRoleCmdEntry			*rolecmdentry;
	PgStat_StatRoleCmdTabEntry		*rolecmdtabentry;
	PgStat_StatRoleCmdTabEntry 		*roletabentry;
	
	/*
	 * Lookup the database in the hashtable.  Nothing to do if not there.
	 */
	dbentry = pgstat_get_db_entry(msg->m_databaseid, false);

	if (!dbentry)
		return;

	/*
	 * We simply throw away all the database's table entries by recreating a
	 * new hash table for them.
	 */
	if (dbentry->tables != NULL)
		hash_destroy(dbentry->tables);

	/* Ashish */
	/* destroy the hash table of per user stats 
	hash_seq_init(&hstat1, dbentry->users);
	while ((userentry = (PgStat_StatRoleEntry *) hash_seq_search(&hstat1)) != NULL)
	{
		if (userentry->roleEntry != NULL)
		{
			hash_seq_init(&hstat2, userentry->roleEntry);
			while ((roleentry = (PgStat_StatRoleEntry *) hash_seq_search(&hstat2)) != NULL)
			{
				if (roleentry->cmdEntry != NULL)
				{
					hash_seq_init(&hstat3, roleentry->cmdEntry);
					while ((rolecmdentry = (PgStat_StatRoleCmdEntry *) hash_seq_search(&hstat3)) != NULL)
					{
						
						if (rolecmdentry->tabEntry != NULL)
						{
							hash_seq_init(&hstat4, rolecmdentry->tabEntry);
							while ((rolecmdtabentry = (PgStat_StatRoleCmdTabEntry *) hash_seq_search(&hstat4)) != NULL)
							{
								if (rolecmdtabentry->colEntry != NULL)
									hash_destroy(rolecmdtabentry->colEntry);								
							}
							
							hash_destroy(rolecmdentry->tabEntry);
						}												
					}
					
					hash_destroy(roleentry->cmdEntry);
				}
			}			
			
			hash_destroy(userentry->roleEntry);
		}

		/* destroy the hash table of per command stats 
		if (userentry->cmdEntry != NULL)
		{
			hash_seq_init(&hstat1, userentry->cmdEntry);
			while ((rolecmdentry = (PgStat_StatRoleCmdEntry *) hash_seq_search(&hstat1)) != NULL)
			{
				
				if (rolecmdentry->tabEntry != NULL)
				{
					hash_seq_initdbentry->users(&hstat2, rolecmdentry->tabEntry);
					while ((rolecmdtabentry = (PgStat_StatRoleCmdTabEntry *) hash_seq_search(&hstat2)) != NULL)
					{
						if (rolecmdtabentry->colEntry != NULL)
							hash_destroy(rolecmdtabentry->colEntry);								
					}
					
					hash_destroy(rolecmdentry->tabEntry);
				}												
			}
			
			hash_destroy(userentry->cmdEntry);
		}
		
		/* destroy the hash table of db tables for nbc 
		if (userentry->tabEntry != NULL)
		{
			hash_seq_init(&hstat1, userentry->tabEntry);
			while ((roletabentry = (PgStat_StatRoleCmdTabEntry *) hash_seq_search(&hstat1)) != NULL)
			{
				if (roletabentry->colEntry != NULL)
					hash_destroy(roletabentry->colEntry);								
			}
			
			hash_destroy(userentry->tabEntry);
		}												

		/* destroy the hash table of db columns for nbc 
		if (userentry->colEntry != NULL)
			hash_destroy(userentry->colEntry);
		
	}
	 */
	
	/* Ashish */
	if (dbentry->users != NULL)
		hash_destroy(dbentry->users);
	
	dbentry->tables = NULL;
	dbentry->n_xact_commit = 0;
	dbentry->n_xact_rollback = 0;
	dbentry->n_blocks_fetched = 0;
	dbentry->n_blocks_hit = 0;

	/* Ashish */
	dbentry->all_roles_cmd_count = 0;
	dbentry->all_roles_tab_count = 0;
	dbentry->all_roles_col_count = 0;
	dbentry->num_roles = 0;
	
	memset(&hash_ctl, 0, sizeof(hash_ctl));
	hash_ctl.keysize = sizeof(Oid);
	hash_ctl.entrysize = sizeof(PgStat_StatTabEntry);
	hash_ctl.hash = oid_hash;
	dbentry->tables = hash_create("Per-database table",
								  PGSTAT_TAB_HASH_SIZE,
								  &hash_ctl,
								  HASH_ELEM | HASH_FUNCTION);

	/* Ashish 
	 */	
	memset(&hash_ctl, 0, sizeof(hash_ctl));
	hash_ctl.keysize = sizeof(Oid);
	hash_ctl.entrysize = sizeof(PgStat_StatRoleEntry);
	hash_ctl.hash = oid_hash;
	dbentry->users = hash_create("Per-database user / role stats",
								  PGSTAT_ROLE_HASH_SIZE,
								  &hash_ctl,
								  HASH_ELEM | HASH_FUNCTION);
								 								 

}

/* ----------
 * pgstat_recv_autovac() -
 *
 *	Process an autovacuum signalling message.
 * ----------
 */
static void
pgstat_recv_autovac(PgStat_MsgAutovacStart *msg, int len)
{
	PgStat_StatDBEntry *dbentry;

	/*
	 * Lookup the database in the hashtable.  Don't create the entry if it
	 * doesn't exist, because autovacuum may be processing a template
	 * database.  If this isn't the case, the database is most likely to have
	 * an entry already.  (If it doesn't, not much harm is done anyway --
	 * it'll get created as soon as somebody actually uses the database.)
	 */
	dbentry = pgstat_get_db_entry(msg->m_databaseid, false);
	if (dbentry == NULL)
		return;

	/*
	 * Store the last autovacuum time in the database entry.
	 */
	dbentry->last_autovac_time = msg->m_start_time;
}

/* ----------
 * pgstat_recv_vacuum() -
 *
 *	Process a VACUUM message.
 * ----------
 */
static void
pgstat_recv_vacuum(PgStat_MsgVacuum *msg, int len)
{
	PgStat_StatDBEntry *dbentry;
	PgStat_StatTabEntry *tabentry;

	/*
	 * Don't create either the database or table entry if it doesn't already
	 * exist.  This avoids bloating the stats with entries for stuff that is
	 * only touched by vacuum and not by live operations.
	 */
	dbentry = pgstat_get_db_entry(msg->m_databaseid, false);
	if (dbentry == NULL)
		return;

	tabentry = hash_search(dbentry->tables, &(msg->m_tableoid),
						   HASH_FIND, NULL);
	if (tabentry == NULL)
		return;

	if (msg->m_autovacuum)
		tabentry->autovac_vacuum_timestamp = msg->m_vacuumtime;
	else
		tabentry->vacuum_timestamp = msg->m_vacuumtime;
	tabentry->n_live_tuples = msg->m_tuples;
	/* Resetting dead_tuples to 0 is an approximation ... */
	tabentry->n_dead_tuples = 0;
	if (msg->m_analyze)
	{
		tabentry->last_anl_tuples = msg->m_tuples;
		if (msg->m_autovacuum)
			tabentry->autovac_analyze_timestamp = msg->m_vacuumtime;
		else
			tabentry->analyze_timestamp = msg->m_vacuumtime;
	}
	else
	{
		/* last_anl_tuples must never exceed n_live_tuples+n_dead_tuples */
		tabentry->last_anl_tuples = Min(tabentry->last_anl_tuples,
										msg->m_tuples);
	}
}

/* ----------
 * pgstat_recv_analyze() -
 *
 *	Process an ANALYZE message.
 * ----------
 */
static void
pgstat_recv_analyze(PgStat_MsgAnalyze *msg, int len)
{
	PgStat_StatDBEntry *dbentry;
	PgStat_StatTabEntry *tabentry;

	/*
	 * Don't create either the database or table entry if it doesn't already
	 * exist.  This avoids bloating the stats with entries for stuff that is
	 * only touched by analyze and not by live operations.
	 */
	dbentry = pgstat_get_db_entry(msg->m_databaseid, false);
	if (dbentry == NULL)
		return;

	tabentry = hash_search(dbentry->tables, &(msg->m_tableoid),
						   HASH_FIND, NULL);
	if (tabentry == NULL)
		return;

	if (msg->m_autovacuum)
		tabentry->autovac_analyze_timestamp = msg->m_analyzetime;
	else
		tabentry->analyze_timestamp = msg->m_analyzetime;
	tabentry->n_live_tuples = msg->m_live_tuples;
	tabentry->n_dead_tuples = msg->m_dead_tuples;
	tabentry->last_anl_tuples = msg->m_live_tuples + msg->m_dead_tuples;
}


/* ----------
 * pgstat_recv_bgwriter() -
 *
 *	Process a BGWRITER message.
 * ----------
 */
static void
pgstat_recv_bgwriter(PgStat_MsgBgWriter *msg, int len)
{
	globalStats.timed_checkpoints += msg->m_timed_checkpoints;
	globalStats.requested_checkpoints += msg->m_requested_checkpoints;
	globalStats.buf_written_checkpoints += msg->m_buf_written_checkpoints;
	globalStats.buf_written_clean += msg->m_buf_written_clean;
	globalStats.maxwritten_clean += msg->m_maxwritten_clean;
	globalStats.buf_written_backend += msg->m_buf_written_backend;
	globalStats.buf_alloc += msg->m_buf_alloc;
}

static void
clearCounts()
{
	PgStat_RoleStatus			*rolestatus;
	PgStat_RoleCmdStatus		*rolecmdstatus;
	PgStat_RoleCmdTabStatus		*rolecmdtabstatus;
	PgStat_RoleCmdTabColStatus	*rolecmdtabcolstatus;

	HASH_SEQ_STATUS 	hstat, hstat1, hstat2, hstat3;

	hash_seq_init(&hstat, MyProcPort->pgstat_user_status->rolStatus);
	while ((rolestatus = (PgStat_RoleStatus *) hash_seq_search(&hstat)) != NULL)
	{
		hash_seq_init(&hstat1, rolestatus->cmdStatus);
		while ((rolecmdstatus = (PgStat_RoleCmdStatus *) hash_seq_search(&hstat1)) != NULL)
		{
			/* clear the count */
			rolecmdstatus->count = 0;

			hash_seq_init(&hstat2, rolecmdstatus->tableStatus);
			while ((rolecmdtabstatus = (PgStat_RoleCmdTabStatus *) hash_seq_search(&hstat2)) != NULL)
			{
				/* clear the count */
				rolecmdtabstatus->count = 0;

				if (rolecmdstatus->command == CMD_DELETE)
					continue;

				hash_seq_init(&hstat3, rolecmdtabstatus->colStatus);
				while ((rolecmdtabcolstatus = (PgStat_RoleCmdTabColStatus *) hash_seq_search(&hstat3)) != NULL)
				{
					/* clear the counts */
					rolecmdtabcolstatus->count = 0;
					rolecmdtabcolstatus->count_0 = 0;
				}
			}
		}
	}

	return;
}

