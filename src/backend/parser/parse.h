/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton interface for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     ABORT_P = 258,
     ABSOLUTE_P = 259,
     ACCESS = 260,
     ACTION = 261,
     ADD_P = 262,
     ADM = 263,
     ADMIN = 264,
     AFTER = 265,
     AGGREGATE = 266,
     ALL = 267,
     ALSO = 268,
     ALTER = 269,
     ALWAYS = 270,
     ANALYSE = 271,
     ANALYZE = 272,
     AND = 273,
     ANY = 274,
     ARRAY = 275,
     AS = 276,
     ASC = 277,
     ASSERTION = 278,
     ASSIGNMENT = 279,
     ASYMMETRIC = 280,
     AT = 281,
     ATTR = 282,
     ATTROID = 283,
     AUTHORIZATION = 284,
     AUTHORIZE = 285,
     BACKWARD = 286,
     BEFORE = 287,
     BEGIN_P = 288,
     BETWEEN = 289,
     BIGINT = 290,
     BINARY = 291,
     BIT = 292,
     BOOLEAN_P = 293,
     BOTH = 294,
     BY = 295,
     CACHE = 296,
     CALLED = 297,
     CASCADE = 298,
     CASCADED = 299,
     CASE = 300,
     CAST = 301,
     CHAIN = 302,
     CHAR_P = 303,
     CHARACTER = 304,
     CHARACTERISTICS = 305,
     CHECK = 306,
     CHECKPOINT = 307,
     CLASS = 308,
     CLOSE = 309,
     CLUSTER = 310,
     COALESCE = 311,
     COLLATE = 312,
     COLUMN = 313,
     COMMENT = 314,
     COMMIT = 315,
     COMMITTED = 316,
     CONCURRENTLY = 317,
     CONDITIONS = 318,
     CONFIGURATION = 319,
     CONFIRM_ACTIONS = 320,
     CONNECTION = 321,
     CONSTRAINT = 322,
     CONSTRAINTS = 323,
     CONTENT_P = 324,
     CONVERSION_P = 325,
     COPY = 326,
     COST = 327,
     CREATE = 328,
     CREATEDB = 329,
     CREATEROLE = 330,
     CREATEUSER = 331,
     CROSS = 332,
     CSV = 333,
     CURRENT_P = 334,
     CURRENT_DATE = 335,
     CURRENT_ROLE = 336,
     CURRENT_TIME = 337,
     CURRENT_TIMESTAMP = 338,
     CURRENT_USER = 339,
     CURSOR = 340,
     CYCLE = 341,
     DATABASE = 342,
     DAY_P = 343,
     DEALLOCATE = 344,
     DEC = 345,
     DECIMAL_P = 346,
     DECLARE = 347,
     DEF = 348,
     DEFAULT = 349,
     DEFAULTS = 350,
     DEFERRABLE = 351,
     DEFERRED = 352,
     DEFINER = 353,
     DELETE_P = 354,
     DELIMITER = 355,
     DELIMITERS = 356,
     DENY = 357,
     DESC = 358,
     DICTIONARY = 359,
     DISABLE_P = 360,
     DISCARD = 361,
     DISTINCT = 362,
     DO = 363,
     DOCUMENT_P = 364,
     DOMAIN_P = 365,
     DOUBLE_P = 366,
     DROP = 367,
     EACH = 368,
     ELSE = 369,
     ENABLE_P = 370,
     ENCODING = 371,
     ENCRYPTED = 372,
     END_P = 373,
     ENUM_P = 374,
     ESCAPE = 375,
     EVENTID = 376,
     EXCEPT = 377,
     EXCLUDING = 378,
     EXCLUSIVE = 379,
     EXECUTE = 380,
     EXISTS = 381,
     EXPLAIN = 382,
     EXTERNAL = 383,
     EXTRACT = 384,
     FAIL_ACTIONS = 385,
     FALSE_P = 386,
     FAMILY = 387,
     FETCH = 388,
     FIRST_P = 389,
     FLOAT_P = 390,
     FOR = 391,
     FORCE = 392,
     FOREIGN = 393,
     FORWARD = 394,
     FREEZE = 395,
     FROM = 396,
     FULL = 397,
     FUNCTION = 398,
     GLOBAL = 399,
     GRANT = 400,
     GRANTED = 401,
     GREATEST = 402,
     GROUP_P = 403,
     HANDLER = 404,
     HAVING = 405,
     HEADER_P = 406,
     HOLD = 407,
     HOUR_P = 408,
     IF_P = 409,
     ILIKE = 410,
     IMMEDIATE = 411,
     IMMUTABLE = 412,
     IMPLICIT_P = 413,
     IN_P = 414,
     INCLUDING = 415,
     INCREMENT = 416,
     INDEX = 417,
     INDEXES = 418,
     INHERIT = 419,
     INHERITS = 420,
     INIT_ACTIONS = 421,
     INITIALLY = 422,
     INNER_P = 423,
     INOUT = 424,
     INPUT_P = 425,
     INSENSITIVE = 426,
     INSERT = 427,
     INSTEAD = 428,
     INT_P = 429,
     INTEGER = 430,
     INTERSECT = 431,
     INTERVAL = 432,
     INTO = 433,
     INVOKER = 434,
     IS = 435,
     ISNULL = 436,
     ISOLATION = 437,
     JOIN = 438,
     JOINT = 439,
     KEY = 440,
     LANCOMPILER = 441,
     LANGUAGE = 442,
     LARGE_P = 443,
     LAST_P = 444,
     LEADING = 445,
     LEAST = 446,
     LEFT = 447,
     LEVEL = 448,
     LIKE = 449,
     LIMIT = 450,
     LISTEN = 451,
     LOAD = 452,
     LOCAL = 453,
     LOCALTIME = 454,
     LOCALTIMESTAMP = 455,
     LOCATION = 456,
     LOCK_P = 457,
     LOGIN_P = 458,
     MAPPING = 459,
     MATCH = 460,
     MAXVALUE = 461,
     MINUTE_P = 462,
     MINVALUE = 463,
     MODE = 464,
     MONTH_P = 465,
     MOVE = 466,
     NAME_P = 467,
     NAMES = 468,
     NATIONAL = 469,
     NATURAL = 470,
     NCHAR = 471,
     NEW = 472,
     NEXT = 473,
     NO = 474,
     NOCREATEDB = 475,
     NOCREATEROLE = 476,
     NOCREATEUSER = 477,
     NOINHERIT = 478,
     NOLOGIN_P = 479,
     NONE = 480,
     NOSUPERUSER = 481,
     NOT = 482,
     NOTHING = 483,
     NOTIFY = 484,
     NOTNULL = 485,
     NOWAIT = 486,
     NULL_P = 487,
     NULLIF = 488,
     NULLS_P = 489,
     NUMERIC = 490,
     OBJECT_P = 491,
     OF = 492,
     OFF = 493,
     OFFSET = 494,
     OIDS = 495,
     OLD = 496,
     ON = 497,
     ONLY = 498,
     OPERATOR = 499,
     OPROID = 500,
     OPTION = 501,
     OR = 502,
     ORDER = 503,
     OUT_P = 504,
     OUTER_P = 505,
     OVERLAPS = 506,
     OVERLAY = 507,
     OWNED = 508,
     OWNER = 509,
     PARSER = 510,
     PARTIAL = 511,
     PASSWORD = 512,
     PLACING = 513,
     PLANS = 514,
     POSITION = 515,
     PRECISION = 516,
     PRED = 517,
     PRESERVE = 518,
     PREPARE = 519,
     PREPARED = 520,
     PRIMARY = 521,
     PRIOR = 522,
     PRIVILEGES = 523,
     PROCEDURAL = 524,
     PROCEDURE = 525,
     QUOTE = 526,
     RACTION = 527,
     READ = 528,
     REAL = 529,
     REASSIGN = 530,
     RECHECK = 531,
     REFERENCES = 532,
     REINDEX = 533,
     RELATIVE_P = 534,
     RELEASE = 535,
     RENAME = 536,
     REPEATABLE = 537,
     REPLACE = 538,
     REPLICA = 539,
     RESET = 540,
     RESOLVE_ACTIONS = 541,
     RESTART = 542,
     RESTRICT = 543,
     RETURNING = 544,
     RETURNS = 545,
     REVOKE = 546,
     RIGHT = 547,
     ROLE = 548,
     ROLLBACK = 549,
     ROW = 550,
     ROWS = 551,
     RPOLICY = 552,
     RULE = 553,
     SAVEPOINT = 554,
     SCHEMA = 555,
     SCROLL = 556,
     SEARCH = 557,
     SECOND_P = 558,
     SECURITY = 559,
     SELECT = 560,
     SEQUENCE = 561,
     SERIALIZABLE = 562,
     SESSION = 563,
     SESSION_USER = 564,
     SET = 565,
     SETOF = 566,
     SEVERITY = 567,
     SHARE = 568,
     SHOW = 569,
     SIMILAR = 570,
     SIMPLE = 571,
     SMALLINT = 572,
     SOME = 573,
     STABLE = 574,
     STANDALONE_P = 575,
     START = 576,
     STATEMENT = 577,
     STATISTICS = 578,
     STDIN = 579,
     STDOUT = 580,
     STORAGE = 581,
     STRICT_P = 582,
     STRIP_P = 583,
     SUBSTRING = 584,
     SUPERUSER_P = 585,
     SUSPEND = 586,
     SYMMETRIC = 587,
     SYSID = 588,
     SYSTEM_P = 589,
     TABLE = 590,
     TABLESPACE = 591,
     TAINT = 592,
     TEMP = 593,
     TEMPLATE = 594,
     TEMPORARY = 595,
     TEXT_P = 596,
     THEN = 597,
     TIME = 598,
     TIMESTAMP = 599,
     TO = 600,
     TRAILING = 601,
     TRANSACTION = 602,
     TREAT = 603,
     TRIGGER = 604,
     TRIM = 605,
     TRUE_P = 606,
     TRUNCATE = 607,
     TRUSTED = 608,
     TYPE_P = 609,
     TYPEOID = 610,
     UNCOMMITTED = 611,
     UNENCRYPTED = 612,
     UNION = 613,
     UNIQUE = 614,
     UNKNOWN = 615,
     UNLISTEN = 616,
     UNTIL = 617,
     UPDATE = 618,
     USER = 619,
     USERS = 620,
     USING = 621,
     VACUUM = 622,
     VAL = 623,
     VALID = 624,
     VALIDATOR = 625,
     VALUE_P = 626,
     VALUES = 627,
     VARCHAR = 628,
     VARYING = 629,
     VERBOSE = 630,
     VERSION_P = 631,
     VIEW = 632,
     VOLATILE = 633,
     WHEN = 634,
     WHERE = 635,
     WHITESPACE_P = 636,
     WITH = 637,
     WITHOUT = 638,
     WORK = 639,
     WRITE = 640,
     XML_P = 641,
     XMLATTRIBUTES = 642,
     XMLCONCAT = 643,
     XMLELEMENT = 644,
     XMLFOREST = 645,
     XMLPARSE = 646,
     XMLPI = 647,
     XMLROOT = 648,
     XMLSERIALIZE = 649,
     YEAR_P = 650,
     YES_P = 651,
     ZONE = 652,
     NULLS_FIRST = 653,
     NULLS_LAST = 654,
     WITH_CASCADED = 655,
     WITH_LOCAL = 656,
     WITH_CHECK = 657,
     IDENT = 658,
     FCONST = 659,
     SCONST = 660,
     BCONST = 661,
     XCONST = 662,
     Op = 663,
     ICONST = 664,
     PARAM = 665,
     POSTFIXOP = 666,
     UMINUS = 667,
     TYPECAST = 668
   };
#endif
/* Tokens.  */
#define ABORT_P 258
#define ABSOLUTE_P 259
#define ACCESS 260
#define ACTION 261
#define ADD_P 262
#define ADM 263
#define ADMIN 264
#define AFTER 265
#define AGGREGATE 266
#define ALL 267
#define ALSO 268
#define ALTER 269
#define ALWAYS 270
#define ANALYSE 271
#define ANALYZE 272
#define AND 273
#define ANY 274
#define ARRAY 275
#define AS 276
#define ASC 277
#define ASSERTION 278
#define ASSIGNMENT 279
#define ASYMMETRIC 280
#define AT 281
#define ATTR 282
#define ATTROID 283
#define AUTHORIZATION 284
#define AUTHORIZE 285
#define BACKWARD 286
#define BEFORE 287
#define BEGIN_P 288
#define BETWEEN 289
#define BIGINT 290
#define BINARY 291
#define BIT 292
#define BOOLEAN_P 293
#define BOTH 294
#define BY 295
#define CACHE 296
#define CALLED 297
#define CASCADE 298
#define CASCADED 299
#define CASE 300
#define CAST 301
#define CHAIN 302
#define CHAR_P 303
#define CHARACTER 304
#define CHARACTERISTICS 305
#define CHECK 306
#define CHECKPOINT 307
#define CLASS 308
#define CLOSE 309
#define CLUSTER 310
#define COALESCE 311
#define COLLATE 312
#define COLUMN 313
#define COMMENT 314
#define COMMIT 315
#define COMMITTED 316
#define CONCURRENTLY 317
#define CONDITIONS 318
#define CONFIGURATION 319
#define CONFIRM_ACTIONS 320
#define CONNECTION 321
#define CONSTRAINT 322
#define CONSTRAINTS 323
#define CONTENT_P 324
#define CONVERSION_P 325
#define COPY 326
#define COST 327
#define CREATE 328
#define CREATEDB 329
#define CREATEROLE 330
#define CREATEUSER 331
#define CROSS 332
#define CSV 333
#define CURRENT_P 334
#define CURRENT_DATE 335
#define CURRENT_ROLE 336
#define CURRENT_TIME 337
#define CURRENT_TIMESTAMP 338
#define CURRENT_USER 339
#define CURSOR 340
#define CYCLE 341
#define DATABASE 342
#define DAY_P 343
#define DEALLOCATE 344
#define DEC 345
#define DECIMAL_P 346
#define DECLARE 347
#define DEF 348
#define DEFAULT 349
#define DEFAULTS 350
#define DEFERRABLE 351
#define DEFERRED 352
#define DEFINER 353
#define DELETE_P 354
#define DELIMITER 355
#define DELIMITERS 356
#define DENY 357
#define DESC 358
#define DICTIONARY 359
#define DISABLE_P 360
#define DISCARD 361
#define DISTINCT 362
#define DO 363
#define DOCUMENT_P 364
#define DOMAIN_P 365
#define DOUBLE_P 366
#define DROP 367
#define EACH 368
#define ELSE 369
#define ENABLE_P 370
#define ENCODING 371
#define ENCRYPTED 372
#define END_P 373
#define ENUM_P 374
#define ESCAPE 375
#define EVENTID 376
#define EXCEPT 377
#define EXCLUDING 378
#define EXCLUSIVE 379
#define EXECUTE 380
#define EXISTS 381
#define EXPLAIN 382
#define EXTERNAL 383
#define EXTRACT 384
#define FAIL_ACTIONS 385
#define FALSE_P 386
#define FAMILY 387
#define FETCH 388
#define FIRST_P 389
#define FLOAT_P 390
#define FOR 391
#define FORCE 392
#define FOREIGN 393
#define FORWARD 394
#define FREEZE 395
#define FROM 396
#define FULL 397
#define FUNCTION 398
#define GLOBAL 399
#define GRANT 400
#define GRANTED 401
#define GREATEST 402
#define GROUP_P 403
#define HANDLER 404
#define HAVING 405
#define HEADER_P 406
#define HOLD 407
#define HOUR_P 408
#define IF_P 409
#define ILIKE 410
#define IMMEDIATE 411
#define IMMUTABLE 412
#define IMPLICIT_P 413
#define IN_P 414
#define INCLUDING 415
#define INCREMENT 416
#define INDEX 417
#define INDEXES 418
#define INHERIT 419
#define INHERITS 420
#define INIT_ACTIONS 421
#define INITIALLY 422
#define INNER_P 423
#define INOUT 424
#define INPUT_P 425
#define INSENSITIVE 426
#define INSERT 427
#define INSTEAD 428
#define INT_P 429
#define INTEGER 430
#define INTERSECT 431
#define INTERVAL 432
#define INTO 433
#define INVOKER 434
#define IS 435
#define ISNULL 436
#define ISOLATION 437
#define JOIN 438
#define JOINT 439
#define KEY 440
#define LANCOMPILER 441
#define LANGUAGE 442
#define LARGE_P 443
#define LAST_P 444
#define LEADING 445
#define LEAST 446
#define LEFT 447
#define LEVEL 448
#define LIKE 449
#define LIMIT 450
#define LISTEN 451
#define LOAD 452
#define LOCAL 453
#define LOCALTIME 454
#define LOCALTIMESTAMP 455
#define LOCATION 456
#define LOCK_P 457
#define LOGIN_P 458
#define MAPPING 459
#define MATCH 460
#define MAXVALUE 461
#define MINUTE_P 462
#define MINVALUE 463
#define MODE 464
#define MONTH_P 465
#define MOVE 466
#define NAME_P 467
#define NAMES 468
#define NATIONAL 469
#define NATURAL 470
#define NCHAR 471
#define NEW 472
#define NEXT 473
#define NO 474
#define NOCREATEDB 475
#define NOCREATEROLE 476
#define NOCREATEUSER 477
#define NOINHERIT 478
#define NOLOGIN_P 479
#define NONE 480
#define NOSUPERUSER 481
#define NOT 482
#define NOTHING 483
#define NOTIFY 484
#define NOTNULL 485
#define NOWAIT 486
#define NULL_P 487
#define NULLIF 488
#define NULLS_P 489
#define NUMERIC 490
#define OBJECT_P 491
#define OF 492
#define OFF 493
#define OFFSET 494
#define OIDS 495
#define OLD 496
#define ON 497
#define ONLY 498
#define OPERATOR 499
#define OPROID 500
#define OPTION 501
#define OR 502
#define ORDER 503
#define OUT_P 504
#define OUTER_P 505
#define OVERLAPS 506
#define OVERLAY 507
#define OWNED 508
#define OWNER 509
#define PARSER 510
#define PARTIAL 511
#define PASSWORD 512
#define PLACING 513
#define PLANS 514
#define POSITION 515
#define PRECISION 516
#define PRED 517
#define PRESERVE 518
#define PREPARE 519
#define PREPARED 520
#define PRIMARY 521
#define PRIOR 522
#define PRIVILEGES 523
#define PROCEDURAL 524
#define PROCEDURE 525
#define QUOTE 526
#define RACTION 527
#define READ 528
#define REAL 529
#define REASSIGN 530
#define RECHECK 531
#define REFERENCES 532
#define REINDEX 533
#define RELATIVE_P 534
#define RELEASE 535
#define RENAME 536
#define REPEATABLE 537
#define REPLACE 538
#define REPLICA 539
#define RESET 540
#define RESOLVE_ACTIONS 541
#define RESTART 542
#define RESTRICT 543
#define RETURNING 544
#define RETURNS 545
#define REVOKE 546
#define RIGHT 547
#define ROLE 548
#define ROLLBACK 549
#define ROW 550
#define ROWS 551
#define RPOLICY 552
#define RULE 553
#define SAVEPOINT 554
#define SCHEMA 555
#define SCROLL 556
#define SEARCH 557
#define SECOND_P 558
#define SECURITY 559
#define SELECT 560
#define SEQUENCE 561
#define SERIALIZABLE 562
#define SESSION 563
#define SESSION_USER 564
#define SET 565
#define SETOF 566
#define SEVERITY 567
#define SHARE 568
#define SHOW 569
#define SIMILAR 570
#define SIMPLE 571
#define SMALLINT 572
#define SOME 573
#define STABLE 574
#define STANDALONE_P 575
#define START 576
#define STATEMENT 577
#define STATISTICS 578
#define STDIN 579
#define STDOUT 580
#define STORAGE 581
#define STRICT_P 582
#define STRIP_P 583
#define SUBSTRING 584
#define SUPERUSER_P 585
#define SUSPEND 586
#define SYMMETRIC 587
#define SYSID 588
#define SYSTEM_P 589
#define TABLE 590
#define TABLESPACE 591
#define TAINT 592
#define TEMP 593
#define TEMPLATE 594
#define TEMPORARY 595
#define TEXT_P 596
#define THEN 597
#define TIME 598
#define TIMESTAMP 599
#define TO 600
#define TRAILING 601
#define TRANSACTION 602
#define TREAT 603
#define TRIGGER 604
#define TRIM 605
#define TRUE_P 606
#define TRUNCATE 607
#define TRUSTED 608
#define TYPE_P 609
#define TYPEOID 610
#define UNCOMMITTED 611
#define UNENCRYPTED 612
#define UNION 613
#define UNIQUE 614
#define UNKNOWN 615
#define UNLISTEN 616
#define UNTIL 617
#define UPDATE 618
#define USER 619
#define USERS 620
#define USING 621
#define VACUUM 622
#define VAL 623
#define VALID 624
#define VALIDATOR 625
#define VALUE_P 626
#define VALUES 627
#define VARCHAR 628
#define VARYING 629
#define VERBOSE 630
#define VERSION_P 631
#define VIEW 632
#define VOLATILE 633
#define WHEN 634
#define WHERE 635
#define WHITESPACE_P 636
#define WITH 637
#define WITHOUT 638
#define WORK 639
#define WRITE 640
#define XML_P 641
#define XMLATTRIBUTES 642
#define XMLCONCAT 643
#define XMLELEMENT 644
#define XMLFOREST 645
#define XMLPARSE 646
#define XMLPI 647
#define XMLROOT 648
#define XMLSERIALIZE 649
#define YEAR_P 650
#define YES_P 651
#define ZONE 652
#define NULLS_FIRST 653
#define NULLS_LAST 654
#define WITH_CASCADED 655
#define WITH_LOCAL 656
#define WITH_CHECK 657
#define IDENT 658
#define FCONST 659
#define SCONST 660
#define BCONST 661
#define XCONST 662
#define Op 663
#define ICONST 664
#define PARAM 665
#define POSTFIXOP 666
#define UMINUS 667
#define TYPECAST 668




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 118 "gram.y"
{
	int					ival;
	char				chr;
	char				*str;
	const char			*keyword;
	bool				boolean;
	JoinType			jtype;
	DropBehavior		dbehavior;
	OnCommitAction		oncommit;
	List				*list;
	Node				*node;
	Value				*value;
	ObjectType			objtype;

	TypeName			*typnam;
	FunctionParameter   *fun_param;
	FunctionParameterMode fun_param_mode;
	FuncWithArgs		*funwithargs;
	DefElem				*defelt;
	SortBy				*sortby;
	JoinExpr			*jexpr;
	IndexElem			*ielem;
	Alias				*alias;
	RangeVar			*range;
	IntoClause			*into;
	A_Indices			*aind;
	ResTarget			*target;
	PrivTarget			*privtarget;

	InsertStmt			*istmt;
	VariableSetStmt		*vsetstmt;
}
/* Line 1489 of yacc.c.  */
#line 908 "y.tab.h"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE base_yylval;

#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif

extern YYLTYPE base_yylloc;
