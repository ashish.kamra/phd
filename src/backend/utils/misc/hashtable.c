/***************************************************************************************************************
	
					Implements the hashtable interface defined in hashtable. 
					It is a static hashtable.
					Hashing function : 

***************************************************************************************************************/
#include <stdio.h>
#include <string.h>

#include "hashtable.h"

static unsigned int hash(Hashtable *, char *);

// interfaces
Hashtable *createHashtable(int size){

	Hashtable *newTable;
    int i;

    if (size < 1) 
		return (Hashtable *)0; /* invalid size for table */

    /* Attempt to allocate memory for the table structure */
    if ((newTable = (Hashtable *)malloc(sizeof(Hashtable))) == (Hashtable *)0)
        return (Hashtable *)0;
    
    /* Attempt to allocate memory for the table itself */
    if ((newTable->table = (List **)malloc(sizeof(List *) * size)) == (List **)0) {
        return (Hashtable *)0;
    }

    /* Initialize the elements of the table */
    for(i=0; i<size; i++) 
		newTable->table[i] = (List *)0;

    /* Set the table's size */
    newTable->size = size;

    return newTable;

}

void *getVal(Hashtable *hTable, char *key){
    List *l;
    unsigned int hashval = hash(hTable, key);

    for(l = hTable->table[hashval]; l != NULL; l = l->next) {
        if (strcmp(key, l->key) == 0) 
			return l->val;
    }
    return (void *)0;
}

int insertVal(Hashtable *hTable, char *key, void *val){

	List *newList;

	/* item already exists, don't insert it again. */
    if (getVal(hTable, key) != (void *)0)
		return 1;

    unsigned int hashval = hash(hTable, key);

    /* Attempt to allocate memory for list */
    if ((newList = (List *)malloc(sizeof(List))) == (List *)0) 
		return -1;
        
    /* Insert into list (at the begining)*/
    newList->key = strdup(key);
	newList->val = val;
    newList->next = hTable->table[hashval];
    hTable->table[hashval] = newList;

    return 1;
}

void freeTable(Hashtable *hTable){
    int i;
    List *l, *t;

    if (hTable==NULL) 
		return;

    /* Free the memory for every item in the table, including the 
     * keys and values themselves.
     */
    for(i=0; i < hTable->size; i++) {
        l = hTable->table[i];
        while(l != (List *)0) {
            t = l;
            l = l->next;
            free(t->key);
			free(t->val);
            free(t);
        }
    }

    /* Free the table itself */
    free(hTable->table);
    free(hTable);
}

// private functions using static keyword
static unsigned int hash(Hashtable *hTable, char *key){
	
	unsigned int hashval;
    
    /* we start our hash out at 0 */
    hashval = 0;

    /* for each character, we multiply the old hash by 31 and add the current
     * character.  Remember that shifting a number left is equivalent to 
     * multiplying it by 2 raised to the number of places shifted.  So we 
     * are in effect multiplying hashval by 32 and then subtracting hashval.  
     * Why do we do this?  Because shifting and subtraction are much more 
     * efficient operations than multiplication.
     */
    for(; *key != '\0'; key++) 
		hashval = *key + (hashval << 5) - hashval;

    /* we then return the hash value mod the hashtable size so that it will
     * fit into the necessary range
     */
    return hashval % hTable->size;
}
