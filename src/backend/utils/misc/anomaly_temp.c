/*-------------------------------------------------------------------------
 *
 * anomaly.c
 *	  Functions for anomaly detection and response
 *
 * NOTES
 *
 *-------------------------------------------------------------------------
 */
#include "postgres.h"

#include "utils/anomaly.h"
#include <access/tupmacs.h>
#include "utils/builtins.h"

/* for def of Port */
#include "libpq/libpq-be.h"

#define DETECTIONPROB 	1.0
#define POLICY_HASHTABLESIZE	50
#define PRED_HASHTABLESIZE		200
#define USE_INTELLI_PRED_EVAL	0 

myHashtable *policyHtable = (myHashtable *)NIL;			/* hashtable to hold the intermediate policy id data */
myHashtable *predHtable = (myHashtable *)NIL;			/* hashtable to hold the intermediate predicate id data */

static const unsigned char myitoa[] = "0123456789";

static Oid	cached_preds = InvalidOid;
static List *cached_preds_list = NIL;

/* Other internal functions */
static bool evaluatePred(Datum lhs, Oid type, Oid opr, Datum rhs);
static void evaluateAttribute(CatCList *, Datum, predCounters *);
static char *convert_oid_to_string(Oid policy_id, char *policy_id_str);
static void IDR_Preds_CacheCallback(Datum arg, Oid relid);

// internal hash table functions  
static myHashtable *createHashtable(unsigned int ); // input: size
static Datum getVal(myHashtable *, Datum); // input: hashtable, key
static int insertVal(myHashtable *, Datum, Datum); // input: hashtable, key, val
											// output : -1 => failure
										    //	         1 => success
static void freeTable(myHashtable *); // input: hashtable
static unsigned int hash(myHashtable *, Datum);

bool pg_anomaly_detection(AnomalyAssessment *anomaly_assessment, Query *query)
{
	
	float8      		result;
	const char			*date = "2008-11-11";
	const char			*time = "14:00:00";
	ListCell   			*l;	
	int					k;
	Port				*port = MyProcPort;
	const char			*local_host_ip = NIL;
	char 				*user_id_str_arr = NIL;
	char 				*user_id_str = NIL;
	char				*client_app = NIL;
	
	/* result [0.0 - 1.0) */
	result = (double) random() / ((double) MAX_RANDOM_VALUE + 1);
	
	/* fill up the anomaly_assessment structure */
	if (result <= DETECTIONPROB)
	{
		/**************** CONTEXTUAL **********************/
		/* current effective user id */				
		user_id_str_arr = (char *)palloc(10 + 2 + 1); // 10 bytes for the oid, 2 bytes for '{'.'}' and 1 byte for '\0'  		
		
		user_id_str_arr[0] = '{';
		user_id_str_arr[1] = '\0';
				
		user_id_str = convert_oid_to_string(GetUserId(), user_id_str);
		
		user_id_str_arr = strcat(user_id_str_arr,user_id_str);
		user_id_str_arr = strcat(user_id_str_arr,"}");
		
		anomaly_assessment->user_id = PointerGetDatum(user_id_str_arr);
		
		/* ipaddress of the client */
		//if (port->raddr.addr.ss_family == AF_INET || port->raddr.addr.ss_family == AF_INET6)
		//	anomaly_assessment->source_ip = DirectFunctionCall1(inet_client_addr, (Datum)NIL);
		//else
		//{
			local_host_ip = (char *)palloc(10);
			local_host_ip = strcpy(local_host_ip, "127.0.0.1");
			anomaly_assessment->source_ip = PointerGetDatum(local_host_ip);
		//}

		/* hard-coded date and time */
		anomaly_assessment->an_date = DirectFunctionCall1(date_in, PointerGetDatum(date));
		anomaly_assessment->an_time = DirectFunctionCall1(time_in, PointerGetDatum(time));
		
		/* client application */ 
		client_app = (char *)palloc(6); // 3 for the dummy app name c_1 and 2 for {} and 1 for \0
		client_app[0] = '{';
		client_app[1] = 'c';
		client_app[2] = '_';
		client_app[3] = '0';
		client_app[4] = '}';
		client_app[5] = '\0';
		anomaly_assessment->client_app = PointerGetDatum(client_app);		
		
		
		/**************** STRUCTURAL **********************/
		/* database Oid */
		char *db_id_str_arr = (char *)palloc(10 + 2 + 1); // 10 bytes for the oid, 2 bytes for '{'.'}' and 1 byte for '\0'  
		char *db_id_str;
		
		db_id_str_arr[0] = '{';
		db_id_str_arr[1] = '\0';
				
		db_id_str = convert_oid_to_string(MyDatabaseId, db_id_str);
		
		db_id_str_arr = strcat(db_id_str_arr, db_id_str);
		db_id_str_arr = strcat(db_id_str_arr,"}");
		
		anomaly_assessment->db_id = PointerGetDatum(db_id_str_arr);		
		
		/* not filling up the schema id */
		
		/* fill up the command type */ 		 			
		switch(query->commandType)
		{
			case CMD_SELECT:
							anomaly_assessment->sql_cmd = ACL_SELECT;			
							break;
			case CMD_INSERT: 
							anomaly_assessment->sql_cmd = ACL_INSERT; 
							break;
			case CMD_UPDATE: 
							anomaly_assessment->sql_cmd = ACL_UPDATE;			
							break;
			case CMD_DELETE: 
							anomaly_assessment->sql_cmd = ACL_DELETE;							 
							break;		
			default:
							/* not catering to other commands right now */							
							return false;
							
							//anomaly_assessment->sql_cmd = 0;
							
								
		} 
							
		/* fill up the relations accessed in the query */
		
		/* count the number of relations 
		k=0;
		foreach(l, query->rtable)
			k++;
		
		/* now allocate memory 
		char *obj_ids_str_arr = (char *)palloc(k*10 + (k-1) + 2 +1); // 10 bytes for each oid, 1 byte for each "," 
																	 // 2 bytes for each '{'.'}' and one byte for '\0' 
		char *obj_oid_str;
		
		obj_ids_str_arr[0] = '{';		
		obj_ids_str_arr[1] = '\0';
						
		foreach(l, query->rtable)
		{		
			obj_oid_str = convert_oid_to_string(((RangeTblEntry *) lfirst(l))->relid, obj_oid_str);
			obj_ids_str_arr = strcat(obj_ids_str_arr, obj_oid_str);
			if (--k != 0)
				obj_ids_str_arr = strcat(obj_ids_str_arr, ",");
		}
		
		obj_ids_str_arr = strcat(obj_ids_str_arr, "}");
		
		anomaly_assessment->obj_ids = PointerGetDatum(obj_ids_str_arr);
		*/
		
		/* hardcoding the relation for matching purposes */
		char *obj_oid_str = (char *)palloc(2 + 1 + 1);
		obj_oid_str[0] = '{';		
		obj_oid_str[1] = '0';
		obj_oid_str[2] = '}';
		obj_oid_str[3] = '\0';
		anomaly_assessment->obj_ids = PointerGetDatum(obj_oid_str);
		
		return true;
	}
	else
	{
		//anomaly_assessment = (AnomalyAssessment *)NIL;
		return false;
	}		
}

void pg_anomaly_response(AnomalyAssessment *anomaly_assessment)
{
	if (OidIsValid(cached_preds))
		pg_anomaly_response_with_cache(anomaly_assessment);
	else
		void pg_anomaly_response_without_cache(anomaly_assessment)
}

void pg_anomaly_response_without_cache(AnomalyAssessment *anomaly_assessment)
{	
	int i;
	myList *l;
	int	num_matching_policies;
	predCounters pred_counters;
	//int	total_preds_evaluated_attr[5];
	//int	diffTimeInMicroSecs[5];
	//struct timeval 	startTime[5], endTime[5];
	//int	total_preds_evaluated_attr[5];
	int	diffTimeInMicroSecs;
	struct timeval 	startTime, endTime;
	
	gettimeofday (&startTime, NULL);
	
	CatCList   *attr_list = (CatCList *)NIL;
	policyHtable = (myHashtable *)createHashtable(POLICY_HASHTABLESIZE);
	predHtable = (myHashtable *)createHashtable(PRED_HASHTABLESIZE);
	
	pred_counters.total_preds_in_database = 0;
	pred_counters.total_preds_with_policies = 0;
	pred_counters.total_preds_evaluated = 0;
	pred_counters.total_preds_skipped_intelligently = 0;
	pred_counters.time_spent_in_pred_evals = 0;
	
	//total_preds_evaluated_attr[0] = 0;
	/* we go by alphabetical order of attributes name for the time being */
	/* command */
	
	attr_list = SearchSysCacheList(Z_RPOLICYATTRSATTRNAME, 1, PointerGetDatum("command"), 0, 0, 0);
	if (attr_list->n_members != 0)
		evaluateAttribute(attr_list, Int32GetDatum(anomaly_assessment->sql_cmd), &pred_counters);
	ReleaseSysCacheList(attr_list);
	
	//total_preds_evaluated_attr[0] = pred_counters.total_preds_evaluated;  
		
	/* user_id */
	//gettimeofday (&startTime[1], NULL);
	attr_list = SearchSysCacheList(Z_RPOLICYATTRSATTRNAME, 1, PointerGetDatum("user_id"), 0, 0, 0);
	if (attr_list->n_members != 0)
		evaluateAttribute(attr_list, anomaly_assessment->user_id, &pred_counters);
	ReleaseSysCacheList(attr_list);

	//gettimeofday (&endTime[1], NULL);
	//total_preds_evaluated_attr[1] = pred_counters.total_preds_evaluated - total_preds_evaluated_attr[0];  

	/* database_id */
	//gettimeofday (&startTime[2], NULL);
	attr_list = SearchSysCacheList(Z_RPOLICYATTRSATTRNAME, 1, PointerGetDatum("database_id"), 0, 0, 0);
	if (attr_list->n_members != 0)
		evaluateAttribute(attr_list, anomaly_assessment->db_id, &pred_counters);
	ReleaseSysCacheList(attr_list);

	//gettimeofday (&endTime[2], NULL);
	//total_preds_evaluated_attr[2] = pred_counters.total_preds_evaluated - total_preds_evaluated_attr[1];  
	
	/* relation_id */
	//gettimeofday (&startTime[3], NULL);
	
	attr_list = SearchSysCacheList(Z_RPOLICYATTRSATTRNAME, 1, PointerGetDatum("relation_id"), 0, 0, 0);
	if (attr_list->n_members != 0)
		evaluateAttribute(attr_list, anomaly_assessment->obj_ids, &pred_counters);
	ReleaseSysCacheList(attr_list);
	
	//gettimeofday (&endTime[3], NULL);
	//total_preds_evaluated_attr[3] = pred_counters.total_preds_evaluated - total_preds_evaluated_attr[2];  
	
	/* client application */
	//gettimeofday (&startTime[4], NULL);

	attr_list = SearchSysCacheList(Z_RPOLICYATTRSATTRNAME, 1, PointerGetDatum("client_app"), 0, 0, 0);
	if (attr_list->n_members != 0)
		evaluateAttribute(attr_list, anomaly_assessment->client_app, &pred_counters);
	ReleaseSysCacheList(attr_list);
	
	//gettimeofday (&endTime[4], NULL);
	//total_preds_evaluated_attr[4] = pred_counters.total_preds_evaluated - total_preds_evaluated_attr[3];  
	
	/* source_ip */
	//gettimeofday (&startTime[5], NULL);
	//gettimeofday (&startTime, NULL);
	attr_list = SearchSysCacheList(Z_RPOLICYATTRSATTRNAME, 1, PointerGetDatum("source_ip"), 0, 0, 0);
	if (attr_list->n_members != 0)
		evaluateAttribute(attr_list, anomaly_assessment->source_ip, &pred_counters);
	ReleaseSysCacheList(attr_list);		

	gettimeofday (&endTime, NULL);
	//ereport(NOTICE,(errmsg("preds_evaluated = %d", pred_counters.total_preds_evaluated)));

	//gettimeofday (&endTime[5], NULL);
	//total_preds_evaluated_attr[5] = pred_counters.total_preds_evaluated - total_preds_evaluated_attr[4];  
	
	/* scan through the Hashtable to get the policies that match */
	num_matching_policies = 0;
	for (i = 0; i < POLICY_HASHTABLESIZE; i++)
	{
		for(l = policyHtable->table[i]; l != NULL; l = l->next) 
		{
			policyHtableNode *policynode = (policyHtableNode *)l->val;			
			if (policynode->finalResult == true)
			{
				//ereport(NOTICE,(errmsg("Matching policy oid = %d", policynode->policy_oid)));
				num_matching_policies++;
			}							
		}		
    } 
	
	/*
	diffTimeInMicroSecs[0] = (endTime[0].tv_sec)*1000000 + endTime[0].tv_usec;
	diffTimeInMicroSecs[0] -= (startTime[0].tv_sec)*1000000 + startTime[0].tv_usec;

	diffTimeInMicroSecs[1] = (endTime[1].tv_sec)*1000000 + endTime[1].tv_usec;
	diffTimeInMicroSecs[1] -= (startTime[1].tv_sec)*1000000 + startTime[1].tv_usec;

	diffTimeInMicroSecs[2] = (endTime[2].tv_sec)*1000000 + endTime[2].tv_usec;
	diffTimeInMicroSecs[2] -= (startTime[2].tv_sec)*1000000 + startTime[2].tv_usec;

	diffTimeInMicroSecs[3] = (endTime[3].tv_sec)*1000000 + endTime[3].tv_usec;
	diffTimeInMicroSecs[3] -= (startTime[3].tv_sec)*1000000 + startTime[3].tv_usec;

	diffTimeInMicroSecs[4] = (endTime[4].tv_sec)*1000000 + endTime[4].tv_usec;
	diffTimeInMicroSecs[4] -= (startTime[4].tv_sec)*1000000 + startTime[4].tv_usec;

	diffTimeInMicroSecs[5] = (endTime[5].tv_sec)*1000000 + endTime[5].tv_usec;
	diffTimeInMicroSecs[5] -= (startTime[5].tv_sec)*1000000 + startTime[5].tv_usec;
	*/
	diffTimeInMicroSecs = (endTime.tv_sec)*1000000 + endTime.tv_usec;
	diffTimeInMicroSecs -= (startTime.tv_sec)*1000000 + startTime.tv_usec;
	//ereport(NOTICE,(errmsg("TimeInMicroSecs on pred evaluation = %d", pred_counters.time_spent_in_pred_evals)));
	//ereport(NOTICE,(errmsg("Total TimeInMicroSecs = %d", diffTimeInMicroSecs)));
	
	//ereport(NOTICE,(errmsg("Num matching policies = %d", num_matching_policies)));
	//ereport(NOTICE,(errmsg("total_preds_in_database = %d", pred_counters.total_preds_in_database)));
	//ereport(NOTICE,(errmsg("total_preds_with_policies = %d", pred_counters.total_preds_with_policies)));	
	//ereport(NOTICE,(errmsg("total_preds_evaluated = %d", pred_counters.total_preds_evaluated)));
	//ereport(NOTICE,(errmsg("total_preds_skipped_intelligently = %d", pred_counters.total_preds_skipped_intelligently)));		
/*
	for (i=0; i <=0; i++)
	{
		ereport(NOTICE,(errmsg("total_preds_evaluated_attr[%d] = %d",i,total_preds_evaluated_attr[i])));
		ereport(NOTICE,(errmsg("TimeInMicroSecs for attr[%d]= %d", i, diffTimeInMicroSecs[i])));
	}
*/	
	//ereport(NOTICE,(errmsg("pg_anomaly_response : TimeInMicroSecs = %d", diffTimeInMicroSecs)));
	/* free the hastable */
	//freeTable(policyHtable);
}

void pg_anomaly_response_with_cache(AnomalyAssessment *anomaly_assessment)
{	
	int i;
	myList *l;
	int	num_matching_policies;
	predCounters pred_counters;
	int	diffTimeInMicroSecs;
	struct timeval 	startTime, endTime;
	
	gettimeofday (&startTime, NULL);
	
	CatCList   *attr_list = (CatCList *)NIL;
	policyHtable = (myHashtable *)createHashtable(POLICY_HASHTABLESIZE);
	predHtable = (myHashtable *)createHashtable(PRED_HASHTABLESIZE);
	
	pred_counters.total_preds_in_database = 0;
	pred_counters.total_preds_with_policies = 0;
	pred_counters.total_preds_evaluated = 0;
	pred_counters.total_preds_skipped_intelligently = 0;
	pred_counters.time_spent_in_pred_evals = 0;
	
	
	/* scan through the Hashtable to get the policies that match */
	num_matching_policies = 0;
	for (i = 0; i < POLICY_HASHTABLESIZE; i++)
	{
		for(l = policyHtable->table[i]; l != NULL; l = l->next) 
		{
			policyHtableNode *policynode = (policyHtableNode *)l->val;			
			if (policynode->finalResult == true)
			{
				//ereport(NOTICE,(errmsg("Matching policy oid = %d", policynode->policy_oid)));
				num_matching_policies++;
			}							
		}		
    } 
	
	diffTimeInMicroSecs = (endTime.tv_sec)*1000000 + endTime.tv_usec;
	diffTimeInMicroSecs -= (startTime.tv_sec)*1000000 + startTime.tv_usec;
}

static
void evaluateAttribute(CatCList *attr_list, Datum anomalyVal, predCounters *pcs)
{
	int i;
	int j;
	int k;
	HeapTuple	attr_tup;
	Oid			attr_oid;
	Oid			attrType_oid;
	CatCList   *pred_list = (CatCList *)NIL;
	Datum		pred_val_text_datum;
	Datum 		pred_val_str_datum;
	bool 		result;
	bool 		isnull;
	bool 		pred_needs_evaluation;
	List		*new_cached_preds_list;
	List		*preds_list;
	
	int	diffTimeInMicroSecs;
	struct timeval 	startTime, endTime;
	
	attr_tup = &attr_list->members[0]->tuple; /* well we know that the attrname is the primary key here */
	attr_oid = HeapTupleGetOid(attr_tup);			
	attrType_oid = ((Form_pg_m_rpolicy_attrs) GETSTRUCT(attr_tup))->attrtype;
	
	/* now hit the pg_m_rpolicy_preds table */
	pred_list = SearchSysCacheList(Z_RPOLICYPREDSATTRID, 1, ObjectIdGetDatum(attr_oid), 0, 0, 0);
	
	/* do this for each predicate of this attribute */
	for (i = 0; i < pred_list->n_members; i++)
	{
		HeapTuple	pred_tup = &pred_list->members[i]->tuple;
		Oid			pred_id = HeapTupleGetOid(pred_tup);
		Oid			pred_opr_id = ((Form_pg_m_rpolicy_preds) GETSTRUCT(pred_tup))->oprid;
		CatCList	*pred_policy_list;
		char 		*pred_id_str;
		Datum 		pred_node;
		predHtableNode *predNode;
		

		pcs->total_preds_in_database++;		/* counter */
		pcs->total_preds_with_policies++;
		
		/* all policies that have this predicate */
		pred_policy_list = SearchSysCacheList(Z_RPOLICYPOLICYPREDSPREDID, 1, ObjectIdGetDatum(pred_id), 0, 0, 0);

		/* now check if this predicate actually has some policies to its credit or not */										
		if (pred_policy_list->n_members == 0)			/* this predicate is not in use */
		{
			pcs->total_preds_with_policies--;
			ReleaseSysCacheList(pred_policy_list);
			continue;
		}

		/* now get the predicate node from the predicate hash table */
		pred_id_str = convert_oid_to_string(pred_id, pred_id_str);
		pred_node = getVal(predHtable, PointerGetDatum(pred_id_str));						
		
		/* form a new predicate node if it is null */
		if (pred_node == (Datum)NIL)
		{
			predNode = (predHtableNode *)palloc(sizeof(predHtableNode));
			predNode->needsEvaluation = true;
			predNode->numPolicies = pred_policy_list->n_members;
			predNode->remainingPolicies = pred_policy_list->n_members;
			predNode->pred_oid = pred_id;
			insertVal(predHtable, PointerGetDatum(pred_id_str), (Datum)predNode);
			
			/* if it is not in the cached_privs_list yet then make the list */
			if (cached_preds_list == (List *)NIL)
				predCountNode *pNode = (predCountNode *)palloc(sizeof(predCountNode));
				pNode->predid = pred_id;
				pNode->policy_count = 0;
				cached_preds_list = list_make1(pNode);
			else
		}
		else /* the predicate node is already there, so check if needs to be evaluated or not */
		{
			predNode = (predHtableNode *)pred_node;
			if (!predNode->needsEvaluation) /* if the node does not required to be evaluated */
			{
				pcs->total_preds_skipped_intelligently++;
				ReleaseSysCacheList(pred_policy_list);
				continue;
			}			
		}
					
		/* now after these checks, go about evaluating the predicate */
		
		/* since it is a variable size attribute */
		pred_val_text_datum = SysCacheGetAttr(Z_RPOLICYPREDSATTRID, pred_tup,
										Anum_pg_m_rpolicy_preds_val, &isnull);
		
		/* get the string datum representation of the text value */
		pred_val_str_datum = DirectFunctionCall1(textout, pred_val_text_datum);
	
		pcs->total_preds_evaluated++;		
		
		gettimeofday (&startTime, NULL);
		result = evaluatePred(anomalyVal, 							/* value provided by detection engine */
							  attrType_oid,                         /* type of the value */
							  pred_opr_id,							/* the operator id of the predicate */
							  pred_val_str_datum);                  /* the predicate value(s) */

		gettimeofday (&endTime, NULL);
		diffTimeInMicroSecs = (endTime.tv_sec)*1000000 + endTime.tv_usec;
		diffTimeInMicroSecs -= (startTime.tv_sec)*1000000 + startTime.tv_usec;
		pcs->time_spent_in_pred_evals = pcs->time_spent_in_pred_evals + diffTimeInMicroSecs;
		
		/* .. now we have the result for the predicate .. so adjust the policy satisfaction accordingly */
		
		/* do this for all the policies applicable to this predicate */
		for (j=0; j < pred_policy_list->n_members; j++)
		{
			HeapTuple pred_policy_tup = &pred_policy_list->members[j]->tuple;
			Oid policy_id = ((Form_pg_m_rpolicy_policypreds) GETSTRUCT(pred_policy_tup))->policyid;
			policyHtableNode *pNode;
			CatCList *policy_pred_list;
			char *policy_id_str;
			Datum policy_node;
			
			/* get all the predicates that this policy has .. its ok to do this repeatedly since we have it cached ..*/
			policy_pred_list = SearchSysCacheList(Z_RPOLICYPOLICYPREDSPOLICYID, 1, ObjectIdGetDatum(policy_id), 0, 0, 0);										
			policy_id_str = convert_oid_to_string(policy_id, policy_id_str);	/* form the policy id as string */																							
			policy_node = getVal(policyHtable, PointerGetDatum(policy_id_str)); /* check if the policy is there in the hash table */					

			/* if not, then create it */
			if (policy_node == (Datum)NIL)
			{
				pNode = (policyHtableNode *)palloc(sizeof(policyHtableNode));
				pNode->policy_oid = policy_id;
				pNode->count_preds = policy_pred_list->n_members;
				pNode->remaining_preds = pNode->count_preds;
				pNode->isFinalResultSet = false;
				pNode->finalResult = false;
				insertVal(policyHtable, PointerGetDatum(policy_id_str), (Datum)pNode);
			}
			else /* the policy is already there in the policy hash table */									
				pNode = (policyHtableNode *)policy_node;

			pNode->remaining_preds--; /* subtracting 1 to account for the current predicate that was just evaluated */
			
			/* now set the policy verification according to the result of the predicate evaluation */
			if (result == false && !pNode->isFinalResultSet)
			{					
				/* this policy is false since one of its predicate has evaluated to false */
				pNode->isFinalResultSet = true;						
				pNode->finalResult = false;
				
				/********************** skipping this part to check affect on performance ************************************/
				/* for all the other predicates of this policy, set that they need not be evaluated if this was their only policy left*/ 
				for (k=0;k < policy_pred_list->n_members; k++)
				{
					HeapTuple	policy_pred_tup = &policy_pred_list->members[k]->tuple;
					Oid			policy_pred_id = ((Form_pg_m_rpolicy_policypreds) GETSTRUCT(policy_pred_tup))->predid;							
					char		*policy_pred_id_str = convert_oid_to_string(policy_pred_id, policy_pred_id_str);
					predHtableNode *policy_pred_node;
					
					if (policy_pred_id == pred_id) // not for this current predicate in progress .. it will set to "not need to be evaluated" later
						continue;
					
					Datum policy_pred_node_datum = getVal(predHtable, PointerGetDatum(policy_pred_id_str));
					
					CatCList *policy_cur_pred_list;
					
					/* policy_pred_id has not been touched yet . if it has been touched then its already been evaluated so nothing to do ..  */
					if (policy_pred_node_datum == (Datum)NIL) 
					{
						policy_pred_node = (predHtableNode *)palloc(sizeof(predHtableNode));
						
						/* check how many policies it belongs to */
						policy_cur_pred_list = SearchSysCacheList(Z_RPOLICYPOLICYPREDSPREDID, 1, ObjectIdGetDatum(policy_pred_id), 0, 0, 0);
						
						/* initialize predicate node */
						policy_pred_node->pred_oid = policy_pred_id;
						policy_pred_node->numPolicies = policy_cur_pred_list->n_members;		
						policy_pred_node->remainingPolicies = policy_cur_pred_list->n_members;
						policy_pred_node->needsEvaluation = true;														
						
						insertVal(predHtable, PointerGetDatum(policy_pred_id_str), (Datum)policy_pred_node);
						
						ReleaseSysCacheList(policy_cur_pred_list);
					}
					else							
						policy_pred_node = (predHtableNode *)policy_pred_node_datum;										
					
					if (policy_pred_node->needsEvaluation == true && policy_pred_node->remainingPolicies > 0) 
					{
						if (--(policy_pred_node->remainingPolicies) == 0)	/* this was the last policy */
							policy_pred_node->needsEvaluation = false;
					}																
				}					
			}
			else if (result == true && !pNode->isFinalResultSet)
			{							
				if (pNode->remaining_preds == 0)
				{
					pNode->isFinalResultSet = true;
					pNode->finalResult = true;
				}
			}			
			
			ReleaseSysCacheList(policy_pred_list);
		}									
		
		predNode->needsEvaluation = false; /* Being extra careful here .. otherwise not supposed to run in to a predicate twice */
		
		ReleaseSysCacheList(pred_policy_list);												
	}	
	
	ReleaseSysCacheList(pred_list);	
}

static
bool evaluatePred(Datum lhs, Oid type, Oid opr, Datum rhs)
{	
	Datum 			rhs_internal, lhs_internal;
	Datum			result;
	int 			i;
		
	result = false;
	
	if (type == 1028) /* the predicate value is an array of oids */
	{					
		if(opr == 2752) /* 2752 is oid for the <@ (is contained by) operator for any arrays */
		{
			/* convert the rhs into internal array format .. Oid for array_in = 750 .. */
			rhs_internal = OidInputFunctionCall(750, DatumGetPointer(rhs), 26, -1); /* 26 is the oid for 'oid' type */
			lhs_internal = OidInputFunctionCall(750, DatumGetPointer(lhs), 26, -1); /* 26 is the oid for 'oid' type*/
						
			/* Oid for arraycontained = 2749 */
			result = OidFunctionCall2(2749,lhs_internal,rhs_internal);						
		}
	}	
	else if (type ==  23) /* the predicate value is an integer representing the ACLMode */
	{
		char *commands = DatumGetPointer(rhs);
		AclMode	lhs_int = DatumGetInt32(lhs);
	 	AclMode	commands_int = 0;

		if (opr == 1883) /* check for the operator id, this is (~) operator but we know that we interpret this a IN */
		{
			/* we know that we are getting the binary representation of an integer as 32 char long string */
			for (i = strlen(commands) - 1; i >=0 ; i--)
			{
				if (commands[i] == '1')
				{
					AclMode commands_int = 1 << (strlen(commands) - 1 - i);
					
					result = ((commands_int & lhs_int) == commands_int) ? true : false;
					
					if (result == true)
						break;
				}
			}			
		}	
	}
	else if (type == 869) /* the predicate value is an inet datatype value */
	{
		/* get the inet representation from the input string datum */
		Datum rhs_inet_datum = DirectFunctionCall1(inet_in, rhs);  				
		Datum lhs_inet_datum = DirectFunctionCall1(inet_in, lhs);
		
		if (opr == 932) /* this means to check if the lhs is contained in the rhs, so this is the '<<=' operator for inet*/		
			result = DatumGetBool(DirectFunctionCall2(network_subeq,lhs_inet_datum,rhs_inet_datum));		 
	}
	
	else if (type == 1009) /* the predicate value is an array of text types */
	{
		if(opr == 2752) /* 2752 is oid for the <@ (is contained by) operator for any arrays */
		{
			/* convert the rhs into internal array format .. Oid for array_in = 750 .. */
			rhs_internal = OidInputFunctionCall(750, DatumGetPointer(rhs), 25, -1); /* 25 is the oid for 'text' type */
			lhs_internal = OidInputFunctionCall(750, DatumGetPointer(lhs), 25, -1); /* 25 is the oid for 'text' type*/
						
			/* Oid for arraycontained = 2749 */
			result = OidFunctionCall2(2749,lhs_internal,rhs_internal);						
		}
	}
    
	return result;
}

/*
 * initialization function (called by InitPostgres)
 */
void
initialize_idr(void)
{
	if (!IsBootstrapProcessingMode())
	{
		/*
		 * In normal mode, set a callback on any syscache invalidation of
		 * pg_m_rpolicy_def rows
		 */
		CacheRegisterSyscacheCallback(Z_RPOLICYDEFOID,
									  IDR_Preds_CacheCallback,
									  (Datum) 0);
	}
}

/*
 * RoleMembershipCacheCallback
 *		Syscache inval callback function
 */
static void
IDR_Preds_CacheCallback(Datum arg, Oid relid)
{
	/* Force IDR predicate count cache to be recomputed */
	cached_preds = InvalidOid;	
}

/*******************************************************************************************************************************************************
 * 
 * new version of pg_anomaly_response
 * 
 * 
 * *****************************************************************/


static
char *convert_oid_to_string(Oid inp_id, char *inp_str)
{
	/* first count the number of digits */
	int k = 0;
	Oid tmp_id = inp_id;
	
	do 
	{
		k++;
		tmp_id = tmp_id/10;
		
	} while(tmp_id > 0);
						
	inp_str = (char *)palloc(k+1); // k digits mean k+1 bytes should be enough to hold
	
	/* now fill in the values starting from the right hand side */
	inp_str[k] = '\0';
	do 
	{
		inp_str[--k] = myitoa[inp_id%10];
		inp_id = inp_id/10;
		
	} while(inp_id > 0);
	
	return inp_str;
}


/****************** HASHTABLE FUNCTIONS ******************/

static myHashtable *createHashtable(unsigned int size){

	myHashtable *newTable;
    int i;
    
    if (size < 1) 
		return (myHashtable *)0; /* invalid size for table */

    /* Attempt to allocate memory for the table structure */
    if ((newTable = (myHashtable *)malloc(sizeof(myHashtable))) == (myHashtable *)0)
        return (myHashtable *)0;
    
    /* Attempt to allocate memory for the table itself */
    if ((newTable->table = (myList **)malloc(sizeof(myList *) * size)) == (myList **)0) {
        return (myHashtable *)0;
    }

    /* Initialize the elements of the table */
    for(i=0; i<size; i++) 
		newTable->table[i] = (myList *)0;

    /* Set the table's size */
    newTable->size = size;

    return newTable;

}

static Datum 
getVal(myHashtable *hTable, Datum key_datum){
    
	myList *l;       
    unsigned int hashval = hash(hTable, key_datum);
    
    char *key = DatumGetPointer(key_datum);
    for(l = hTable->table[hashval]; l != NULL; l = l->next) {
        if (strcmp(key, (char *)l->key) == 0) 
			return l->val;
    }
    return (Datum)0;
}

static int 
insertVal(myHashtable *hTable, Datum key_datum, Datum val){

	myList *newList;

	char *key = DatumGetPointer(key_datum);
	
	/* item already exists, don't insert it again. */
    if (getVal(hTable, key_datum) != (Datum)0)
		return 1;

    unsigned int hashval = hash(hTable, key_datum);

    /* Attempt to allocate memory for list */
    if ((newList = (myList *)palloc(sizeof(myList))) == (myList *)0) 
		return -1;
        
    /* Insert into list (at the begining)*/
    newList->key = (Datum)strdup(key);
	newList->val = val;
    newList->next = hTable->table[hashval];
    hTable->table[hashval] = newList;

    return 1;
}

static void freeTable(myHashtable *hTable){
    int i;
    myList *l, *t;

    if (hTable==NULL) 
		return;

    /* Free the memory for every item in the table, including the 
     * keys and values themselves.
     */
    for(i=0; i < hTable->size; i++) {
        l = hTable->table[i];
        while(l != (myList *)0) {
            t = l;
            l = l->next;
            pfree((char *)t->key);
			pfree((policyHtableNode *)t->val);
            pfree(t);
        }
    }

    /* Free the table itself */
    pfree(hTable->table);
    pfree(hTable);
}

// private functions using static keyword
static unsigned int hash(myHashtable *hTable, Datum key_datum){
	
	unsigned int hashval;
    
	char *key = DatumGetPointer(key_datum);
	
    /* we start our hash out at 0 */
    hashval = 0;

    /* for each character, we multiply the old hash by 31 and add the current
     * character.  Remember that shifting a number left is equivalent to 
     * multiplying it by 2 raised to the number of places shifted.  So we 
     * are in effect multiplying hashval by 32 and then subtracting hashval.  
     * Why do we do this?  Because shifting and subtraction are much more 
     * efficient operations than multiplication.
     */
    for(; *key != '\0'; key++) 
		hashval = *key + (hashval << 5) - hashval;

    /* we then return the hash value mod the hashtable size so that it will
     * fit into the necessary range
     */
    return hashval % hTable->size;
}

/****************** HASHTABLE FUNCTIONS ******************/
