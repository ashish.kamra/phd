void pg_anomaly_response(AnomalyAssessment *anomaly_assessment)
{	
	int i;
	myList *l;
	int	num_matching_policies;
	predCounters pred_counters;
	int	diffTimeInMicroSecs;
	struct timeval 	startTime, endTime;
	
	gettimeofday (&startTime, NULL);
	
	CatCList   *attr_list = (CatCList *)NIL;
	policyHtable = (myHashtable *)createHashtable(POLICY_HASHTABLESIZE);
	predHtable = (myHashtable *)createHashtable(PRED_HASHTABLESIZE);
	
	pred_counters.total_preds_in_database = 0;
	pred_counters.total_preds_with_policies = 0;
	pred_counters.total_preds_evaluated = 0;
	pred_counters.total_preds_skipped_intelligently = 0;
	pred_counters.time_spent_in_pred_evals = 0;
	
	//total_preds_evaluated_attr[0] = 0;
	/* we go by alphabetical order of attributes name for the time being */
	/* command */
	
	attr_list = SearchSysCacheList(Z_RPOLICYATTRSATTRNAME, 1, PointerGetDatum("command"), 0, 0, 0);
	if (attr_list->n_members != 0)
		evaluateAttribute(attr_list, Int32GetDatum(anomaly_assessment->sql_cmd), &pred_counters);
	ReleaseSysCacheList(attr_list);
	
	//total_preds_evaluated_attr[0] = pred_counters.total_preds_evaluated;  
		
	/* user_id */
	//gettimeofday (&startTime[1], NULL);
	attr_list = SearchSysCacheList(Z_RPOLICYATTRSATTRNAME, 1, PointerGetDatum("user_id"), 0, 0, 0);
	if (attr_list->n_members != 0)
		evaluateAttribute(attr_list, anomaly_assessment->user_id, &pred_counters);
	ReleaseSysCacheList(attr_list);

	//gettimeofday (&endTime[1], NULL);
	//total_preds_evaluated_attr[1] = pred_counters.total_preds_evaluated - total_preds_evaluated_attr[0];  

	/* database_id */
	//gettimeofday (&startTime[2], NULL);
	attr_list = SearchSysCacheList(Z_RPOLICYATTRSATTRNAME, 1, PointerGetDatum("database_id"), 0, 0, 0);
	if (attr_list->n_members != 0)
		evaluateAttribute(attr_list, anomaly_assessment->db_id, &pred_counters);
	ReleaseSysCacheList(attr_list);

	//gettimeofday (&endTime[2], NULL);
	//total_preds_evaluated_attr[2] = pred_counters.total_preds_evaluated - total_preds_evaluated_attr[1];  
	
	/* relation_id */
	//gettimeofday (&startTime[3], NULL);
	
	attr_list = SearchSysCacheList(Z_RPOLICYATTRSATTRNAME, 1, PointerGetDatum("relation_id"), 0, 0, 0);
	if (attr_list->n_members != 0)
		evaluateAttribute(attr_list, anomaly_assessment->obj_ids, &pred_counters);
	ReleaseSysCacheList(attr_list);
	
	//gettimeofday (&endTime[3], NULL);
	//total_preds_evaluated_attr[3] = pred_counters.total_preds_evaluated - total_preds_evaluated_attr[2];  
	
	/* client application */
	//gettimeofday (&startTime[4], NULL);

	attr_list = SearchSysCacheList(Z_RPOLICYATTRSATTRNAME, 1, PointerGetDatum("client_app"), 0, 0, 0);
	if (attr_list->n_members != 0)
		evaluateAttribute(attr_list, anomaly_assessment->client_app, &pred_counters);
	ReleaseSysCacheList(attr_list);
	
	//gettimeofday (&endTime[4], NULL);
	//total_preds_evaluated_attr[4] = pred_counters.total_preds_evaluated - total_preds_evaluated_attr[3];  
	
	/* source_ip */
	//gettimeofday (&startTime[5], NULL);
	//gettimeofday (&startTime, NULL);
	attr_list = SearchSysCacheList(Z_RPOLICYATTRSATTRNAME, 1, PointerGetDatum("source_ip"), 0, 0, 0);
	if (attr_list->n_members != 0)
		evaluateAttribute(attr_list, anomaly_assessment->source_ip, &pred_counters);
	ReleaseSysCacheList(attr_list);		

	gettimeofday (&endTime, NULL);
	//ereport(NOTICE,(errmsg("preds_evaluated = %d", pred_counters.total_preds_evaluated)));

	//gettimeofday (&endTime[5], NULL);
	//total_preds_evaluated_attr[5] = pred_counters.total_preds_evaluated - total_preds_evaluated_attr[4];  
	
	/* scan through the Hashtable to get the policies that match */
	num_matching_policies = 0;
	for (i = 0; i < POLICY_HASHTABLESIZE; i++)
	{
		for(l = policyHtable->table[i]; l != NULL; l = l->next) 
		{
			policyHtableNode *policynode = (policyHtableNode *)l->val;			
			if (policynode->finalResult == true)
			{
				//ereport(NOTICE,(errmsg("Matching policy oid = %d", policynode->policy_oid)));
				num_matching_policies++;
			}							
		}		
    } 
	
	/*
	diffTimeInMicroSecs[0] = (endTime[0].tv_sec)*1000000 + endTime[0].tv_usec;
	diffTimeInMicroSecs[0] -= (startTime[0].tv_sec)*1000000 + startTime[0].tv_usec;

	diffTimeInMicroSecs[1] = (endTime[1].tv_sec)*1000000 + endTime[1].tv_usec;
	diffTimeInMicroSecs[1] -= (startTime[1].tv_sec)*1000000 + startTime[1].tv_usec;

	diffTimeInMicroSecs[2] = (endTime[2].tv_sec)*1000000 + endTime[2].tv_usec;
	diffTimeInMicroSecs[2] -= (startTime[2].tv_sec)*1000000 + startTime[2].tv_usec;

	diffTimeInMicroSecs[3] = (endTime[3].tv_sec)*1000000 + endTime[3].tv_usec;
	diffTimeInMicroSecs[3] -= (startTime[3].tv_sec)*1000000 + startTime[3].tv_usec;

	diffTimeInMicroSecs[4] = (endTime[4].tv_sec)*1000000 + endTime[4].tv_usec;
	diffTimeInMicroSecs[4] -= (startTime[4].tv_sec)*1000000 + startTime[4].tv_usec;

	diffTimeInMicroSecs[5] = (endTime[5].tv_sec)*1000000 + endTime[5].tv_usec;
	diffTimeInMicroSecs[5] -= (startTime[5].tv_sec)*1000000 + startTime[5].tv_usec;
	*/
	diffTimeInMicroSecs = (endTime.tv_sec)*1000000 + endTime.tv_usec;
	diffTimeInMicroSecs -= (startTime.tv_sec)*1000000 + startTime.tv_usec;
	ereport(NOTICE,(errmsg("TimeInMicroSecs on pred evaluation = %d", pred_counters.time_spent_in_pred_evals)));
	ereport(NOTICE,(errmsg("Total TimeInMicroSecs = %d", diffTimeInMicroSecs)));
	
	ereport(NOTICE,(errmsg("Num matching policies = %d", num_matching_policies)));
	ereport(NOTICE,(errmsg("total_preds_in_database = %d", pred_counters.total_preds_in_database)));
	ereport(NOTICE,(errmsg("total_preds_with_policies = %d", pred_counters.total_preds_with_policies)));	
	ereport(NOTICE,(errmsg("total_preds_evaluated = %d", pred_counters.total_preds_evaluated)));
	ereport(NOTICE,(errmsg("total_preds_skipped_intelligently = %d", pred_counters.total_preds_skipped_intelligently)));		
/*
	for (i=0; i <=0; i++)
	{
		ereport(NOTICE,(errmsg("total_preds_evaluated_attr[%d] = %d",i,total_preds_evaluated_attr[i])));
		ereport(NOTICE,(errmsg("TimeInMicroSecs for attr[%d]= %d", i, diffTimeInMicroSecs[i])));
	}
*/	
	//ereport(NOTICE,(errmsg("pg_anomaly_response : TimeInMicroSecs = %d", diffTimeInMicroSecs)));
	/* free the hastable */
	//freeTable(policyHtable);
}

static
void evaluateAttribute(CatCList *attr_list, Datum anomalyVal, predCounters *pcs)
{
	int i;
	int j;
	int k;
	HeapTuple	attr_tup;
	Oid			attr_oid;
	Oid			attrType_oid;
	CatCList   *pred_list = (CatCList *)NIL;
	Datum		pred_val_text_datum;
	Datum 		pred_val_str_datum;
	bool 		result;
	bool 		isnull;
	bool 		pred_needs_evaluation;

	int	diffTimeInMicroSecs;
	struct timeval 	startTime, endTime;
	
	attr_tup = &attr_list->members[0]->tuple; /* well we know that the attrname is the primary key here */
	attr_oid = HeapTupleGetOid(attr_tup);			
	attrType_oid = ((Form_pg_m_rpolicy_attrs) GETSTRUCT(attr_tup))->attrtype;
	
	/* now hit the pg_m_rpolicy_preds table */
	pred_list = SearchSysCacheList(Z_RPOLICYPREDSATTRID, 1, ObjectIdGetDatum(attr_oid), 0, 0, 0);
	
	/* do this for each predicate of this attribute */
	for (i = 0; i < pred_list->n_members; i++)
	{
		HeapTuple	pred_tup = &pred_list->members[i]->tuple;
		Oid			pred_id = HeapTupleGetOid(pred_tup);
		Oid			pred_opr_id = ((Form_pg_m_rpolicy_preds) GETSTRUCT(pred_tup))->oprid;
		CatCList	*pred_policy_list;
		char 		*pred_id_str;
		Datum 		pred_node;
		predHtableNode *predNode;
		

		pcs->total_preds_in_database++;		/* counter */
		pcs->total_preds_with_policies++;
		
		/* all policies that have this predicate */
		pred_policy_list = SearchSysCacheList(Z_RPOLICYPOLICYPREDSPREDID, 1, ObjectIdGetDatum(pred_id), 0, 0, 0);

		/* now check if this predicate actually has some policies to its credit or not */										
		if (pred_policy_list->n_members == 0)			/* this predicate is not in use */
		{
			pcs->total_preds_with_policies--;
			ReleaseSysCacheList(pred_policy_list);
			continue;
		}

		/* now get the predicate node from the predicate hash table */
		pred_id_str = convert_oid_to_string(pred_id, pred_id_str);
		pred_node = getVal(predHtable, PointerGetDatum(pred_id_str));						
		
		/* form a new predicate node if it is null */
		if (pred_node == (Datum)NIL)
		{
			predNode = (predHtableNode *)palloc(sizeof(predHtableNode));
			predNode->needsEvaluation = true;
			predNode->numPolicies = pred_policy_list->n_members;
			predNode->remainingPolicies = pred_policy_list->n_members;
			predNode->pred_oid = pred_id;
			insertVal(predHtable, PointerGetDatum(pred_id_str), (Datum)predNode);
		}
		else /* the predicate node is already there, so check if needs to be evaluated or not */
		{
			predNode = (predHtableNode *)pred_node;
			if (!predNode->needsEvaluation) /* if the node does not required to be evaluated */
			{
				pcs->total_preds_skipped_intelligently++;
				ReleaseSysCacheList(pred_policy_list);
				continue;
			}			
		}
					
		/* now after these checks, go about evaluating the predicate */
		
		/* since it is a variable size attribute */
		pred_val_text_datum = SysCacheGetAttr(Z_RPOLICYPREDSATTRID, pred_tup,
										Anum_pg_m_rpolicy_preds_val, &isnull);
		
		/* get the string datum representation of the text value */
		pred_val_str_datum = DirectFunctionCall1(textout, pred_val_text_datum);
	
		pcs->total_preds_evaluated++;		
		
		gettimeofday (&startTime, NULL);
		result = evaluatePred(anomalyVal, 							/* value provided by detection engine */
							  attrType_oid,                         /* type of the value */
							  pred_opr_id,							/* the operator id of the predicate */
							  pred_val_str_datum);                  /* the predicate value(s) */

		gettimeofday (&endTime, NULL);
		diffTimeInMicroSecs = (endTime.tv_sec)*1000000 + endTime.tv_usec;
		diffTimeInMicroSecs -= (startTime.tv_sec)*1000000 + startTime.tv_usec;
		pcs->time_spent_in_pred_evals = pcs->time_spent_in_pred_evals + diffTimeInMicroSecs;
		
		/* .. now we have the result for the predicate .. so adjust the policy satisfaction accordingly */
		
		/* do this for all the policies applicable to this predicate */
		for (j=0; j < pred_policy_list->n_members; j++)
		{
			HeapTuple pred_policy_tup = &pred_policy_list->members[j]->tuple;
			Oid policy_id = ((Form_pg_m_rpolicy_policypreds) GETSTRUCT(pred_policy_tup))->policyid;
			policyHtableNode *pNode;
			CatCList *policy_pred_list;
			char *policy_id_str;
			Datum policy_node;
			
			/* get all the predicates that this policy has .. its ok to do this repeatedly since we have it cached ..*/
			policy_pred_list = SearchSysCacheList(Z_RPOLICYPOLICYPREDSPOLICYID, 1, ObjectIdGetDatum(policy_id), 0, 0, 0);										
			policy_id_str = convert_oid_to_string(policy_id, policy_id_str);	/* form the policy id as string */																							
			policy_node = getVal(policyHtable, PointerGetDatum(policy_id_str)); /* check if the policy is there in the hash table */					

			/* if not, then create it */
			if (policy_node == (Datum)NIL)
			{
				pNode = (policyHtableNode *)palloc(sizeof(policyHtableNode));
				pNode->policy_oid = policy_id;
				pNode->count_preds = policy_pred_list->n_members;
				pNode->remaining_preds = pNode->count_preds;
				pNode->isFinalResultSet = false;
				pNode->finalResult = false;
				insertVal(policyHtable, PointerGetDatum(policy_id_str), (Datum)pNode);
			}
			else /* the policy is already there in the policy hash table */									
				pNode = (policyHtableNode *)policy_node;

			pNode->remaining_preds--; /* subtracting 1 to account for the current predicate that was just evaluated */
			
			/* now set the policy verification according to the result of the predicate evaluation */
			if (result == false && !pNode->isFinalResultSet)
			{					
				/* this policy is false since one of its predicate has evaluated to false */
				pNode->isFinalResultSet = true;						
				pNode->finalResult = false;
				
				/********************** skipping this part to check affect on performance ************************************/
				/* for all the other predicates of this policy, set that they need not be evaluated if this was their only policy left*/ 
				for (k=0;k < policy_pred_list->n_members; k++)
				{
					HeapTuple	policy_pred_tup = &policy_pred_list->members[k]->tuple;
					Oid			policy_pred_id = ((Form_pg_m_rpolicy_policypreds) GETSTRUCT(policy_pred_tup))->predid;							
					char		*policy_pred_id_str = convert_oid_to_string(policy_pred_id, policy_pred_id_str);
					predHtableNode *policy_pred_node;
					
					if (policy_pred_id == pred_id) // not for this current predicate in progress .. it will set to "not need to be evaluated" later
						continue;
					
					Datum policy_pred_node_datum = getVal(predHtable, PointerGetDatum(policy_pred_id_str));
					
					CatCList *policy_cur_pred_list;
					
					/* policy_pred_id has not been touched yet . if it has been touched then its already been evaluated so nothing to do ..  */
					if (policy_pred_node_datum == (Datum)NIL) 
					{
						policy_pred_node = (predHtableNode *)palloc(sizeof(predHtableNode));
						
						/* check how many policies it belongs to */
						policy_cur_pred_list = SearchSysCacheList(Z_RPOLICYPOLICYPREDSPREDID, 1, ObjectIdGetDatum(policy_pred_id), 0, 0, 0);
						
						/* initialize predicate node */
						policy_pred_node->pred_oid = policy_pred_id;
						policy_pred_node->numPolicies = policy_cur_pred_list->n_members;		
						policy_pred_node->remainingPolicies = policy_cur_pred_list->n_members;
						policy_pred_node->needsEvaluation = true;														
						
						insertVal(predHtable, PointerGetDatum(policy_pred_id_str), (Datum)policy_pred_node);
						
						ReleaseSysCacheList(policy_cur_pred_list);
					}
					else							
						policy_pred_node = (predHtableNode *)policy_pred_node_datum;										
					
					if (policy_pred_node->needsEvaluation == true && policy_pred_node->remainingPolicies > 0) 
					{
						if (--(policy_pred_node->remainingPolicies) == 0)	/* this was the last policy */
							policy_pred_node->needsEvaluation = false;
					}																
				}					
			}
			else if (result == true && !pNode->isFinalResultSet)
			{							
				if (pNode->remaining_preds == 0)
				{
					pNode->isFinalResultSet = true;
					pNode->finalResult = true;
				}
			}			
			
			ReleaseSysCacheList(policy_pred_list);
		}									
		
		predNode->needsEvaluation = false; /* Being extra careful here .. otherwise not supposed to run in to a predicate twice */
		
		ReleaseSysCacheList(pred_policy_list);												
	}	
	
	ReleaseSysCacheList(pred_list);	
}
