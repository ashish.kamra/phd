/*-------------------------------------------------------------------------
 *
 * pg_m_rpolicy_actions.h
 *	  definition of the system "pg_m_rpolicy_actions" relation
 *	  along with the relation's initial contents.
 *
 *
 * Portions Copyright (c) 2008-2010, Ashish Kamra, Purdue University
 *
 * $PostgreSQL: pgsql/src/include/catalog/pg_m_rpolicy_actions.h,v 1 2008/10/18
 *
 * NOTES
 *	  the genbki.sh script reads this file and generates .bki
 *	  information from the DATA() statements.
 *
 *-------------------------------------------------------------------------
 */
#ifndef pg_m_rpolicy_actions_H
#define pg_m_rpolicy_actions_H

/* ----------------
 *		postgres.h contains the system type definitions and the
 *		CATALOG(), BKI_BOOTSTRAP and DATA() sugar words so this file
 *		can be read by both genbki.sh and the C compiler.
 * ----------------
 */

/* ----------------
 *		pg_m_rpolicy_actions definition.  cpp turns this into
 *		typedef struct FormData_pg_m_rpolicy_actions
 * ----------------
 */
#define RPolicyActionsRelationId	4010

CATALOG(pg_m_rpolicy_actions,4010) BKI_SHARED_RELATION
{
	NameData	actionname;			/* name of the action */
	int2		severity;			/* The severity level of the action */
} FormData_pg_m_rpolicy_actions;

/* ----------------
 *		Form_pg_m_rpolicy_actions corresponds to a pointer to a row with
 *		the format of pg_m_rpolicy_actions relation.
 * ----------------
 */
typedef FormData_pg_m_rpolicy_actions *Form_pg_m_rpolicy_actions;

/* ----------------
 *		compiler constants for pg_m_rpolicy_actions
 * ----------------
 */
#define Natts_pg_m_rpolicy_actions					2
#define Anum_pg_m_rpolicy_actions_actionname		1
#define Anum_pg_m_rpolicy_actions_severity			2

/* ----------------
 *		initial contents of pg_m_rpolicy_actions
 * ----------------
 */

 /* NONE */

#endif /*pg_m_rpolicy_actions_H*/
