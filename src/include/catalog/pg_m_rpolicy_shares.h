/*-------------------------------------------------------------------------
 *
 * pg_m_rpolicy_shares.h
 *	  definition of the system "pg_m_rpolicy_shares" relation
 *	  along with the relation's initial contents.
 *
 *
 * Portions Copyright (c) 2008-2010, Ashish Kamra, Purdue University
 *
 * $PostgreSQL: pgsql/src/include/catalog/pg_m_rpolicy_shares.h,v 1 2008/10/18
 *
 * NOTES
 *	  the genbki.sh script reads this file and generates .bki
 *	  information from the DATA() statements.
 *
 *-------------------------------------------------------------------------
 */

#ifndef pg_m_rpolicy_shares_H
#define pg_m_rpolicy_shares_H

/* ----------------
 *		postgres.h contains the system type definitions and the
 *		CATALOG(), BKI_BOOTSTRAP and DATA() sugar words so this file
 *		can be read by both genbki.sh and the C compiler.
 * ----------------
 */

/* ----------------
 *		pg_m_rpolicy_shares definition.  cpp turns this into
 *		typedef struct FormData_pg_m_rpolicy_shares
 * ----------------
 */
#define RPolicySharesRelationId	4030

CATALOG(pg_m_rpolicy_shares,4030) BKI_SHARED_RELATION
{
	int4			l;		/* number of users */
	int4			k;		/* threshold 'k' */		 		
	
	/* Variable Length fields */
	text		e;				/* Public Key */
	text		n;				/* Public Key, BIGNUM n = p*q */
	text		u;				/* an element with Jacobi symbol (u|n) = -1 */ 
	Oid			user_ids[1];	/* Oids of the users whose shares are stored */
	text		shares[1];		/* shares of the respective user ids */
	
} FormData_pg_m_rpolicy_shares;

/* ----------------
 *		Form_pg_m_rpolicy_shares corresponds to a pointer to a row with
 *		the format of pg_m_rpolicy_shares relation.
 * ----------------
 */
typedef FormData_pg_m_rpolicy_shares *Form_pg_m_rpolicy_shares;

/* ----------------
 *		compiler constants for pg_m_rpolicy_shares
 * ----------------
 */
#define Natts_pg_m_rpolicy_shares				7
#define Anum_pg_m_rpolicy_shares_l				1
#define Anum_pg_m_rpolicy_shares_k				2
#define Anum_pg_m_rpolicy_shares_e				3
#define Anum_pg_m_rpolicy_shares_n				4
#define Anum_pg_m_rpolicy_shares_u				5
#define Anum_pg_m_rpolicy_shares_user_ids		6
#define Anum_pg_m_rpolicy_shares_shares			7


/* ----------------
 *		initial contents of pg_m_rpolicy_shares
 * ----------------
 */

 /* NONE */

#endif /*pg_m_rpolicy_shares_H*/
