/*-------------------------------------------------------------------------
 *
 * pg_m_rpolicy_policypreds.h
 *	  definition of the system "pg_m_rpolicy_policypreds" relation
 *	  along with the relation's initial contents.
 *
 *
 * Portions Copyright (c) 2008-2010, Ashish Kamra, Purdue University
 *
 * $PostgreSQL: pgsql/src/include/catalog/pg_m_rpolicy_policypreds.h,v 1 2008/10/18
 *
 * NOTES
 *	  the genbki.sh script reads this file and generates .bki
 *	  information from the DATA() statements.
 *
 *-------------------------------------------------------------------------
 */
#ifndef pg_m_rpolicy_policypreds_H
#define pg_m_rpolicy_policypreds_H

/* ----------------
 *		postgres.h contains the system type definitions and the
 *		CATALOG(), BKI_BOOTSTRAP and DATA() sugar words so this file
 *		can be read by both genbki.sh and the C compiler.
 * ----------------
 */

/* ----------------
 *		pg_m_rpolicy_policypreds definition.  cpp turns this into
 *		typedef struct FormData_pg_m_rpolicy_policypreds
 * ----------------
 */
#define RPolicyPolicyPredsRelationId	4025

CATALOG(pg_m_rpolicy_policypreds,4025) BKI_SHARED_RELATION
{
	Oid			predid;			/* OID of the predicate */
	Oid			policyid;		/* OID of the policy */
	int4		eventid;		/* the event id associated with the policy */
} FormData_pg_m_rpolicy_policypreds;

/* ----------------
 *		Form_pg_m_rpolicy_policypreds corresponds to a pointer to a row with
 *		the format of pg_m_rpolicy_policypreds relation.
 * ----------------
 */
typedef FormData_pg_m_rpolicy_policypreds *Form_pg_m_rpolicy_policypreds;

/* ----------------
 *		compiler constants for pg_m_rpolicy_policypreds
 * ----------------
 */
#define Natts_pg_m_rpolicy_policypreds				3
#define Anum_pg_m_rpolicy_policypreds_predid		1
#define Anum_pg_m_rpolicy_policypreds_policyid		2
#define Anum_pg_m_rpolicy_policypreds_eventid		3


/* ----------------
 *		initial contents of pg_m_rpolicy_policypreds
 * ----------------
 */

 /* NONE */

#endif /*pg_m_rpolicy_policypreds_H*/
