/*-------------------------------------------------------------------------
 *
 * pg_m_rpolicy_preds.h
 *	  definition of the system "pg_m_rpolicy_preds" relation
 *	  along with the relation's initial contents.
 *
 *
 * Portions Copyright (c) 2008-2010, Ashish Kamra, Purdue University
 *
 * $PostgreSQL: pgsql/src/include/catalog/pg_m_rpolicy_preds.h,v 1 2008/10/18
 *
 * NOTES
 *	  the genbki.sh script reads this file and generates .bki
 *	  information from the DATA() statements.
 *
 *-------------------------------------------------------------------------
 */
#ifndef pg_m_rpolicy_preds_H
#define pg_m_rpolicy_preds_H

/* ----------------
 *		postgres.h contains the system type definitions and the
 *		CATALOG(), BKI_BOOTSTRAP and DATA() sugar words so this file
 *		can be read by both genbki.sh and the C compiler.
 * ----------------
 */

/* ----------------
 *		pg_m_rpolicy_preds definition.  cpp turns this into
 *		typedef struct FormData_pg_m_rpolicy_preds
 * ----------------
 */
#define RPolicyPredsRelationId	4005

CATALOG(pg_m_rpolicy_preds,4005) BKI_SHARED_RELATION
{
	NameData	predname;		/* name of the predicate */
	Oid			attrid;			/* OID of the attribute involved */
	Oid			oprid;			/* OID of the operator involved */
	text		val;			/* The value to which the attribute is compared in the predicate */
} FormData_pg_m_rpolicy_preds;

/* ----------------
 *		Form_pg_m_rpolicy_preds corresponds to a pointer to a row with
 *		the format of pg_m_rpolicy_preds relation.
 * ----------------
 */
typedef FormData_pg_m_rpolicy_preds *Form_pg_m_rpolicy_preds;

/* ----------------
 *		compiler constants for pg_m_rpolicy_preds
 * ----------------
 */
#define Natts_pg_m_rpolicy_preds					4
#define Anum_pg_m_rpolicy_preds_predname			1
#define Anum_pg_m_rpolicy_preds_attrid				2
#define Anum_pg_m_rpolicy_preds_oprid				3
#define Anum_pg_m_rpolicy_preds_val					4

/* ----------------
 *		initial contents of pg_m_rpolicy_preds
 * ----------------
 */

 /* NONE */

#endif /*pg_m_rpolicy_preds_H*/
