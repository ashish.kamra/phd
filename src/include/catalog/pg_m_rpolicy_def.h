/*-------------------------------------------------------------------------
 *
 * pg_m_rpolicy_def.h
 *	  definition of the system "pg_m_rpolicy_def" relation
 *	  along with the relation's initial contents.
 *
 *
 * Portions Copyright (c) 2008-2010, Ashish Kamra, Purdue University
 *
 * $PostgreSQL: pgsql/src/include/catalog/pg_m_rpolicy_def.h,v 1 2008/10/18
 *
 * NOTES
 *	  the genbki.sh script reads this file and generates .bki
 *	  information from the DATA() statements.
 *
 *-------------------------------------------------------------------------
 */
#ifndef pg_m_rpolicy_def_H
#define pg_m_rpolicy_def_H

/* ----------------
 *		postgres.h contains the system type definitions and the
 *		CATALOG(), BKI_BOOTSTRAP and DATA() sugar words so this file
 *		can be read by both genbki.sh and the C compiler.
 * ----------------
 */

/* ----------------
 *		pg_m_rpolicy_def definition.  cpp turns this into
 *		typedef struct FormData_pg_m_rpolicy_def
 * ----------------
 */
#define RPolicyDefRelationId	4015

CATALOG(pg_m_rpolicy_def,4015) BKI_SHARED_RELATION
{
	NameData	policyname;			/* name of the policy */
	int4		severity;			/* The severity level of the policy */
	int4 		eventid;			/* event associated with the policy 
									   1 = any query
									   2 = anomalous query */
	
	/* Variable Length fields */
	Oid			condition[1];			/* Array of predicate Oids - used in policy hashing */
	Oid			intialaction[1];		/* Array of sequence of initial action Oids */
	Oid			confirmationaction[1];	/* Array of sequence of confirmation action Oids */
	Oid			resolutionaction[1];	/* Array of sequence of resolution action Oids */
	Oid			failureaction[1];		/* Array of sequence of failure action Oids */	
} FormData_pg_m_rpolicy_def;

/* ----------------
 *		Form_pg_m_rpolicy_def corresponds to a pointer to a row with
 *		the format of pg_m_rpolicy_def relation.
 * ----------------
 */
typedef FormData_pg_m_rpolicy_def *Form_pg_m_rpolicy_def;

/* ----------------
 *		compiler constants for pg_m_rpolicy_def
 * ----------------
 */
#define Natts_pg_m_rpolicy_def						8
#define Anum_pg_m_rpolicy_def_policyname			1
#define Anum_pg_m_rpolicy_def_severity				2
#define Anum_pg_m_rpolicy_def_eventid				3
#define Anum_pg_m_rpolicy_def_condition				4
#define Anum_pg_m_rpolicy_def_initialaction			5
#define Anum_pg_m_rpolicy_def_confirmationaction	6
#define Anum_pg_m_rpolicy_def_resolutionaction		7
#define Anum_pg_m_rpolicy_def_failureaction			8

/* ----------------
 *		initial contents of pg_m_rpolicy_def
 * ----------------
 */

 /* NONE */

#endif /*pg_m_rpolicy_def_H*/
