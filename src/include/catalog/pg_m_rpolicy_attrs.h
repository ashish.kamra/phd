/*-------------------------------------------------------------------------
 *
 * pg_m_rpolicy_attrs.h
 *	  definition of the system "pg_m_rpolicy_attrs" relation
 *	  along with the relation's initial contents.
 *
 *
 * Portions Copyright (c) 2008-2010, Ashish Kamra, Purdue University
 *
 * $PostgreSQL: pgsql/src/include/catalog/pg_m_rpolicy_attrs.h,v 1 2008/10/18
 *
 * NOTES
 *	  the genbki.sh script reads this file and generates .bki
 *	  information from the DATA() statements.
 *
 *-------------------------------------------------------------------------
 */
#ifndef pg_m_rpolicy_attrs_H
#define pg_m_rpolicy_attrs_H

/* ----------------
 *		postgres.h contains the system type definitions and the
 *		CATALOG(), BKI_BOOTSTRAP and DATA() sugar words so this file
 *		can be read by both genbki.sh and the C compiler.
 * ----------------
 */

/* ----------------
 *		pg_m_rpolicy_attrs definition.  cpp turns this into
 *		typedef struct FormData_pg_m_rpolicy_attrs
 * ----------------
 */
#define RPolicyAttrsRelationId	4000

CATALOG(pg_m_rpolicy_attrs,4000) BKI_SHARED_RELATION
{
	NameData	attrname;		/* name of the attribute */
	Oid			attrtype;		/* OID representing the attribute type */
} FormData_pg_m_rpolicy_attrs;

/* ----------------
 *		Form_pg_m_rpolicy_attrs corresponds to a pointer to a row with
 *		the format of pg_m_rpolicy_attrs relation.
 * ----------------
 */
typedef FormData_pg_m_rpolicy_attrs *Form_pg_m_rpolicy_attrs;

/* ----------------
 *		compiler constants for pg_m_rpolicy_attrs
 * ----------------
 */
#define Natts_pg_m_rpolicy_attrs					2
#define Anum_pg_m_rpolicy_attrs_attrname			1
#define Anum_pg_m_rpolicy_attrs_attrtype			2

/* ----------------
 *		initial contents of pg_m_rpolicy_attrs
 * ----------------
 */

 /* NONE */

#endif /*pg_m_rpolicy_attrs_H*/
