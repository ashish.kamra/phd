/*-------------------------------------------------------------------------
 *
 * pg_m_rpolicy_adm.h
 *	  definition of the system "pg_m_rpolicy_adm" relation
 *	  along with the relation's initial contents.
 *
 *
 * Portions Copyright (c) 2008-2010, Ashish Kamra, Purdue University
 *
 * $PostgreSQL: pgsql/src/include/catalog/pg_m_rpolicy_adm.h,v 1 2008/10/18
 *
 * NOTES
 *	  the genbki.sh script reads this file and generates .bki
 *	  information from the DATA() statements.
 *
 *-------------------------------------------------------------------------
 */
#ifndef pg_m_rpolicy_adm_H
#define pg_m_rpolicy_adm_H

/* ----------------
 *		postgres.h contains the system type definitions and the
 *		CATALOG(), BKI_BOOTSTRAP and DATA() sugar words so this file
 *		can be read by both genbki.sh and the C compiler.
 * ----------------
 */

/* ----------------
 *		pg_m_rpolicy_adm definition.  cpp turns this into
 *		typedef struct FormData_pg_m_rpolicy_adm
 * ----------------
 */
#define RPolicyAdmRelationId	4020

CATALOG(pg_m_rpolicy_adm,4020) BKI_SHARED_RELATION
{
	Oid			policyid;				/* policy Oid */	
	int4		joint_admns;			/* threshold : k */
	int4		num_admns;				/* l */
	int4		rem_admns;				/* remaining admin that need to authorized the policy : r */
	
	/* Variable Length fields */				
	text		state;					/* Current Policy State */
	text		signature[1];			/* joint signature */
	int4		user_index[1];			/* index of the users (from pg_m_rpolicy_shares) who have signed the policy */ 
	 
} FormData_pg_m_rpolicy_adm;

/* ----------------
 *		Form_pg_m_rpolicy_adm corresponds to a pointer to a row with
 *		the format of pg_m_rpolicy_adm relation.
 * ----------------
 */
typedef FormData_pg_m_rpolicy_adm *Form_pg_m_rpolicy_adm;

/* ----------------
 *		compiler constants for pg_m_rpolicy_adm
 * ----------------
 */
#define Natts_pg_m_rpolicy_adm				7
#define Anum_pg_m_rpolicy_adm_policyid		1
#define Anum_pg_m_rpolicy_adm_jointadmns	2
#define Anum_pg_m_rpolicy_adm_numadmns		3
#define Anum_pg_m_rpolicy_adm_remadmns		4
#define Anum_pg_m_rpolicy_adm_state			5
#define Anum_pg_m_rpolicy_adm_signature		6
#define Anum_pg_m_rpolicy_adm_user_index	7

/* ----------------
 *		initial contents of pg_m_rpolicy_adm
 * ----------------
 */

 /* NONE */

#endif /*pg_m_rpolicy_adm_H*/
