/*-------------------------------------------------------------------------
 *
 * anomaly.h
 *	  Definition of (and support for) anomaly detection and response data structures.
 * 
 * Author: Ashish Kamra, Purdue University
 *-------------------------------------------------------------------------
 */

#ifndef ANOMALY_H
#define ANOMALY_H

#include "fmgr.h"

#include "utils/inet.h"
#include "utils/date.h"
#include "utils/acl.h"
#include "miscadmin.h"

#include "utils/catcache.h"
#include "utils/syscache.h"

#include "catalog/pg_m_rpolicy_attrs.h"
#include "catalog/pg_m_rpolicy_preds.h"
#include "catalog/pg_m_rpolicy_policypreds.h"

#include "access/htup.h"

#include <stdlib.h>

typedef struct QueryAssessment
{
	/* contextual */	
	Datum	user_id;   /* oid array */
	Datum	source_ip; /* inet */
	Datum   client_app; /* text array */
	Datum 	an_date;   /* DateADT */
	Datum	an_time;   /* TimeADT */
	
	/* structural */
	Datum	db_id; /* oid array */
	Datum	schema_id; /* not used */
	char	*object_type; /* not used */	
	AclMode	sql_cmd; /* integer */
	Datum	obj_ids; /* oid array */
	
} QueryAssessment; 

/* interfaces */
extern void pg_query_assessment(QueryAssessment *query_assessment, const Query *query);
extern bool pg_anomaly_detection(QueryAssessment *query_assessment, const Query *query);
extern void pg_query_response(QueryAssessment *query_assessment, uint32 eventid);
extern void initialize_idr(void);
//extern void test_pg_roles_scan(void);

/************************* HASHTABLE **************************/

// chaining list structure
typedef struct list_t 
{
    Datum key;
	Datum val; // value of any pointer type
    struct list_t *next;
} myList;

// hashtable
typedef struct hash_table_t
{
    Datum size;       /* the size of the table */
    myList **table; /* the table elements */
} myHashtable;


// policy node val in the hash table
typedef struct policy_htable_node 
{
	int		policy_oid;
	int		count_preds;
	int		remaining_preds;
	bool	isFinalResultSet;
	bool	finalResult;
	bool	isSigChecked;
	bool	isSigValid;
} policyHtableNode;

typedef struct pred_htable_node
{
	int	 pred_oid;
	bool needsEvaluation;
	int  numPolicies;
	float	weight;
	int  remainingPolicies;
} predHtableNode;

typedef struct predCounters 
{
	int	total_preds_evaluated;
	int	total_preds_in_database;
	int	total_preds_with_policies;
	int	total_preds_skipped_intelligently;
	int	time_spent_in_pred_evals;
	int num_matching_policies;
	int sig_verification_time;
	int num_policy_sigantures;
} predCounters;

typedef struct predCount_Node
{
	int pred_oid;
	float	weight; // 			if (USE_INTELLI_PRED_EVAL == 2) weight = num policies else weight is a special function
	struct predCount_Node *next; 
	struct predCount_Node *prev;
} predCountList;
/************************* HASHTABLE **************************/


#endif /*ANOMALY_H*/
