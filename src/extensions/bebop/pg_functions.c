#include "postgres.h"
#include <utils/varbit.h>
#include "executor/spi.h"       /* this is what you need to work with SPI */
#include "fmgr.h"
#include "rand_graph_gen.h"
#include "assert.h"

#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

PG_FUNCTION_INFO_V1(create_role_hierarchy);
PG_FUNCTION_INFO_V1(create_priv_hierarchy);
PG_FUNCTION_INFO_V1(assign_roles);

int
create_role_hierarchy(PG_FUNCTION_ARGS)
{
    int         ret,i,j;
    int             numRowsProcessed;
    char *buf;
    bool isnull;
    Datum origval, shiftedval;
    
    /* connect to SPI manager */
    if ((ret = SPI_connect()) < 0)
        elog(INFO, "new_role_hierarchy : SPI_connect returned %d", ret);
    
    ret = SPI_execute("DELETE FROM pgm_role_hierarchy",false,0);
    
    int32 max_levels = PG_GETARG_INT32(0);
    int32 max_children = PG_GETARG_INT32(1);
    role_hierarchy rh = new_role_hierarchy(max_levels,max_children);
    //print_graph(rh);
    Oid argtypes[3];
    argtypes[0]=23;
    //argtypes[1]=1562;
    //argtypes[2]=1562;
    argtypes[1]=23;
    argtypes[2]=23;

    SPIPlanPtr plan = SPI_prepare("Insert into pgm_role_hierarchy values($1,$2,$3)",3,argtypes);
    printf("here3\n");
    Datum vals[3];

    for (i = 0; i < rh->total_nodes; i++)
    {
	vals[0] = Int32GetDatum(rh->gr[i].id+1);
        // vals[0] = Int32GetDatum(0);
	interval *ptr1 = rh->gr[i].intervals;
	while (ptr1 != NULL)
	{
	  //  vals[1] = Int32GetDatum(-1);
	    vals[1] = Int32GetDatum(ptr1->s);
	    vals[2] = Int32GetDatum(ptr1->e);
	    //vals[2] = Int32GetDatum(-1);
	    SPI_execute_plan(plan,vals, NULL, false, 1);
	    ptr1 = ptr1->next;
	}
    }

    /*real postgres role hierarchy*************************************/

    /* print_graph(rh->orig_graph);
    
     Oid typname_oid = 19;
     bool isNull;

     ret = SPI_execute("Select rolname from pg_roles",true,0);

     char dropQuery[100];
     char partialQuery[] = "drop role ";

     strcpy(dropQuery,partialQuery);
     
     //ret = SPI_execute("select rolname from pg_roles where rolname like 'tmp%'", true, 1);
     
     TupleDesc tupdesc = SPI_tuptable->tupdesc;
     SPITupleTable *tuptable = SPI_tuptable;
     int n = SPI_processed;
     
     for (i = 1; i < n; i++)
     {
	 HeapTuple tuple = tuptable->vals[i];
	 origval = heap_getattr(tuple, 1, tupdesc, &isnull);
	 
	 strcat(dropQuery, DatumGetPointer(origval));
	 
	 SPI_execute(dropQuery,false,1);
	 dropQuery[10] = '\0';
     }

     char createQuery[200];
     for (i = 0; i < rh->size; i++)
     {
	 ret = sprintf(createQuery,"create role r%d",rh->orig_graph->gr[i].id);
	 printf("%s\n",createQuery);
	 SPI_execute(createQuery,false,1);
     }
     for (i = 0; i < rh->size; i++)
     {
	 int id = rh->orig_graph->gr[i].id;
	 child *ptr = rh->orig_graph->gr[i].children;
	 while (ptr != NULL)
	 {
	     ret = sprintf(createQuery,"grant r%d to r%d",ptr->id,id);
	     printf("%s\n",createQuery);
	     SPI_execute(createQuery,false,1);
	     ptr = ptr->next;
	 }
	 }*/
    
    /*end real*****************************************************/
    
    /*Datum val[3];
    for (i = 0; i < rh->size; i++)
    {
	val[0] = Int32GetDatum(rh->path[i].id);
	val[1] = DirectFunctionCall3(bit_in, PointerGetDatum(rh->path[i].paths), ObjectIdGetDatum(InvalidOid), Int32GetDatum(-1));
	val[2] = DirectFunctionCall3(bit_in, PointerGetDatum(rh->inverted_path[i].paths), ObjectIdGetDatum(InvalidOid), Int32GetDatum(-1));
	SPI_execute_plan(plan, val, NULL, false, 1);
	}*/

    //numRowsProcessed = SPI_processed;
    
    //TupleDesc tupdesc = SPI_tuptable->tupdesc;
    //SPITupleTable *tuptable = SPI_tuptable;
    
    //HeapTuple tuple = tuptable->vals[0];
    //origval = heap_getattr(tuple, 1, tupdesc, &isnull);
    //shiftedval = DirectFunctionCall2(bitshiftright, origval, Int32GetDatum(2));
    //elog(INFO, "bit_string_shift: %s", DatumGetPointer(DirectFunctionCall1(bit_out, shiftedval)));
    
    SPI_finish();

    return 0;

//    return rh->total_nodes;

}

int
create_priv_hierarchy(PG_FUNCTION_ARGS)
{
     int         ret,i,j;
    int             numRowsProcessed;
    char *buf;
    bool isnull;
    Datum origval, shiftedval;
    
    /* connect to SPI manager */
    if ((ret = SPI_connect()) < 0)
        elog(INFO, "create_priv_hierarchy : SPI_connect returned %d", ret);
    
    ret = SPI_execute("DELETE FROM pgm_priv_hierarchy",false,0);
    //ret = SPI_execute("DELETE FROM pgm_priv_hierarchy_inv",false,0);

    int32 max_levels = PG_GETARG_INT32(0);
    int32 max_children = PG_GETARG_INT32(1);
    priv_hierarchy ph = new_priv_hierarchy(max_levels,max_children);
    
    Oid argtypes[3];
    argtypes[0]=23;
    argtypes[1]=1562;
    argtypes[2]=1562;

    SPIPlanPtr plan = SPI_prepare("Insert into pgm_priv_hierarchy values($1,$2,$3)",3,argtypes);

    Datum val[3];
    for (i = 0; i < ph->size; i++)
    {
	val[0] = Int32GetDatum(ph->path[i].id);
	val[1] = DirectFunctionCall3(bit_in, PointerGetDatum(ph->path[i].paths), ObjectIdGetDatum(InvalidOid), Int32GetDatum(-1));
	val[2] = DirectFunctionCall3(bit_in, PointerGetDatum(ph->inverted_path[i].paths), ObjectIdGetDatum(InvalidOid), Int32GetDatum(-1));
	SPI_execute_plan(plan,val, NULL, false, 1);
    }
    
    /*plan = SPI_prepare("Insert into pgm_priv_hierarchy_inv values($1,$2)",2,argtypes);

    for (i = 0; i < ph->size; i++)
    {
	val[0] = Int32GetDatum(ph->inverted_path[i].id);
	val[1] = DirectFunctionCall3(bit_in, PointerGetDatum(ph->inverted_path[i].paths), ObjectIdGetDatum(InvalidOid), Int32GetDatum(-1));
	SPI_execute_plan(plan,val, NULL, false, 1);
	}*/


    SPI_finish();
    
    return numRowsProcessed;
}
int
assign_roles(PG_FUNCTION_ARGS)
{
    int user_id = PG_GETARG_DATUM(0);
    int max_ura = PG_GETARG_INT32(1);
    int max_roles = PG_GETARG_INT32(2);
    int ret;
    if ((ret = SPI_connect()) < 0)
        elog(INFO, "create_priv_hierarchy : SPI_connect returned %d", ret);
    ret = SPI_execute("DELETE FROM pgm_ura",false,0);

    Oid argtypes[2];
    argtypes[0]=23;
    argtypes[1]=23;
    
    SPIPlanPtr plan = SPI_prepare("Insert into pgm_ura values($1,$2)",2,argtypes);
    

    int *used_vals = (int *)palloc(sizeof(int)*max_ura);
    bool used = false;
    Datum vals[2];
    vals[0] = user_id;
    int i = 0;
    srand((int)time(0));
    for (i = 0; i < max_ura; i++)
    {
	int j,k;
	do{
	    j = rand()%max_roles+1;
	    used = false;
	    for (k = 0; k < i; k++)
	    {
		if (used_vals[k] == j)
		    used = true;
	    }
	}while(j == 0 || used);
	used_vals[i] = j;
	vals[1] = Int32GetDatum(j);
	SPI_execute_plan(plan,vals, NULL, false, 1);
    }
    SPI_finish();
    return i;
}

int 
create_pg_rh(PG_FUNCTION_ARGS)
{
    
}
