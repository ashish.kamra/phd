#include "rand_graph_gen.h"
#include "assert.h"
#include "postgres.h"

int main()
{
    //priv_hierarchy *ph = new_priv_hierarchy(3,2);
    
    /*if (new_graph != NULL)
    {
	print_graph(new_graph);
	int **path = get_all_paths(new_graph);
	print_closure(path,new_graph->total_nodes);
	path_str_ptr p = convert_to_str(path,new_graph->total_nodes);
	print_path_str(p, new_graph->total_nodes);
	}*/
    role_hierarchy rh = new_role_hierarchy(5,3);
    return 0;
}


/*role_hierarchy new_role_hierarchy(int max_levels,int max_children, int *size)
{
    graph *new_graph = create_random_graph(max_levels,max_children);
    path_str_ptr p = NULL;
    if (new_graph != NULL)
    {
	//print_graph(new_graph);
	int **path = get_all_paths(new_graph);
	//print_closure(path,new_graph->total_nodes);
	p = convert_to_str(path,new_graph->total_nodes);
	//print_path_str(p, new_graph->total_nodes);
	*size = new_graph->total_nodes;
    }
    return p;
    }*/

 role_hierarchy new_role_hierarchy(int max_levels, int max_children)
{

//   int max_nodes = pow(max_children,max_levels+1)+1;
     int max_nodes = (pow(max_children,max_levels + 1) - 1)/(max_children - 1);
	
    graph *rand_graph = new_graph(max_nodes);

    create_random_graph(rand_graph, max_levels,max_children);
    
   role_hierarchy g = rand_graph; 
	
    assert(g!=NULL);	
    //print_graph(g);
    int *list = (int *)palloc(sizeof(int)*(g->total_nodes));
    int n = 0;

    //int i = 0;

    //for (i = 0; i < g->total_nodes; i++)
//	list[i] = g->gr[i].id;

 //   assert(list!=NULL);
    dfs(g,0,1,NULL,list,&n);
    //print_graph(g);
    assign_intervals(g,list);
    //print_graph(g);
    return g;
}

void print_list(int *l, int n)
{
    int i;
    for (i = 0; i < n; i++)
    {
	printf("%d ",l[i]);
    }
    printf("\n");
}

/*role_hierarchy new_role_hierarchy(int max_levels, int max_children)
{
    role_hierarchy rh = (role_hierarchy)malloc(sizeof(hierarchy));
    if (rh==NULL)
    return NULL;
    graph *g = create_random_graph(max_levels,max_children);
    rh->orig_graph = g;
    //print_graph(g);
    if (g == NULL)
	return NULL;
    //dfs(g,0,1);
    int **path = get_all_paths(g);
    rh->path = convert_to_str(path,g->total_nodes);
    graph *g2 = invert_graph(g);
     //print_graph(g2);
    if (g2 == NULL)
	return NULL;
    int **path2 = get_all_paths(g2);
    rh->inverted_path = convert_to_str(path2,g2->total_nodes);
    rh->size = g2->total_nodes;
    return rh;
    //return g;
    }*/

int dfs(graph *g, int i, int clock, child *p, int *list, int *n)
{
    child *ptr = g->gr[i].children;
    //if (g->gr[i].interval[0] != -1)
    if (g->gr[i].intervals->e != -1)
    {
	p->edge_type = NON_TREE_EDGE;
	return clock;
    }
    g->gr[i].intervals->s = clock;
    while (ptr != NULL)
    {
	clock = dfs(g,ptr->id,clock,ptr,list,n);
	ptr = ptr->next;
    }
    g->gr[i].intervals->e = clock;
    list[*n] = g->gr[i].id;
    *n = *n+1;
    return clock+1;
}

void assign_intervals(graph *g, int *list)
{
    int i;
    for (i = 0; i < g->total_nodes;i++)
    {
	child *ptr = g->gr[i].children;
	while (ptr != NULL)
	{
	    if (g->gr[ptr->id].intervals->e < g->gr[i].intervals->s
		&& ptr->edge_type == TREE_EDGE)
			g->gr[i].intervals->s = g->gr[ptr->id].intervals->e;
	    ptr = ptr->next;
	}
    }
   
    for (i = 0; i < g->total_nodes;i++)
    {
	child *ptr = NULL;
	interval *p = NULL;

	if ((list + i) != NULL)
	{
          ptr = g->gr[list[i]].children;
	  p = g->gr[list[i]].intervals;
	}

	while (ptr != NULL)
	{
	    if (ptr->edge_type == NON_TREE_EDGE)
	    {
		interval *tmp = g->gr[ptr->id].intervals;
		while (tmp != NULL)
		{
		    while (p != NULL)
		    {
			if (p->s <= tmp->s && p->e >= tmp->e)
			    break;
			if (tmp->s <= p->s && tmp->e >= p->e)
			{
			    //interval *p_1 = p;
			    delete_interval(g->gr[list[i]].intervals,p);
                            //p = p->next;
                           // pfree(p_1);
                           //continue;
			}
			p = p->next;
		    }
		    if (p==NULL)
		    {
			add_interval(g->gr[list[i]].intervals,tmp);
		    }
		    p = g->gr[list[i]].intervals;
		    tmp = tmp->next;
		}
	    }
	    ptr = ptr->next;
	}
    }
}

void delete_interval(interval *a, interval *b)
{
    while (a->next != b)
    {
	a = a->next;
    }
    a->next = b->next;
}

void add_interval(interval *a, interval *b)
{
    while (a->next != NULL)
    {
	a = a->next;
    }
    a->next = (interval *)palloc(sizeof(interval));
    assert(a->next!=NULL);
    a->next->s = b->s;
    a->next->e = b->e;
    a->next->next = NULL;
}

priv_hierarchy new_priv_hierarchy(int max_levels, int max_children)
{
    priv_hierarchy ph = (priv_hierarchy)palloc(sizeof(hierarchy));
/*
    if (ph==NULL)
	return NULL;
    graph *g = create_random_graph(max_levels,max_children);
    ph->orig_graph = g;
    //print_graph(g);
    if (g == NULL)
	return NULL;
    int **path = get_all_paths(g);
    ph->path = convert_to_str(path,g->total_nodes);
    graph *g2 = invert_graph(g);
    //print_graph(g2);
    if (g2 == NULL)
	return NULL;
    int **path2 = get_all_paths(g2);
    ph->inverted_path = convert_to_str(path2,g2->total_nodes);
    ph->size = g->total_nodes;
*/
    return ph;
}

graph *invert_graph(graph *g)
{
    if (g==NULL)
	return NULL;
    int i = 0;
    graph *g2 = new_graph(g->total_nodes);
    if (g2==NULL)
	return NULL;
    for (i = 0; i < g->total_nodes; i++)
    {
	child *ptr = g->gr[i].children;
	while (ptr != NULL)
	{
	    if (!add_new_child(&g2->gr[ptr->id],i))
		return NULL;
	    ptr = ptr->next;
	}
    }
    g2->total_nodes = g->total_nodes;
    return g2;
}

void print_path_str(path_str_ptr p, int size)
{
    int i = 0;
    if (p==NULL)
	return;
    for (i = 0; i < size; i++)
    {
	printf("%s: ",p[i].id);
	printf("%s\n", p[i].paths);
    }
}

path_str_ptr convert_to_str(int** path, int size)
{
    int i = 0, j = 0;
    path_str_ptr p = new_pathstr_array(size);
    int sum = 0;
    if (p!=NULL)
    {
	for (i = 0; i < size; i++)
	{
	    //sprintf(p[i].id,"%d",i);
	    p[i].id = i+1;
	    for (j = 0; j < size; j++)
	    {
		if (path[i][j]==1)
		{
		    p[i].paths[j]='1';
		    sum++;
		}
		else
		    p[i].paths[j]='0';
	    }
	    p[i].paths[j]='\0';
	}
    }
    printf("avg reachable nodes per node: %d\n", sum/size);
    return p;
}

path_str_ptr new_pathstr_array(int size)
{
    int i = 0;
    path_str_ptr p = (path_str_ptr)palloc(size*sizeof(path_str_item));
    if (p!=NULL)
    {
	for (i = 0; i < size; i++)
	{
	    //p[i].id = (char *)malloc(sizeof(char)*6);
	    p[i].paths = (char *)palloc(sizeof(char)*(size+1));
	    if (p[i].paths==NULL)
		return NULL;
	}
    }
    return p;
}

void create_random_graph(graph *rand_graph, int max_levels,int max_children)
{
    int *levels = (int *)palloc(sizeof(int)*(max_levels+1));
    create_graph_tree(rand_graph, max_levels,max_children,levels);
    if (rand_graph != NULL)
    {
	//print_graph(rand_graph);
	create_graph_from_tree(rand_graph,levels,max_levels);
    }
    int sum = 0, i;
    for (i = 0; i < rand_graph->total_nodes; i++)
    {
	child *ptr = rand_graph->gr[i].children;
	while (ptr != NULL)
	{
	    sum++;
	    ptr = ptr->next;
	}
    }
    int avg_child = sum/rand_graph->total_nodes;
    printf("total nodes: %d\n",rand_graph->total_nodes);
    printf("avg child per node: %d\n",avg_child);
    printf("depth: %d\n",max_levels);
    return;
}

int add_new_child(graph_node *node, int child_id)
{
    child *new_child = NULL;
    if ((new_child = (child *) palloc(sizeof(child))) == NULL)
    {
	printf("failed to allocate memory for list_node\n");
	return 0;
    }
    new_child->id = child_id;
    new_child->next = node->children;
    new_child->edge_type = TREE_EDGE;
    node->children = new_child;
    return 1;
}

void print_graph(graph *g)
{
    int i = 0;
    child *ptr = NULL;
    if (g == NULL)
	return;
    for (i = 0; i < g->total_nodes; i++)
    {
	printf("node %d:\nchildren: ", g->gr[i].id);
	ptr = g->gr[i].children;
	while (ptr != NULL)
	{
	    printf(" %d ", ptr->id);
	    ptr = ptr->next;
	}
	interval *iptr = g->gr[i].intervals;
	printf("interval: ");
	while (iptr != NULL)
	{
	    printf("[%d %d] ", iptr->s,iptr->e);
	    iptr = iptr->next;
	}
	printf("\n");
    }
}

void print_closure(int **p, int totalsize)
{
    int i = 0, j = 0;
    for (i = 0; i < totalsize; i++)
    {
	printf("node %d: ", i);
	for (j = 0; j < totalsize; j++)
	{
	    printf(" %d ", p[i][j]);
	}
	printf("\n");
    }
}

int** get_all_paths(graph *g)
{
    int i = 0, j = 0;
    child *ptr = NULL;
    int totalsize = g->total_nodes;
    int **path = (int**)palloc(sizeof(int *)*totalsize);
    for (i = 0; i < totalsize; i++)
    {
	path[i] = (int *)calloc(totalsize, sizeof(int));
	for (j = 0; j < totalsize; j++)
	{
	    path[i][j] = 0;
	}
    }
    for (i = 0; i < totalsize; i++)
    {
	ptr = g->gr[i].children;
	while (ptr != NULL)
	{
	    path[i][ptr->id] = 1;
	    ptr= ptr->next;
	}
    }
    return compute_all_paths(path,totalsize);
}


int** compute_all_paths(int **path, int totalsize)
{
    int i = 0, j = 0, k = 0;
    for(i = 0;i < totalsize; i++)
    {
	for(j = 0; j < totalsize; j++)
	{
	    if(path[i][j] == 1)
	    {
		for(k = 0; k < totalsize; k++)
		{
		    if(path[j][k] == 1)
			path[i][k] = 1;
		}
	    }
	}
    }
    return path;
}

void create_graph_from_tree(graph *g, int *levels, int max_levels)
{
    int index = 1,
	i = 0, j = 0, curr_level = 0,
	next_level = 0, k = 0, smallest_child = 0, largest_child = 0;
    /*for (k = 0; k < max_levels+1; k++)
    {
	printf(" %d",levels[k]);
    }
    printf("\n");*/
    for (curr_level = 1; curr_level < max_levels; curr_level++)
    {
	//printf("curr_level: %d\n",curr_level);
	int p = curr_level+1;
	for (i = index; i < index+levels[curr_level]; i++)
	{
	    //get current_nodes children range
	    get_child_range(g->gr[i],&smallest_child, &largest_child);
	    //printf("i %d: ", i);
	    p = curr_level+1;
	    for (j = next_level=index+levels[curr_level]; j < g->total_nodes; j++)
	    {
		/*while not current node's child
		  determine weather to add edge*/
		if (j >= next_level+levels[p])
		{
		    next_level+=levels[p];
		    p++;
		    //printf("j %d: level %d\n", j, ++p);
		}
		if (!(j >= smallest_child && j <= largest_child))
		{
		    //printf("%d ", j);
		    if (!determine_cross_edge(&g->gr[i],j,p-curr_level)){
			g = NULL;
			return;
			}
		}
	    }
	    //printf("\n");
	}
	index +=levels[curr_level];
    }
    return ;
}

void get_child_range(graph_node g, int *a, int *b)
{
    *b = g.children->id;
    child *ptr = g.children;
    while (ptr->next != NULL)
	ptr = ptr->next;
    *a = ptr->id;
}

int determine_cross_edge(graph_node *a, int id, int level_diff)
{
    //printf("level diff: %d\n", level_diff);
    int add_child = rand()%(level_diff+1);
    if (add_child == 1)
    {
	//printf("cross_edge from %d to %d\n",a->id,id);
	return add_new_child(a,id);
    }
    return 1;
}


graph *new_graph(int max_nodes)
{
   
    int i = 0;
    graph *g = (graph *) palloc(sizeof(graph));
    g->gr = (graph_node *) palloc(sizeof(graph_node)*max_nodes);
    if (g->gr != NULL)
    {
	for (i = 0; i < max_nodes; i++)
	{
	    g->gr[i].id = i;
	    g->gr[i].children = NULL;
	    g->gr[i].intervals = (interval *)palloc(sizeof(interval));
	    assert(g->gr[i].intervals!=NULL);
	    g->gr[i].intervals->s=-1;
	    g->gr[i].intervals->e=-1;
	    g->gr[i].intervals->next = NULL;
	}
	g->total_nodes = max_nodes;
    }
    return g;
    
  /* 
    graph *g = (graph *) palloc(sizeof(graph));
    g->gr = (graph_node *) palloc(sizeof(graph_node));
    if (g->gr != NULL)

    {
	g->gr->id =0;
	g->gr->children = NULL;
	g->gr->intervals = (interval *)palloc(sizeof(interval));
	assert(g->gr->intervals!=NULL);
	g->gr->intervals->s=-1;
	g->gr->intervals->e=-1;
	g->total_nodes = 1;
    }

   return g;
*/
}


void create_graph_tree(graph *rand_graph,int max_levels, int max_children, int *levels)
 
{
    int i = 0, curr_level = 0, num_level_nodes = 1,
	num_children = 0, index = 1, max_nodes = 0;
    //max_nodes = pow(max_children,max_levels+1)+1;
    srand((int)time(0));
    while (curr_level < max_levels)
    {
	int temp = levels[curr_level]=num_level_nodes;
	num_level_nodes =  0;
	while (temp)
	{
	    //get number of children 
	    do{
		num_children = rand()%(max_children+1);
	    }while (num_children == 0);
	    add_children(&rand_graph->gr[i++],num_children, index);
	    index += num_children;
	    num_level_nodes+=num_children;
	    temp--;
	}
	curr_level++;
    }
    levels[curr_level] = num_level_nodes;
    rand_graph->total_nodes = index;
    return ;
}

int add_children(graph_node *g, int num_children, int index)
{
    int i = 0;
    child *ptr = NULL;
    for (i = 0; i < num_children; i++)
    {
	ptr =  (child *) palloc(sizeof(child));
	if (ptr == NULL)
	    return 0;
	ptr->id = index+i;
	ptr->next = g->children;
	g->children = ptr;
    }
}
