#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define TREE_EDGE 1
#define NON_TREE_EDGE 2

typedef struct list_node {
    int id;
    int edge_type;
    struct list_node *next;
}child;

typedef struct interval{
    int s;
    int e;
    struct interval *next;
}interval;

typedef struct graph_node{
    int id;
    child *children;
    interval *intervals;
}graph_node;

typedef struct level_node{
    graph_node *node;
    struct level_node *next;
}level_node;

typedef struct graph{
    graph_node *gr;
    int total_nodes;
}graph;

typedef struct path_str_item
{
    int id;
    char *paths;
}path_str_item;

typedef struct path_str_item *path_str_ptr;

typedef struct hierarchy
{
    graph *orig_graph;
    path_str_ptr path;
    path_str_ptr inverted_path;
    int size;
}hierarchy;


typedef struct graph_node role;
typedef struct graph *role_hierarchy;
//typedef struct hierarchy *role_hierarchy;
typedef struct hierarchy *priv_hierarchy;
typedef struct level_node **levelArray;
typedef char * string;

void create_random_graph(graph *,int,int);
void create_graph_tree(graph *,int,int, int *);
int add_new_child(graph_node *,int);
void print_graph(graph *);
int get_nodes(int,int);
int **get_all_paths(graph *);
int **compute_all_paths(int**,int);
void print_closure(int**,int);
graph *new_graph(int);
int add_children(graph_node *,int,int);
void create_graph_from_tree(graph *,int *,int);
int determine_cross_edge(graph_node *,int,int);
void get_child_range(graph_node,int *, int *);
path_str_ptr convert_to_str(int**,int);
path_str_ptr new_pathstr_array(int);
void print_path_str(path_str_ptr,int);
role_hierarchy new_role_hierarchy(int,int);
priv_hierarchy new_priv_hierarchy(int,int);
graph *invert_graph(graph *);
void assign_intervals(graph *,int *);
void delete_interval(interval *, interval *);
void add_interval(interval *, interval *);
void print_list(int *,int);
