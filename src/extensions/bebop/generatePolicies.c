/***********
 This function generates random policies and inserts policy tuples in the PG_POLICY table
 
 Author : Ashish Kamra
 Date   : 6th March 2008
 
 Steps :
 
 Pre-policy generation
 1. Get all the database oids in the system.
 
 For every policy do the following
 	Choose the number of databases for the policy = a 
 	Randomely choose n databases : db1, .., dba
 	
 	For every database
 	
 	either
 		get the database roles
 		choose number of database roles for the policy = b
 		randomely choose b roles for the policy
 	or
 		get the applicable database users
 		choose number of database roles for the policy = b
 		randomely choose b roles for the policy
 	  
 
 1. Get all the user oids in the system
 2. Get all the role oids in the system
 
 
 4. OBJ_TYPEs is always = 'table'
 5. SQL_CMDs = {Select, Insert, Update, Delete}
 6.
  
 *********************/


#include <postgres.h>			/* mandatory */
#include "fmgr.h"				/* using the function manager interface */
#include "executor/spi.h"       /* for SPI */

Datum policyGenerator(PG_FUNCTION_ARGS);

PG_FUNCTION_INFO_V1(policyGenerator);		/* version 1 calling convention */

PG_MODULE_MAGIC;			/* Magic Block */


Datum
policyGenerator(PG_FUNCTION_ARGS)
{
	
	
	
	
}



