/**********************
This function is a trigger function. It is invoked when a new anomaly tuple is inserted on the ANOMALY_CHARACTERISATION table.
Following are the series of steps performed by this function : 

1. Collect the following OIDs from the incoming anomaly tuple :
	ROLE OIDs
	USER OIDs
	DB OIDs
	OBJ OIDs (tables)
	OBJ ATTRs (columns)

2. Go to the PG_PCL table to get a listing of all policy bit strings for these OIDs. 'AND' the policy bit strings to get a
final bit str that represents all the policies that match these OIDs. Get the policy ids from the bit str.

3. Now get all the policy tuples corresponding to these policy ids. From these project the following fields,

SOURCEs, CLIENTS , OBJ TYPEs, SQL CMDs, START DT, END DT

4. Also, from the incoming anomaly extract the following fields,
SOURCE, CLIENT, OBJ TYPE, SQL CMD, DATE TIME.

5. Now comes the matching algorithm. Naive way is to invoke a function for each of the policy tuple to get a match. This is what 
I will do in the first cut.

***********************/


#include "postgres.h"
#include "fmgr.h"
#include "executor/spi.h"       /* this is what you need to work with SPI */
#include "commands/trigger.h"   /* ... and triggers */

extern Datum responsetrigfunc(PG_FUNCTION_ARGS);

PG_FUNCTION_INFO_V1(trigf);

PG_MODULE_MAGIC;

Datum
responsetrigfunc(PG_FUNCTION_ARGS)
{
    TriggerData *trigdata = (TriggerData *) fcinfo->context;
    TupleDesc   tupdesc;
    HeapTuple   rettuple;
    char       *when;
    bool        checknull = false;
    bool        isnull;
    int         ret, i;

    /* make sure it's called as a trigger at all */
    if (!CALLED_AS_TRIGGER(fcinfo))
        elog(ERROR, "trigf: not called by trigger manager");

    /* tuple to return to executor */
    if (TRIGGER_FIRED_BY_UPDATE(trigdata->tg_event))
        rettuple = trigdata->tg_newtuple;
    else
        rettuple = trigdata->tg_trigtuple;

    /* check for null values */
    if (!TRIGGER_FIRED_BY_DELETE(trigdata->tg_event)
        && TRIGGER_FIRED_BEFORE(trigdata->tg_event))
        checknull = true;

    if (TRIGGER_FIRED_BEFORE(trigdata->tg_event))
        when = "before";
    else
        when = "after ";

    tupdesc = trigdata->tg_relation->rd_att;

    /* connect to SPI manager */
    if ((ret = SPI_connect()) < 0)
        elog(INFO, "trigf (fired %s): SPI_connect returned %d", when, ret);

    /* get number of rows in table */
    ret = SPI_exec("SELECT count(*) FROM ttest", 0);

    if (ret < 0)
        elog(NOTICE, "trigf (fired %s): SPI_exec returned %d", when, ret);

    /* count(*) returns int8, so be careful to convert */
    i = DatumGetInt64(SPI_getbinval(SPI_tuptable->vals[0],
                                    SPI_tuptable->tupdesc,
                                    1,
                                    &isnull));

    elog (INFO, "trigf (fired %s): there are %d rows in ttest", when, i);

    SPI_finish();

    if (checknull)
    {
        SPI_getbinval(rettuple, tupdesc, 1, &isnull);
        if (isnull)
            rettuple = NULL;
    }

    return PointerGetDatum(rettuple);
}
