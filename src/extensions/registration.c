/* Function to execute the register the security parameters for a combination of k, l value */

#include "postgres.h"
#include "executor/spi.h"
#include "fmgr.h"
#include "utils/builtins.h"
#include "utils/array.h"

#include "catalog/pg_m_rpolicy_shares.h"

#include <TC.h>
#include <openssl/rand.h>

#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif


PG_FUNCTION_INFO_V1(f_pg_m_rpolicy_jtam_register);

int
f_pg_m_rpolicy_jtam_register(PG_FUNCTION_ARGS)
{
	int i;
	int ret;
	int k; 		/* threshold */
	int l; 		/* total number of users */
	int bits; 	/* bit length of n */
	int e;
	char *e_str;		/* prime number usually 3, 17, 65537 */
	char *n_str;
	char *u_str;
	ArrayType *shares_str_arr;	
	ArrayType *user_ids_arr;
	HeapTuple *htups;
	TupleDesc	tupdesc;
	int cmd_res;
	bool isnull;
	
	Oid argtypes[Natts_pg_m_rpolicy_shares];
	Datum values[Natts_pg_m_rpolicy_shares];
	char nulls[Natts_pg_m_rpolicy_shares];		
	
	/* connect to SPI manager */
    if ((ret = SPI_connect()) < 0)
    	ereport(ERROR,
    		(errcode(ERRCODE_SYNTAX_ERROR),
    				errmsg("f_pg_m_rpolicy_jtam_register : SPI_connect returned %d", ret)));    	
                
	//k = PG_GETARG_INT32(0);
	//l = PG_GETARG_INT32(1);
	bits = PG_GETARG_INT32(0);
	e = PG_GETARG_INT32(1);

	cmd_res = SPI_exec("Select oid from pg_authid where rolsuper = 't'", 0);
	
	if (cmd_res != SPI_OK_SELECT)
    	ereport(ERROR,
    		(errcode(ERRCODE_SYNTAX_ERROR),
    				errmsg("f_pg_m_rpolicy_jtam_register : can not get the number of DBAs.")));    
	
	htups = SPI_tuptable->vals;
	tupdesc = SPI_tuptable->tupdesc;
	
	l = SPI_processed;
	
	if (l < 2)
    	ereport(ERROR,
    		(errcode(ERRCODE_SYNTAX_ERROR),
    				errmsg("f_pg_m_rpolicy_jtam_register : number of DBAs registered with the DBMS must be atleast 2.")));    
	
	user_ids_arr = construct_empty_array(26);	// 26: oid
	
	for (i = 0; i < l; i++)
	{
		Datum super_oid_datum = heap_getattr(htups[i], 1, tupdesc, &isnull);
	//	super_oid_str = SPI_getvalue(SPI_tuptable->vals[i], SPI_tuptable->tupdesc, 1);
		int index_user = i+1;
		user_ids_arr = array_set(user_ids_arr,
									1,				// number of subscripts
									&index_user,	// index of the inserted value
									super_oid_datum, // value to be inserted
									false,			// isnull
									-1, 			//int arraytyplen = -1 since pg_type.typlen for _int4 = -1,
									4, 				//int elmlen = 4, since pg_type.typlen for int4 = 4
									true, 			//bool elmbyval = true, since pg_type.typbyval = true for int4
									'i'); 			//char elmalign as pg_type.typalign = 'i' for int4
													
	}		
	
	for (k = 2; k < l; k++)
	{
		elog(INFO, "generating prameters for k = %d, l = %d", k, l);
		
		const char *tmp = (const char *)palloc(sizeof(char));
		RAND_seed(tmp, 1);	
		pfree((void *)tmp);
		
		/* generate the keys */
		TC_DEALER *tc_d= TC_generate(bits, l, k, e);
		
		if (tc_d == (TC_DEALER *)NULL)
	    	ereport(ERROR,
	    		(errcode(ERRCODE_SYNTAX_ERROR),
	    				errmsg("f_pg_m_rpolicy_jtam_register : can not generate the security parameters for k=%d, l=%d.",k,l)));    	
		
				
		n_str = BN_bn2dec(tc_d->n);
		u_str = BN_bn2dec(tc_d->u);
		e_str = BN_bn2dec(tc_d->e);
					
		shares_str_arr = construct_empty_array(25);	// 25: text
		
		TC_IND *tc_ind;
		
		for (i = 1; i <= l; i++)
		{
			tc_ind = TC_get_ind(i,tc_d);
			
			if (tc_ind->mynum != -1) 
			{				
				char *shares_str = BN_bn2dec(tc_ind->si);
							
				shares_str_arr = array_set(shares_str_arr,
						           			1,				// number of subscripts
						           			&i,				// index of the inserted value
						           			DirectFunctionCall1(textin, PointerGetDatum(shares_str)), // value to be inserted
						           			false,			// isnull
						           			-1, 				//int arraytyplen = -1 since pg_type.typlen for _text = -1
						           			-1, 				//int elmlen = -1, since pg_type.typlen for text = -1
						           			false, 			//int elmlen = -1, since pg_type.typlen for text = -1
						           			'i'); 			//char elmalign as pg_type.typalign = 'i' for text
	
			}					
					
		}	
	
		MemSet(argtypes, 0, sizeof(argtypes));
		MemSet(values, 0, sizeof(values));
		MemSet(nulls, ' ', sizeof(nulls));
	
		argtypes[Anum_pg_m_rpolicy_shares_l - 1] = 23;					/* l : int4 */ 
		argtypes[Anum_pg_m_rpolicy_shares_k - 1] = 23;					/* k : int4 */
		argtypes[Anum_pg_m_rpolicy_shares_e - 1] = 25;					/* e : text */
		argtypes[Anum_pg_m_rpolicy_shares_n - 1] = 25;					/* n : text */
		argtypes[Anum_pg_m_rpolicy_shares_u - 1] = 25;					/* u : text */
		argtypes[Anum_pg_m_rpolicy_shares_user_ids - 1] = 1028; 		/* user_ids : _oid or oid[] */
		argtypes[Anum_pg_m_rpolicy_shares_shares - 1] = 1009; 			/* shares : _text or text[] */
		
		SPIPlanPtr plan = SPI_prepare("Insert into pg_m_rpolicy_shares values($1,$2,$3,$4,$5,$6,$7)", Natts_pg_m_rpolicy_shares, argtypes);
		
		values[Anum_pg_m_rpolicy_shares_l - 1] = Int32GetDatum(l);
		values[Anum_pg_m_rpolicy_shares_k - 1] = Int32GetDatum(k);
		values[Anum_pg_m_rpolicy_shares_e - 1] = DirectFunctionCall1(textin, PointerGetDatum(e_str));;
		values[Anum_pg_m_rpolicy_shares_n - 1] = DirectFunctionCall1(textin, PointerGetDatum(n_str));
		values[Anum_pg_m_rpolicy_shares_u - 1] = DirectFunctionCall1(textin, PointerGetDatum(u_str));
		values[Anum_pg_m_rpolicy_shares_user_ids - 1] = PointerGetDatum(user_ids_arr);
		values[Anum_pg_m_rpolicy_shares_shares - 1] = PointerGetDatum(shares_str_arr);
				
		ret = SPI_execute_plan(plan, values, nulls, false, 1);
		
		if (ret != SPI_OK_INSERT)
	    	ereport(ERROR,
	    		(errcode(ERRCODE_SYNTAX_ERROR),
	    				errmsg("f_pg_m_rpolicy_jtam_register : can not insert data in pg_m_rpolicy_shares.")));    	


		TC_IND_free(tc_ind);
		TC_DEALER_free(tc_d);
		
	}
	
	SPI_finish();
	
}
