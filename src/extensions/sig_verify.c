#include "postgres.h"
#include "executor/spi.h"
#include "fmgr.h"
#include "utils/builtins.h"
#include "utils/array.h"
#include <utils/syscache.h>
#include <TC.h>
#include "catalog/pg_m_rpolicy_shares.h"
#include "catalog/pg_m_rpolicy_adm.h"
#include "catalog/pg_m_rpolicy_def.h"
#include <openssl/evp.h>


#ifndef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

static bool check_signature_state(int policy_id, char *state);
static void
catPolicyData(char *policy_id, char *severity, char *condition, char *init_actions, char *confirmation_actions, char *resolution_actions, 
			  char *failure_actions, char *k_str, char *l_str, const char *state, char *policy_hash);

PG_FUNCTION_INFO_V1(f_pg_m_rpolicy_jtam_sig_verify);

bool
f_pg_m_rpolicy_jtam_sig_verify(PG_FUNCTION_ARGS)
{

	int		policy_id;
	bool 	result;
	char	*state;
	int ret;
	
	result = false;
	
	/* connect to SPI manager */
    if ((ret = SPI_connect()) < 0)
        elog(ERROR, "f_pg_m_rpolicy_jtam_register : SPI_connect returned %d", ret);
        
	policy_id = PG_GETARG_INT32(0);
	
	result = check_signature_state(policy_id, state);
	
	if (!result)
		elog(ERROR, "Signature on policy %d is invalid.", policy_id);
	else
		elog(INFO, "f_pg_m_rpolicy_jtam_register: signature on policy id = %d is valid.", policy_id);
	
	SPI_finish();
	
	return result;
}

static bool 
check_signature_state(int policy_id, char *state)
{
	HeapTuple	adm_tup, share_tup, policy_def_tuple;
	int			k, l;

	Datum		sigs_arr_datum;
	Datum  	 	n_text_datum, e_text_datum, state_text_datum; 
	Datum   	n_str_datum, e_str_datum, state_str_datum;

	BIGNUM		*n_bn, *e_bn, *hash_bn, *final_sig_bn;	
	char    	*n_str, *e_str, *state_str; 
	
	bool		isnull, result;				
	
	/* by default */
	result = false;
	
	adm_tup =	SearchSysCache(Z_RPOLICYADMPOLICYID, Int32GetDatum(policy_id), 0, 0, 0);
	
	if (!HeapTupleIsValid(adm_tup))
		ereport(ERROR,
				(errcode(ERRCODE_SYNTAX_ERROR),
						errmsg("f_pg_m_rpolicy_jtam_sig_verify: Invalid policy id %d", policy_id)));


	state_text_datum = SysCacheGetAttr(Z_RPOLICYADMPOLICYID, adm_tup, Anum_pg_m_rpolicy_adm_state, &isnull);
	state_str_datum = DirectFunctionCall1(textout, state_text_datum);			
	state = DatumGetCString(state_str_datum);
	
	k = ((Form_pg_m_rpolicy_adm) GETSTRUCT(adm_tup))->joint_admns;	
	l = ((Form_pg_m_rpolicy_adm) GETSTRUCT(adm_tup))->num_admns;

	share_tup =	SearchSysCache(Z_RPOLICYSHARES_LK, Int32GetDatum(l), Int32GetDatum(k), 0, 0);
	if (!HeapTupleIsValid(share_tup))
		ereport(ERROR,
				(errcode(ERRCODE_SYNTAX_ERROR),
						errmsg("f_pg_m_rpolicy_jtam_sig_verify: Invalid k = %d, l = %d", k,l)));
	
		
	/* re-create the policy hash */				
	char	*policy_hash = (char *)palloc(1000*sizeof(char));
	HeapTuple policy_def_tup = SearchSysCache(Z_RPOLICYDEFOID, Int32GetDatum(policy_id), 0, 0, 0);

	if (!HeapTupleIsValid(policy_def_tup))
		ereport(ERROR,
				(errcode(ERRCODE_SYNTAX_ERROR),
						errmsg("f_pg_m_rpolicy_jtam_sig_verify: Invalid policy id %d", policy_id)));

		
	// policy_id
	char policy_id_str[10];
	sprintf(policy_id_str,"%d", policy_id) ;				 

	// severity
	int severity = ((Form_pg_m_rpolicy_def) GETSTRUCT(policy_def_tup))->severity;
	char severity_str[5];
	sprintf(severity_str,"%d",severity);
	
	// policy conditions				
	Datum 		conditions_datum = SysCacheGetAttr(Z_RPOLICYDEFOID, policy_def_tup, Anum_pg_m_rpolicy_def_condition, &isnull);
	char 		*conditions_arr_str = OidOutputFunctionCall(751, conditions_datum);
	
	// policy initial actions
	Datum 		init_actions_datum = SysCacheGetAttr(Z_RPOLICYDEFOID, policy_def_tup, Anum_pg_m_rpolicy_def_initialaction, &isnull);
	char 		*init_actions_arr_str = OidOutputFunctionCall(751, init_actions_datum);
						
	// k and l
	char	k_str[5], l_str[5];
	sprintf(k_str,"%d",k);
	sprintf(l_str,"%d",l);
	
	catPolicyData(policy_id_str, severity_str, conditions_arr_str,  init_actions_arr_str, 
					(char *)NULL, (char *)NULL, (char *)NULL, k_str, l_str, "ACTIVATED", policy_hash);

	// now hash it and store it in hash_bn				
	OpenSSL_add_all_digests();
	
	EVP_MD_CTX mdctx;
	const EVP_MD *md = EVP_get_digestbyname("sha1");
	unsigned char md_value[EVP_MAX_MD_SIZE];
	int md_len;
	
	EVP_MD_CTX_init(&mdctx);
	EVP_DigestInit_ex(&mdctx, md, NULL);
	EVP_DigestUpdate(&mdctx, policy_hash, strlen(policy_hash));
	EVP_DigestFinal_ex(&mdctx, md_value, &md_len);
	EVP_MD_CTX_cleanup(&mdctx);
					
	hash_bn = BN_new();
	hash_bn = BN_bin2bn(md_value, md_len, hash_bn);				
	
	//ereport(NOTICE,(errmsg("Hash = %s",BN_bn2hex(hash_bn))));	
	sigs_arr_datum = SysCacheGetAttr(Z_RPOLICYADMPOLICYID, adm_tup, Anum_pg_m_rpolicy_adm_signature, &isnull);							
																										
	n_text_datum = SysCacheGetAttr(Z_RPOLICYSHARES_LK, share_tup, Anum_pg_m_rpolicy_shares_n, &isnull);
	n_str_datum = DirectFunctionCall1(textout, n_text_datum);
	n_str = DatumGetPointer(n_str_datum);
	n_bn = BN_new();
	BN_dec2bn(&n_bn, n_str);
	
	e_text_datum = SysCacheGetAttr(Z_RPOLICYSHARES_LK, share_tup, Anum_pg_m_rpolicy_shares_e, &isnull);
	e_str_datum = DirectFunctionCall1(textout, e_text_datum);
	e_str = DatumGetPointer(e_str_datum);
	e_bn = BN_new();
	BN_dec2bn(&e_bn, e_str);
		
	ArrayType *sigs_text_arr = DatumGetArrayTypeP(sigs_arr_datum);
	int next_index = 1;
	Datum sigs_text_datum = array_ref(sigs_text_arr,
			           					1,				// number of subscripts
			           					&next_index,	// index of the inserted value
			           					-1, 			//int arraytyplen = -1 since pg_type.typlen for _text = -1
			           					-1, 			//int elmlen = -1, since pg_type.typlen for text = -1
			           					false, 			//int typbyval = false, since pg_type.typbyval for text = false
			           					'i',			//char elmalign as pg_type.typalign = 'i' for text
			           					&isnull);			// isnull								
	   		              
	final_sig_bn = BN_new();
	BN_dec2bn(&final_sig_bn, DatumGetPointer(DirectFunctionCall1(textout, sigs_text_datum)));
		
												
	if (TC_verify_jtam(hash_bn, final_sig_bn, e_bn, n_bn)) /* signature is not verified */											
		result = true;
	else
		result = false;
		
	ReleaseSysCache(policy_def_tup);
	ReleaseSysCache(share_tup);
	ReleaseSysCache(adm_tup);

	return result;
}

static void
catPolicyData(char *policy_id, char *severity, char *condition, char *init_actions, char *confirmation_actions, char *resolution_actions, 
			  char *failure_actions, char *k_str, char *l_str, const char *state, char *policy_hash)
{	
	
	if (policy_id != (char *)NULL)
		policy_hash = strcpy(policy_hash, policy_id);
	
	if (severity != (char *)NULL)
		policy_hash = strcat(policy_hash, severity);
	
	if (condition != (char *)NULL)
		policy_hash = strcat(policy_hash, condition);
	
	if (init_actions != (char *)NULL)
		policy_hash = strcat(policy_hash, init_actions);
	
	if (confirmation_actions != (char *)NULL)
		policy_hash = strcat(policy_hash, confirmation_actions);
	
	if (resolution_actions != (char *)NULL)
		policy_hash = strcat(policy_hash, resolution_actions);
	
	if (failure_actions != (char *)NULL)
		policy_hash = strcat(policy_hash, failure_actions);
	
	if (k_str != (char *)NULL)
		policy_hash = strcat(policy_hash, k_str);
	
	if (l_str != (char *)NULL)
		policy_hash = strcat(policy_hash, l_str);
	
	if (state != (char *)NULL)
		policy_hash = strcat(policy_hash, state);
		
}
