#include "postgres.h"
#include <utils/varbit.h>
#include "executor/spi.h"       /* this is what you need to work with SPI */
#include "fmgr.h"

#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

PG_FUNCTION_INFO_V1(bit_string_shift);

int
bit_string_shift(PG_FUNCTION_ARGS)	
{
    int         ret,i,j;
	int 		numRowsProcessed;
	char *buf;
	Oid argtypes[1];
	bool isnull;
	Datum origval, shiftedval;
	
    /* connect to SPI manager */
    if ((ret = SPI_connect()) < 0)
        elog(INFO, "bit_string_shift : SPI_connect returned %d", ret);        

   char dropQuery[100];
   char partialQuery[] = "drop role ";
   
   strcpy(dropQuery,partialQuery);

    ret = SPI_execute("select rolname from pg_roles where rolname like 'tmp%'", true, 1);
    
    numRowsProcessed = SPI_processed;
  
    TupleDesc tupdesc = SPI_tuptable->tupdesc;
    SPITupleTable *tuptable = SPI_tuptable;
    
    HeapTuple tuple = tuptable->vals[0];  	    	    
    origval = heap_getattr(tuple, 1, tupdesc, &isnull);       

   strcat(dropQuery, DatumGetPointer(origval));
   
	elog(INFO, "bit_string_shift: %s", dropQuery);

     SPI_execute(dropQuery,false,1);
//    shiftedval = DirectFunctionCall2(bitshiftright, origval, Int32GetDatum(2));
//	elog(INFO, "bit_string_shift: %s", DatumGetPointer(DirectFunctionCall1(bit_out, shiftedval)));
    	 
    SPI_finish();
        
    return numRowsProcessed;
}
