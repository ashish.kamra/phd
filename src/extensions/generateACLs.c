#include "postgres.h"
#include "executor/spi.h"       /* this is what you need to work with SPI */
#include "fmgr.h"

#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

PG_FUNCTION_INFO_V1(generateACLs);

int
generateACLs(PG_FUNCTION_ARGS)	
{

   int cur_acl_size = 0, i, ret;
  
	
   int acl_size = PG_GETARG_INT32(0);
   int num_roles = PG_GETARG_INT32(1);
   int role_no;
   int role_nos[acl_size];
   char role_no_str[6];
   
   /* connect to SPI manager */
     if ((ret = SPI_connect()) < 0)
         elog(INFO, "generateACLs : SPI_connect returned %d", ret);        
      
   
   while (cur_acl_size < acl_size)
   {
	   char fullQueryGrant[200]; 
	   char fullQueryDeny[200];
	   char fullQueryTaint[200];
	   char fullQuerySuspend[200];
	   
	   char partialQueryGrant[] = "grant insert on t1 to r";
	   char partialQueryDeny[] = "deny update on t1 to r";
	   char partialQueryTaint[] = "taint delete on t1 to r";
	   char partialQuerySuspend[] = "suspend references on t1 to r";	   
	   
	   strcpy(fullQueryGrant,partialQueryGrant);
	   strcpy(fullQueryDeny,partialQueryDeny);
	   strcpy(fullQueryTaint,partialQueryTaint);
	   strcpy(fullQuerySuspend,partialQuerySuspend);
	   bool found = true;
	
	   while (found)
	   {
		   found = false;
		   role_no = (int)(rand() / (((double)RAND_MAX + 1)/ num_roles));
		   
		   for (i = 0; i < cur_acl_size; i++)
		   {
			   if (role_nos[i] == role_no)
			   {
				   found = true;
				   break;
			   }
		   }		   
	   }
	  	  
	   sprintf(role_no_str,"%d",role_no);
	   
	   strcat(fullQueryGrant, role_no_str);	
	   strcat(fullQueryTaint, role_no_str);
	   strcat(fullQueryDeny, role_no_str);
	   strcat(fullQuerySuspend, role_no_str);
	   
	   elog(LOG, "generateACLs: %s", fullQueryGrant);
	   elog(LOG, "generateACLs: %s", fullQueryTaint);
	   elog(LOG, "generateACLs: %s", fullQueryDeny);
	   elog(LOG, "generateACLs: %s", fullQuerySuspend);
	   
	   //SPI_execute(fullQueryGrant,false,1);
	   SPI_execute(fullQueryTaint,false,1);
	   SPI_execute(fullQueryDeny,false,1);
	   SPI_execute(fullQuerySuspend,false,1);
	
	   cur_acl_size = cur_acl_size + 4;
	   
   };
	    
    	 
    SPI_finish();
        
    return ret;
}
