#include "postgres.h"
#include "fmgr.h"
#include "pgstat.h"
#include "nodes/nodes.h"
#include "utils/hsearch.h"

PG_FUNCTION_INFO_V1(f_pgm_stat_get_user_login_count);
PG_FUNCTION_INFO_V1(f_pgm_stat_get_user_role_activation_count);
PG_FUNCTION_INFO_V1(f_pgm_stat_get_user_role_sel_count);
PG_FUNCTION_INFO_V1(f_pgm_stat_get_user_role_tab_sel_count);
PG_FUNCTION_INFO_V1(f_pgm_stat_get_user_role_tab_col_sel_count);

/* for the nbc */
PG_FUNCTION_INFO_V1(f_pgm_stat_get_role_count);
PG_FUNCTION_INFO_V1(f_pgm_stat_get_role_cmd_count);
PG_FUNCTION_INFO_V1(f_pgm_stat_get_role_tab_count);
PG_FUNCTION_INFO_V1(f_pgm_stat_get_role_col_count);
/* for the nbc */

#ifndef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

/************* nbc end ************/
int64 
f_pgm_stat_get_role_count(PG_FUNCTION_ARGS)
{	
	Oid			dbid   = PG_GETARG_OID(0);
	Oid			roleid = PG_GETARG_OID(1);
	int64		result;
	
	PgStat_StatDBEntry	 	*dbentry;		
	PgStat_StatRoleEntry 	*roleentry; 
		
	dbentry = pgstat_fetch_stat_dbentry(dbid);
	
	if (dbentry == NULL)
	{
		result = -1;
		PG_RETURN_INT64(result);
	}
	
	roleentry = (PgStat_StatRoleEntry *)hash_search(dbentry->users, (void *)&roleid, HASH_FIND, NULL);

	if (roleentry == NULL)
	{
		result = -2;
		PG_RETURN_FLOAT4(result);
	}
	
	result = roleentry->all_cmd_count;
	
	PG_RETURN_INT64(result);
}

int64 
f_pgm_stat_get_role_cmd_count(PG_FUNCTION_ARGS)
{	
	Oid			dbid   = PG_GETARG_OID(0);
	Oid			roleid = PG_GETARG_OID(1);
	uint32		command = PG_GETARG_INT32(2);
	
	int64		result;
	
	PgStat_StatDBEntry	 	*dbentry;		
	PgStat_StatRoleEntry 	*roleentry; 
	PgStat_StatRoleCmdEntry *rolecmdentry;
		
	dbentry = pgstat_fetch_stat_dbentry(dbid);
	
	if (dbentry == NULL)
	{
		result = -1;
		PG_RETURN_INT64(result);
	}
	
	roleentry = (PgStat_StatRoleEntry *)hash_search(dbentry->users, (void *)&roleid, HASH_FIND, NULL);

	if (roleentry == NULL)
	{
		result = -2;
		PG_RETURN_INT64(result);
	}
	
	rolecmdentry = (PgStat_StatRoleCmdEntry *)hash_search(roleentry->cmdEntry, (void *)&command, HASH_FIND, NULL);

	if (rolecmdentry == NULL)
	{
		result = -3;
		PG_RETURN_INT64(result);
	}
	
	result = rolecmdentry->count;
	
	PG_RETURN_INT64(result);
}

int64 
f_pgm_stat_get_role_tab_count(PG_FUNCTION_ARGS)
{	
	Oid			dbid   = PG_GETARG_OID(0);
	Oid			roleid = PG_GETARG_OID(1);
	Oid			table = PG_GETARG_OID(2);
	
	int64		result;
	
	PgStat_StatDBEntry	 	*dbentry;		
	PgStat_StatRoleEntry 	*roleentry; 
	PgStat_StatRoleCmdTabEntry 	*roletabentry;
	
	dbentry = pgstat_fetch_stat_dbentry(dbid);
	
	if (dbentry == NULL)
	{
		result = -1;
		PG_RETURN_INT64(result);
	}
	
	roleentry = (PgStat_StatRoleEntry *)hash_search(dbentry->users, (void *)&roleid, HASH_FIND, NULL);

	if (roleentry == NULL)
	{
		result = -2;
		PG_RETURN_INT64(result);
	}
	
	roletabentry = (PgStat_StatRoleCmdTabEntry *)hash_search(roleentry->tabEntry, (void *)&table, HASH_FIND, NULL);

	if (roletabentry == NULL)
	{
		result = -3;
		PG_RETURN_INT64(result);
	}
	
	result = roletabentry->count;
	
	PG_RETURN_INT64(result);
}

int64 
f_pgm_stat_get_role_col_count(PG_FUNCTION_ARGS)
{	
	Oid			dbid    = PG_GETARG_OID(0);
	Oid			roleid  = PG_GETARG_OID(1);
	Oid			table 	= PG_GETARG_OID(2);
	int			column  = PG_GETARG_INT32(3);
	int			count_0_1 = PG_GETARG_INT32(4);
	
	float4		result;
	
	PgStat_StatDBEntry	 			*dbentry;		
	PgStat_StatRoleEntry 			*roleentry; 
	PgStat_StatRoleCmdTabEntry 		*roletabentry;
	PgStat_StatRoleCmdTabColEntry 	*roletabcolentry;
	PgStat_StatRoleCmdTabColEntry 	*rolecolentry;
	
	dbentry = pgstat_fetch_stat_dbentry(dbid);
	
	if (dbentry == NULL)
	{
		result = -1;
		PG_RETURN_INT64(result);
	}
	
	roleentry = (PgStat_StatRoleEntry *)hash_search(dbentry->users, (void *)&roleid, HASH_FIND, NULL);

	if (roleentry == NULL)
	{
		result = -2;
		PG_RETURN_INT64(result);
	}
	
	if (IDR_LEVEL == 1 || IDR_LEVEL == 2)
	{
		roletabentry = (PgStat_StatRoleCmdTabEntry *)hash_search(roleentry->tabEntry, (void *)&table, HASH_FIND, NULL);

		if (roletabentry == NULL)
		{
			result = -3;
			PG_RETURN_INT64(result);
		}
		
		if (IDR_LEVEL == 1 && column == 0)
		{
			result = roletabentry->count_0;
			PG_RETURN_INT64(result);
		}
							
		roletabcolentry = (PgStat_StatRoleCmdTabColEntry *)hash_search(roletabentry->colEntry, (void *)&column, HASH_FIND, NULL);
	
		if (roletabcolentry == NULL)
		{
			result = -4;
			PG_RETURN_INT64(result);
		}
		
		result = (count_0_1 == 1)?roletabcolentry->count:roletabcolentry->count_0;
		
	}else if (IDR_LEVEL == 0)
	{
		rolecolentry = (PgStat_StatRoleCmdTabColEntry *)hash_search(roleentry->colEntry, (void *)&column, HASH_FIND, NULL);		

		if (rolecolentry == NULL)
		{
			result = -5;
			PG_RETURN_INT64(result);
		}
		
		
		result = rolecolentry->count;
	}		
	
	PG_RETURN_INT64(result);
}

/************* 
 * nbc end 
 * ************/



int64 
f_pgm_stat_get_user_role_activation_count(PG_FUNCTION_ARGS)
{	
	Oid			dbid   = PG_GETARG_OID(0);
	Oid			roleid   = PG_GETARG_OID(1);
	Oid			userid = GetUserId();
	int64		result;
	
	PgStat_StatDBEntry	 *dbentry;
	PgStat_StatRoleEntry *userentry;		
	PgStat_StatRoleEntry *roleentry;
	
	dbentry = pgstat_fetch_stat_dbentry(dbid);
	
	if (dbentry == NULL)
	{
		result = 0;
		PG_RETURN_INT64(result);
	}
	
	userentry = (PgStat_StatRoleEntry *)hash_search(dbentry->users, (void *)&userid, HASH_FIND, NULL);

	if (userentry == NULL)
	{
		result = -1;
		PG_RETURN_INT64(result);
	}
	
	roleentry = (PgStat_StatRoleEntry *)hash_search(userentry->roleEntry, (void *)&roleid, HASH_FIND, NULL);

	if (roleentry == NULL)
	{
		result = -2;
		PG_RETURN_INT64(result);
	}
	
	result = roleentry->login_or_activation_count;
	
	PG_RETURN_INT64(result);
}

int64 
f_pgm_stat_get_user_login_count(PG_FUNCTION_ARGS)
{	
	Oid			dbid   = PG_GETARG_OID(0);	
	Oid			userid = GetUserId();
	int64		result;
	
	PgStat_StatDBEntry	 *dbentry;
	PgStat_StatRoleEntry *userentry;		
	
	dbentry = pgstat_fetch_stat_dbentry(dbid);
	
	if (dbentry == NULL)
	{
		result = 0;
		PG_RETURN_INT64(result);
	}
	
	userentry = (PgStat_StatRoleEntry *)hash_search(dbentry->users, (void *)&userid, HASH_FIND, NULL);

	if (userentry == NULL)
	{
		result = -1;
		PG_RETURN_INT64(result);
	}
	
	result = userentry->login_or_activation_count;
	
	PG_RETURN_INT64(result);
}

int64 
f_pgm_stat_get_user_role_sel_count(PG_FUNCTION_ARGS)
{	
	Oid			dbid   = PG_GETARG_OID(0);
	Oid			roleid = PG_GETARG_OID(1);
	Oid			userid = GetUserId();
	int64		result;
	
	PgStat_StatDBEntry	 	*dbentry;
	PgStat_StatRoleEntry 	*userentry;		
	PgStat_StatRoleEntry 	*roleentry;
	PgStat_StatRoleCmdEntry *rolecmd_entry;
	
	CmdType operation = CMD_SELECT; 
		
	dbentry = pgstat_fetch_stat_dbentry(dbid);
	
	if (dbentry == NULL)
	{
		result = 0;
		PG_RETURN_INT64(result);
	}
	
	userentry = (PgStat_StatRoleEntry *)hash_search(dbentry->users, (void *)&userid, HASH_FIND, NULL);

	if (userentry == NULL)
	{
		result = -1;
		PG_RETURN_INT64(result);
	}
	
	roleentry = (PgStat_StatRoleEntry *)hash_search(userentry->roleEntry, (void *)&roleid, HASH_FIND, NULL);

	if (roleentry == NULL)
	{
		result = -2;
		PG_RETURN_INT64(result);
	}
	
	rolecmd_entry = (PgStat_StatRoleCmdEntry *)hash_search(roleentry->cmdEntry, (void *)&operation, HASH_FIND, NULL);

	if (rolecmd_entry == NULL)
	{
		result = -3;
		PG_RETURN_INT64(result);
	}
	
	result = rolecmd_entry->count;
	
	PG_RETURN_INT64(result);
}

int64 
f_pgm_stat_get_user_role_tab_sel_count(PG_FUNCTION_ARGS)
{	
	Oid			dbid   = PG_GETARG_OID(0);
	Oid			roleid = PG_GETARG_OID(1);
	Oid			table = PG_GETARG_OID(2);
	Oid			userid = GetUserId();
	int64		result;
	
	PgStat_StatDBEntry	 	*dbentry;
	PgStat_StatRoleEntry 	*userentry;		
	PgStat_StatRoleEntry 	*roleentry;
	PgStat_StatRoleCmdEntry *rolecmd_entry;
	PgStat_StatRoleCmdTabEntry *rolecmdtab_entry;
	
	CmdType operation = CMD_SELECT; 
		
	dbentry = pgstat_fetch_stat_dbentry(dbid);
	
	if (dbentry == NULL)
	{
		result = 0;
		PG_RETURN_INT64(result);
	}
	
	userentry = (PgStat_StatRoleEntry *)hash_search(dbentry->users, (void *)&userid, HASH_FIND, NULL);

	if (userentry == NULL)
	{
		result = -1;
		PG_RETURN_INT64(result);
	}
	
	roleentry = (PgStat_StatRoleEntry *)hash_search(userentry->roleEntry, (void *)&roleid, HASH_FIND, NULL);

	if (roleentry == NULL)
	{
		result = -2;
		PG_RETURN_INT64(result);
	}
	
	rolecmd_entry = (PgStat_StatRoleCmdEntry *)hash_search(roleentry->cmdEntry, (void *)&operation, HASH_FIND, NULL);

	if (rolecmd_entry == NULL)
	{
		result = -3;
		PG_RETURN_INT64(result);
	}
	
	rolecmdtab_entry = (PgStat_StatRoleCmdTabEntry *)hash_search(rolecmd_entry->tabEntry, (void *)&table, HASH_FIND, NULL);

	if (rolecmdtab_entry == NULL)
	{
		result = -4;
		PG_RETURN_INT64(result);
	}
	
	
	result = rolecmdtab_entry->count;
	
	PG_RETURN_INT64(result);
}

int64 
f_pgm_stat_get_user_role_tab_col_sel_count(PG_FUNCTION_ARGS)
{	
	Oid			dbid   = PG_GETARG_OID(0);
	Oid			roleid = PG_GETARG_OID(1);
	Oid			table = PG_GETARG_OID(2);
	Oid			column = PG_GETARG_OID(3);
	
	Oid			userid = GetUserId();
	int64		result;
	
	PgStat_StatDBEntry	 			*dbentry;
	PgStat_StatRoleEntry 			*userentry;		
	PgStat_StatRoleEntry 			*roleentry;
	PgStat_StatRoleCmdEntry 		*rolecmd_entry;
	PgStat_StatRoleCmdTabEntry 		*rolecmdtab_entry;
	PgStat_StatRoleCmdTabColEntry 	*rolecmdtabcol_entry;
	
	CmdType operation = CMD_SELECT; 
		
	dbentry = pgstat_fetch_stat_dbentry(dbid);
	
	if (dbentry == NULL)
	{
		result = 0;
		PG_RETURN_INT64(result);
	}
	
	userentry = (PgStat_StatRoleEntry *)hash_search(dbentry->users, (void *)&userid, HASH_FIND, NULL);

	if (userentry == NULL)
	{
		result = -1;
		PG_RETURN_INT64(result);
	}
	
	roleentry = (PgStat_StatRoleEntry *)hash_search(userentry->roleEntry, (void *)&roleid, HASH_FIND, NULL);

	if (roleentry == NULL)
	{
		result = -2;
		PG_RETURN_INT64(result);
	}
	
	rolecmd_entry = (PgStat_StatRoleCmdEntry *)hash_search(roleentry->cmdEntry, (void *)&operation, HASH_FIND, NULL);

	if (rolecmd_entry == NULL)
	{
		result = -3;
		PG_RETURN_INT64(result);
	}
	
	rolecmdtab_entry = (PgStat_StatRoleCmdTabEntry *)hash_search(rolecmd_entry->tabEntry, (void *)&table, HASH_FIND, NULL);

	if (rolecmdtab_entry == NULL)
	{
		result = -4;
		PG_RETURN_INT64(result);
	}
	
	rolecmdtabcol_entry = (PgStat_StatRoleCmdTabColEntry *)hash_search(rolecmdtab_entry->colEntry, (void *)&column, HASH_FIND, NULL);

	if (rolecmdtabcol_entry == NULL)
	{
		result = -4;
		PG_RETURN_INT64(result);
	}
	
	
	result = rolecmdtabcol_entry->count;
	
	PG_RETURN_INT64(result);
}
