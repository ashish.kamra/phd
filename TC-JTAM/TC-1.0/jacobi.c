/**
 * file: jacobi.c - Implements jacobi function, used internally
 *
 * Threshold Signatures
 *
 * This library is the legal property of its developers.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Developers:
 *         Sarvjeet Singh, <sarvjeet_s@yahoo.com, sarvjeet@purdue.edu>
 *         Abhilasha Bhargav, <bhargav@cs.purdue.edu>
 *         Rahim Sewani, <sewani@cs.purdue.edu>
 */

#include "TC.h"

int jacobi(BIGNUM* paramp,BIGNUM* paramq) 
{
	int j, result;
  	BN_CTX *temp=BN_CTX_new();

	BIGNUM* p ;
        BIGNUM* q ;
        BIGNUM* ptemp ;
        BIGNUM *z ;
        BIGNUM *o ;
        BIGNUM *m ;

  	BN_ULONG r;
  	BN_ULONG u;

	if( temp == NULL ) return TC_ALLOC_ERROR;

        BN_CTX_start(temp);

        p = BN_CTX_get(temp);
        q = BN_CTX_get(temp);
        ptemp = BN_CTX_get(temp);
        z = BN_CTX_get(temp);
        o = BN_CTX_get(temp);
        m = BN_CTX_get(temp);

        /* check if either of these variables failed to initialize */
        if( m == NULL ) goto null_err;

  	BN_zero(z); /*used for zero everywhere*/
  	BN_one(o); /*used for one everywhere*/

	/* copy the variable, if its fails return error */
  	if ( BN_copy(p,paramp) == NULL ) goto null_err;
  	if ( BN_copy(q,paramq) == NULL ) goto null_err;
	
	/* error if q <= 0 or q  is even */
 	if ( (BN_cmp(q,z) <= 0) || (BN_is_odd(q) == 0) ) 
		return (-2);

  	/*if (p < 0) {p = p%q; if (p < 0) p += q;}*/
  	if( BN_cmp(p,z) < 0 ) 
	{
    		result =  BN_mod(ptemp,p,q,temp);
		if( result == 0 ) goto arth_err;

    		if( BN_copy(p,ptemp) == NULL ) goto null_err;

    		if ( BN_cmp(p,z) < 0 ) 
		{
     			result = BN_add(p,p,q);
			if( result == 0 ) goto null_err;
    		}
  	}

   	/*if (p == 1 || q == 1) return(1);*/
   	if ( (BN_cmp(p,o) == 0) || (BN_cmp(q,o) == 0) ) 
		return(1);

   	/*if (q <= p) p = p%q;*/
   	if ( BN_cmp(p,q) <= 0 )
	{
     		result = BN_mod(ptemp,p,q,temp);
		if( result == 0 ) goto arth_err;

     		if( BN_copy(p,ptemp) == NULL ) goto null_err;
   	}

  	/*if (p == 0) return(0);*/
  	if( BN_cmp(p,z) == 0) 
		return(0);

     	j = 1;
     	while ( BN_cmp(p,o) != 0 )
	{
       		u = BN_mod_word(p,4);
       		switch (u) 
		{
         		/* break into four cases of p mod 4 */
       			case 0: 
				result = BN_rshift(ptemp,p,2); 
				if( result == 0 ) goto arth_err;

				if( BN_copy(p,ptemp) == NULL ) goto null_err; 
			break; 
			
			/* divide by 4 */
       			case 2: 
				result = BN_rshift(ptemp,p,1); 
				if( result == 0 ) goto arth_err;

				if( BN_copy(p,ptemp) == NULL ) goto null_err;
         		
				r = BN_mod_word(q,8);
         			if (r == 3 || r == 5) 
					j = -j; 
			break;

       			case 3: 
				if ( BN_mod_word(q,4) == 3 ) 
				{
					j = -j; 
				}

         		/* fall through into case  1 */
       			case 1:
         			result = BN_mod(m,q,p,temp);
				if( result == 0 ) goto arth_err;

         			if( BN_cmp(m,z) == 0 ) 
					return(0);
         			else
           			{
             				if( BN_copy(q,p) == NULL ) goto null_err;
             				if( BN_copy(p,m) == NULL ) goto null_err;
           			}
         		break;
       		}/* end of switch */
     	}/* end of while */
     
	/* free the memory */
	BN_CTX_end(temp);
     	BN_CTX_free(temp);

     	return(j);

null_err:
	BN_CTX_end(temp);
        BN_CTX_free(temp);
	return TC_ALLOC_ERROR;

arth_err:
	BN_CTX_end(temp);
        BN_CTX_free(temp);
	return TC_BN_ARTH_ERROR;
}
