/**
 * file: verify.c - Implements TC_verify
 *
 * Threshold Signatures
 *
 * This library is the legal property of its developers.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Developers:
 *         Sarvjeet Singh, <sarvjeet_s@yahoo.com, sarvjeet@purdue.edu>
 *         Abhilasha Bhargav, <bhargav@cs.purdue.edu>
 *         Rahim Sewani, <sewani@cs.purdue.edu>
 */

#include "TC.h"

int TC_verify(BIGNUM *hM, TC_SIG sig, TC_PK *tcpk) {
  BN_CTX *ctx=NULL;
  BIGNUM *temp=NULL;

  if ((ctx=BN_CTX_new()) == NULL) return(TC_ALLOC_ERROR);
  BN_CTX_start(ctx);
  temp = BN_CTX_get(ctx);
  if ((temp = BN_CTX_get(ctx))== NULL) {
    BN_CTX_end(ctx);
    BN_CTX_free(ctx);
    return(TC_ALLOC_ERROR);
  }
  
  if (!BN_mod_exp(temp,sig,tcpk->e,tcpk->n,ctx)) {
    BN_CTX_end(ctx);
    BN_CTX_free(ctx);
    return TC_BN_ARTH_ERROR;    
  };
 
  if (BN_cmp(temp,hM)==0) {
    BN_CTX_end(ctx);
    BN_CTX_free(ctx);
    return 1;
  } else {
    BN_CTX_end(ctx);
    BN_CTX_free(ctx);
    return 0;
  }
}

/* Ashish: modified for JTAM */
int TC_verify_jtam(BIGNUM *hM, BIGNUM *sig, BIGNUM *e, BIGNUM *n) 
{
  BN_CTX *ctx=NULL;
  BIGNUM *temp=NULL;

  if ((ctx=BN_CTX_new()) == NULL) 
	  return(TC_ALLOC_ERROR);
  
  BN_CTX_start(ctx);
  
  temp = BN_CTX_get(ctx);
  
  if ((temp = BN_CTX_get(ctx))== NULL) {
    BN_CTX_end(ctx);
    BN_CTX_free(ctx);
    return(TC_ALLOC_ERROR);
  }
  
  if (!BN_mod_exp(temp, sig, e, n, ctx)) {
    BN_CTX_end(ctx);
    BN_CTX_free(ctx);
    return TC_BN_ARTH_ERROR;    
  };
 
  if (BN_cmp(temp,hM)==0) 
  {
    BN_CTX_end(ctx);
    BN_CTX_free(ctx);
    return 1;
  } else {
    BN_CTX_end(ctx);
    BN_CTX_free(ctx);
    return 0;
  }
}
