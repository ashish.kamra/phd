/* config.h.  Generated automatically by configure.  */
/* config.h.in.  Generated automatically from configure.in by autoheader.  */

/* Define to empty if the keyword does not work.  */
/* #undef const */

/* Define if you have the ANSI C header files.  */
#define STDC_HEADERS 1

/* Define if you have the <dlfcn.h> header file.  */
#define HAVE_DLFCN_H 1

/* Define if you have the <openssl/bn.h> header file.  */
#define HAVE_OPENSSL_BN_H 1

/* Define if you have the <openssl/evp.h> header file.  */
#define HAVE_OPENSSL_EVP_H 1

/* Define if you have the <stdio.h> header file.  */
#define HAVE_STDIO_H 1

/* Define if you have the <unistd.h> header file.  */
#define HAVE_UNISTD_H 1

/* Define if you have the crypto library (-lcrypto).  */
#define HAVE_LIBCRYPTO 1

/* Name of package */
#define PACKAGE "TC"

/* Version number of package */
#define VERSION "1.0"

