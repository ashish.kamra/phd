#include <TC.h>

main() {
  TC_DEALER *a= TC_generate(128, 3, 2,3);
  TC_DEALER_print(a);

  TC_PK *p1;
  TC_PK *p2;
  
  char buf[1000];

  p1 = TC_get_pub(a);
  TC_PK_Print(p1);

  printf("size %d\n", TC_PK_size(p1));
  printf("%d\n", TC_PK_marshal(p1,buf));
  p2 = TC_PK_demarshal(buf);
  TC_PK_Print(p2);

  //printf("Done 1\n");
  TC_IND *a1 = TC_get_ind(1,a);
  TC_IND_Print(a1);
  
  printf("size %d\n", TC_IND_size(a1));
  printf("%d\n", TC_IND_marshal(a1,buf));
  a1 = TC_IND_demarshal(buf);
  TC_IND_Print(a1);

  //printf("Done 2\n");
  TC_IND *a2 = TC_get_ind(2,a);
  //TC_IND_Print(a2);
  //printf("Done 2.1\n");
  TC_IND *a3 = TC_get_ind(3,a);
  //TC_IND_Print(a3);
  //printf("Done 2.5\n");

  BIGNUM *hM = BN_new();
  
  BN_set_word(hM, 12581);

  TC_IND_SIG *s = TC_IND_SIG_new();
  
  //printf("Done 2.6\n");
  TC_IND_SIG **array = TC_SIG_Array_new(3);

  //printf("Done 2.7\n");

  genIndSig(a1,hM,s);

  printf("\n\n");
   TC_IND_SIG_Print(s);

  printf("---size %d\n", TC_IND_SIG_size(s));
  printf("%d\n", TC_IND_SIG_marshal(s,buf)); 
  s = TC_IND_SIG_demarshal(buf); 
  
  TC_IND_SIG_Print(s);

  set_TC_SIG(1, s,array);
  
  genIndSig(a2,hM,s);
  set_TC_SIG(2,s,array);

  genIndSig(a3,hM,s);
  set_TC_SIG(3,s,array);

  TC_SIG sig;

  printf("ret combine %d\n",TC_Combine_Sigs(array, a1, hM, &sig));

  printf("Signature = %s\n",BN_bn2dec(sig));
  
  BIGNUM *try=BN_new();
  BN_CTX *ctx=BN_CTX_new();

  BN_mod_exp(try, sig, a1->e,a1->n, ctx);
  
  printf("and = %s\n",BN_bn2dec(try));
  
}



