#include <TC.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#define MAX_SIZE 5000

#define name_m1 "xinu4.cs.purdue.edu"
#define name_m2 "xinu4.cs.purdue.edu"
#define name_m3 "xinu4.cs.purdue.edu"
#define name_c "xinu5.cs.purdue.edu"
#define name_v "xinu4.cs.purdue.edu"

#define port_m1 5990
#define port_m2 5991
#define port_m3 5992
#define port_c 5993
#define port_v 5994

int listenTCP(int port) {
  struct sockaddr_in sin;
  int sockd;
  
  bzero((char *)&sin, sizeof(sin));
  sin.sin_family = AF_INET;
  sin.sin_addr.s_addr = INADDR_ANY;
  sin.sin_port = htons((u_short)port);
  

  if ((sockd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
    printf("can't create socket\n");
    exit(1);
  }
  
  if (bind(sockd, (struct sockaddr *)&sin, sizeof(sin)) < 0) {
    //printf("can't bind to port %d: %s\n", port, sys_errlist[errno]);
    printf("can't bind to port\n");
    exit(1);
  }

  if (listen(sockd, 50) < 0) {
    printf("can't listen on port");
    exit(1);
  }
  
  return sockd;
}


int Read(int sockd, void *buf) {
  socklen_t temp;
  int fd;
  void *initbuf=buf;

  if ((fd=accept(sockd, NULL, &temp)) < 0) {
    printf("Can't accept\n");
    exit(1);
  }

  int size=0;
  
  while ((size=read(fd,buf+=size, MAX_SIZE)) != 0);
    
  return (buf - initbuf);
} 


int connectTCP(const char *host, int port)
{
  struct sockaddr_in sin;
  int sockd;
  struct hostent  *hptr;   /* pointer to host information entry    */
  char str[16], *strptr;
  
  bzero((char *)&sin, sizeof(sin));
  sin.sin_family = AF_INET;
  sin.sin_port = htons((u_short)port);
  
  if (hptr =  gethostbyname(host) )
    memcpy(&sin.sin_addr, hptr->h_addr, hptr->h_length);
  else if ( (sin.sin_addr.s_addr = inet_addr(host)) == INADDR_NONE ) {
    printf("can't get \"%s\" host entry\n", host);
    exit(1);
  }

  if ((sockd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
    printf("can't create socket:\n");
    exit(1);
  }
  
  if (connect(sockd, (struct sockaddr *)&sin, sizeof(sin)) < 0) {
    printf("ConnectTCP; connect: can't connect\n");
    exit(1);
  }
  return sockd;
}

void Write(int sock,const void *buf, size_t size) {

  write(sock, buf, size);
} 

int main(int argc, char *argv[]) {
  
  switch(argv[1][0]) {
  case 'D': dealer();break;
  case 'C': comb();break;
  case '1': mem(1);break;
  case '2': mem(2); break;
  case '3': mem(3); break;
  case 'V': ver(); break;
  }

}


dealer() {
  int fdm1, fdm2, fdm3, fdc, fdv;

  fdm1= connectTCP(name_m1, port_m1);
  fdm2= connectTCP(name_m2, port_m2);
  fdm3= connectTCP(name_m3, port_m3);
  fdc = connectTCP(name_c, port_c);
  fdv = connectTCP(name_v, port_v);
  
  unsigned char sendbuf[MAX_SIZE];

  char pause[10];
  printf("DEALER Ready ...");
  scanf("%s", &pause);
  
  TC_DEALER *a= TC_generate(128, 3, 2,3);
  printf("GENERATED KEYS :\n");
  TC_DEALER_print(a);
  
  printf("\nReady to send ...");
  scanf("%s", &pause);

  TC_IND *ind;
  
  printf("Sending Key to Member 1...");
  ind = TC_get_ind(1,a);
  TC_IND_marshal(ind,sendbuf);
  Write(fdm1, sendbuf, TC_IND_size(ind));
  printf("done\n");

  printf("Sending Key to Member 2...");
  ind = TC_get_ind(2,a);
  TC_IND_marshal(ind,sendbuf);
  Write(fdm2, sendbuf, TC_IND_size(ind));
  printf("done\n");

  printf("Sending Key to Member 3...");
  ind = TC_get_ind(3,a);
  TC_IND_marshal(ind,sendbuf);
  Write(fdm3, sendbuf, TC_IND_size(ind));
  printf("done\n");

  printf("Sending Combine key to combiner...");
  ind = TC_get_combine(a);
  TC_IND_marshal(ind,sendbuf);
  Write(fdc, sendbuf, TC_IND_size(ind));
  printf("done\n");

  printf("Sending Public key to verifier...");
  TC_PK *pk = TC_get_pub(a);
  TC_PK_marshal(pk,sendbuf);
  Write(fdv, sendbuf, TC_PK_size(pk));
  printf("done\n");

  close(fdm1);close(fdm2);close(fdm3);
  close(fdc);close(fdv);
  

  printf("DEALER's work is over. Therefore, I die! Goodbye\n");
}

comb(){
  int mysock=listenTCP(port_c);
  
  int fdm1, fdm2, fdm3, fdv;
  
  unsigned char buf[MAX_SIZE];
  char pause[10];

  printf("COMBINER Ready ...\n");

  Read(mysock, buf);
  TC_IND *key = TC_IND_demarshal(buf);

  //TC_IND_Print(key);

  printf("Received combine key...");
  scanf("%s", &pause);

  fdm1= connectTCP(name_m1, port_m1);
  fdm2= connectTCP(name_m2, port_m2);
  fdm3= connectTCP(name_m3, port_m3);
  //fdv = connectTCP(name_v, port_v);

  BIGNUM *hM=BN_new();
  
  BN_rand(hM, 12, 0, 1);

  printf("Generated random message\nTrying to get signature on Message = %s ...", BN_bn2dec(hM));
  scanf("%s", &pause);
  
  printf("Asking Member 1 for signature...\n");
  BN_bn2bin(hM, buf);
  Write(fdm1, buf, BN_num_bytes(hM));

  printf("Asking Member 2 for signature...\n");
  Write(fdm2, buf, BN_num_bytes(hM));

  printf("Asking Member 3 for signature...\n");
  Write(fdm3, buf, BN_num_bytes(hM));

  close(fdm1);close(fdm2);close(fdm3);
  
  printf("Waiting for response ....\n");

  TC_IND_SIG **array = TC_SIG_Array_new(3);

  int number;

  while (1) {
    Read(mysock, buf);
    number=buf[0];
    
    printf("\n\nGot Signature from Member %d\n", number);

    TC_IND_SIG *s;
    set_TC_SIG(number,s=TC_IND_SIG_demarshal(buf+1),array);

    //TC_IND_SIG_Print(s);
    

    if (TC_Check_Proof(key,hM,s,number))
      printf("Signature Verified\n");
    else
      printf("Signature NOT Verified. Member %d is corrupt!!\n", number);
    
    printf("Trying to combine... ");

    TC_SIG sig;

    if (TC_Combine_Sigs(array, key, hM, &sig) == 0) {
      printf("Combine Successful\n");
      printf("Signature = %s\n",BN_bn2dec(sig));
      
      fdv = connectTCP(name_v, port_v);
      printf("\nSending Signature to Verifier ... ");
      TC_SIG_marshal(sig, buf);
      Write(fdv, buf, TC_SIG_size(sig));
      printf("DONE\nCOMBINER's work is over. Goodbye!\n");
      close(fdm1); close(fdm2); close(fdm3); close(fdv);
      exit(0);
    } else {
      printf("Failed\n");
    }
  }
}


mem(int mynum){
  int mysock;
  
  switch(mynum){
  case 1:mysock=listenTCP(port_m1);break;
  case 2:mysock=listenTCP(port_m2);break;
  case 3:mysock=listenTCP(port_m3);break;
  }

  unsigned char buf[MAX_SIZE];
  char pause[10];

  printf("Member %d Ready ...\n",mynum);

  Read(mysock, buf);
  TC_IND *key = TC_IND_demarshal(buf);

  printf("Received secret key...\n");

  BIGNUM *hM=BN_new();

  int mlen;
  
  mlen = Read(mysock, buf);
  BN_bin2bn(buf, mlen, hM);

  printf("Combiner Wants my Sign on %s ... ", BN_bn2dec(hM));
  scanf("%s", &pause);

  if (pause[0]=='y') {
    TC_IND_SIG *s = TC_IND_SIG_new();
    genIndSig(key,hM,s);
    printf("Signature and \"Proof of Correctness\" generated\nSending to combiner...");

    int fdc;
    fdc = connectTCP(name_c, port_c);
    buf[0]=mynum;

    Write(fdc, buf, 1);

    TC_IND_SIG_marshal(s, buf);
    //TC_IND_SIG_Print(s);
    Write(fdc, buf, TC_IND_SIG_size(s));

    close(fdc);
    printf("Done\nMy Work completed. Goodbye!\n");
    exit(1);
  } else {
    TC_IND_SIG *s = TC_IND_SIG_new();
    genIndSig(key,hM,s);
    printf("Let me fool combiner >:) and send him wrong signature >:)\n");
    BN_rand(s->sig, 100, 1, 1);

    printf("Sending to combiner...");
    

    int fdc;
    fdc = connectTCP(name_c, port_c);
    buf[0]=mynum;
    Write(fdc, buf, 1);
    TC_IND_SIG_marshal(s, buf);
    Write(fdc, buf, TC_IND_SIG_size(s));

    close(fdc);
    printf("Done\nMy Work completed. Goodbye!\n");
    
    exit(0);
  }
}

ver() {
  char pause[100];
  int mysock;
  mysock=listenTCP(port_v);
  
  unsigned char buf[MAX_SIZE];
  
  printf("Verifier Ready ...\n");
  
  Read(mysock, buf);
  TC_PK *pk = TC_PK_demarshal(buf);
  
  printf("Received Public Key\n");
  
  Read(mysock, buf);

  TC_SIG sig=TC_SIG_demarshal(buf);
  
  printf("Received Signature\n");
  
  scanf("%s",pause);

  printf("(Sign)^e mod n = ");
  
  BIGNUM *try=BN_new();
  BN_CTX *ctx=BN_CTX_new();

  BN_mod_exp(try, sig, pk->e,pk->n, ctx);
  
  printf("%s\n",BN_bn2dec(try));

  printf("Thank you!\n");
}
/*
  if (argc==1) {
  Read(listenTCP(6666), buf);
  printf("%s",buf);
  
  } else {
    Write(connectTCP(argv[1], 6666), sendbuf, strlen(sendbuf));
  }    
}
*/
  
