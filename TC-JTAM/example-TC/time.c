#include <TC.h>
#include <openssl/rsa.h>
#include <openssl/engine.h>
#include <sys/time.h> 

#define N 100
#define L 67

#define RUNS 5

main() {
  int i,runs;
  struct timeval before,after;
  
  double TC_g=0, TC_s=0, TC_c=0, TC_v=0, R_g=0, R_s=0, R_v=0;

  for(runs=0;runs<RUNS;runs++) {
    gettimeofday(&before, NULL);
    TC_DEALER *a= TC_generate(128, N, L,3);
    gettimeofday(&after,NULL);
    
    printf("----\n");

    TC_g +=( ((after.tv_sec)-(before.tv_sec))*1000000 + ((after.tv_usec)-(before.tv_usec)));
    
    TC_PK *p1;
    
    p1 = TC_get_pub(a);
    
    TC_IND *ind[L];
    
    for(i=0;i<L;i++)
      ind[i]=TC_get_ind(i+1,a);
    
    BIGNUM *hM = BN_new();
    BN_set_word(hM, 12051981);
    
    TC_IND_SIG *s;
    TC_IND_SIG **array = TC_SIG_Array_new(N);
    
    for(i=0;i<L;i++) {
      s=TC_IND_SIG_new();
      
      gettimeofday(&before, NULL);
      genIndSig(ind[i],hM,s);
      gettimeofday(&after,NULL);
      
      TC_s +=( ((after.tv_sec)-(before.tv_sec))*1000000 + ((after.tv_usec)-(before.tv_usec)));
      
      set_TC_SIG(i+1, s,array);
      TC_IND_SIG_free(s);
    }
    
    TC_SIG sig;
    
    gettimeofday(&before, NULL);
    printf("ret %d\n",TC_Combine_Sigs(array, ind[0], hM, &sig));
    gettimeofday(&after,NULL);
    
    TC_c +=( ((after.tv_sec)-(before.tv_sec))*1000000 + ((after.tv_usec)-(before.tv_usec)));
    
    gettimeofday(&before, NULL);
    printf("verify = %d\n",TC_verify(hM, sig, p1));
    gettimeofday(&after,NULL);
    
    TC_v +=( ((after.tv_sec)-(before.tv_sec))*1000000 + ((after.tv_usec)-(before.tv_usec)));
  
    /****************************************************************/

  
    RSA *k=RSA_new();

    gettimeofday(&before, NULL);
    RSA *key = RSA_generate_key(512,17,NULL, NULL);
    gettimeofday(&after,NULL);
    
    R_g +=( ((after.tv_sec)-(before.tv_sec))*1000000 + ((after.tv_usec)-(before.tv_usec)));
    
    
    unsigned char m[]="This is";
    unsigned char sigret[1000];
    int siglen;
    int hLength;
    
    unsigned char tempc[EVP_MAX_MD_SIZE];
    EVP_MD_CTX ctx;
    
    
    EVP_DigestInit(&ctx,EVP_sha1());
    EVP_DigestUpdate(&ctx,hM->d,(hM->top)*sizeof(BN_ULONG));
    EVP_DigestFinal(&ctx,tempc,&hLength);
    
    gettimeofday(&before, NULL);
    printf("ret =%d\n",RSA_sign(NID_sha1, tempc, hLength,sigret,&siglen,key));
    gettimeofday(&after,NULL);
    
    R_s +=( ((after.tv_sec)-(before.tv_sec))*1000000 + ((after.tv_usec)-(before.tv_usec)));
    
    k->n=key->n;
    k->e=key->e;

    gettimeofday(&before, NULL);
    printf("ver= %d\n", RSA_verify(NID_sha1, tempc,hLength,sigret, siglen, k));
    gettimeofday(&after,NULL);
    R_v +=( ((after.tv_sec)-(before.tv_sec))*1000000 + ((after.tv_usec)-(before.tv_usec)));
  }

  TC_g = TC_g / RUNS;
  TC_s = TC_s / RUNS;
  TC_c = TC_c / RUNS;
  TC_v = TC_v / RUNS;

  R_g = N*R_g / RUNS;
  R_s = L*R_s / RUNS;
  R_v = L*R_v / RUNS;

  printf("TC_g %e\n", TC_g);
  printf("TC_s %e\n", TC_s);
  printf("TC_c %e\n", TC_c);
  printf("TC_v %e\n", TC_v);
  printf("R_g %e\n", R_g);
  printf("R_s %e\n", R_s);
  printf("R_v %e\n", R_v);
  


}



